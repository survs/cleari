from import_export import resources

from .models import Violation


class ViolationResource(resources.ModelResource):
    class Meta:
        model = Violation
