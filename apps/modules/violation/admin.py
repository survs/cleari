from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import Violation


@admin.register(Violation)
class Violation(ImportExportModelAdmin):
    pass
