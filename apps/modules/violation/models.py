from django.db import models
from django.utils import timezone

from apps.modules.semester.models import Semester
from apps.modules.users.models import CUser


class ViolationManager(models.Manager):

    def issue(self, number, sys, date_violated, reason):
        if number and sys and date_violated and reason:
            v_format = ''.join([str(number), '-', str(timezone.now().timestamp()).partition('.')[0]])
            try:
                self.create(violation=v_format,
                            number_id=CUser.objects.get(number=number),
                            sys_id=Semester.objects.get(sys=sys),
                            date_violated=date_violated,
                            reason=reason)
                return 'Issued'
            except CUser.DoesNotExist:
                return 'User does not exist'
            except Semester.DoesNotExist:
                return 'Semester does not exist'
        else:
            return 'Some fields are empty'

    def resolve(self, violation):
        if violation:
            if self.filter(violation=violation).exists():
                v = self.get(violation=violation)
                v.resolve = True
                v.save()
                return 'Resolve'
            else:
                return 'Violation ID does not exist'
        else:
            return 'Field is empty'

    def edit_reason(self, violation, reason):
        if violation and reason:
            if self.filter(violation=violation).exists():
                v = self.get(violation=violation)
                v.reason = reason
                v.save()
                return 'Changes Saved'
            else:
                return 'Violation ID does not exist'
        else:
            return 'Some fields are empty'


class IEViolationManager(models.Manager):
    @staticmethod
    def c_export(file_type):
        """
        Usage:
        from apps.apps.college.models import College
        College.import_export.c_export(file_type=('xls', 'xlsx', 'csv', 'json', 'yaml'))
        """
        # Import specific components and resources
        from apps.contrib.components import supported_types, export
        from .resource import ViolationResource
        # Verify supplied file type is supported
        if supported_types.__contains__(file_type):
            # Return raw application data
            data = export(ViolationResource(), 'Semester', 'all', file_type)
            return data
        else:
            # Show error message
            return 'File Type not Supported'

    @staticmethod
    def c_import(file):
        """
        from apps.apps.college.models import College
        College.import_export.c_import(raw_file)
        """
        # Import specific resources and data sets
        from .resource import ViolationResource
        from tablib import Dataset
        # Call Violation Resource
        res = ViolationResource()
        # Create Empty Data set
        ds = Dataset()
        # Get File file from request and load to data sets
        ds.load(file.read())
        # Dry run receive file if valid
        result = res.import_data(ds, dry_run=True)
        # Verify that file has no errors
        if not result.has_errors():
            # Import data into database
            res.import_data(ds, dry_run=False)
            # Show Success Message
            return 'Success Importing data'
        else:
            # Show Error message
            return ''.join(['Failed to import. ', '[', str(ds), ']'])


class Violation(models.Model):
    violation = models.CharField(max_length=30, unique=True, default='')
    number = models.ForeignKey(CUser, on_delete=models.DO_NOTHING)
    sys_id = models.ForeignKey(Semester, on_delete=models.DO_NOTHING)
    date_violated = models.DateTimeField()
    resolve = models.BooleanField(default=False)
    reason = models.TextField(max_length=500)
    objects = ViolationManager()
    ie = IEViolationManager()
