from django.http import JsonResponse
from django.middleware.csrf import get_token

from apps.contrib.ie.forms import DataImportForm
from apps.modules.users.models import CUser
from .forms import CDT1Form, CDT2Form, CDT3Form
from .models import CDT1, CDT2, CDT3


def add(request, schema):
    csrf = get_token(request)
    if schema == 'csrf':
        return JsonResponse(status=203, data={'csrf': csrf}, safe=False)
    if not request.user.is_authenticated and request.method != 'POST':
        return JsonResponse(status=403, data='Not allowed to perform', safe=False)
    if CUser.objects.filter(id__username=request.user.username).exists():
        u = CUser.objects.get(id__username=request.user.username)
        if str(u.type).lower() == 'admin':
            if schema == 'cdt1':
                c1 = CDT1Form(request.POST)
                if c1.is_valid():
                    c1.save()
                    return JsonResponse(status=200, data='Created', safe=False)
                else:
                    return JsonResponse(status=203, data=str(c1.errors).replace('Cd t1', 'Group'), safe=False)
            elif schema == 'cdt2':
                c2 = CDT2Form(request.POST)
                if c2.is_valid():
                    c2.save()
                    return JsonResponse(status=200, data='Created', safe=False)
                else:
                    return JsonResponse(status=400, data=str(c2.errors), safe=False)
            elif schema == 'cdt3':
                c3 = CDT3Form(request.POST)
                if c3.is_valid():
                    c3.save()
                    return JsonResponse(status=200, data='Created', safe=False)
                else:
                    return JsonResponse(status=400, data=str(c3.errors), safe=False)
        else:
            return JsonResponse(status=403, data='Not allowed to perform', safe=False)
    else:
        return JsonResponse(status=400, data='User does not exist on Cleari Database', safe=False)


def edit(request, schema, pk):
    if not request.user.is_authenticated:
        return JsonResponse(status=403, data='Not allowed to perform', safe=False)
    csrf = get_token(request)
    if schema == 'csrf':
        return JsonResponse(status=203, data={'csrf': csrf}, safe=False)
    if CUser.objects.filter(id__username=request.user.username).exists():
        u = CUser.objects.get(id__username=request.user.username)
        print(str(u.type))
        if str(u.type).lower() == 'admin':
            if schema == 'cdt1' and request.method == 'GET':
                if not CDT1.objects.filter(pk=pk).exists():
                    return JsonResponse(status=404, data='Not Found', safe=False)
                c1 = CDT1Form(instance=CDT1.objects.get(pk=pk))
                college = []
                for c in c1.fields.get('college')._get_choices():
                    college.append(c)
                typ = []
                for t in c1.fields.get('type')._get_choices():
                    typ.append(t)
                context = dict([('initial', c1.initial),
                                ('fields', dict([('college', dict(college)),
                                                 ('type', dict(typ))]))])
                return JsonResponse(status=200, data=context, safe=False)
            elif schema == 'cdt2' and request.method == 'GET':
                if not CDT2.objects.filter(pk=pk).exists():
                    return JsonResponse(status=404, data='Not Found', safe=False)
                c2 = CDT2Form(instance=CDT2.objects.get(pk=pk))
                context = dict([('initial', c2.initial)])
                return JsonResponse(status=200, data=context, safe=False)
            elif schema == 'cdt3' and request.method == 'GET':
                if not CDT3.objects.filter(pk=pk).exists():
                    return JsonResponse(status=404, data='Not Found', safe=False)
                c3 = CDT3Form(instance=CDT3.objects.get(pk=pk))
                context = dict([('initial', c3.initial)])
                return JsonResponse(status=200, data=context, safe=False)
            elif schema == 'cdt1' and request.method == 'POST':
                if not CDT1.objects.filter(pk=pk).exists():
                    return JsonResponse(status=404, data='Not Found', safe=False)
                a = request.POST['abbreviation']
                n = request.POST['name']
                c = request.POST['college']
                t = request.POST['type']
                r = CDT1.objects.edit(pk, a, n, c, t)
                if r != 'Saved':
                    return JsonResponse(status=203, data=r, safe=False)
                return JsonResponse(status=200, data=r, safe=False)
            elif schema == 'cdt2' and request.method == 'POST':
                if not CDT2.objects.filter(pk=pk).exists():
                    return JsonResponse(status=404, data='Not Found', safe=False)
                a = request.POST['type']
                r = CDT2.objects.edit(pk, a)
                if r != 'Saved':
                    return JsonResponse(status=203, data=r, safe=False)
                return JsonResponse(status=200, data=r, safe=False)
            elif schema == 'cdt3' and request.method == 'POST':
                if not CDT3.objects.filter(pk=pk).exists():
                    return JsonResponse(status=404, data='Not Found', safe=False)
                a = request.POST['position']
                r = CDT3.objects.edit(pk, a)
                if r != 'Saved':
                    return JsonResponse(status=203, data=r, safe=False)
                return JsonResponse(status=200, data=r, safe=False)
            else:
                return JsonResponse(status=404, data='Schema not Found', safe=False)
        else:
            return JsonResponse(status=403, data='Not allowed to perform', safe=False)
    else:
        return JsonResponse(status=400, data='User does not exist on Cleari Database', safe=False)


def delete(request, schema, pk):
    if not request.user.is_authenticated:
        return JsonResponse(status=403, data='Not allowed to perform', safe=False)
    if CUser.objects.filter(id__username=request.user.username).exists():
        u = CUser.objects.get(id__username=request.user.username)
        print(str(u.type))
        if str(u.type).lower() == 'admin':
            if schema == 'cdt1':
                if not CDT1.objects.filter(pk=pk).exists():
                    return JsonResponse(status=202, data='Not Found', safe=False)
                c1 = CDT1.objects.delete(pk)
                if c1 != 'Deleted':
                    return JsonResponse(status=203, data=c1, safe=False)
                return JsonResponse(status=200, data=str(c1).upper(), safe=False)
            elif schema == 'cdt2':
                if not CDT2.objects.filter(pk=pk).exists():
                    return JsonResponse(status=202, data='Not Found', safe=False)
                c2 = CDT2.objects.delete(pk)
                if c2 != 'Deleted':
                    return JsonResponse(status=203, data=c2, safe=False)
                return JsonResponse(status=200, data=str(c2).upper(), safe=False)
            elif schema == 'cdt3':
                if not CDT3.objects.filter(pk=pk).exists():
                    return JsonResponse(status=202, data='Not Found', safe=False)
                c3 = CDT3.objects.delete(pk)
                if c3 != 'Deleted':
                    return JsonResponse(status=203, data=c3, safe=False)
                return JsonResponse(status=200, data=str(c3).upper(), safe=False)
            else:
                return JsonResponse(status=404, data='Schema not Found', safe=False)
        else:
            return JsonResponse(status=403, data='Not allowed to perform', safe=False)
    else:
        return JsonResponse(status=400, data='User does not exist on Cleari Database', safe=False)


def data_export(request, schema, typ):
    if typ and schema:
        # Verify file type is supported
        if schema == 'cdt1':
            return CDT1.ie.c_export(typ)
        elif schema == 'cdt2':
            return CDT2.ie.c_export(typ)
        elif schema == 'cdt3':
            return CDT3.ie.c_export(typ)
        else:
            return JsonResponse(status=404, data='Schema Not Found', safe=False)
    else:
        return JsonResponse(status=400, data='Some parameters are empty', safe=False)


def data_import(request, schema):
    # Call Data Import Form with request data and request files
    di = DataImportForm(request.POST, request.FILES)
    # Verify form is valid
    if di.is_valid():
        d = request.FILES['file']
        if schema == 'cdt1':
            return CDT1.ie.c_import(d)
        elif schema == 'cdt2':
            return CDT2.ie.c_import(d)
        elif schema == 'cdt3':
            return CDT3.ie.c_import(d)
        else:
            return JsonResponse(status=404, data='Schema Not Found', safe=False)
    else:
        return JsonResponse(status=400, data=str(di.errors), safe=False)
