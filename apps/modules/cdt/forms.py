from django import forms

from .models import CDT1, CDT2, CDT3


class CDT1Form(forms.ModelForm):
    class Meta:
        model = CDT1
        fields = ('abbreviation', 'name', 'college', 'type')
        widgets = {
            'abbreviation': forms.TextInput(attrs={'ng-model': 'form.abbreviation'}),
            'name': forms.TextInput(attrs={'ng-model': 'form.name'}),
            'college': forms.Select(attrs={'ng-model': 'form.college'}),
            'type': forms.Select(attrs={'ng-model': 'form.type'}),
        }


class CDT2Form(forms.ModelForm):
    class Meta:
        model = CDT2
        fields = ('type',)
        widgets = {
            'type': forms.TextInput(attrs={'ng-model': 'form.type'}),
        }


class CDT3Form(forms.ModelForm):
    class Meta:
        model = CDT3
        fields = ('position',)
        widgets = {
            'position': forms.TextInput(attrs={'ng-model': 'form.position'}),
        }
