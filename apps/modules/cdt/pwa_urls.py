from django.urls import path, include

from . import pwa_views

app_name = 'cdt'

urlpatterns = [
    path('add/<str:schema>', pwa_views.add, name='add'),
    path('edit/<str:schema>/<str:pk>', pwa_views.edit, name='edit'),
    path('delete/<str:schema>/<str:pk>', pwa_views.delete, name='delete'),
    path('import/<str:schema>', pwa_views.data_import, name='import_data'),
    path('export/<str:schema>/<str:typ>', pwa_views.data_export, name='import_form'),
    path('provider/', include('apps.modules.cdt.pwa_form_urls', namespace='form_provider')),
]
