from django.urls import path

from . import pwa_form_view

app_name = 'form_provider'

urlpatterns = [
    path('cdt/<str:schema>', pwa_form_view.cdt_form, name='cdt'),
    path('import', pwa_form_view.import_form, name='import'),
    path('export', pwa_form_view.export_form, name='export'),
]
