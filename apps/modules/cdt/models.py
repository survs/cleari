from django.db import models, IntegrityError
from django.utils import timezone

from apps.contrib.components import whitelist
from apps.modules.college.models import College


class CDT1Manager(models.Manager):

    def add(self, abbreviation, name, college_id, type_id):
        try:
            if abbreviation and name and college_id and type:
                if not College.objects.filter(college=college_id).exists():
                    return 'College not Found'
                elif not CDT2.objects.filter(type=type_id).exists():
                    return 'Type not found'
                self.create(abbreviation=abbreviation,
                            name=name,
                            college_id=college_id,
                            type_id=type_id)
        except IntegrityError as e:
            return e
        except:
            return "Error Occurred"
        return ''.join(['Created', ' [', str(abbreviation), ']'])

    def delete(self, pk):
        try:
            if pk:
                a = self.get(pk=pk)
                r = a.delete()
                return r
            else:
                return 'Abbreviation is Empty'
        except:
            return 'Error Occurred'

    def edit(self, pk, abbreviation_new, name, college_id, type_id):
        try:
            if pk and abbreviation_new and name and college_id and type_id:
                if not self.filter(pk=pk).exists():
                    return 'Abbreviation not found'
                else:
                    s = self.filter(pk=pk)
                    s.update(abbreviation=abbreviation_new, name=name, college_id=college_id, type_id=type_id)
            else:
                return 'Some fields are empty'
        except:
            return 'Error Occurred'
        return 'Saved'


class CDT2Manager(models.Manager):

    def add(self, typ):
        try:
            if typ:
                self.create(type=typ)
        except IntegrityError as e:
            return e
        except:
            return "Error Occurred"
        return ''.join(['Created', ' [', str(typ), ']'])

    def delete(self, pk):
        try:
            if pk:
                a = self.get(pk=pk)
                r = a.delete()
                return r
            else:
                return 'Type is Empty'
        except:
            return 'Error Occurred'

    def edit(self, pk, typ):
        try:
            if pk and typ:
                if not self.filter(pk=pk).exists():
                    return 'Type not found'
                else:
                    s = self.filter(pk=pk)
                    s.update(type=typ)
            else:
                return 'Type is Empty'
        except:
            return 'Error Occurred'
        return 'Saved'


class CDT3Manager(models.Manager):

    def add(self, position):
        try:
            if position:
                self.create(position=position)
        except IntegrityError as e:
            return e
        except:
            return "Error Occurred"
        return ''.join(['Created', ' [', str(position), ']'])

    def delete(self, pk):
        try:
            if pk:
                a = self.get(pk=pk)
                r = a.delete()
                return r
            else:
                return 'Position is Empty'
        except:
            return 'Error Occurred'

    def edit(self, pk, position):
        try:
            if pk and position:
                if not self.filter(pk=pk).exists():
                    return 'Type not found'
                else:
                    s = self.filter(pk=pk)
                    s.update(position=position)
            else:
                return 'Position is Empty'
        except:
            return 'Error Occurred'
        return 'Saved'


class IECDT1Manager(models.Manager):

    @staticmethod
    def c_export(file_type):
        """
        Usage:
        from apps.apps.college.models import College
        College.import_export.c_export(file_type=('xls', 'xlsx', 'csv', 'json', 'yaml'))
        """
        # Import specific components and resources
        from apps.contrib.components import supported_types, export
        from .resource import CDT1Resource
        # Verify supplied file type is supported
        if supported_types.__contains__(file_type):
            # Return raw application data
            data = export(CDT1Resource(), 'CDT1', 'all', file_type)
            return data
        else:
            # Show error message
            return 'File Type not Supported'

    @staticmethod
    def c_import(file):
        """
        from apps.apps.college.models import College
        College.import_export.c_import(raw_file)
        """
        # Import specific resources and data sets
        from .resource import CDT1Resource
        from tablib import Dataset
        # Call College Resource
        res = CDT1Resource()
        # Create Empty Data set
        ds = Dataset()
        # Get File file from request and load to data sets
        ds.load(file.read())
        # Dry run receive file if valid
        result = res.import_data(ds, dry_run=True)
        # Verify that file has no errors
        if not result.has_errors():
            # Import data into database
            res.import_data(ds, dry_run=False)
            # Show Success Message
            return 'Success Importing data'
        else:
            # Show Error message
            return ''.join(['Failed to import. ', '[', str(ds), ']'])


class IECDT2Manager(models.Manager):

    @staticmethod
    def c_export(file_type):
        """
        Usage:
        from apps.apps.college.models import College
        College.import_export.c_export(file_type=('xls', 'xlsx', 'csv', 'json', 'yaml'))
        """
        # Import specific components and resources
        from apps.contrib.components import supported_types, export
        from .resource import CDT2Resource
        # Verify supplied file type is supported
        if supported_types.__contains__(file_type):
            # Return raw application data
            data = export(CDT2Resource, 'CDT2', 'all', file_type)
            return data
        else:
            # Show error message
            return 'File Type not Supported'

    @staticmethod
    def c_import(file):
        """
        from apps.apps.college.models import College
        College.import_export.c_import(raw_file)
        """
        # Import specific resources and data sets
        from .resource import CDT2Resource
        from tablib import Dataset
        # Call College Resource
        res = CDT2Resource()
        # Create Empty Data set
        ds = Dataset()
        # Get File file from request and load to data sets
        ds.load(file.read())
        # Dry run receive file if valid
        result = res.import_data(ds, dry_run=True)
        # Verify that file has no errors
        if not result.has_errors():
            # Import data into database
            res.import_data(ds, dry_run=False)
            # Show Success Message
            return 'Success Importing data'
        else:
            # Show Error message
            return ''.join(['Failed to import. ', '[', str(ds), ']'])


class IECDT3Manager(models.Manager):

    @staticmethod
    def c_export(file_type):
        """
        Usage:
        from apps.apps.college.models import College
        College.import_export.c_export(file_type=('xls', 'xlsx', 'csv', 'json', 'yaml'))
        """
        # Import specific components and resources
        from apps.contrib.components import supported_types, export
        from .resource import CDT3Resource
        # Verify supplied file type is supported
        if supported_types.__contains__(file_type):
            # Return raw application data
            data = export(CDT3Resource(), 'CDT3', 'all', file_type)
            return data
        else:
            # Show error message
            return 'File Type not Supported'

    @staticmethod
    def c_import(file):
        """
        from apps.apps.college.models import College
        College.import_export.c_import(raw_file)
        """
        # Import specific resources and data sets
        from .resource import CDT3Resource
        from tablib import Dataset
        # Call College Resource
        res = CDT3Resource()
        # Create Empty Data set
        ds = Dataset()
        # Get File file from request and load to data sets
        ds.load(file.read())
        # Dry run receive file if valid
        result = res.import_data(ds, dry_run=True)
        # Verify that file has no errors
        if not result.has_errors():
            # Import data into database
            res.import_data(ds, dry_run=False)
            # Show Success Message
            return 'Success Importing data'
        else:
            # Show Error message
            return ''.join(['Failed to import. ', '[', str(ds), ']'])


class CDT1(models.Model):
    abbreviation = models.CharField(max_length=50, unique=True)
    college = models.ForeignKey(College, on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=100)
    type = models.ForeignKey('CDT2', on_delete=models.DO_NOTHING)
    objects = CDT1Manager()
    ie = IECDT1Manager()

    def __str__(self):
        return self.abbreviation

    def save(self, *args, **kwargs):
        self.abbreviation = ''.join(filter(str.isalpha, self.abbreviation)).upper()
        self.name = ''.join(filter(whitelist.__contains__, self.name))
        super().save(self, *args, **kwargs)

    def delete(self, using=None, keep_parents=False):
        """
        Module Delete Interceptor, it will intercept requested deleted CDT1 object.
        The module will create a snapshot of a deleted CDT1 and upload it to system
        Deleted User Bin
        """
        # Import specific resources, files, and db
        import csv
        from apps.contrib.recyclebin.resource import DeletedUserBin
        from django.core.files import File
        from django.db import IntegrityError
        # Get College info
        cdt1_abbreviation = self.abbreviation
        cdt1_college = self.college
        cdt1_name = self.name
        cdt1_type = self.type
        # Try to delete CDT2
        try:
            # Confirm deletion
            super().delete(using=None, keep_parents=False)
        except IntegrityError as e:
            # Catch if CDT1 has existing relation to other models
            return ''.join(['Failed to delete: ', str(e.__cause__).partition('DETAIL:')[2]])
        # Call Deleted User Bin() for creating snapshot
        d = DeletedUserBin()
        # Open new file and write information related to user requested
        with open('temp.csv', 'w', newline='') as csvfile:
            cw = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            cw.writerow(['Abbreviation', 'College', 'Name', 'type'])
            cw.writerow([cdt1_abbreviation, cdt1_college, cdt1_name, cdt1_type])
        # Read temp CVS file
        local_file = open('temp.csv')
        d.timestamp = timezone.localtime()
        d.attachment.save(''.join(['CDT1', '_',
                                   str(cdt1_abbreviation), '_',
                                   str(timezone.localtime()).partition(' ')[0]]),
                          File(local_file))
        # Save data into database
        d.save()
        # Return string success message
        return 'Deleted'


class CDT2(models.Model):
    type = models.CharField(max_length=50)
    objects = CDT2Manager()
    ie = IECDT2Manager()

    def __str__(self):
        return self.type

    def save(self, *args, **kwargs):
        self.type = ''.join(filter(str.isalpha, self.type)).upper()
        super().save(self, *args, **kwargs)

    def delete(self, using=None, keep_parents=False):
        """
        Module Delete Interceptor, it will intercept requested deleted CDT2 object.
        The module will create a snapshot of a deleted college and upload it to system
        Deleted User Bin
        """
        # Import specific resources, files, and db
        import csv
        from apps.contrib.recyclebin.resource import DeletedUserBin
        from django.core.files import File
        from django.db import IntegrityError
        # Get College info
        cdt2_type = self.type
        # Try to delete CDT2
        try:
            # Confirm deletion
            super().delete(using=None, keep_parents=False)
        except IntegrityError as e:
            # Catch if CDT2 has existing relation to other models
            return ''.join(['Failed to delete: ', str(e.__cause__).partition('DETAIL:')[2]])
        # Call Deleted User Bin() for creating snapshot
        d = DeletedUserBin()
        # Open new file and write information related to user requested
        with open('temp.csv', 'w', newline='') as csvfile:
            cw = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            cw.writerow(['Type'])
            cw.writerow([cdt2_type])
        # Read temp CVS file
        local_file = open('temp.csv')
        d.timestamp = timezone.localtime()
        d.attachment.save(''.join(['CDT2', '_',
                                   str(cdt2_type), '_',
                                   str(timezone.localtime()).partition(' ')[0]]),
                          File(local_file))
        # Save data into database
        d.save()
        # Return string success message
        return 'Deleted'


class CDT3(models.Model):
    position = models.CharField(max_length=50)
    objects = CDT3Manager()
    ie = IECDT3Manager()

    def __str__(self):
        return self.position

    def save(self, *args, **kwargs):
        self.type = ''.join(filter(str.isalpha, self.position)).upper()
        super().save(self, *args, **kwargs)

    def delete(self, using=None, keep_parents=False):
        """
        Module Delete Interceptor, it will intercept requested deleted CDT3 object.
        The module will create a snapshot of a deleted college and upload it to system
        Deleted User Bin
        """
        # Import specific resources, files, and db
        import csv
        from apps.contrib.recyclebin.resource import DeletedUserBin
        from django.core.files import File
        from django.db import IntegrityError
        # Get College info
        cdt3_position = self.position
        # Try to delete CDT3
        try:
            # Confirm deletion
            super().delete(using=None, keep_parents=False)
        except IntegrityError as e:
            # Catch if CDT3 has existing relation to other models
            return ''.join(['Failed to delete: ', str(e.__cause__).partition('DETAIL:')[2]])
        # Call Deleted User Bin() for creating snapshot
        d = DeletedUserBin()
        # Open new file and write information related to user requested
        with open('temp.csv', 'w', newline='') as csvfile:
            cw = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            cw.writerow(['Type'])
            cw.writerow([cdt3_position])
        # Read temp CVS file
        local_file = open('temp.csv')
        d.timestamp = timezone.localtime()
        d.attachment.save(''.join(['CDT2', '_',
                                   str(cdt3_position), '_',
                                   str(timezone.localtime()).partition(' ')[0]]),
                          File(local_file))
        # Save data into database
        d.save()
        # Return string success message
        return 'Deleted'
