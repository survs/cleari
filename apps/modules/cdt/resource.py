from django.contrib.auth.models import User
from import_export import resources

from .models import CDT1, CDT2, CDT3


class CDT1Resource(resources.ModelResource):
    class Meta:
        model = CDT1


class CDT2Resource(resources.ModelResource):
    class Meta:
        model = CDT2


class CDT3Resource(resources.ModelResource):
    class Meta:
        model = CDT3


class DjangoAdmin(resources.ModelResource):
    class Meta:
        model = User
