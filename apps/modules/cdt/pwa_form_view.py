from django.http import JsonResponse
from django.middleware.csrf import get_token
from django.shortcuts import render

from apps.contrib.ie.forms import DataImportForm, DataExportForm
from apps.modules.users.models import CUser
from .forms import CDT1Form, CDT2Form, CDT3Form


def cdt_form(request, schema):
    if not request.user.is_authenticated:
        return JsonResponse(status=403, data='Not allowed to perform', safe=False)
    if CUser.objects.filter(id__username=request.user.username).exists():
        u = CUser.objects.get(id__username=request.user.username)
        csrf_token = get_token(request)
        if str(u.type).lower() == 'admin':
            if schema == 'cdt1':
                c1 = CDT1Form()
                context = dict([('html', str(c1)), ('token', csrf_token)])
                return JsonResponse(status=200, data=context, safe=False)
            elif schema == 'cdt2':
                c2 = CDT2Form()
                context = dict([('html', str(c2)), ('token', csrf_token)])
                return JsonResponse(status=200, data=context, safe=False)
            elif schema == 'cdt3':
                c3 = CDT3Form()
                context = dict([('html', str(c3)), ('token', csrf_token)])
                return JsonResponse(status=200, data=context, safe=False)
            else:
                return JsonResponse(status=404, data='Schema not Found', safe=False)
        else:
            return JsonResponse(status=403, data='Not allowed to perform', safe=False)
    else:
        return JsonResponse(status=400, data='User does not exist on Cleari Database', safe=False)


def export_form(request):
    # Call DataExportForm
    ef = DataExportForm()
    # Render Signup template with Export Form
    return render(request,
                  'registration/signup.html',
                  {'form': ef,
                   'title': 'Export',
                   'submit_title': 'Download'})


def import_form(request):
    # Call Data Import Form
    di = DataImportForm()
    # Render Signup template with Data Import form into HTML format
    return render(request,
                  'core/submission/file.html',
                  {'form': di,
                   'title': 'Import',
                   'submit_title': 'Upload'})
