from django.urls import path
from django.views.decorators.http import require_POST

from . import pwa_views

app_name = 'semester'

urlpatterns = [
    # path('addx', require_POST(pwa_views.add), name='addx'),
    path('add/<str:semester>', pwa_views.add, name='add'),
    path('edit/<str:semester>/<str:pk>', pwa_views.edit, name='edit'),
    path('delete/<str:semester>/<str:pk>', pwa_views.delete, name='delete'),
    path('import', require_POST(pwa_views.data_import), name='import_data'),
    path('export/<str:typ>', pwa_views.data_export, name='import_form'),
]
