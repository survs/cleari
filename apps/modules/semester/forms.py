import datetime

from django import forms

from .models import Semester


class SemesterForm(forms.Form):
    sem = forms.IntegerField(label='Semester', widget=forms.NumberInput(attrs={'ng-model': 'form.sem'}))
    yr1 = forms.DateField(label='from', widget=forms.DateInput(attrs={'ng-model': 'form.yr1'}))
    yr2 = forms.DateField(label='to', widget=forms.DateInput(attrs={'ng-model': 'form.yr2'}))
    clearance_start = forms.DateTimeField(label='Clearance Start',
                                          widget=forms.DateTimeInput(attrs={'ng-model': 'form.clearance_start'}))
    clearance_end = forms.DateTimeField(label='Clearance End',
                                        widget=forms.DateTimeInput(attrs={'ng-model': 'form.clearance_end'}))

    def submit(self):
        sem = self.cleaned_data['sem']
        yr1 = datetime.datetime.strptime(str(self.cleaned_data['yr1']), '%Y-%m-%d').year
        yr2 = datetime.datetime.strptime(str(self.cleaned_data['yr2']), '%Y-%m-%d').year
        start = self.cleaned_data['clearance_start']
        end = self.cleaned_data['clearance_end']
        r = Semester.objects.add(yr1, yr2, sem, start, end)
        return r
