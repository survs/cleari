from django.http import JsonResponse
from django.shortcuts import render

from .forms import SemesterForm


def add(request, semester):
    if request.method == 'POST':
        r = SemesterForm(request.POST)
        if r.is_valid():
            d = r.submit()
            return JsonResponse(status=200, data=str(d), safe=False)
        else:
            return JsonResponse(status=203, data=str(r.errors), safe=False)
    else:
        r = SemesterForm()
        return render(request, 'registration/signup.html', {'form': r})
