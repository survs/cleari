from import_export import resources

from .models import Semester


class SemesterResource(resources.ModelResource):
    class Meta:
        model = Semester
