from django.forms.models import model_to_dict
from django.http import JsonResponse

from apps.contrib.ie.forms import DataImportForm
from apps.modules.users.models import CUser
from .forms import SemesterForm
from .models import Semester


def add(request, semester):
    if not request.user.is_authenticated:
        return JsonResponse(status=403, data='Not allowed to perform')
    if CUser.objects.filter(id__username=request.user.username).exists():
        u = CUser.objects.get(id__username=request.user.username)
        if str(u.type).lower() == 'admin':
            c1 = SemesterForm(request.POST)
            if c1.is_valid():
                c1.submit()
                return JsonResponse(status=200, data='Created', safe=False)
            else:
                return JsonResponse(status=400, data=str(c1.errors), safe=False)
        else:
            return JsonResponse(status=403, data='Not allowed to perform', safe=False)
    else:
        return JsonResponse(status=400, data='User does not exist on Cleari Database', safe=False)


def edit(request, semester, pk):
    if not request.user.is_authenticated:
        return JsonResponse(status=403, data='Not allowed to perform')
    if CUser.objects.filter(id__username=request.user.username).exists():
        u = CUser.objects.get(id__username=request.user.username)
        print(str(u.type))
        if str(u.type).lower() == 'admin':
            if not Semester.objects.filter(pk=pk).exists():
                return JsonResponse(status=404, data='Not Found', safe=False)
            c1 = SemesterForm(initial=model_to_dict(Semester.objects.get(pk=pk)))
            context = dict([('initial', c1.initial)])
            return JsonResponse(status=200, data=context, safe=False)
        else:
            return JsonResponse(status=403, data='Not allowed to perform', safe=False)
    else:
        return JsonResponse(status=400, data='User does not exist on Cleari Database', safe=False)


def delete(request, semester, pk):
    if not request.user.is_authenticated:
        return JsonResponse(status=403, data='Not allowed to perform')
    if CUser.objects.filter(id__username=request.user.username).exists():
        u = CUser.objects.get(id__username=request.user.username)
        print(str(u.type))
        if str(u.type).lower() == 'admin':
            if not Semester.objects.filter(pk=pk).exists():
                return JsonResponse(status=404, data='Not Found', safe=False)
            c1 = Semester.objects.delete(pk)
            if c1 != 'Deleted':
                return JsonResponse(status=500, data=c1, safe=False)
            return JsonResponse(status=200, data=str(c1).upper(), safe=False)
        else:
            return JsonResponse(status=403, data='Not allowed to perform', safe=False)
    else:
        return JsonResponse(status=400, data='User does not exist on Cleari Database', safe=False)


def data_export(request, typ):
    if typ:
        return Semester.ie.c_export(typ)
    else:
        return JsonResponse(status=400, data='Some parameters are empty', safe=False)


def data_import(request):
    # Call Data Import Form with request data and request files
    di = DataImportForm(request.POST, request.FILES)
    # Verify form is valid
    if di.is_valid():
        d = request.FILES['file']
        return Semester.ie.c_import(d)
    else:
        return JsonResponse(status=400, data=str(di.errors), safe=False)
