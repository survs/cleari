from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import Semester


@admin.register(Semester)
class Semester(ImportExportModelAdmin):
    pass
