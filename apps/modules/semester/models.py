from django.contrib.humanize.templatetags.humanize import ordinal
from django.db import models
from django.utils import timezone


class SemesterManager(models.Manager):

    def add(self, yr1, yr2, sem, start, end):
        from apps.modules.clearance.models import ClearanceDuration
        if yr1 and yr2 and sem and start and end:
            # Calculate remaining time of scheduled date
            duration = timezone.localtime(end) - timezone.localtime(start)
            # Verify if schedule provided is valid
            print(duration, start, end)
            if duration.days <= 0:
                return 'Invalid Clearance Schedule'
            if sem > 10:
                return 'Failed to set semester'
            # Prepare to save data
            i = ':'.join([str(yr1), str(yr2), str(sem)])
            self.create(sys=i,
                        active=None,
                        semester=sem,
                        school_year=':'.join([str(yr1), str(yr2)]))
            c = ClearanceDuration(sys_id=self.get(sys=i).pk,
                                  clearance_start=start,
                                  clearance_end=end, )
            c.save()
            return 'Created'
        else:
            return 'Some fields are empty'

    def edit(self, pk, sys, yr1, yr2, sem, start, end):
        from apps.modules.clearance.models import ClearanceDuration
        if pk and sys and yr1 and yr2 and sem and start and end:
            if self.filter(pk=pk).exists():
                s = self.get(pk=pk)
                i = ':'.join([yr1, yr2, sem])
                s.sys = i
                s.semester = sem
                s.school_year = ':'.join([yr1, yr2])
                s.save()
                c = ClearanceDuration(sys_id=self.get(sys=i),
                                      clearance_start=start,
                                      clearance_end=end, )
                c.save()
                return 'Changes Saved'
            else:
                return 'Semester does not exist'
        else:
            return 'Some fields are empty'

    def activate(self, pk):
        if pk:
            if self.filter(pk=pk).exists():
                if Semester.objects.filter(active=True).exists():
                    s = Semester.objects.get(active=True)
                    a = Semester.objects.get(pk=pk)
                    if s.sys == a.sys:
                        return 'Semester already Activated'
                    # Deactivate Current active semester
                    s.active = None
                    # Save into database
                    s.save()
                    # Activate requested semester
                    a.active = True
                    # Save into database
                    a.save()
                    # Add success message information
                    return 'New Semester Activated'
                else:
                    # Activate requested semester
                    a = self.get(pk=pk)
                    a.active = True
                    # Save into database
                    a.save()
                    # Add success message information
                    return 'Semester Activated'
            else:
                return 'Semester does not exist'
        else:
            return 'Some fields are empty'

    def deactivate(self, pk):
        if pk:
            if self.filter(pk).exists():
                if Semester.objects.filter(active=True).exists():
                    a = Semester.objects.get(pk=pk)
                    # Activate requested semester
                    a.active = None
                    # Save into database
                    a.save()
                    # Add success message information
                    return 'Semester deactivated'
                else:
                    return 'No Active Semester'
            else:
                return 'Semester does not exist'
        else:
            return 'Some fields are empty'

    def delete(self, pk):
        if pk:
            if self.filter(pk=pk).exists():
                s = self.get(pk=pk)
                r = s.delete()
                return r
            else:
                return 'Semester does not exist'
        else:
            return 'Some fields are empty'


class IESemesterManager(models.Manager):
    @staticmethod
    def c_export(file_type):
        """
        Usage:
        from apps.apps.college.models import College
        College.import_export.c_export(file_type=('xls', 'xlsx', 'csv', 'json', 'yaml'))
        """
        # Import specific components and resources
        from apps.contrib.components import supported_types, export
        from .resource import SemesterResource
        # Verify supplied file type is supported
        if supported_types.__contains__(file_type):
            # Return raw application data
            data = export(SemesterResource(), 'Semester', 'all', file_type)
            return data
        else:
            # Show error message
            return 'File Type not Supported'

    @staticmethod
    def c_import(file):
        """
        from apps.apps.college.models import College
        College.import_export.c_import(raw_file)
        """
        # Import specific resources and data sets
        from .resource import SemesterResource
        from tablib import Dataset
        # Call Semester Resource
        res = SemesterResource()
        # Create Empty Data set
        ds = Dataset()
        # Get File file from request and load to data sets
        ds.load(file.read())
        # Dry run receive file if valid
        result = res.import_data(ds, dry_run=True)
        # Verify that file has no errors
        if not result.has_errors():
            # Import data into database
            res.import_data(ds, dry_run=False)
            # Show Success Message
            return 'Success Importing data'
        else:
            # Show Error message
            return ''.join(['Failed to import. ', '[', str(ds), ']'])


class Semester(models.Model):
    sys = models.CharField(unique=True, max_length=30)
    active = models.NullBooleanField(default=None, unique=True)
    school_year = models.CharField(max_length=30)
    semester = models.IntegerField()
    objects = SemesterManager()
    ie = IESemesterManager()

    def __str__(self):
        sy = str(self.school_year).split(':')
        is_active = 'Active' if self.active else 'Inactive'
        return ''.join([str(ordinal(self.semester)),
                        ' Semester, S.Y.',
                        sy[0], '-',
                        sy[1], ' | ',
                        is_active])

    def delete(self, using=None, keep_parents=False):
        """
        Module Delete Interceptor, it will intercept requested deleted Semester object.
        The module will create a snapshot of a deleted Clearance Liability and upload it to system
        Deleted User Bin
        """
        # Import specific resources, files, and db
        import csv
        from apps.contrib.recyclebin.resource import DeletedUserBin
        from django.core.files import File
        from django.db import IntegrityError
        # Get Clearance Duration info
        sys = self.sys
        active = self.active
        school_year = self.school_year
        semester = self.semester
        # Try to delete clearance
        try:
            # Confirm deletion
            super().delete(using=None, keep_parents=False)
        except IntegrityError as e:
            # Catch if CL has existing relation to other models
            return ''.join(['Failed to delete: ', str(e.__cause__).partition('DETAIL:')[2]])
        # Call Deleted User Bin() for creating snapshot
        d = DeletedUserBin()
        # Open new file and write information related to user requested
        with open('temp.csv', 'w', newline='') as csvfile:
            cw = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            cw.writerow(['SYS ID', 'Active', 'SY', 'Semester'])
            cw.writerow([sys, active, school_year, semester])
        # Read temp CVS file
        local_file = open('temp.csv')
        d.timestamp = timezone.localtime()
        d.attachment.save(''.join(['Semester', '_',
                                   str('All'), '_',
                                   str(timezone.localtime()).partition(' ')[0]]),
                          File(local_file))
        # Save data into database
        d.save()
        # Return string success message
        return 'Deleted'
