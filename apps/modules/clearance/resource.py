from import_export import resources

from .models import ClearanceDuration, ClearanceLiability


class CDResource(resources.ModelResource):
    class Meta:
        model = ClearanceDuration


class CLResource(resources.ModelResource):
    class Meta:
        model = ClearanceLiability
