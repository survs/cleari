from django.db import models
from django.utils import timezone

from apps.modules.cdt.models import CDT1
from apps.modules.college.models import College
from apps.modules.course.models import Course
from apps.modules.semester.models import Semester
from apps.modules.users.models import UserSignatory, CUser


class CDManager(models.Manager):

    def add(self, sys, start, end):
        try:
            if sys and start and end:
                if not Semester.objects.filter(sys=sys).exists():
                    return 'Semester not found'
                self.create(sys=sys, clearance_start=start, clearance_end=end)
            else:
                return 'Some fields are empty'
        except:
            return 'Error Occurred'
        return ''.join(['Created Clearance Duration for ', ' [', str(sys), ']'])

    def delete(self, pk):
        try:
            if pk:
                if not self.filter(pk=pk).exists():
                    return 'Clearance Duration Schema not found'
                s = self.get(pk=pk)
                r = s.delete()
                return r
            else:
                return 'sys is empty'
        except:
            return 'Error Occurred'

    def edit(self, pk, sys, start, end):
        print(start, end)
        if pk and sys and start and end:
            if not self.filter(pk=pk).exists():
                return 'Semester not found'
            s = self.filter(pk=pk)
            s.update(sys_id=sys,
                     clearance_start=start,
                     clearance_end=end)
        else:
            return 'Some fields are empty'


class CManager(models.Manager):

    def cleared(self, pk, number):
        self.filter(pk=pk, clearancedependency__receipt__number=number)

    def delete(self, pk):
        try:
            if pk:
                if not self.filter(pk=pk).exists():
                    return 'Clearance Schema not found'
                dep = ClearanceDependency.objects.filter(clearance_id=pk)
                for i in dep:
                    i.delete()
                s = self.get(pk=pk)
                r = s.delete()
                return r
            else:
                return 'Primary Key is empty'
        except:
            return 'Error Occurred'


class CLManager(models.Manager):

    def add(self, liability, description, cdt, course, sys):
        try:
            if liability and description and cdt and course and sys:
                if not CDT1.objects.filter(abbreviation=cdt).exists():
                    return 'CDT not found'
                elif not Course.objects.filter(course=course).exists():
                    return 'Course not found'
                elif not Semester.objects.filter(sys=sys).exists():
                    return 'Semester not found'
                self.create(liability=liability, description=description,
                            cdt_id=cdt, course_id=course, sys_id=sys)
            else:
                return 'Some fields are empty'
        except:
            return 'Error Occurred'
        return ''.join(['Created', ' [', liability, ']'])

    def edit(self, pk, liability, description, cdt, course, sys):
        try:
            if pk and liability and description and cdt and course and sys:
                if not CDT1.objects.filter(abbreviation=cdt).exists():
                    return 'CDT not found'
                elif not Course.objects.filter(course=course).exists():
                    return 'Course not found'
                elif not Semester.objects.filter(sys=sys).exists():
                    return 'Semester not found'
                elif not self.filter(pk=pk).exists():
                    return 'Liability not found'
                s = self.filter(pk=pk)
                s.update(liability=liability,
                         description=description,
                         signatory_id=cdt,
                         course_id=course,
                         sys_id=sys, )
            else:
                return 'Some fields are empty'
        except:
            return 'Error Occurred'
        return 'Saved'

    def delete(self, pk):
        try:
            if not self.filter(pk=pk).exists():
                return 'Clearance Liability not found'
            s = self.get(pk=pk)
            r = s.delete()
            return r
        except:
            return 'Error Occurred'


class IECDManager(models.Manager):

    @staticmethod
    def c_export(file_type):
        """
        Usage:
        from apps.apps.college.models import College
        College.import_export.c_export(file_type=('xls', 'xlsx', 'csv', 'json', 'yaml'))
        """
        # Import specific components and resources
        from apps.contrib.components import supported_types, export
        from .resource import CDResource
        # Verify supplied file type is supported
        if supported_types.__contains__(file_type):
            # Return raw application data
            data = export(CDResource(), 'Clearance Duration', 'all', file_type)
            return data
        else:
            # Show error message
            return 'File Type not Supported'

    @staticmethod
    def c_import(file):
        """
        from apps.apps.college.models import College
        College.import_export.c_import(raw_file)
        """
        # Import specific resources and data sets
        from .resource import CDResource
        from tablib import Dataset
        # Call College Resource
        res = CDResource()
        # Create Empty Data set
        ds = Dataset()
        # Get File file from request and load to data sets
        ds.load(file.read())
        # Dry run receive file if valid
        result = res.import_data(ds, dry_run=True)
        # Verify that file has no errors
        if not result.has_errors():
            # Import data into database
            res.import_data(ds, dry_run=False)
            # Show Success Message
            return 'Success Importing data'
        else:
            # Show Error message
            return ''.join(['Failed to import. ', '[', str(ds), ']'])


class IECLManager(models.Manager):

    @staticmethod
    def c_export(file_type):
        """
        Usage:
        from apps.apps.college.models import College
        College.import_export.c_export(file_type=('xls', 'xlsx', 'csv', 'json', 'yaml'))
        """
        # Import specific components and resources
        from apps.contrib.components import supported_types, export
        from .resource import CLResource
        # Verify supplied file type is supported
        if supported_types.__contains__(file_type):
            # Return raw application data
            data = export(CLResource(), 'Clearance Liability', 'all', file_type)
            return data
        else:
            # Show error message
            return 'File Type not Supported'

    @staticmethod
    def c_import(file):
        """
        from apps.apps.college.models import College
        College.import_export.c_import(raw_file)
        """
        # Import specific resources and data sets
        from .resource import CLResource
        from tablib import Dataset
        # Call College Resource
        res = CLResource()
        # Create Empty Data set
        ds = Dataset()
        # Get File file from request and load to data sets
        ds.load(file.read())
        # Dry run receive file if valid
        result = res.import_data(ds, dry_run=True)
        # Verify that file has no errors
        if not result.has_errors():
            # Import data into database
            res.import_data(ds, dry_run=False)
            # Show Success Message
            return 'Success Importing data'
        else:
            # Show Error message
            return ''.join(['Failed to import. ', '[', str(ds), ']'])


class ClearanceDuration(models.Model):
    sys = models.OneToOneField(Semester, on_delete=models.DO_NOTHING)
    clearance_start = models.DateTimeField()
    clearance_end = models.DateTimeField()
    objects = CDManager()
    ie = IECDManager()

    def __str__(self):
        return ''.join([str(self.clearance_start), '-', str(self.clearance_end)])

    def delete(self, using=None, keep_parents=False):
        """
        Module Delete Interceptor, it will intercept requested deleted Clearance Duration object.
        The module will create a snapshot of a deleted Clearance Duration and upload it to system
        Deleted User Bin
        """
        # Import specific resources, files, and db
        import csv
        from apps.contrib.recyclebin.resource import DeletedUserBin
        from django.core.files import File
        from django.db import IntegrityError
        # Get Clearance Duration info
        cd_sys = self.sys
        cd_start = self.clearance_start
        cd_end = self.clearance_end
        # Try to delete clearance
        try:
            # Confirm deletion
            super().delete(using=None, keep_parents=False)
        except IntegrityError as e:
            # Catch if CD has existing relation to other models
            return ''.join(['Failed to delete: ', str(e.__cause__).partition('DETAIL:')[2]])
        # Call Deleted User Bin() for creating snapshot
        d = DeletedUserBin()
        # Open new file and write information related to user requested
        with open('temp.csv', 'w', newline='') as csvfile:
            cw = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            cw.writerow(['Semester', 'Clearance Start', 'Clearance End'])
            cw.writerow([cd_sys, cd_start, cd_end])
        # Read temp CVS file
        local_file = open('temp.csv')
        d.timestamp = timezone.localtime()
        d.attachment.save(''.join(['Clearance Duration', '_',
                                   str(''.join([str(self.clearance_start), '-', str(self.clearance_end)])), '_',
                                   str(timezone.localtime()).partition(' ')[0]]),
                          File(local_file))
        # Save data into database
        d.save()
        # Return string success message
        return 'Deleted'


class ClearanceLiability(models.Model):
    liability = models.CharField(max_length=30, unique=True)
    description = models.TextField(max_length=500)
    signatory = models.ForeignKey(CDT1, on_delete=models.DO_NOTHING)
    course = models.ForeignKey(Course, on_delete=models.DO_NOTHING)
    sys = models.ForeignKey(Semester, on_delete=models.DO_NOTHING)
    objects = CLManager()
    ie = IECLManager()

    def __str__(self):
        return str(self.liability)

    def save(self, *args, **kwargs):
        self.liability = ''.join(filter(str.isalpha, self.liability)).upper()
        self.description = ''.join(filter(str.isalpha, self.description))
        super().save(self, *args, **kwargs)

    def delete(self, using=None, keep_parents=False):
        """
        Module Delete Interceptor, it will intercept requested deleted Clearance Liability object.
        The module will create a snapshot of a deleted Clearance Liability and upload it to system
        Deleted User Bin
        """
        # Import specific resources, files, and db
        import csv
        from apps.contrib.recyclebin.resource import DeletedUserBin
        from django.core.files import File
        from django.db import IntegrityError
        # Get Clearance Duration info
        cl_liability = self.liability
        cl_description = self.description
        cl_signatory = self.signatory
        cl_course = self.course
        cl_sy = self.sys
        # Try to delete clearance
        try:
            # Confirm deletion
            super().delete(using=None, keep_parents=False)
        except IntegrityError as e:
            # Catch if CL has existing relation to other models
            return ''.join(['Failed to delete: ', str(e.__cause__).partition('DETAIL:')[2]])
        # Call Deleted User Bin() for creating snapshot
        d = DeletedUserBin()
        # Open new file and write information related to user requested
        with open('temp.csv', 'w', newline='') as csvfile:
            cw = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            cw.writerow(['Liability', 'Description', 'Signatory', 'Course', 'SYS'])
            cw.writerow([cl_liability, cl_description, cl_signatory, cl_course, cl_sy])
        # Read temp CVS file
        local_file = open('temp.csv')
        d.timestamp = timezone.localtime()
        d.attachment.save(''.join(['Clearance Liability', '_',
                                   str('All'), '_',
                                   str(timezone.localtime()).partition(' ')[0]]),
                          File(local_file))
        # Save data into database
        d.save()
        # Return string success message
        return 'Deleted'


class Clearance(models.Model):
    active = models.BooleanField(default=False)
    name = models.CharField(max_length=50)
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE)
    college = models.ForeignKey(College, on_delete=models.DO_NOTHING)
    objects = CManager()

    def delete(self, using=None, keep_parents=False):
        """
        Module Delete Interceptor, it will intercept requested deleted Clearance Liability object.
        The module will create a snapshot of a deleted Clearance Liability and upload it to system
        Deleted User Bin
        """
        # Import specific resources, files, and db
        import csv
        from apps.contrib.recyclebin.resource import DeletedUserBin
        from django.core.files import File
        from django.db import IntegrityError
        # Get Clearance Duration info
        cl_active = self.active
        cl_name = self.name
        cl_semester = self.semester
        cl_college = self.college
        # Try to delete clearance
        try:
            # Confirm deletion
            super().delete(using=None, keep_parents=False)
        except IntegrityError as e:
            # Catch if CL has existing relation to other models
            return ''.join(['Failed to delete: ', str(e.__cause__).partition('DETAIL:')[2]])
        # Call Deleted User Bin() for creating snapshot
        d = DeletedUserBin()
        # Open new file and write information related to user requested
        with open('temp.csv', 'w', newline='') as csvfile:
            cw = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            cw.writerow(['Status', 'Name', 'Semester', 'College'])
            cw.writerow([cl_active, cl_name, cl_semester, cl_college])
        # Read temp CVS file
        local_file = open('temp.csv')
        d.timestamp = timezone.localtime()
        d.attachment.save(''.join(['Clearance ', '_',
                                   str('All'), '_',
                                   str(timezone.localtime()).partition(' ')[0]]),
                          File(local_file))
        # Save data into database
        d.save()
        # Return string success message
        return 'Deleted'


class ClearanceDependency(models.Model):
    clearance = models.ForeignKey("Clearance", on_delete=models.CASCADE)
    signatory = models.ForeignKey(UserSignatory, on_delete=models.CASCADE)
    as_name = models.CharField(null=True, max_length=60)

    def delete(self, using=None, keep_parents=False):
        """
        Module Delete Interceptor, it will intercept requested deleted Clearance Liability object.
        The module will create a snapshot of a deleted Clearance Liability and upload it to system
        Deleted User Bin
        """
        # Import specific resources, files, and db
        import csv
        from apps.contrib.recyclebin.resource import DeletedUserBin
        from django.core.files import File
        from django.db import IntegrityError
        # Get Clearance Duration info
        cl_clearance = self.clearance
        cl_signatory = self.signatory
        cl_as_name = self.as_name
        # Try to delete clearance
        try:
            # Confirm deletion
            super().delete(using=None, keep_parents=False)
        except IntegrityError as e:
            # Catch if CL has existing relation to other models
            return ''.join(['Failed to delete: ', str(e.__cause__).partition('DETAIL:')[2]])
        # Call Deleted User Bin() for creating snapshot
        d = DeletedUserBin()
        # Open new file and write information related to user requested
        with open('temp.csv', 'w', newline='') as csvfile:
            cw = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            cw.writerow(['Clearance ID', 'Semester ID', 'Alias'])
            cw.writerow([cl_clearance, cl_signatory, cl_as_name])
        # Read temp CVS file
        local_file = open('temp.csv')
        d.timestamp = timezone.localtime()
        d.attachment.save(''.join(['Clearance Dependency', '_',
                                   str('All'), '_',
                                   str(timezone.localtime()).partition(' ')[0]]),
                          File(local_file))
        # Save data into database
        d.save()
        # Return string success message
        return 'Deleted'


class ClearanceUserStorage(models.Model):
    number = models.ForeignKey(CUser, on_delete=models.DO_NOTHING)
    clearance = models.ForeignKey(Clearance, on_delete=models.DO_NOTHING)
    status = models.BooleanField(default=False)