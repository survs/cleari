from django.urls import path
from django.views.decorators.http import require_POST

from . import pwa_views

app_name = 'clearance'

urlpatterns = [
    path('add/<str:schema>', require_POST(pwa_views.add), name='add'),
    path('edit/<str:schema>/<str:pk>', pwa_views.edit, name='edit'),
    path('delete/<str:schema>/<str:pk>', pwa_views.delete, name='delete'),
    path('import/<str:schema>', require_POST(pwa_views.data_import), name='import_data'),
    path('export/<str:schema>/<str:typ>', pwa_views.data_export, name='import_form'),
]
