# Generated by Django 2.1 on 2018-08-11 09:27

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('college', '0001_initial'),
        ('course', '0001_initial'),
        ('semester', '0001_initial'),
        ('cdt', '0001_initial'),
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Clearance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('active', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=50)),
                ('college', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='college.College')),
                ('semester', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='semester.Semester')),
            ],
        ),
        migrations.CreateModel(
            name='ClearanceDependency',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('as_name', models.CharField(max_length=60, null=True)),
                ('clearance', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='clearance.Clearance')),
                ('signatory', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='users.UserSignatory')),
            ],
        ),
        migrations.CreateModel(
            name='ClearanceDuration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('clearance_start', models.DateTimeField()),
                ('clearance_end', models.DateTimeField()),
                ('sys', models.OneToOneField(on_delete=django.db.models.deletion.DO_NOTHING, to='semester.Semester')),
            ],
        ),
        migrations.CreateModel(
            name='ClearanceLiability',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('liability', models.CharField(max_length=30, unique=True)),
                ('description', models.TextField(max_length=500)),
                ('course', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='course.Course')),
                ('signatory', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='cdt.CDT1')),
                ('sys', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='semester.Semester')),
            ],
        ),
    ]
