from django.http import JsonResponse
from django.middleware.csrf import get_token

from apps.contrib.ie.forms import DataImportForm
from apps.modules.users.models import CUser
from .forms import ClearanceDurationForm, ClearanceLiabilityForm
from .models import ClearanceDuration, ClearanceLiability, Clearance


def add(request, schema):
    if not request.user.is_authenticated:
        return JsonResponse(status=403, data='Not allowed to perform')
    csrf = get_token(request)
    if schema == 'csrf':
        return JsonResponse(status=203, data={'csrf': csrf}, safe=False)
    if CUser.objects.filter(id__username=request.user.username).exists():
        u = CUser.objects.get(id__username=request.user.username)
        if str(u.type).lower() == 'admin':
            if schema == 'cd':
                c1 = ClearanceDurationForm(request.POST)
                if c1.is_valid():
                    c1.save()
                    return JsonResponse(status=200, data='Created', safe=False)
                else:
                    return JsonResponse(status=400, data=str(c1.errors), safe=False)
            elif schema == 'cl':
                c2 = ClearanceLiabilityForm(request.POST)
                if c2.is_valid():
                    c2.save()
                    return JsonResponse(status=200, data='Created', safe=False)
                else:
                    return JsonResponse(status=400, data=str(c2.errors), safe=False)
            else:
                return JsonResponse(status=404, data='Schema not Found', safe=False)
        else:
            return JsonResponse(status=403, data='Not allowed to perform', safe=False)
    else:
        return JsonResponse(status=400, data='User does not exist on Cleari Database', safe=False)


def edit(request, schema, pk):
    if not request.user.is_authenticated:
        return JsonResponse(status=403, data='Not allowed to perform')
    csrf = get_token(request)
    if schema == 'csrf':
        return JsonResponse(status=203, data={'csrf': csrf}, safe=False)
    if CUser.objects.filter(id__username=request.user.username).exists():
        u = CUser.objects.get(id__username=request.user.username)
        if str(u.type).lower() == 'admin':
            if schema == 'cd' and request.method == 'GET':
                if not ClearanceDuration.objects.filter(pk=pk).exists():
                    return JsonResponse(status=404, data='Not Found', safe=False)
                c1 = ClearanceDurationForm(instance=ClearanceDuration.objects.get(pk=pk))
                sys = []
                for c in c1.fields.get('sys')._get_choices():
                    sys.append(c)
                context = dict([('initial', c1.initial),
                                ('fields', dict([('sys', dict(sys))]))])
                return JsonResponse(status=200, data=context, safe=False)
            elif schema == 'cl' and request.method == 'GET':
                if not ClearanceLiability.objects.filter(pk=pk).exists():
                    return JsonResponse(status=404, data='Not Found', safe=False)
                c2 = ClearanceLiabilityForm(instance=ClearanceLiability.objects.get(pk=pk))
                signatory = []
                for c in c2.fields.get('signatory')._get_choices():
                    signatory.append(c)
                course = []
                for c in c2.fields.get('course')._get_choices():
                    course.append(c)
                sys = []
                for c in c2.fields.get('sys')._get_choices():
                    signatory.append(c)
                context = dict([('initial', c2.initial),
                                ('fields', dict([('signatory', dict(signatory)),
                                                 ('course', dict(course)),
                                                 ('sys', dict(sys))]))])
                return JsonResponse(status=200, data=context, safe=False)
            elif schema == 'cd' and request.method == 'POST':
                if not ClearanceDuration.objects.filter(pk=pk).exists():
                    return JsonResponse(status=404, data='Not Found', safe=False)
                a = request.POST['sys']
                b = request.POST['clearance_start']
                c = request.POST['clearance_end']
                r = ClearanceDuration.objects.edit(pk, a, b, c)
                if r != 'Saved':
                    return JsonResponse(status=203, data=r, safe=False)
                return JsonResponse(status=200, data=r, safe=False)
            elif schema == 'cl' and request.method == 'POST':
                if not ClearanceLiability.objects.filter(pk=pk).exists():
                    return JsonResponse(status=404, data='Not Found', safe=False)
                a = request.POST['liability']
                b = request.POST['description']
                c = request.POST['signatory']
                d = request.POST.get('course', request.POST['course_list'])
                e = request.POST['sys']
                r = []
                if str(d).find(','):
                    for i in str(d).split(','):
                        r.append(ClearanceLiability.objects.edit(pk, a, b, c, i, e))
                else:
                    r = ClearanceLiability.objects.edit(pk, a, b, c, d, e)
                if r != 'Saved':
                    return JsonResponse(status=203, data=r, safe=False)
                return JsonResponse(status=200, data=r, safe=False)
            else:
                return JsonResponse(status=404, data='Schema not Found', safe=False)
        else:
            return JsonResponse(status=403, data='Not allowed to perform', safe=False)
    else:
        return JsonResponse(status=400, data='User does not exist on Cleari Database', safe=False)


def delete(request, schema, pk):
    if not request.user.is_authenticated:
        return JsonResponse(status=403, data='Not allowed to perform')
    if CUser.objects.filter(id__username=request.user.username).exists():
        u = CUser.objects.get(id__username=request.user.username)
        print(str(u.type))
        if str(u.type).lower() == 'admin':
            if schema == 'cd':
                if not ClearanceDuration.objects.filter(pk=pk).exists():
                    return JsonResponse(status=404, data='Not Found', safe=False)
                c1 = ClearanceDuration.objects.delete(pk)
                if c1 != 'Deleted':
                    return JsonResponse(status=500, data=c1, safe=False)
                return JsonResponse(status=200, data=str(c1).upper(), safe=False)
            elif schema == 'c':
                if not Clearance.objects.filter(pk=pk).exists():
                    return JsonResponse(status=404, data='Not Found', safe=False)
                c2 = Clearance.objects.delete(pk)
                if c2 != 'Deleted':
                    return JsonResponse(status=500, data=c2, safe=False)
                return JsonResponse(status=200, data=str(c2).upper(), safe=False)
            else:
                return JsonResponse(status=404, data='Schema not Found', safe=False)
        else:
            return JsonResponse(status=403, data='Not allowed to perform', safe=False)
    else:
        return JsonResponse(status=400, data='User does not exist on Cleari Database', safe=False)


def data_export(request, schema, typ):
    if typ and schema:
        # Verify file type is supported
        if schema == 'cd':
            return ClearanceDuration.ie.c_export(typ)
        elif schema == 'cl':
            return ClearanceLiability.ie.c_export(typ)
        else:
            return JsonResponse(status=404, data='Schema Not Found', safe=False)
    else:
        return JsonResponse(status=400, data='Some parameters are empty', safe=False)


def data_import(request, schema):
    # Call Data Import Form with request data and request files
    di = DataImportForm(request.POST, request.FILES)
    # Verify form is valid
    if di.is_valid():
        d = request.FILES['file']
        if schema == 'cd':
            return ClearanceDuration.ie.c_import(d)
        elif schema == 'cl':
            return ClearanceLiability.ie.c_import(d)
        else:
            return JsonResponse(status=404, data='Schema Not Found', safe=False)
    else:
        return JsonResponse(status=400, data=str(di.errors), safe=False)
