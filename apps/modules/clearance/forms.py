from django import forms

from .models import ClearanceDuration, ClearanceLiability


class ClearanceDurationForm(forms.ModelForm):
    class Meta:
        model = ClearanceDuration
        fields = ('sys', 'clearance_start', 'clearance_end')
        widgets = {
            'sys': forms.Select(attrs={'ng-model': 'form.sys'}),
            'clearance_start': forms.DateInput(attrs={'ng-model': 'form.clearance_start'}),
            'clearance_end': forms.DateInput(attrs={'ng-model': 'form.clearance_end'}),
        }


class ClearanceLiabilityForm(forms.ModelForm):

    class Meta:
        model = ClearanceLiability
        fields = ('liability', 'description', 'signatory', 'course', 'sys')
        widgets = {
            'liability': forms.TextInput(attrs={'ng-model': 'form.liability'}),
            'description': forms.TextInput(attrs={'ng-model': 'form.description'}),
            'signatory': forms.Select(attrs={'ng-model': 'form.signatory'}),
            'course': forms.SelectMultiple(attrs={'ng-model': 'form.course'}),
            'sys': forms.Select(attrs={'ng-model': 'form.sys'}),
        }
