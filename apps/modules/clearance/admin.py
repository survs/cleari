from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import ClearanceDuration, ClearanceLiability, Clearance, ClearanceDependency


@admin.register(ClearanceDuration)
class ClearanceDuration(ImportExportModelAdmin):
    pass


@admin.register(ClearanceLiability)
class ClearanceLiability(ImportExportModelAdmin):
    pass


@admin.register(Clearance)
class Clearance(ImportExportModelAdmin):
    pass


@admin.register(ClearanceDependency)
class ClearanceDependency(ImportExportModelAdmin):
    pass
