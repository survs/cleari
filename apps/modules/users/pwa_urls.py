from django.urls import path

from . import pwa_views

app_name = 'user'

urlpatterns = [
    path('edit', pwa_views.edit, name='edit'),
]
