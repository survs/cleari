from django import forms

from .models import CUser, UserSignatory, UserStudent


class UserEditForm(forms.ModelForm):
    class Meta:
        model = CUser
        fields = ('first_name', 'middle_name', 'last_name',
                  'address', 'birth_date',)
        widgets = {
            'first_name': forms.TextInput(attrs={'ng-model': 'form.first_name'}),
            'middle_name': forms.TextInput(attrs={'ng-model': 'form.middle_name'}),
            'last_name': forms.TextInput(attrs={'ng-model': 'form.last_name'}),
            'address': forms.TextInput(attrs={'ng-model': 'form.address'}),
            'birth_date': forms.DateInput(attrs={'ng-model': 'form.birth_date'}),
        }


class UserPhotoForm(forms.ModelForm):
    class Meta:
        model = CUser
        fields = ('profile',)
        widgets = {
            'profile': forms.FileInput(attrs={'ng-model': 'form.profile',
                                              'accept': 'image/png, image/jpeg, image/jpg',
                                              'ngf-select': '',
                                              }),
        }


class ChangePasswordForm(forms.Form):
    old = forms.CharField(widget=forms.PasswordInput, label='Old Password')
    new1 = forms.CharField(widget=forms.PasswordInput, label='New Password')
    new2 = forms.CharField(widget=forms.PasswordInput, label='Confirm New Password')

    def submit(self, username):
        r = CUser.objects.change_password(username, self.cleaned_data['old'],
                                          self.cleaned_data['new1'], self.cleaned_data['new2'])
        return r

    pass


class EmailForm(forms.Form):
    email = forms.EmailField(max_length=50)

    def submit(self, username):
        r = CUser.objects.set_email(username, self.cleaned_data['email'])
        return r

    pass


class SignatureForm(forms.Form):
    signature = forms.CharField(widget=forms.PasswordInput, label='Signature')

    def submit(self, username):
        r = UserSignatory.objects.set_signature(username, self.cleaned_data['signature'])
        return r


class AUserEditForm(forms.ModelForm):
    class Meta:
        model = CUser
        fields = ('first_name', 'middle_name', 'last_name',
                  'address', 'birth_date', 'type')


class StudentUserForm(forms.ModelForm):
    class Meta:
        model = UserStudent
        fields = ('number', 'year', 'course',)


class SignatoryUserForm(forms.ModelForm):
    class Meta:
        model = UserSignatory
        fields = ('number', 'assign', 'position', 'signature', 'cdt1')
