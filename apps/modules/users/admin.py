from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import CUser, UserStudent, UserSignatory, CUserType


@admin.register(CUser)
class User(ImportExportModelAdmin):
    pass


@admin.register(UserStudent)
class UserStudent(ImportExportModelAdmin):
    pass


@admin.register(UserSignatory)
class UserSignatory(ImportExportModelAdmin):
    pass


@admin.register(CUserType)
class UserType(ImportExportModelAdmin):
    pass
