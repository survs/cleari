from django.contrib.auth.models import User as DUser
from import_export import resources
from import_export.fields import Field

from .models import User as CUser


class DjangoUserResource(resources.ModelResource):
    class Meta:
        model = DUser


class UserResource(resources.ModelResource):
    class Meta:
        model = CUser


class SafeUserResource(resources.ModelResource):
    password = Field(column_name='password')

    class Meta:
        model = CUser
        exclude = ('password',)
