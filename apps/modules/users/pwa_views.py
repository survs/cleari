from django.http import JsonResponse

from apps.modules.users.models import CUser
from .forms import UserEditForm


def edit(request):
    if not request.user.is_authenticated:
        return JsonResponse(status=403, data='Not allowed to perform')
    if CUser.objects.filter(id__username=request.user.username).exists():
        u = CUser.objects.get(id__username=request.user.username)
        c1 = UserEditForm(instance=u)
        context = dict([('initial', c1.initial)])
        return JsonResponse(status=200, data=context, safe=False)
    else:
        return JsonResponse(status=400, data='User does not exist on Cleari Database', safe=False)
