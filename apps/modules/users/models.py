from django.contrib.auth.models import User
from django.contrib.humanize.templatetags.humanize import ordinal
from django.core.signing import Signer
from django.db import models

from apps.modules.cdt.models import CDT1, CDT3
from apps.modules.course.models import Course

sign = Signer(salt='@cleari_?_secret)_k3^y')


# User Manager for Adding Cleari Users
class AddUserManager(models.Manager):

    def user(self, username, password, user_type, first_name, middle_name, last_name, birth_date, address):
        """
        Module for adding Cleari User without user type relations
        Usage:
        from apps.apps.clients.models import User
        User.add.user(username, password, user_type=('Student', 'Signatory', 'Administrator'), first_name, middle_name, last_name)
        """
        if username and password and user_type and first_name and middle_name and last_name:
            if not CUserType.objects.filter(type=user_type).exists():
                return 'User Type not Found'
            # Call Django user modelcollege
            u = User.objects.create_user(username=username, first_name=first_name,
                                         last_name=last_name, password=password, email=None)
            # Set password into encrypted base
            u.set_password(password)
            # Save to database
            u.save()
            # Prepare to save into Cleari Database
            c = self.create(id_id=User.objects.get(username=username).pk,
                            number=User.objects.get(username=username).username,
                            password=User.objects.get(username=username).password,
                            type_id=CUserType.objects.filter(type=user_type).pk, first_name=first_name,
                            middle_name=middle_name, last_name=last_name,
                            birth_date=birth_date, address=address)
            # Save to database
            c.save()
            # Return string message
            return 'Saved'
        else:
            return 'Some fields are empty'

    def student(self, username, first_name, middle_name, last_name, birth_date, address, year, course, department,
                password):
        """
        Module for adding student users
        Usage:
        from apps.apps.clients.models import User
        User.add.student(username, password, first_name, middle_name, last_name, year, course, password)
        """
        if username and first_name and middle_name and last_name and year and course and department and password:
            # Import specific models
            from apps.modules.course.models import Course
            if not CUserType.objects.filter(type='student').exists():
                CUserType.objects.create(type='student')
            # Call Django Model
            u = User.objects.create_user(username=username, first_name=first_name,
                                         last_name=last_name, password=password, email=None)
            # Set password into encrypted base
            u.set_password(password)
            # Save to Database
            u.save()
            # Prepare to save into Database
            c = self.create(id_id=User.objects.get(username=username).pk,
                            number=User.objects.get(username=username).username,
                            password=User.objects.get(username=username).password,
                            type_id=CUserType.objects.filter(type='student').pk, first_name=first_name,
                            middle_name=middle_name, last_name=last_name,
                            birth_date=birth_date, address=address)
            # Save to database
            c.save()
            # Call User Student Model for preparation of saving
            s = UserStudent(number_id=self.get(number=username).number,
                            year_level=year, department=department,
                            course_id=Course.objects.get(pk=course).pk)
            # Save to database
            s.save()
            # Return string message
            return 'Saved'
        else:
            return 'Some fields are empty'

    def signatory(self, username, first_name, middle_name, last_name, signatory, birth_date, address, password):
        """
        Module for adding signatory users
        Usage:
        from apps.apps.clients.models import User
        User.add.signatory(username, password, first_name, middle_name, last_name, signatory, password)
        """
        if username and first_name and middle_name and last_name and signatory and password:
            if not CUserType.objects.filter(type='signatory').exists():
                CUserType.objects.create(type='signatory')
            # Call Django Model
            u = User.objects.create_user(username=username, first_name=first_name, last_name=last_name,
                                         password=password, email=None)
            # Set password into encrypted base
            u.set_password(password)
            # Save to database
            u.save()
            # Prepare to save into Database
            c = self.create(id_id=User.objects.get(username=username).pk,
                            number=User.objects.get(username=username).username,
                            password=User.objects.get(username=username).password,
                            type_id=CUserType.objects.get(type='signatory').pk, first_name=first_name,
                            middle_name=middle_name, last_name=last_name,
                            birth_date=birth_date, address=address)
            # Save to database
            c.save()
            # Call User Signatory Model
            s = UserSignatory(number_id=self.get(number=username).pk, signatory_id=signatory)
            # Save to database
            s.save()
            # Return string message
            return 'Saved'
        else:
            return 'Some fields are empty'

    def admin(self, username, first_name, middle_name, last_name, birth_date, address, password):
        """
        Module for adding admin users
        from apps.apps.clients.models import User
        User.add.admin(username, password, first_name, middle_name, last_name, password)
        """
        if username and first_name and middle_name and last_name and password:
            if not CUserType.objects.filter(type='admin').exists():
                CUserType.objects.create(type='admin')
            # Call Django Model
            u = User.objects.create_user(username=username, first_name=first_name, last_name=last_name,
                                         password=password, email=None)
            # Set password into encrypted base
            u.set_password(password)
            # Save to database
            u.save()
            # Prepare to save into Database
            c = self.create(id_id=User.objects.get(username=username).pk,
                            number=User.objects.get(username=username).username,
                            password=User.objects.get(username=username).password,
                            user_type=CUserType.objects.get(type='admin').pk, first_name=first_name,
                            middle_name=middle_name, last_name=last_name,
                            birth_date=birth_date, address=address)
            # Save to database
            c.save()
            # Return string message
            return 'Saved'
        else:
            return 'Some fields empty'


# User Manager for Import Export Users
class IEUserManager(models.Manager):

    def c_export(self, user_type, file_type, safe):
        """
        Module for Exporting User Accounts
        Usage:
        from apps.apps.clients.models import User
        User.import_export.c_export(user_type=('student','signatory','administrator'), file_type=('xls', 'xlsx', 'csv', 'json', 'yaml'), safe=(True, False))
        """
        # Import specific components and resources
        from apps.contrib.components import supported_types, filter_export, export
        from .resource import UserResource, SafeUserResource, DjangoUserResource
        # Verify if file type supplied is existing
        if supported_types.__contains__(file_type):
            # Filters user type is existing on Cleari Model
            if self.filter(user_type=user_type).exists():
                # Verify if data is safe
                if safe:
                    # Send Required data to Filter Export Function
                    data = filter_export(UserResource(), self.filter(user_type=user_type).all(), 'User', user_type,
                                         file_type)
                else:
                    # Send Required data to Filter Export Function
                    data = filter_export(SafeUserResource(), self.filter(user_type=user_type).all(), 'User - Safe ',
                                         user_type, file_type)
                # Return file data
                return data
            # Compares if user type is 'All'
            elif user_type == 'All':
                # Verify if data is safe
                if safe:
                    # Send data to export function
                    data = export(UserResource(), 'User', 'all', file_type)
                else:
                    # Send data to export function
                    data = export(SafeUserResource(), 'User - Safe ', 'all', file_type)
                # Return file data
                return data
            # Compares if user type is 'Django'
            elif user_type == 'Django':
                # Send data to export function
                data = export(DjangoUserResource(), 'User - Django ', 'django', file_type)
                # Return file data
                return data
            else:
                return ''.join(['No User ', user_type.title(), ' found.'])
        else:
            return ''.join(['File type not supported'])

    @staticmethod
    def c_import(django_file, cleari_file):
        """
        Module for Importing User Accounts
        Usage:
        from apps.apps.clients.models import User
        User.import_export.c_import(user_type=('student','signatory','administrator'), file_type=('xls', 'xlsx', 'csv', 'json', 'yaml'), safe=(True, False))
        """
        # Import specific components and resources
        from tablib import Dataset
        from .resource import UserResource, DjangoUserResource
        # Get Django Model Resource
        res1 = DjangoUserResource()
        # Get Cleari Model Resource
        res2 = UserResource()
        # Create Empty Data set for two resources
        data_set1 = Dataset()
        data_set2 = Dataset()
        # Get File files from request and load to data sets
        data_set1.load(django_file.read())
        data_set2.load(cleari_file.read())
        # Dry run receive files if valid
        result1 = res1.import_data(data_set1, dry_run=True)
        result2 = res2.import_data(data_set2, dry_run=True)
        # Verify that two files has no errors
        if not result1.has_errors() and not result2.has_errors():
            # Import data into database
            res1.import_data(data_set1, dry_run=False)
            res2.import_data(data_set2, dry_run=False)
            return 'Success Importing data'
        else:
            return ''.join(['Failed to import. ', '[', str(data_set1), str(data_set2), ']'])


# User Manager for Managing User Accounts
class CleariUserManager(models.Manager):

    @staticmethod
    def activate(username):
        """
        Module for Activating User Accounts
        Usage:
        from apps.apps.clients.models import User
        User.objects.activate(username)
        """
        if username:
            try:
                # Get user info
                s = User.objects.get(username=username)
                # Set user as active
                s.is_active = True
                s.save()
                # Return string success message
                return 'Activated'
            except User.DoesNotExist:
                return 'User does not Exist'
        else:
            return 'Field is empty'

    @staticmethod
    def deactivate(username):
        """
        Module for deactivating User Accounts
        Usage:
        from apps.apps.clients.models import User
        User.objects.deactivate(username)
        """
        if username:
            try:
                # Get user info
                s = User.objects.get(username=username)
                # Set user as inactive
                s.is_active = False
                s.save()
                # Return string success message
                return 'Deactivated'
            except User.DoesNotExist:
                return 'User does not exist'
        else:
            return 'Field is empty'

    def search(self, question, exclude_user):
        """
        Module for searching user related information
        Usage:
        from apps.apps.clients.models import User
        User.objects.search(question, excluded_user(Current user should be excluded))
        """
        # Import specific query sets, forms, and framework shortcuts
        from django.db.models import Q
        # Verify that the question is not empty
        if not exclude_user:
            return 'Excluded User is Empty'
        if question:
            # Query question against Cleari User Model
            results = self.filter(Q(number__contains=question) | Q(first_name__contains=question) |
                                  Q(middle_name__contains=question) | Q(last_name__contains=question) |
                                  Q(id__email__contains=question)).all().exclude(number=exclude_user)
            r = []
            for i in results:
                r.append(tuple((i.pk, (dict([('number', i.number),
                                             ('full_name', i.full_name()),
                                             ('type', i.type)])))))
            # Create Context Dictionary
            context = dict([('has_results', True if results.__len__() > 0 else False),
                            ('result', dict(r)),
                            ('title', str(question).title())])

            return context
        else:
            return 'Question Empty'

    def edit(self, username, first_name, middle_name, last_name, birth_date, address):
        try:
            if User.objects.filter(username=username) and self.filter(number=username).exists():
                # Get Django User Model by username
                u = User.objects.filter(username=username)
                # Get Cleari User Model by username
                c = self.filter(number=username)
                # Save Django User Information
                u.update(first_name=first_name,
                         last_name=last_name)
                # Save Cleari User Information
                c.update(first_name=first_name,
                         middle_name=middle_name,
                         last_name=last_name,
                         address=address,
                         birth_date=birth_date)
                return 'Saved'
            else:
                return 'User not found'
        except:
            return 'Something went wrong'

    def change_password(self, username, old, new1, new2):
        if username and old and new1 and new2:
            if self.filter(number=username).exists():
                u = User.objects.get(username=username)
                if u.check_password(old):
                    if new1 == new2:
                        u.set_password(new1)
                        u.save()
                        return 'Saved'
                    else:
                        return 'New Password didn\'t matched'
                else:
                    return 'Old Password didn\'t matched'
            else:
                return 'User does not exist'
        else:
            return 'Some fields are empty'

    def set_email(self, username, email):
        if username and email:
            if self.filter(number=username).exists():
                u = User.objects.get(username=username)
                u.email = email
                u.save()
                return 'Saved'
            else:
                return 'User does not exist'
        else:
            return 'Some fields are empty'


class USignatoryManager(models.Manager):

    def set_signature(self, username, passkey):
        us = self.filter(number=username)
        if not us.first().signature and passkey:
            us.update(signature=sign.signature(passkey))
            return 'Signature Set, please keep your signature key [passkey] secure. \n ' \
                   'Once signature key is set there is no way to recover, please be guided'
        else:
            return 'Signature Key is already set. Once Signature key is set it can \'t ' \
                   'be be modified or changed'


class CUserType(models.Model):
    type = models.CharField(max_length=50, unique=True)

    def __str__(self):
        """
        :return: User username/id number
        """
        return str(self.type).upper()


class CUser(models.Model):
    id = models.OneToOneField(User, on_delete=models.CASCADE)
    profile = models.FileField(upload_to='user_profile_photo/', null=True)
    name_prefix = models.CharField(null=True, max_length=10)
    name_postfix = models.CharField(null=True, max_length=10)
    number = models.CharField(primary_key=True, max_length=50)
    first_name = models.CharField(max_length=50)
    middle_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    birth_date = models.DateField(null=True)
    address = models.TextField(null=True, max_length=1000)
    type = models.ForeignKey(CUserType, on_delete=models.DO_NOTHING)

    add = AddUserManager()
    import_export = IEUserManager()
    objects = CleariUserManager()

    def __str__(self):
        """
        :return: User username/id number
        """
        return self.number

    def full_name(self):
        """
        :return: Full name of the user
        """
        return ''.join([str(self.name_prefix).replace('.', '') if self.name_prefix else '',
                        '.' if self.name_prefix else '',
                        self.first_name,
                        ' ', self.middle_name,
                        ' ', self.last_name,
                        ' ', str(self.name_postfix).replace('.', '') if self.name_postfix else '',
                        '.' if self.name_postfix else '']).replace(',', ' ').title()

    def delete(self, using=None, keep_parents=False):
        """
        Module Delete Interceptor, it will intercept requested deleted users.
        The module will create a snapshot of a deleted user and upload it to system
        Deleted User Bin
        """
        # Import specific models, files and utilities
        from apps.contrib.recyclebin.resource import DeletedUserBin
        from django.core.files import File
        from django.utils import timezone
        import csv
        # Call Deleted User Bin() for creating snapshot
        d = DeletedUserBin()
        # Open new file and write information related to user requested
        with open('temp.csv', 'w', newline='') as csvfile:
            cw = csv.writer(csvfile, delimiter=' ',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
            typ = self.type
            # Compare if user is a Student
            if typ == 'student':
                # Code Block prepare to write queried information into CVS format
                cw.writerow(['User ID', 'Username', 'First Name', 'Last Name',
                             'Email', 'Password', 'Course', 'Year', 'College',
                             'Receipt List', 'Violation List'])
                cw.writerow([str(self.id), str(self.number), str(self.first_name),
                             str(self.last_name), str(self.id.email),
                             str(self.userstudent.course), str(self.userstudent.year),
                             str(self.userstudent.course.college), str('|'.join(self.receipt_set.all())),
                             str('|'.join(self.violation_set.all()))])
            # Compare if user is a Signatory
            elif typ == 'signatory':
                # Code Block prepare to write queried information into CVS format
                cw.writerow(['User ID', 'Username', 'First Name', 'Last Name',
                             'Email', 'Position', 'Office'])
                cw.writerow([str(self.id), str(self.number), str(self.first_name),
                             str(self.last_name), str(self.id.email),
                             str(self.usersignatory.position), str(self.usersignatory.cdt1)])
            # Compare if user is an Administrator or 'Non Cleari Client'
            elif typ == 'administrator' or typ == 'NCU':
                # Code Block prepare to write queried information into CVS format
                cw.writerow(['User ID', 'Username', 'First Name', 'Last Name',
                             'Email'])
                cw.writerow([str(self.id), str(self.number), str(self.first_name),
                             str(self.last_name), str(self.id.email)])
        # Read temp CVS file
        local_file = open('temp.csv')
        # Add Snapshot information into Delete User Bin
        d.timestamp = timezone.localtime()
        d.attachment.save(''.join(['USER', '_',
                                   str(self.number), '_',
                                   str(timezone.localtime()).partition(' ')[0]]),
                          File(local_file))
        # Save data into database
        d.save()
        # Confirm Deletion
        super().delete(using=None, keep_parents=False)


class UserStudent(models.Model):
    number = models.OneToOneField(CUser, on_delete=models.CASCADE)
    year = models.IntegerField()
    course = models.ForeignKey(Course, on_delete=models.DO_NOTHING)

    def __str__(self):
        return ''.join([self.number.full_name(), '-', ordinal(self.year), ' Year', '-', str(self.course)])


class UserSignatory(models.Model):
    number = models.OneToOneField(CUser, on_delete=models.CASCADE)
    assign = models.BooleanField(default=False)
    position = models.ForeignKey(CDT3, on_delete=models.DO_NOTHING)
    signature = models.CharField(max_length=500, null=True)
    cdt1 = models.ForeignKey(CDT1, on_delete=models.DO_NOTHING)
    objects = USignatoryManager()

    def __str__(self):
        return ''.join([self.number.full_name(), '-', str(self.position.position), '-', str(self.cdt1.abbreviation)])
