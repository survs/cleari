import sys

from django.conf.urls import url

from . import consumers as consumer_mod

sys.modules["django_eventstream.consumers"] = consumer_mod

urlpatterns = [
    url(r'^$', sys.modules["django_eventstream.consumers"].MessageConsumer),
]
