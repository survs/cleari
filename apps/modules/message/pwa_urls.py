from django.urls import path

from . import pwa_views

app_name = 'message'

urlpatterns = [
    path('view/provider/<str:view>', pwa_views.message_view_provider, name="message_view_provider"),
    path('d/inbox', pwa_views.data_inbox, name="data_inbox"),
    path('d/chat/<str:msg_id>', pwa_views.data_chat_box, name="data_chat"),
    path('d/chat/chunks/<str:t>/<str:r>/<str:msg_id>/<str:page>', pwa_views.data_chat_box_paginated,
         name='data_inbox_chunks'),
    path('d/g/latest/<str:msg_id>', pwa_views.data_get_latest, name="data_inbox"),
    path('d/g/recipient/<str:msg_id>', pwa_views.data_get_recipient, name="data_get_recipient"),
    path('r/new', pwa_views.data_new, name="data_new"),
    path('r/reply/<str:msg_id>', pwa_views.data_reply, name="data_reply"),
    path('r/delete/<str:msg_id>/<str:pk>', pwa_views.data_delete, name="data_inbox"),
    path('d/c/exist/<str:msg_id>', pwa_views.data_inbox, name="data_exist"),
    path('d/chat/head/<str:msg_id>', pwa_views.data_chat_profile, name="chat_profile"),
    path('d/files/<str:pk>/<str:msg_id>/<str:hash_code>', pwa_views.data_file_attachment,
         name="private_image_provider")
]
