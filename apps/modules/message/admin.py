from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import Message


@admin.register(Message)
class Message(ImportExportModelAdmin):
    pass
