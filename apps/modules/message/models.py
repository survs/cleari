
import json

import magic
from django.contrib.auth.models import User as DUser
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.signing import Signer
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django_eventstream import send_event

from apps.contrib.components import ph_resize, get_recipient, get_profile, get_name, to_human_date, full_name, \
    to_date, get_file_name, bytes_to_human, ftype, get_hash
from apps.modules.users.models import CUser

sign = Signer()


# TODO: Since messaging app is accepting file uploads, it became prone to Denial of Service(DDOS)
# TODO: To resolve this issue https://httpd.apache.org/docs/2.4/mod/core.html#limitrequestbody


class MessageManager(models.Manager):

    def inbox(self, user):
        """
        Filter all recent user conversation
        :param user: Django Username
        :return: List view of users recent messages
        """
        a = self.filter(sender__username=user).distinct('msg_id').exclude(sender_deleted__isnull=False)
        b = self.filter(recipient__username=user).distinct('msg_id').exclude(recipient_deleted__isnull=False)
        c = a.union(b)
        lst = []
        dl = []
        for i in c:
            for k in c:
                if i.msg_id == k.msg_id:
                    x = self.filter(msg_id=i.msg_id, sender__username=str(user), sender_deleted__isnull=True)
                    y = self.filter(msg_id=i.msg_id, recipient__username=str(user), recipient_deleted__isnull=True)
                    if x.union(y).exists():
                        z = x.union(y).latest('created')
                    else:
                        break
                    if not (str(lst).find(str(i.msg_id)) >= 1):
                        lst.append(json.dumps({'pk': str(z.pk),
                                               'msg_id': str(z.msg_id),
                                               'placeholder': dict([('name', get_recipient(user, z.msg_id)),
                                                                    ('profile', ph_resize(
                                                                        "media/" + str(get_profile(user, z.msg_id)), 40,
                                                                        40)),
                                                                    ('from',
                                                                     'Me' if str(z.sender) == str(user) else get_name(
                                                                         z.sender, 'name'))]),
                                               'sender': str(z.sender),
                                               'recipient': str(z.recipient),
                                               'created': to_human_date(str(z.created.astimezone())),
                                               'body': z.body,
                                               'attachment': z.has_attachment()}))
                    break
                continue
        print(lst)
        for k in list(set(lst)):
            dl.append(json.loads(k))
        return dl

    def chat_box(self, user, msg_id):
        if self.filter((Q(sender_id=DUser.objects.get(username=user)) |
                        Q(recipient_id=DUser.objects.get(username=user)) and
                        Q(msg_id=msg_id))).exists():
            a = self.filter(msg_id=msg_id, sender__username=user, sender_deleted__isnull=True)
            b = self.filter(msg_id=msg_id, recipient__username=user, recipient_deleted__isnull=True)
            c = a.union(b)
            e = c.order_by('created')
            d = c.order_by('-created').first()
            try:
                if str(d.msg_id) == str(msg_id) and str(self.get_recipient(user, msg_id)) == str(user):
                    m = self.get(pk=d.pk)
                    if not m.recipient_read:
                        m.recipient_read = timezone.localtime()
                        m.save()
                r = []
                d = []
                for a in e:
                    d.append(to_date(a.created))
                d = list(set(d))
                for i in e:
                    x = 0
                    if d.__contains__(to_date(i.created)):
                        d.remove(to_date(i.created))
                        x = 1
                    if i.attachment:
                        if str(i.attachment.name).split('.').__len__() > 0:
                            p = str(i.attachment.name).split('.')[str(i.attachment.name).split('.').__len__()-1]
                        else:
                            p = 'unk'
                        a = dict([('name', get_file_name(str(i.attachment.name))),
                                  ('human_size', bytes_to_human(i.attachment.size)),
                                  ('blob_url',
                                   ''.join([str(i.pk), '/', i.msg_id, '/', str(get_hash(i.attachment.file)), '.', p])),
                                  ('size', str(i.attachment.size)),
                                  ('ext_desc', ftype(p)),
                                  ('ext', p), ('content_type', str(magic.from_file(i.attachment.path, mime=True)))])
                    else:
                        a = ''
                    r.append(dict([('id', i.id), ('msg_id', i.msg_id),
                                   ('sender_id', i.sender_id), ('recipient_id', i.recipient_id),
                                   ('placeholder', dict([('name', get_recipient(user, i.msg_id)),
                                                         ('from',
                                                                     'me' if str(i.sender) == str(user) else 'them'),
                                                         ('user',
                                                          full_name(str(i.sender)) if str(i.sender) == str(
                                                              user) else full_name(str(i.sender))),
                                                         ])),
                                   ('is_first', x),
                                   ('created', timezone.localtime(i.created).isoformat()),
                                   ('created_human', to_human_date(timezone.localtime(i.created))), ('attachment', a),
                                   ('type', 'Cleari Messaging'),
                                   ('body', i.body), ('recipient_deleted', str(i.recipient_deleted)),
                                   ('recipient_read', str(i.recipient_read)),
                                   ('sender_deleted', str(i.sender_deleted)),
                                   ('sender_archive', str(i.sender_archive))]))
                return r
            except AttributeError as e:
                return str(e)
        else:
            return 'Invalid Request'

    def chat_box_paginated(self, user, msg_id, page, t):
        if self.filter((Q(sender_id=DUser.objects.get(username=user)) |
                        Q(recipient_id=DUser.objects.get(username=user)) and
                        Q(msg_id=msg_id))).exists():
            a = self.filter(msg_id=msg_id, sender__username=user, sender_deleted__isnull=True)
            b = self.filter(msg_id=msg_id, recipient__username=user, recipient_deleted__isnull=True)
            c = a.union(b)
            e = c.order_by('created')
            d = c.order_by('-created').first()
            paginator = Paginator(e, 10)
            try:
                e = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                e = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 7777), deliver last page of results.
                e = paginator.page(paginator.num_pages)
            if t == 'manifest':
                return dict([('num_pages', str(paginator.num_pages)),
                             ('count', str(paginator.count))])
            try:
                if str(d.msg_id) == str(msg_id) and str(self.get_recipient(user, msg_id)) == str(user):
                    m = self.get(pk=d.pk)
                    if not m.recipient_read:
                        m.recipient_read = timezone.localtime()
                        m.save()
                r = []
                d = []
                for a in e:
                    d.append(to_date(a.created))
                d = list(set(d))
                for i in e:
                    x = 0
                    if d.__contains__(to_date(i.created)):
                        d.remove(to_date(i.created))
                        x = 1
                    if i.attachment:
                        if str(i.attachment.name).split('.').__len__() > 0:
                            p = str(i.attachment.name).split('.')[str(i.attachment.name).split('.').__len__() - 1]
                        else:
                            p = 'unk'
                        a = dict([('name', get_file_name(str(i.attachment.name))),
                                  ('human_size', bytes_to_human(i.attachment.size)),
                                  ('blob_url',
                                   ''.join([str(i.pk), '/', i.msg_id, '/', str(get_hash(i.attachment.file)), '.', p])),
                                  ('size', str(i.attachment.size)),
                                  ('ext_desc', ftype(p)),
                                  ('ext', p), ('content_type', str(magic.from_file(i.attachment.path, mime=True)))])
                    else:
                        a = ''
                    r.append(dict([('id', i.id), ('msg_id', i.msg_id),
                                   ('sender_id', i.sender_id), ('recipient_id', i.recipient_id),
                                   ('placeholder', dict([('name', get_recipient(user, i.msg_id)),
                                                         ('profile', ''),
                                                         ('from',
                                                          'me' if str(i.sender) == str(user) else 'them'),
                                                         ('user',
                                                          full_name(str(i.sender)) if str(i.sender) == str(
                                                              user) else full_name(str(i.sender))),
                                                         ])),
                                   ('is_first', x),
                                   ('created', str(timezone.localtime(i.created).isoformat())),
                                   ('created_human', to_human_date(timezone.localtime(i.created))), ('attachment', a),
                                   ('type', 'Cleari Messaging'),
                                   ('body', i.body), ('recipient_deleted', str(i.recipient_deleted)),
                                   ('recipient_read', str(i.recipient_read)),
                                   ('sender_deleted', str(i.sender_deleted)),
                                   ('sender_archive', str(i.sender_archive))]))
                    print(r)
                return r
            except AttributeError as e:
                return str(e)
        else:
            return 'Invalid Request'

    def get_latest(self, user, msg_id):
        a = Message.objects.filter(msg_id=msg_id,
                                   sender_id=DUser.objects.get(username=user))
        b = Message.objects.filter(msg_id=msg_id,
                                   recipient_id=DUser.objects.get(username=user))
        c = a.union(b)
        if c.exists():
            g = self.filter(msg_id=msg_id)
            d = g.order_by('-created').first()
            return d
        else:
            return 'Invalid Request'

    def get_recipient(self, user, msg_id):
        a = self.filter(msg_id=msg_id,
                        sender_id=DUser.objects.get(username=user))
        b = self.filter(msg_id=msg_id,
                        recipient_id=DUser.objects.get(username=user))
        c = a.union(b)
        if c.exists():
            for i in c:
                if msg_id == str(sign.signature(''.join([str(i.sender),
                                                    '|', str(i.recipient)])).replace('(\'', '').replace('\',)', '')):
                    decoded = sign.unsign(''.join([''.join([str(i.sender), '|', str(i.recipient)]), ':', msg_id]))
                    if not str(decoded).__contains__(user):
                        return '404'
                    d = str(decoded).split('|')
                    for h in d:
                        print('q', h, user)
                        if h != user:
                            try:
                                print('t1', h)
                                return CUser.objects.get(number=h)
                            except CUser.DoesNotExist:
                                print('t2', h)
                                return DUser.objects.get(username=h)
                    break

    def new(self, user, recipients, body, attachment):
        pks = []
        err = []
        for i in str(recipients).split(','):
            if i != user and DUser.objects.filter(username=i).exists():
                a = self.filter(sender_id=DUser.objects.get(username=str(user)),
                                recipient_id=DUser.objects.get(username=i))
                b = self.filter(sender_id=DUser.objects.get(username=str(user)),
                                recipient_id=DUser.objects.get(username=i))
                c = a.union(b)
                if c.exists():
                    msg_id = c.first().msg_id
                    print('e: ', msg_id)
                else:
                    msg_id = sign.signature(''.join([user, '|', i])).replace('(\'', '').replace('\',)', '')
                    print('n: ', msg_id)
                self.create(msg_id=msg_id,
                            sender_id=DUser.objects.get(username=str(user)).pk,
                            recipient_id=DUser.objects.get(username=i).pk,
                            created=timezone.localtime(),
                            body=body,
                            attachment=attachment,
                            sender_archive=None,
                            sender_deleted=None,
                            recipient_archive=None,
                            recipient_deleted=None,
                            recipient_read=None)
                dataset = self.filter(msg_id=msg_id, sender_id=DUser.objects.get(username=user).pk).latest('created')
                if dataset.attachment:
                    if str(dataset.attachment.name).split('.').__len__() > 0:
                        p = str(dataset.attachment.name).split('.')[
                            str(dataset.attachment.name).split('.').__len__() - 1]
                    else:
                        p = 'unk'
                    a = dict([('name', get_file_name(str(dataset.attachment.name))),
                              ('human_size', bytes_to_human(dataset.attachment.size)),
                              ('blob_url',
                               ''.join(
                                   [str(dataset.pk), '/', dataset.msg_id, '/', str(get_hash(dataset.attachment.file)),
                                    '.', p])),
                              ('size', str(dataset.attachment.size)),
                              ('ext_desc', ftype(p)),
                              ('ext', p), ('content_type', str(magic.from_file(dataset.attachment.path, mime=True)))])
                else:
                    a = ''
                pks.append(''.join([str(self.filter(msg_id=msg_id).latest('created').pk), ':',
                                    str(self.filter(msg_id=msg_id).latest('created').msg_id)]))
                send_event('chat_list_' + str(DUser.objects.get(username=i).pk), 'message',
                           self.single_message(dataset, True if str(dataset.sender) != str(user) else False, a, 1, user,
                                               dataset.recipient))
            else:
                err.append(i)
        return dict([('success', pks), ('error', err)])

    def reply(self, user, msg_id, body, attachment):
        a = self.filter(msg_id=msg_id, sender__username=user, sender_deleted__isnull=True)
        b = self.filter(msg_id=msg_id, recipient__username=user, recipient_deleted__isnull=True)
        c = a.union(b)
        e = c.order_by('created')
        d = []
        for a in e:
            d.append(to_date(a.created))
        d = list(set(d))
        recipient = self.get_recipient(user, msg_id)
        print(recipient)
        self.create(msg_id=msg_id,
                    sender_id=DUser.objects.get(username=user).pk,
                    recipient_id=DUser.objects.get(username=recipient).pk,
                    created=timezone.localtime(),
                    body=body,
                    attachment=attachment,
                    sender_archive=None,
                    sender_deleted=None,
                    recipient_archive=None,
                    recipient_deleted=None,
                    recipient_read=None)
        i = self.filter(msg_id=msg_id, sender_id=DUser.objects.get(username=user).pk).latest('created')
        x = 0
        for n in e:
            if d.__contains__(to_date(n.created)) and n.pk == i.pk:
                d.remove(to_date(n.created))
                x = 1
        if i.attachment:
            if str(i.attachment.name).split('.').__len__() > 0:
                p = str(i.attachment.name).split('.')[str(i.attachment.name).split('.').__len__() - 1]
            else:
                p = 'unk'
            a = dict([('name', get_file_name(str(i.attachment.name))),
                      ('human_size', bytes_to_human(i.attachment.size)),
                      ('blob_url', ''.join([str(i.pk), '/', i.msg_id, '/', str(get_hash(i.attachment.file)), '.', p])),
                      ('size', str(i.attachment.size)),
                      ('ext_desc', ftype(p)),
                      ('ext', p), ('content_type', str(magic.from_file(i.attachment.path, mime=True)))])
        else:
            a = ''
        send_event('chat_list_' + str(DUser.objects.get(username=recipient).pk), 'message',
                   self.single_message(i, True if str(i.sender) != str(user) else False, a, x, user, recipient))
        return self.single_message(i, True if str(i.sender) == str(user) else False, a, x, user, recipient)

    @staticmethod
    def single_message(i, t, a, x, user, recipient):
        initial = dict([('id', i.pk), ('msg_id', i.msg_id),
                        ('sender_id', i.sender_id), ('recipient_id', i.recipient_id),
                        ('placeholder', dict([('name', get_recipient(user, i.msg_id)),
                                              ('from', 'me' if t else 'them'),
                                              ('user',
                                               full_name(str(i.sender)) if t else full_name(
                                                   str(i.sender))),
                                              ])),
                        ('is_first', x),
                        ('created', str(timezone.localtime(i.created).isoformat())),
                        ('created_human', to_human_date(timezone.localtime(i.created))), ('attachment', a),
                        ('type', 'Cleari Messaging'),
                        ('body', i.body), ('recipient_deleted', str(i.recipient_deleted)),
                        ('recipient_read', str(i.recipient_read)),
                        ('sender_deleted', str(i.sender_deleted)), ('sender_archive', str(i.sender_archive))])
        return initial

    def instant_delete(self, user, msg_id, pk):
        if self.filter(Q(msg_id=msg_id) |
                       Q(sender_id=DUser.objects.get(username=user)) |
                       Q(recipient_id=DUser.objects.get(username=user))).exists():
            g = self.get(msg_id=msg_id, pk=pk)
            typ = self.classify(user, pk)
            try:
                if typ == 'recipient':
                    g.recipient_deleted = timezone.localtime()
                    g.save()
                    return dict([('code', '200')])
                elif typ == 'sender':
                    g.sender_deleted = timezone.localtime()
                    g.save()
                    return dict([('code', '200')])
            except:
                return dict([('code', '500')])
        else:
            return dict([('code', '404')])

    def delete(self, user, msg_id, pk):
        if self.filter(Q(msg_id=msg_id) |
                       Q(sender_id=DUser.objects.get(username=user)) |
                       Q(recipient_id=DUser.objects.get(username=user))).exists():
            g = self.get(msg_id=msg_id, pk=pk)
            typ = self.classify(user, pk)
            try:
                if typ == 'recipient':
                    g.recipient_deleted = timezone.localtime()
                    g.save()
                    a = self.filter(msg_id=msg_id, sender__username=user, sender_deleted__isnull=True)
                    b = self.filter(msg_id=msg_id, recipient__username=user, recipient_deleted__isnull=True)
                    d = a.union(b).latest('created')
                    return d
                elif typ == 'sender':
                    g.sender_deleted = timezone.localtime()
                    g.save()
                    a = self.filter(msg_id=msg_id, sender__username=user, sender_deleted__isnull=True)
                    b = self.filter(msg_id=msg_id, recipient__username=user, recipient_deleted__isnull=True)
                    d = a.union(b).latest('created')
                    return d
            except:
                return None
        else:
            return None

    def classify(self, user, pk):
        q = self.filter((Q(sender_id=DUser.objects.get(username=user)) |
                         Q(recipient_id=DUser.objects.get(username=user)) and
                         Q(pk=pk)))
        if q.exists():
            a = self.get((Q(sender_id=DUser.objects.get(username=user)) |
                          Q(recipient_id=DUser.objects.get(username=user)) and
                          Q(pk=pk)))
            if str(a.recipient) == str(user):
                return 'recipient'
            elif str(a.sender) == str(user):
                return 'sender'
        else:
            return None

    def chat_exist(self, user, msg_id):
        a = self.filter(msg_id=msg_id, sender__username=user, sender_deleted__isnull=True)
        b = self.filter(msg_id=msg_id, recipient__username=user, recipient_deleted__isnull=True)
        d = a.union(b)
        return d.exists()


class IECourseManager(models.Manager):

    @staticmethod
    def c_export(file_type):
        """
        Usage:
        from apps.apps.course.models import Course
        Course.ie.c_export(file_type=('xls', 'xlsx', 'csv', 'json', 'yaml'))
        """
        # Import specific components and resource
        from apps.contrib.components import supported_types, export
        from .resource import MessageResource
        # Verify supplied file type is supported
        if supported_types.__contains__(file_type):
            # Return raw application data
            data = export(MessageResource(), 'Message', 'all', file_type)
            return data
        else:
            # Show error message
            return 'File Type not Supported'

    @staticmethod
    def c_import(file):
        """
        Usage:
        from apps.apps.course.models import Course
        Course.ie.c_import(file)
        """
        # Import specific resource and data set
        from .resource import MessageResource
        from tablib import Dataset
        # Call Course Resource
        res = MessageResource()
        # Create Empty Data set
        ds = Dataset()
        # Get File file from request and load to data sets
        ds.load(file.read())
        # Dry run receive file if valid
        result = res.import_data(ds, dry_run=True)
        # Verify that file has no errors
        if not result.has_errors():
            # Import data into database
            res.import_data(ds, dry_run=False)
            # Show Success Message
            return 'Success Importing data'
        else:
            # Show Error message
            return ''.join(['Failed to import. ', '[', str(ds), ']'])


class Message(models.Model):
    msg_id = models.CharField(max_length=50, db_column='msg_id', default='')
    sender = models.ForeignKey(DUser, on_delete=models.CASCADE, related_name='sent_messages',
                               null=True, blank=True, verbose_name="sender")
    recipient = models.ForeignKey(DUser, on_delete=models.CASCADE, related_name='received_messages',
                                  null=True, blank=True, verbose_name="recipient")
    created = models.DateTimeField()
    attachment = models.FileField(upload_to='messages/attachment/', default=None, )
    body = models.TextField()
    recipient_deleted = models.DateTimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
    recipient_archive = models.DateTimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
    recipient_read = models.DateTimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
    sender_deleted = models.DateTimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
    sender_archive = models.DateTimeField(auto_now=False, auto_now_add=False, null=True, blank=True)

    objects = MessageManager()

    def has_attachment(self):
        return None if not self.attachment else True

    def has_seen(self):
        return True if self.recipient_read else False

    def seen_at(self):
        return self.recipient_read

    def is_latest(self):
        s = Message.objects.get_latest(self.recipient, self.msg_id)
        if s.msg_id == self.msg_id:
            return True
        else:
            return False

    def has_sender_archive(self):
        return True if self.sender_archive else False

    def has_sender_deleted(self):
        return True if self.sender_deleted else False

    def has_recipient_archive(self):
        return True if self.recipient_archive else False

    def has_recipient_deleted(self):
        return True if self.recipient_deleted else False

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)  # Call the "real" save() method.
