from import_export import resources

from .models import Message


class MessageResource(resources.ModelResource):
    class Meta:
        model = Message
