from django.urls import path
from django.views.decorators.http import require_POST

from . import pwa_views

app_name = 'message'

urlpatterns = [
    path('inbox/', pwa_views.Inbox.as_view(), name='inbox'),
    path('new/', pwa_views.NewMessage.as_view(), name='new_message'),
    path('m/<str:msg_id>', pwa_views.Chat.as_view(), name='chat'),
    path('deleted/<str:msg_id>/<str:pk>', pwa_views.DeleteMessage.as_view(), name='delete'),
    path('reply/<str:msg_id>', require_POST(pwa_views.Reply.as_view()), name='reply'),
]
