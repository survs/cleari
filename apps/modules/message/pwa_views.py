import json

import magic
from django.http import JsonResponse, StreamingHttpResponse, HttpResponse
from django.shortcuts import render
from django_eventstream import consumers
from django_eventstream.channelmanager import DefaultChannelManager

from apps.contrib.components import ph_resize, get_hash, get_recipient
from apps.modules.users.models import CUser
from .models import Message


class MyChannelManager(DefaultChannelManager):
    def can_read_channel(self, user, channel):
        if channel.startswith('_'):
            return False
        return True

    def is_channel_reliable(self, channel):
        if channel.startswith('~'):
            return False
        return True


def message_listener(request):
    return consumers.EventsConsumer(scope=request)


def message_view_provider(request, view):
    if not request.user.is_authenticated:
        return JsonResponse(data='User not authenticated', status=203, safe=False)
    if view == 'inbox':
        return render(request, 'pwa/message/inbox.html')
    elif view == 'new':
        return render(request, 'pwa/message/new.html')
    elif view == 'chat':
        return render(request, 'pwa/message/chat.html')
    elif view == 'reply':
        return render(request, 'pwa/message/reply.html')
    elif view == 'delete':
        return render(request, 'pwa/message/delete.html')


def data_inbox(request):
    if not request.user.is_authenticated:
        return JsonResponse(data='User not authenticated', status=203, safe=False)
    m = Message.objects.inbox(request.user.username)
    return JsonResponse(data=m, safe=False)


def data_chat_box(request, msg_id):
    if not request.user.is_authenticated:
        return JsonResponse(data='User not authenticated', status=203, safe=False)
    m = Message.objects.chat_box(request.user.username, msg_id)
    return JsonResponse(data=m, safe=False)


def data_chat_profile(request, msg_id):
    if not request.user.is_authenticated:
        return JsonResponse(data='User not authenticated', status=203, safe=False)
    m = Message.objects.get_recipient(request.user.username, msg_id)
    if m == '404':
        return JsonResponse(data="Request not valid", safe=False)
    user = CUser.objects.get(number=str(m))
    c = dict([('me_40', ph_resize("media/" + str(request.user.cuser.profile), 40, 40)),
              ('them_40', ph_resize("media/" + str(user.profile), 40, 40)),
              ('me_100', ph_resize("media/" + str(request.user.cuser.profile), 100, 100)),
              ('them_100', ph_resize("media/" + str(user.profile), 100, 100)),
              ('r_name', get_recipient(request.user.username, msg_id))])
    return JsonResponse(data=json.dumps(c), safe=False)


def data_chat_box_paginated(request, msg_id, page, t, r):
    if not request.user.is_authenticated:
        return JsonResponse(data='User not authenticated', status=203, safe=False)
    if t == 'c':
        m = Message.objects.chat_box_paginated(request.user.username, msg_id, page, 'none')
        return StreamingHttpResponse(streaming_content=message_chunks_manifest(m, r))
    elif t == 'l':
        m = Message.objects.chat_box_paginated(request.user.username, msg_id, page, 'manifest')
        return JsonResponse(m, status=200, safe=False)
    else:
        return JsonResponse('Invalid Request', status=203, safe=False)


def data_file_attachment(request, pk, msg_id, hash_code):
    if not request.user.is_authenticated:
        return JsonResponse(data='User not authenticated', status=203, safe=False)
    m = Message.objects.filter(pk=pk, msg_id=msg_id)
    path = ''
    if m:
        t = m.first()
        m = ''.join([str(t.sender_id), ';', str(t.recipient_id)])
        if m.__contains__(str(request.user.pk)) and get_hash(t.attachment.file) == str(hash_code).split('.')[0]:
            path = t.attachment.path
        else:
            return HttpResponse(content='Request not Found', status=404)
    image_data = open(path, "rb").read()
    return HttpResponse(image_data, content_type=magic.from_file(path, mime=True))


def message_chunks_manifest(dataset, r):
    if r == str('1'):
        dataset.reverse()
    for i in dataset:
        yield json.dumps(i)
    yield json.dumps(dict([('r', 'eof')]))


def data_get_latest(request, msg_id):
    if not request.user.is_authenticated:
        return JsonResponse(data='User not authenticated', status=203, safe=False)
    m = Message.objects.get_latest(request.user.username, msg_id)
    return JsonResponse(data=m, safe=False)


def data_get_recipient(request, msg_id):
    if not request.user.is_authenticated:
        return JsonResponse(data='User not authenticated', status=203, safe=False)
    m = Message.objects.get_recipient(request.user.username, msg_id)
    return JsonResponse(data=m, safe=False)


def data_new(request):
    if not request.user.is_authenticated:
        return JsonResponse(data='User not authenticated', status=203, safe=False)
    if request.method == "POST":
        r = request.POST['recipients']
        b = request.POST['body']
        a = request.FILES['attachment'] if str(request.POST['has_attachment']) == '1' else None
        m = Message.objects.new(request.user.username, r, b, a)
        return JsonResponse(data=m, safe=False)
    else:
        return JsonResponse(data='Request Unknown', status=203, safe=False)


def data_reply(request, msg_id):
    if not request.user.is_authenticated:
        return JsonResponse(data='User not authenticated', status=203, safe=False)
    if request.method == "POST":
        b = request.POST['body']
        a = request.FILES['attachment'] if str(request.POST['has_attachment']) == '1' else None
        m = Message.objects.reply(request.user.username, msg_id, b, a)
        return JsonResponse(data=m, safe=False)
    else:
        return JsonResponse(data='Request Unknown', status=203, safe=False)


def data_delete(request, msg_id, pk):
    if not request.user.is_authenticated:
        return JsonResponse(data='User not authenticated', status=203, safe=False)
    m = Message.objects.instant_delete(request.user.username, msg_id, pk)
    return JsonResponse(data=json.dumps(m), safe=False, status=200)


def data_chat_exist(request, msg_id):
    if not request.user.is_authenticated:
        return JsonResponse(data='User not authenticated', status=203, safe=False)
    m = Message.objects.chat_exist(request.user.username, msg_id)
    return JsonResponse(data=m, safe=False)
