from django.contrib import messages
from django.shortcuts import reverse
from django.urls import reverse_lazy
from django.views.generic.edit import FormView
from django.views.generic.list import ListView

from .forms import NewMessageForm, ReplyMessageForm, DeleteMessageForm
from .models import Message


class Inbox(ListView):
    model = Message
    paginate_by = 100  # if pagination is desired

    def get_context_data(self, **kwargs):
        msgs = getattr(Message.objects, 'inbox')(self.request.user)
        context = super(Inbox, self).get_context_data(**kwargs)
        context.update({
            'msg': msgs,
            'app': {
                'title': 'Messages',
                'type': 'Inbox'
            }
        })
        return context


class NewMessage(FormView):
    form_class = NewMessageForm
    template_name = 'message/new.html'

    def get_success_url(self):
        return reverse('message:inbox')

    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS, 'Message Sent')
        form.submit(self.request.user.username)
        return super().form_valid(form)


class Chat(ListView):
    model = Message
    paginate_by = 100  # if pagination is desired
    template_name = 'message/message_detail.html'

    def get_context_data(self, **kwargs):
        msg_id = self.kwargs.get('msg_id', None)
        msgs = getattr(Message.objects, 'chat_box')(self.request.user, msg_id)
        context = super(Chat, self).get_context_data(**kwargs)
        recipient = getattr(Message.objects, 'get_recipient')(self.request.user.username, msg_id)
        context['form'] = ReplyMessageForm(initial={'recipient': str(recipient),
                                                    'msg_id': msg_id})
        context.update({
            'msg': msgs,
            'chat_id': msg_id,
            'recipient': recipient,
            'app': {
                'title': 'Messages',
                'type': 'Chat'
            }
        })
        return context


class Reply(FormView):
    form_class = ReplyMessageForm
    template_name = 'message/message_detail.html'

    def get_success_url(self):
        msg_id = self.kwargs.get('msg_id', None)
        return reverse_lazy('message:chat', kwargs={'msg_id': msg_id})

    def form_valid(self, form):
        msg_id = self.kwargs.get('msg_id', None)
        messages.add_message(self.request, messages.SUCCESS, 'Message Sent')
        form.submit(self.request.user.username, msg_id=msg_id)
        return super().form_valid(form)


class DeleteMessage(FormView):
    form_class = DeleteMessageForm
    template_name = 'message/message_confirm_delete.html'

    def get_success_url(self):
        msg_id = self.kwargs.get('msg_id', None)
        if getattr(Message.objects, 'chat_exist')(self.request.user, msg_id):
            return reverse_lazy('message:chat', kwargs={'msg_id': msg_id})
        else:
            return reverse('message:inbox')

    def form_valid(self, form):
        msg_id = self.kwargs.get('msg_id', None)
        pk = self.kwargs.get('pk', None)
        messages.add_message(self.request, messages.SUCCESS, 'Message Deleted')
        form.submit(self.request.user.username, msg_id=msg_id, pk=pk)
        return super().form_valid(form)
