from django import forms

from .models import Message


class NewMessageForm(forms.Form):
    recipient = forms.CharField(max_length=100)
    body = forms.CharField(widget=forms.Textarea)
    attachment = forms.FileField(required=False)

    def submit(self, user):
        Message.objects.new(user,
                            self.cleaned_data['recipient'],
                            self.cleaned_data['body'],
                            self.cleaned_data['attachment'])
        pass


class ReplyMessageForm(forms.Form):
    msg_id = forms.CharField(widget=forms.HiddenInput, required=False)
    recipient = forms.CharField(widget=forms.HiddenInput, required=False)
    attachment = forms.FileField(required=False)
    body = forms.CharField(widget=forms.Textarea)

    def submit(self, user, msg_id):
        Message.objects.reply(user,
                              msg_id,
                              self.cleaned_data['recipient'],
                              self.cleaned_data['body'],
                              self.cleaned_data['attachment'])
        pass


class DeleteMessageForm(forms.Form):

    @staticmethod
    def submit(user, msg_id, pk):
        if Message.objects.chat_exist(user, msg_id):
            Message.objects.delete(user, msg_id, pk)
            pass
        else:
            pass
