from django.urls import path
from django.views.decorators.http import require_POST

from . import pwa_views

app_name = 'receipt'

urlpatterns = [
    path('issue/', require_POST(pwa_views.issue_receipt), name='issue_receipt'),
    path('issue/filter/<str:course>', pwa_views.issue_filter, name='issue_filter'),
    path('import', require_POST(pwa_views.data_import), name='import_data'),
    path('export/<str:typ>', pwa_views.data_export, name='export_data'),
    path('export/clearance/<str:sys_id>/<str:number>/<str:typ>', pwa_views.data_export_clearance,
         name='export_clearance'),
    path('export/clearance/<str:sys_id>/<str:receipt>/<str:typ>', pwa_views.data_export_receipt, name='export_receipt'),
    path('search', pwa_views.search, name='search'),
]
