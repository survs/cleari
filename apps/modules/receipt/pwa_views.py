from django.contrib import messages
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import redirect
from tablib import Dataset

from apps.contrib.components import export, supported_types, filter_export
from apps.contrib.ie.forms import DataImportForm
from apps.modules.clearance.models import ClearanceLiability
from apps.modules.course.models import Course
from apps.modules.receipt.models import Receipt
from apps.modules.semester.models import Semester
from apps.modules.users.models import CUser, UserSignatory
from .resource import ReceiptResource


def issue_receipt(request):
    # Verify if user is a signatory
    if 'signatory' != str(CUser.objects.get(number=request.user.user.number).user_type).lower():
        # Show error message and redirect to receipt manage view
        return JsonResponse(status=403, data='Non Signatory Users are not Allowed to issue or sign clearance',
                            safe=False)
    # Get Signatory User Information by ID Number
    signatory_get = UserSignatory.objects.get(number_id=request.user.cuser.pk)
    if request.POST.get("all", None):
        # Get 'all' data from POST request to liability
        liability = str(request.POST.get("all", None))
        # Get liability Information
        l1 = ClearanceLiability.objects.get(liability=liability)
        # Get Course Information
        c = Course.objects.get(course=l1.course)
        # Get Student by Course and College
        u = CUser.objects.filter(userstudent__course_id=c.pk,
                                 userstudent__course__college_id=c.pk).all()
        # Initialize two list variable for message API
        s = []
        e = []
        # Loops Liability Data
        for n in u:
            # Concatenates receipt ID into system recognisable format
            receipt_id = ''.join([liability, '-',
                                  n.number, '-',
                                  str(signatory_get.cdt1.abbreviation)])
            # Verify if the receipt is not already exist
            if not Receipt.objects.filter(receipt=receipt_id).exists():
                # Prepare required data before saving
                lby = ClearanceLiability.objects.get(liability=liability)
                r = Receipt(receipt=receipt_id,
                            number_id=CUser.objects.get(number=n.number),
                            is_requested=False,
                            liability_id=lby.pk)
                # Save data to database(Postgres)
                r.save()
                # Adds Student ID into successfully issued
                s.append(n.number)
            else:
                # Adds Student ID into already existed issued
                e.append(n.number)
        if s:
            # Expand Successfully Issued Students into Message API
            return JsonResponse(status=200, data=''.join(['Receipt Created for ', ','.join(s)]), safe=False)
        if e:
            # Expand Unsuccessful Issued Students into Message API
            return JsonResponse(status=500, data=''.join(['Receipt Existed from ', ','.join(s)]), safe=False)
    # Verify if 'selected' is not Null
    elif request.POST.get("selected", None):
        # Get selected student Information
        number = str(request.POST.get("selected", None)).partition('-')[0]
        # Get selected liability
        liability = str(request.POST.get("selected", None)).partition('-')[2]
        # Concatenates receipt ID into system recognisable format
        receipt_id = ''.join([liability, '-',
                              number, '-',
                              str(signatory_get.office_id)])
        # Verify if the receipt is not already exist
        if not Receipt.objects.filter(receipt=receipt_id).exists():
            # Prepare required data before saving
            lby = ClearanceLiability.objects.get(liability=liability)
            r = Receipt(receipt=receipt_id,
                        number_id=CUser.objects.get(number=number),
                        is_requested=False,
                        received_date=None,
                        liability_id=lby.pk)
            # Save data to database(Postgres)
            r.save()
            # Redirect to Issue receipt form with successful messages
            return JsonResponse(status=200, data='Receipt Created', safe=False)
        else:
            # Redirect to Issue receipt form with unsuccessful messages
            return JsonResponse(status=409, data='Receipt Existed', safe=False)


def issue_filter(request, course):
    """
    Description: Filter Student by course
    :param request:
    Contains HTTP request META including User Signatory Information
    :param course:
    Raw String Course ID
    :return:
        1: Render Filter result by course
    """
    # Verify if user is a signatory
    if 'signatory' != str(CUser.objects.get(number=request.user.user.number).user_type).lower():
        # Show error message and redirect to receipt manage view
        messages.add_message(request, messages.ERROR, 'Non Signatory Users are not Allowed to issue or sign clearance')
        return redirect('receipt:manage')
    # Verify if request method is 'GET'
    if request.method == 'GET':
        # Get Signatory User Information
        signatory_get = UserSignatory.objects.get(number_id=request.user.cuser.pk)
        # Get active Semester
        semester = Semester.objects.get(active=True)
        # Get Course Lists
        course_list = Course.objects.filter(college=signatory_get.cdt1.college_id).all()
        c = []
        for i in course_list:
            c.append(tuple((i.pk, (
                dict([('description', i.description),
                      ('department', i.department_id), ('college', i.college_id)])))))

        # Get Liabilities by semester and course
        liabilities = ClearanceLiability.objects.filter(liability__istartswith=semester.semester,
                                                        liability__iendswith=course)
        l = []
        for j in liabilities:
            l.append(tuple((j.pk, (dict([('liability', j.liability), ('description', j.description),
                                         ('signatory', j.signatory_id), ('course', j.course_id),
                                         ('sys', j.sys_id)])))))
        # Query Block: Filters Student with the same college as signatory office which has no liabilities
        stu1 = CUser.objects.filter(userstudent__course__college_id=signatory_get.cdt1.college_id,
                                    userstudent__course_id=course).exclude(
            receipt__receipt__contains=signatory_get.cdt1.abbreviation
        )
        s = []
        for k in stu1:
            s.append(tuple((k.pk, (dict([('full_name', k.full_name()), ('id', k.number)])))))

        context = dict([('signatory', dict([('id', str(request.user.username)),
                                            ('college', signatory_get.cdt1.college),
                                            ('office', signatory_get.cdt1.abbreviation),
                                            ('liabilities', dict(l))])),
                        ('school', dict([('semester', dict([('text', str(semester))])),
                                         ('course_list', dict(c))])),
                        ('clients', dict([('fresh', dict(s)), ('bool', True),
                                          ('query', course)]))])
        return JsonResponse(status=200, data=context, safe=False)


def data_export(request, typ):
    # Verify file type is supported
    if supported_types.__contains__(typ):
        # Return raw application data
        data = export(ReceiptResource(), 'Receipt', 'all', typ)
        return data
    else:
        # Show error message and redirect to export view
        return JsonResponse(status=400, data='File Type not Supported', safe=False)


def data_export_clearance(request, sys_id, number, typ):
    if not CUser.objects.filter(number=number).exists():
        return JsonResponse(status=404, data='User not Found', safe=False)
    if not Semester.objects.filter(sys__exact=sys_id).exists():
        return JsonResponse(status=404, data='Semester not Found', safe=False)
    # Verify file type is supported
    if supported_types.__contains__(typ):
        # Fetch Semester School Year
        sys = sys_id if str(sys_id).isnumeric() else Semester.objects.get(active=True).pk
        # Get user type
        user = CUser.objects.get(number=number)
        # Return raw application data
        data = filter_export(ReceiptResource(),
                             Receipt.objects.filter(number=number, sys=sys),
                             'User',
                             user.user_type,
                             typ)

        return data
    else:
        return JsonResponse(status=400, data='File Type not Supported', safe=False)


def data_export_receipt(request, sys_id, receipt, typ):
    # Try to parse requested data type is valid
    # Check if Receipt ID requested is existing
    if not Receipt.objects.filter(receipt=receipt).exists():
        # Show error message and redirect export receipt view
        return JsonResponse(status=404, data='Receipt not Found', safe=False)
    # Check if Receipt ID requested is existing
    if not Semester.objects.filter(sys__exact=sys_id).exists():
        # Show error message and redirect export receipt view
        return JsonResponse(status=404, data='Semester not Found', safe=False)
    # Verify file type is supported
    if supported_types.__contains__(typ):
        # Fetch Semester School Year
        sys = sys_id if str(sys_id).isnumeric() else Semester.objects.get(active=True).pk
        # Get user information by receipt
        r = Receipt.objects.get(receipt=receipt)
        # Return raw application data
        data = filter_export(ReceiptResource(),
                             Receipt.objects.filter(receipt=receipt, sys=sys),
                             'User',
                             r.number.user_type,
                             typ)

        return data
    else:
        # Show error message and redirect to export view
        return JsonResponse(status=412, data='File Type not Supported', safe=False)


def data_import(request):
    # Call Data Import Form with request data and request files
    di = DataImportForm(request.POST, request.FILES)
    # Verify form is valid
    if di.is_valid():
        # Call College Resource
        res = ReceiptResource()
        # Create Empty Data set
        ds = Dataset()
        # Get File file from request and load to data sets
        ds.load(request.FILES['file'].read())
        # Dry run receive file if valid
        result = res.import_data(ds, dry_run=True)
        # Verify that file has no errors
        if not result.has_errors():
            # Import data into database
            res.import_data(ds, dry_run=False)
            # Show Success Message and redirect to import user view
            return JsonResponse(status=200, data='Success Importing data', safe=False)
        else:
            # Show Error message and redirect to import user view
            return JsonResponse(status=500, data=''.join(['Failed to import. ',
                                                          '[', str(ds), ']']), safe=False)
    else:
        # Show Error message and redirect to import user view
        return JsonResponse(status=400, data=str(di.errors), safe=False)


def search(request):
    """
    Description: Allow Admin to search liability
    :param request:
    Contains Authenticated Information and request META
    :return:
        1: Render result to result template
        2: Redirect to Liability Manage View
    """
    question = request.GET['q']
    # Verify that the question is not empty
    if question:
        sys = question if str(question).isnumeric() else 0
        # Query question against Cleari Receipt Model
        results = Receipt.objects.filter(Q(receipt__icontains=question) |
                                         Q(number__number__icontains=question) |
                                         Q(liability__liability__contains=question) |
                                         Q(liability__course__course__contains=question) |
                                         Q(liability__course__college=question) |
                                         Q(number__first_name__icontains=question) |
                                         Q(number__middle_name__icontains=question) |
                                         Q(number__last_name__icontains=question) |
                                         Q(sys=sys))
        r = []
        for i in results:
            r.append(tuple((i.pk, (dict([('receipt', i.receipt), ('number', i.number),
                                         ('remarks', i.remarks), ('cleared', i.is_cleared),
                                         ('requested', i.is_requested), ('req_date', i.requested_date),
                                         ('rec_by', i.receive_by), ('rec_date', i.receive_date),
                                         ('liability', i.liability), ('signature', i.signature)])))))
        # Create Context Dictionary
        context = dict([('has_results', True if results.__len__() > 0 else False),
                        ('results', dict(r)),
                        ('title', str(question).title())])
        # Render results into Result template with context
        return JsonResponse(status=200, data=context, safe=False)
    else:
        # Redirect to liability manage
        return JsonResponse(status=400, data='Query is empty', safe=False)
