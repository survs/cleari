import json

from django.core import signing
from django.db import models
from django.utils import timezone

from apps.modules.clearance.models import ClearanceDependency, ClearanceUserStorage
from apps.modules.users.models import CUser, UserSignatory, UserStudent


class ReceiptManager(models.Manager):

    def sign(self, user, receipt, remarks):
        try:
            UserSignatory.objects.get(number=user)
        except UserSignatory.DoesNotExist:
            return 'Non Signatories cannot sign receipt'
        if not receipt and not remarks:
            return 'Unable to sign, Supplied receipt and remarks value are/is empty.'
        lib = UserSignatory.objects.get(number=user)
        signer = signing.Signer(salt=lib.signature)
        r1 = self.get(receipt=receipt)
        r1.is_cleared = True
        r1.remarks = remarks
        r1.received_date = timezone.localtime()
        r1.received_by = lib.number.full_name()
        r1.save()
        r = self.get(receipt=receipt)
        structure = dict([('receipt', str(r.receipt)),
                          ('number', str(r.number)),
                          ('remarks', str(r.remarks)),
                          ('is_cleared', str(r.is_cleared)),
                          ('requested_date', str(r.requested_date)),
                          ('is_requested', str(r.is_requested)),
                          ('received_by', str(r.received_by)),
                          ('receive_date', str(r.received_date)),
                          ('liability', str(r.liability))])
        dump = json.dumps(structure)
        r.signature = ''.join([str(signer.signature(dump)), '@',
                               str(user)])
        r.save()
        return 'Receipt Signed'

    def verify(self, receipt):
        if self.filter(receipt=receipt).exists():
            r = self.get(receipt=receipt)
            if r.signature:
                temp = str(r.signature).split('@')
                sig_tory = UserSignatory.objects.get(number=temp[1])
                signer = signing.Signer(salt=str(sig_tory.signature))
                structure = dict([('receipt', str(r.receipt)),
                                  ('number', str(r.number)),
                                  ('remarks', str(r.remarks)),
                                  ('is_cleared', str(r.is_cleared)),
                                  ('requested_date', str(r.requested_date)),
                                  ('is_requested', str(r.is_requested)),
                                  ('received_by', str(r.received_by)),
                                  ('receive_date', str(r.received_date)),
                                  ('liability', str(r.liability))])
                raw = json.dumps(structure)
                return signer.signature(raw) == temp[0]
            else:
                return False
        else:
            return False

    def check_clearance(self, c, number):
        d = set([entry.is_cleared and Receipt.objects.verify(entry.receipt) for entry in
                 self.filter(liability__clearance_id=c, number=number)])
        if d.__len__() == 1:
            return d.pop()
        else:
            return False


class Receipt(models.Model):
    receipt = models.CharField(max_length=200, unique=True)
    number = models.ForeignKey(CUser, on_delete=models.DO_NOTHING)
    remarks = models.CharField(max_length=250, default='')
    is_cleared = models.BooleanField(default=False)
    is_requested = models.BooleanField(default=False)
    requested_date = models.DateTimeField(null=True)
    received_by = models.CharField(max_length=100)
    received_date = models.DateTimeField(null=True)
    liability = models.ForeignKey(ClearanceDependency, on_delete=models.DO_NOTHING)
    signatory = models.ForeignKey(UserSignatory, on_delete=models.DO_NOTHING)
    signature = models.TextField(default='')
    objects = ReceiptManager()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        d = set([entry.is_cleared and Receipt.objects.verify(entry.receipt) for entry in
                 Receipt.objects.filter(liability=self.liability, number=self.number)])
        if d.__len__() == 1:
            ClearanceUserStorage.objects.update_or_create(status=d.pop(),
                                                          number=self.number,
                                                          clearance_id=self.liability.clearance.pk)
        else:
            ClearanceUserStorage.objects.update_or_create(status=False,
                                                          number=self.number,
                                                          clearance_id=self.liability.clearance.pk)
        super().save(force_insert=False, force_update=False, using=None, update_fields=None)

