from django import forms
from django.forms import ModelForm

from apps.contrib.components import whitelist, supported_types_choices
from apps.modules.clearance.models import ClearanceLiability
from .models import Receipt


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = ('number', 'is_cleared',
                  'received_by', 'received_date',
                  'liability')

    def clean_received_by(self):
        return ''.join(filter(whitelist.__contains__, self.cleaned_data['received_by']))


class ExportClearanceForm(forms.ModelForm):
    number = forms.CharField(max_length=15, label='ID Number or Username', required=True)
    file_type = forms.ChoiceField(required=True, choices=tuple(supported_types_choices))
    liability = forms.CharField(required=False, disabled=True, widget=forms.HiddenInput)
    signatory = forms.CharField(required=False, disabled=True, widget=forms.HiddenInput)
    course = forms.CharField(required=False, disabled=True, widget=forms.HiddenInput)
    description = forms.CharField(required=False, disabled=True, widget=forms.HiddenInput)

    class Meta:
        model = ClearanceLiability
        fields = ('number', 'file_type', 'liability', 'description', 'sys')


class DataExportForm(forms.Form):
    file_type = forms.ChoiceField(required=True, choices=tuple(supported_types_choices))
