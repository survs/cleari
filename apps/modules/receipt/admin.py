from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import Receipt


@admin.register(Receipt)
class Receipt(ImportExportModelAdmin):
    pass
