from django import forms

from .models import Course


class CourseForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ('course', 'description', 'department', 'college')
        widgets = {
            'course': forms.TextInput(attrs={'ng-model': 'form.course'}),
            'description': forms.TextInput(attrs={'ng-model': 'form.description'}),
            'department': forms.Select(attrs={'ng-model': 'form.department'}),
            'college': forms.Select(attrs={'ng-model': 'form.college'}),
        }
