from django.urls import path
from django.views.decorators.http import require_POST

from . import pwa_views

app_name = 'course'

urlpatterns = [
    path('add/<str:course>', require_POST(pwa_views.add), name='add'),
    path('edit/<str:course>/<str:pk>', pwa_views.edit, name='edit'),
    path('delete/<str:course>/<str:pk>', pwa_views.delete, name='delete'),
    path('import', require_POST(pwa_views.data_import), name='import_data'),
    path('export/<str:typ>', pwa_views.data_export, name='import_form'),
]
