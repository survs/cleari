from django.http import JsonResponse

from apps.contrib.ie.forms import DataImportForm
from apps.modules.users.models import CUser
from .forms import CourseForm
from .models import Course


def add(request, course):
    if not request.user.is_authenticated:
        return JsonResponse(status=403, data='Not allowed to perform')
    if CUser.objects.filter(id__username=request.user.username).exists():
        u = CUser.objects.get(id__username=request.user.username)
        if str(u.type).lower() == 'admin':
            c1 = CourseForm(request.POST)
            if c1.is_valid():
                c1.save()
                return JsonResponse(status=200, data='Created', safe=False)
            else:
                return JsonResponse(status=400, data=str(c1.errors), safe=False)
        else:
            return JsonResponse(status=403, data='Not allowed to perform', safe=False)
    else:
        return JsonResponse(status=400, data='User does not exist on Cleari Database', safe=False)


def edit(request, course, pk):
    if not request.user.is_authenticated:
        return JsonResponse(status=403, data='Not allowed to perform')
    if CUser.objects.filter(id__username=request.user.username).exists():
        u = CUser.objects.get(id__username=request.user.username)
        print(str(u.type))
        if str(u.type).lower() == 'admin':
            if not Course.objects.filter(pk=pk).exists():
                return JsonResponse(status=404, data='Not Found', safe=False)
            if request.method == 'GET':
                c1 = CourseForm(instance=Course.objects.get(pk=pk))
                department = []
                for t in c1.fields.get('department')._get_choices():
                    department.append(t)
                college = []
                for c in c1.fields.get('college')._get_choices():
                    college.append(c)
                context = dict([('initial', c1.initial),
                                ('fields', dict([('college', dict(college)),
                                                 ('department', dict(department))]))])
                return JsonResponse(status=200, data=context, safe=False)
            else:
                r = Course.objects.edit(pk, request.POST['course'], request.POST['description'],
                                        request.POST['department'], request.POST['college'])
                if r == 'Saved':
                    return JsonResponse(status=200, data=r, safe=False)
                else:
                    return JsonResponse(status=203, data=r, safe=False)
        else:
            return JsonResponse(status=403, data='Not allowed to perform', safe=False)
    else:
        return JsonResponse(status=400, data='User does not exist on Cleari Database', safe=False)


def delete(request, course, pk):
    if not request.user.is_authenticated:
        return JsonResponse(status=403, data='Not allowed to perform')
    if CUser.objects.filter(id__username=request.user.username).exists():
        u = CUser.objects.get(id__username=request.user.username)
        print(str(u.type))
        if str(u.type).lower() == 'admin':
            if not Course.objects.filter(pk=pk).exists():
                return JsonResponse(status=404, data='Not Found', safe=False)
            c1 = Course.objects.delete(pk)
            if c1 != 'Deleted':
                return JsonResponse(status=203, data=c1, safe=False)
            return JsonResponse(status=200, data=str(c1).upper(), safe=False)
        else:
            return JsonResponse(status=403, data='Not allowed to perform', safe=False)
    else:
        return JsonResponse(status=400, data='User does not exist on Cleari Database', safe=False)


def data_export(request, typ):
    if typ:
        return Course.ie.c_export(typ)
    else:
        return JsonResponse(status=400, data='Some parameters are empty', safe=False)


def data_import(request):
    # Call Data Import Form with request data and request files
    di = DataImportForm(request.POST, request.FILES)
    # Verify form is valid
    if di.is_valid():
        d = request.FILES['file']
        return Course.ie.c_import(d)
    else:
        return JsonResponse(status=400, data=str(di.errors), safe=False)
