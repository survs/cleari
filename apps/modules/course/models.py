from django.db import models
from django.utils import timezone

from apps.modules.cdt.models import CDT1
from apps.modules.college.models import College


# Course Manager for managing course model
class CourseManager(models.Manager):

    def add(self, course, description, department, college):
        """
        Module for adding course
        Usage:
        from apps.apps.course.models import Course
        Course.objects.add(course, description, college)
        """
        # Import specific models
        from apps.modules.college.models import College
        from django.db import IntegrityError
        # Get college info
        c = College.objects.filter(college=college)
        # verify college exist
        if not c.exists():
            # Return error message
            return 'College does not exist'
        # Try to save
        try:
            # Add course
            self.create(course=course, description=description, department_id=department, college=c.first())
        except IntegrityError as e:
            return ''.join(['Failed to create: ', str(e.__cause__).partition('DETAIL:')[2]])
        # Return success message
        return 'Saved'

    def delete(self, pk):
        """
        Module for deleting course
        Usage:
        from apps.apps.course.models import Course
        Course.objects.delete(course)
        """
        if pk:
            # Get course info
            c = self.get(pk=pk)
            # Proceed to deletion and return deletion status
            return c.delete()
        else:
            return 'Field is empty'

    def edit(self, pk, course, description, department, college):
        """
        Module for editing course
        Usage:
        from apps.apps.course.models import Course
        Course.objects.edit(course, description)
        """
        if pk and course and description and department and college:
            if not self.filter(pk=pk).exists():
                # Return error message
                return 'Course does not exist'
            # Prepare to save
            c = self.get(course=course)
            c.course = course
            c.department_id = department
            c.description = description
            c.college_id = college
            # Save to database
            c.save()
            return 'Saved'
        else:
            return 'Some fields are empty'

    # TODO: Static Method Update to pwa enable searching
    def search(self, question):
        """
        Module for searching course related information
        Usage:
        from apps.apps.course.models import Course
        Course.objects.search(question)
        """
        # Import specific models. shortcuts, and forms
        from django.db.models import Q
        from django.shortcuts import reverse
        from apps.contrib.q.forms import SearchForm
        # Verify question exist
        if question:
            # Query question against Cleari Course Model
            results = self.filter(Q(course__contains=question) |
                                  Q(description__contains=question) |
                                  Q(department__abbreviation__contains=question) |
                                  Q(college__college__contains=question))
            # Call Search Form and Load initial question data
            sf = SearchForm({'q': question})
            # Create Context Dictionary
            ctx = {
                'data': {
                    'has_results': True if results.__len__() > 0 else False,
                    'results': results,
                    'action': reverse('course:search'),
                    'title': str(question).title(),
                    'search': sf,
                    'referer': 'course'
                }
            }
            # Return context
            return ctx
        else:
            # Show error message
            return 'Question is empty'


# Import Export Course Manager
class IECourseManager(models.Manager):

    @staticmethod
    def c_export(file_type):
        """
        Usage:
        from apps.apps.course.models import Course
        Course.ie.c_export(file_type=('xls', 'xlsx', 'csv', 'json', 'yaml'))
        """
        # Import specific components and resource
        from apps.contrib.components import supported_types, export
        from .resource import CourseResource
        # Verify supplied file type is supported
        if supported_types.__contains__(file_type):
            # Return raw application data
            data = export(CourseResource(), 'Course', 'all', file_type)
            return data
        else:
            # Show error message
            return 'File Type not Supported'

    @staticmethod
    def c_import(file):
        """
        Usage:
        from apps.apps.course.models import Course
        Course.ie.c_import(file)
        """
        # Import specific resource and data set
        from .resource import CourseResource
        from tablib import Dataset
        # Call Course Resource
        res = CourseResource()
        # Create Empty Data set
        ds = Dataset()
        # Get File file from request and load to data sets
        ds.load(file.read())
        # Dry run receive file if valid
        result = res.import_data(ds, dry_run=True)
        # Verify that file has no errors
        if not result.has_errors():
            # Import data into database
            res.import_data(ds, dry_run=False)
            # Show Success Message
            return 'Success Importing data'
        else:
            # Show Error message
            return ''.join(['Failed to import. ', '[', str(ds), ']'])


class Course(models.Model):
    course = models.CharField(max_length=50, unique=True)
    description = models.TextField(max_length=500)
    department = models.ForeignKey(CDT1, on_delete=models.DO_NOTHING)
    college = models.ForeignKey(College, on_delete=models.DO_NOTHING)

    # Register custom manager
    objects = CourseManager()
    ie = IECourseManager()

    def __str__(self):
        """
        :return: Course Name
        """
        return self.course

    def delete(self, using=None, keep_parents=False):
        """
        Module Delete Interceptor, it will intercept requested deleted course object.
        The module will create a snapshot of a deleted course and upload it to system
        Deleted User Bin
        """
        # Import specific files, resource and db
        import csv
        from django.core.files import File
        from django.db import IntegrityError
        from apps.contrib.recyclebin.resource import DeletedUserBin
        # Get course information
        course_name = self.course
        course_description = self.description
        course_college = self.college
        course_department = self.department
        # Try to delete course
        try:
            # Confirm deletion
            super().delete(using=None, keep_parents=False)
        # Catch if there is an Integrity Issue
        except IntegrityError as e:
            # Show Error summary of cause of error
            return ''.join(['Failed to delete: ', str(e.__cause__).partition('DETAIL:')[2]])
        # Call Deleted User Bin() for creating snapshot
        d = DeletedUserBin()
        # Open new file and write information related to user requested
        with open('temp.csv', 'w', newline='') as csvfile:
            cw = csv.writer(csvfile, delimiter=' ',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
            cw.writerow(['Course', 'Description', 'College', 'Department'])
            cw.writerow([course_name, course_description, course_college, course_department])
        # Read temp CVS file
        local_file = open('temp.csv')
        d.timestamp = timezone.localtime()
        d.attachment.save(''.join(['COURSE', '_', str(course_name), '_',
                                   str(timezone.localtime()).partition(' ')[0]]), File(local_file))
        # Save data into database
        d.save()
        # Show success message
        return 'Deleted'
