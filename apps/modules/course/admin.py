from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import Course


@admin.register(Course)
class Course(ImportExportModelAdmin):
    pass
