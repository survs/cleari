from import_export import resources

from .models import College


class CollegeResource(resources.ModelResource):
    class Meta:
        model = College
