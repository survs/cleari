from django.db import models
from django.utils import timezone


# College Manager for managing college model
class CollegeManager(models.Manager):

    def add(self, college, description):
        """
        Module for adding college
        Usage:
        from apps.apps.college.models import College
        College.objects.add(college, description)
        """
        if college and description:
            # Call College model to create new object
            self.create(college=college, description=description)
            # Return string success message
            return 'Saved'
        else:
            return 'Some fields are empty'

    def delete(self, pk):
        """
        Module for college deletion
        Usage:
        from apps.apps.college.models import College
        College.objects.delete(college)
        """
        if pk:
            if self.filter(pk=pk).exists():
                # Get college info
                c = self.get(pk=pk)
                # Proceed to deletion and return deletion status
                return c.delete()
            else:
                return 'College not Found'
        else:
            return 'Field is empty'

    def edit(self, pk, college, description):
        """
        Module for editing existing college object
        Usage:
        from apps.apps.college.models import College
        College.objects.edit(college, description)
        """
        if pk and college and description:
            if self.filter(pk=pk).exists():
                # Get College data by college name
                c = self.get(pk=pk)
                # Prepare to save changes
                c.college = college
                c.description = description
                # Save Changes
                c.save()
                # Return string success message
                return 'Saved'
            else:
                return 'College not Found'
        else:
            return 'Some fields are empty'

    # TODO: Static method should migrate to pwa enabled
    def search(self, question):
        """
        Module for searching college related information
        Usage:
        from apps.apps.college.models import College
        College.objects.search(question)
        """
        # Import specific models, shortcuts, and forms
        from django.db.models import Q
        from django.shortcuts import reverse
        from apps.contrib.q.forms import SearchForm
        # Verify supplied information is not empty
        if question:
            # Query question against Cleari College Model
            results = self.filter(Q(college__contains=question) | Q(description__contains=question))
            # Call Search Form and Load initial question data
            sf = SearchForm({'q': question})
            # Create Context Dictionary
            ctx = {
                'data': {
                    'has_results': True if results.__len__() > 0 else False,
                    'results': results, 'action': reverse('college:search'),
                    'title': str(question).title(), 'search': sf, 'referer': 'college'
                }
            }
            # Render results into Result template with context
            return ctx
        else:
            # Redirect to college manage
            return 'Query Set is empty'


# Import Export College Manager
class IECollegeManager(models.Manager):

    @staticmethod
    def c_export(file_type):
        """
        Usage:
        from apps.apps.college.models import College
        College.ie.c_export(file_type=('xls', 'xlsx', 'csv', 'json', 'yaml'))
        """
        # Import specific components and resources
        from apps.contrib.components import supported_types, export
        from .resource import CollegeResource
        # Verify supplied file type is supported
        if supported_types.__contains__(file_type):
            # Return raw application data
            data = export(CollegeResource(), 'College', 'all', file_type)
            return data
        else:
            # Show error message
            return 'File Type not Supported'

    @staticmethod
    def c_import(file):
        """
        from apps.apps.college.models import College
        College.ie.c_import(raw_file)
        """
        # Import specific resources and data sets
        from .resource import CollegeResource
        from tablib import Dataset
        # Call College Resource
        res = CollegeResource()
        # Create Empty Data set
        ds = Dataset()
        # Get File file from request and load to data sets
        ds.load(file.read())
        # Dry run receive file if valid
        result = res.import_data(ds, dry_run=True)
        # Verify that file has no errors
        if not result.has_errors():
            # Import data into database
            res.import_data(ds, dry_run=False)
            # Show Success Message
            return 'Success Importing data'
        else:
            # Show Error message
            return ''.join(['Failed to import. ', '[', str(ds), ']'])


class College(models.Model):
    college = models.CharField(max_length=15, unique=True)
    description = models.CharField(max_length=500)

    # Register Custom Managers
    ie = IECollegeManager()
    objects = CollegeManager()

    def __str__(self):
        """
        :return: College Name
        """
        return self.college

    def delete(self, using=None, keep_parents=False):
        """
        Module Delete Interceptor, it will intercept requested deleted college object.
        The module will create a snapshot of a deleted college and upload it to system
        Deleted User Bin
        """
        # Import specific resources, files, and db
        import csv
        from apps.contrib.recyclebin.resource import DeletedUserBin
        from django.core.files import File
        from django.db import IntegrityError
        # Get College info
        college_name = self.college
        college_description = self.description
        # Try to delete college
        try:
            # Confirm deletion
            super().delete(using=None, keep_parents=False)
        except IntegrityError as e:
            # Catch if college has existing relation to other models
            return ''.join(['Failed to delete: ', str(e.__cause__).partition('DETAIL:')[2]])
        # Call Deleted User Bin() for creating snapshot
        d = DeletedUserBin()
        # Open new file and write information related to user requested
        with open('temp.csv', 'w', newline='') as csvfile:
            cw = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            cw.writerow(['College', 'Description'])
            cw.writerow([college_name, college_description])
        # Read temp CVS file
        local_file = open('temp.csv')
        d.timestamp = timezone.localtime()
        d.attachment.save(''.join(['COLLEGE', '_',
                                   str(college_name), '_',
                                   str(timezone.localtime()).partition(' ')[0]]),
                          File(local_file))
        # Save data into database
        d.save()
        # Return string success message
        return 'Deleted'
