from django import forms

from .models import College


class CollegeForm(forms.ModelForm):
    class Meta:
        model = College
        fields = ('college', 'description')
        widgets = {
            'college': forms.TextInput(attrs={'ng-model': 'form.college'}),
            'description': forms.TextInput(attrs={'ng-model': 'form.description'}),
        }
