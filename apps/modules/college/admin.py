from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import College


@admin.register(College)
class College(ImportExportModelAdmin):
    pass
