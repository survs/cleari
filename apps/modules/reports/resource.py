from django.contrib.humanize.templatetags.humanize import ordinal
from import_export import resources
from import_export.fields import Field

from apps.modules.receipt.models import Receipt
from apps.modules.users.models import UserStudent, UserSignatory
from apps.modules.violation.models import Violation


class StudentResource(resources.ModelResource):
    number = Field(column_name='ID Number')
    first_name = Field(column_name='First Name')
    middle_name = Field(column_name='Surname')
    last_name = Field(column_name='Last Name')
    course = Field(column_name='Course')
    year = Field(column_name='Year')
    college = Field(column_name='College')

    class Meta:
        model = UserStudent
        exclude = ('id',)

    def dehydrate_number(self, id):
        return '%s' % str(id.number.number)

    def dehydrate_first_name(self, fn):
        return '%s %s' % (fn.number.name_prefix, fn.number.first_name)

    def dehydrate_middle_name(self, mn):
        return '%s.' % (str(mn.number.middle_name).replace('.', ''))

    def dehydrate_last_name(self, ln):
        return '%s %s' % (ln.number.name_postfix, ln.number.last_name)

    def dehydrate_course(self, c):
        return '%s' % (str(c.course.course).upper())

    def dehydrate_year(self, y):
        return '%s Year' % (ordinal(y.year))

    def dehydrate_college(self, c):
        return '%s' % (str(c.course.college.college).upper())


class SignatoryResource(resources.ModelResource):
    number = Field(column_name='ID Number')
    first_name = Field(column_name='First Name')
    middle_name = Field(column_name='Surname')
    last_name = Field(column_name='Last Name')
    position = Field(column_name='Position')
    assign = Field(column_name='Assign Status')
    group = Field(column_name='Group Name')
    college = Field(column_name='College')

    class Meta:
        model = UserSignatory
        exclude = ('id', 'signature', 'cdt1')

    def dehydrate_number(self, id):
        return '%s' % str(id.number.number)

    def dehydrate_first_name(self, fn):
        return '%s %s' % (fn.number.name_prefix, fn.number.first_name)

    def dehydrate_middle_name(self, mn):
        return '%s.' % (str(mn.number.middle_name).replace('.', ''))

    def dehydrate_last_name(self, ln):
        return '%s %s' % (ln.number.name_postfix, ln.number.last_name)

    def dehydrate_position(self, p):
        return '%s' % str(p.position.position).upper()

    def dehydrate_assign(self, a):
        return '%s' % str('Assigned' if a.assign else 'Not Assigned')

    def dehydrate_group(self, g):
        return '%s' % str(g.cdt1.abbreviation).upper()

    def dehydrate_college(self, c):
        return '%s' % str(c.cdt1.college.college).upper()


class ViolationResource(resources.ModelResource):
    number = Field(column_name='ID Number')
    first_name = Field(column_name='First Name')
    middle_name = Field(column_name='Surname')
    last_name = Field(column_name='Last Name')
    type = Field(column_name='User Type')
    violation = Field(column_name='Violation Name')
    resolve = Field(column_name='Status')
    reason = Field(column_name='Remarks')
    sem = Field(column_name='Semester')
    sy = Field(column_name='School Year')

    class Meta:
        model = Violation
        exclude = ('id', 'sys_id')


    def dehydrate_number(self, id):
        return '%s' % str(id.number.number)

    def dehydrate_first_name(self, fn):
        return '%s %s' % (fn.number.name_prefix, fn.number.first_name)

    def dehydrate_middle_name(self, mn):
        return '%s.' % (str(mn.number.middle_name).replace('.', ''))

    def dehydrate_last_name(self, ln):
        return '%s %s' % (ln.number.name_postfix, ln.number.last_name)

    def dehydrate_type(self, t):
        return '%s' % str(t.number.type.type).upper()

    def dehydrate_violation(self, v):
        return '%s' % str(v.violation).capitalize()

    def dehydrate_resolve(self, r):
        return '%s' % str('Resolved' if r.resolve else 'Pending')

    def dehydrate_reason(self, r):
        return '%s' % str(r.reason).capitalize()

    def dehydrate_sem(self, s):
        sp = str(s.sys_id.school_year).split(':')
        return '%s - %s' % (sp[0], sp[1])

    def dehydrate_sy(self, s):
        return '%s' % ordinal(s.sys_id.semester)


class ClearanceResource(resources.ModelResource):
    number = Field(column_name='ID Number')
    first_name = Field(column_name='First Name')
    middle_name = Field(column_name='Surname')
    last_name = Field(column_name='Last Name')
    course = Field(column_name='Course')
    year = Field(column_name='Year')
    college = Field(column_name='College')

    class Meta:
        model = Receipt
        exclude = ('id',)

    def dehydrate_number(self, id):
        return '%s' % str(id.number)

    def dehydrate_first_name(self, fn):
        return '%s %s' % (fn.name_prefix, fn.first_name)

    def dehydrate_middle_name(self, mn):
        return '%s.' % (str(mn.middle_name).replace('.', ''))

    def dehydrate_last_name(self, ln):
        return '%s %s' % (ln.name_postfix, ln.last_name)

    def dehydrate_course(self, c):
        return '%s' % (str(c.userstudent.course.course).upper())

    def dehydrate_year(self, y):
        return '%s Year' % (ordinal(y.userstudent.year))

    def dehydrate_college(self, c):
        return '%s' % (str(c.userstudent.course.college.college).upper())


class ReceiptResource(resources.ModelResource):
    number = Field(attribute='number', column_name='ID Number')
    receipt = Field(attribute='receipt', column_name='Receipt ID')
    received_by = Field(attribute='received_by', column_name='Signed By')
    received_date = Field(attribute='received_date', column_name='Signed Date')
    is_cleared = Field(column_name='Clearance Status')
    college = Field(column_name='College')

    class Meta:
        model = Receipt
        exclude = ('id', 'receipt')


