from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import Report, ReportPrivilege


@admin.register(Report)
class Report(ImportExportModelAdmin):
    pass


@admin.register(ReportPrivilege)
class ReportPrivilege(ImportExportModelAdmin):
    pass