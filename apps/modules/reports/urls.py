from django.urls import path

from . import views

app_name = 'reports'

urlpatterns = [
    path('test/<str:kwarg>', views.test, name='addx'),
    path('option/provider/', views.report_choice_provider, name='report_provider'),
    path('source/provider/', views.report_kwargs, name='report_generator'),
    path('gen/inv/<str:uid>/<str:pk>', views.report_clearance, name='report_clearance'),
    path('gen/<str:pk>/<str:privilege>/<str:kwarg>', views.report_generator, name='report_generator')
]
