from django import template
from django.contrib.humanize.templatetags.humanize import ordinal

from apps.modules.receipt.models import Receipt

register = template.Library()

@register.filter
def dehydrate_sem(string):
    sp = str(string).split(':')
    return '%s - %s' % (sp[0], sp[1])


@register.filter
def dehydrate_sy(string):
    return '%s' % ordinal(string)


@register.filter
def validate_receipt(r):
    return Receipt.objects.verify(receipt=str(r))


@register.filter
def compare(value1, value2):
    return True if str(value1) == str(value2) else False


@register.filter
def to_str(value):
    return str(value)



