import json

from django.contrib.humanize.templatetags.humanize import ordinal
from django.core import signing
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render
from django.utils import timezone

from apps.contrib.utils import render_to_pdf
from apps.modules.cdt.models import CDT1
from apps.modules.college.models import College
from apps.modules.course.models import Course
from apps.modules.receipt.models import Receipt
from apps.modules.users.models import UserStudent, UserSignatory
from apps.modules.violation.models import Violation
# from django.shortcuts import render
from .models import Report


def report(request):
    if not request.user.is_authenticated:
        return JsonResponse(status=203, data='Unauthenticated', safe=False)
    user = request.user.cuser.type.type
    if user == 'signatory':
        return render(request, 'pwa/reports/reports.html')
    else:
        return JsonResponse(status=203, data='Not Authorized', safe=False)


def report_kwargs(request):
    if not request.user.is_authenticated:
        return JsonResponse(status=203, data='Unauthenticated', safe=False)
    user = request.user.cuser.type.type
    if user == 'signatory' or user == 'admin':
        cre = Course.objects.all().values('course', 'description', 'pk')
        cle = College.objects.all().values('course', 'description', 'pk')
        yr1 = [1, 2, 3, 4, 5]
        yr = []
        for i in yr1:
            yr.append(dict([('year', i), ('year_ordinal', ordinal(i))]))
        if user == 'signatory':
            u = UserSignatory.objects.get(number=request.user.username)
            cre = Course.objects.filter(college_id=u.cdt1.college.pk).values('course', 'description', 'pk')
            cle = College.objects.filter(college=u.cdt1.college.college).values('college', 'description', 'pk')
        context = dict([('course', list(cre)),
                        ('college', list(cle)),
                        ('year', list(yr))])
        return JsonResponse(status=200, data=context, safe=False)
    else:
        return JsonResponse(status=203, data='Not Authorized', safe=False)


def report_choice_provider(request):
    if not request.user.is_authenticated:
        return JsonResponse(status=203, data='Unauthenticated', safe=False)
    user = request.user.cuser.type.type
    if user == 'signatory' or user == 'admin':
        u = Report.objects.filter(position__usersignatory__number=request.user.username)
        if u.exists():
            u1 = u.first()
            options = []
            if u1.privilege.can_list_of_students:
                options.append(dict([('privilege', 'list_of_students'),
                                     ('name', 'List of Students'),
                                     ('description', 'Generate a Copy Students Information'),
                                     ('src', 'static/images/svg/reports/list_of_students.svg'),
                                     ('type', 'userstudent'),
                                     ('q', dict([('course', 'course_id'), ('year', 'year__exact'),
                                                 ('college', 'course__college_id')]))]))
            if u1.privilege.can_list_of_signatory:
                options.append(dict([('privilege', 'list_of_signatory'),
                                     ('name', 'List of Signatories'),
                                     ('description', 'Generate a Copy Signatories Information'),
                                     ('src', 'static/images/svg/reports/list_of_signatory.svg'),
                                     ('type', 'usersignatory'),
                                     ('q', dict([('position', 'position_id'), ('assign', 'assign__exact'),
                                                 ('group', 'cdt1_id'), ('college', 'cdt1__college_id')]))]))
            if u1.privilege.can_list_violation:
                options.append(dict([('privilege', 'list_violation'),
                                     ('name', 'Violation Report'),
                                     ('description', 'Generate a Copy Violation Report'),
                                     ('src', 'static/images/svg/reports/list_violation.svg'),
                                     ('type', 'violation'),
                                     ('q', dict([('course', 'number__userstudent__course_id'),
                                                 ('year', 'number__userstudent__year__exact'),
                                                 ('college', 'sys_id__clearance__college_id')]))
                                     ]))
            if u1.privilege.can_list_cleared_students:
                options.append(dict([('privilege', 'cleared_students'),
                                     ('name', 'List of Cleared Students'),
                                     ('description', 'Generate a Copy Cleared Students'),
                                     ('src', 'static/images/svg/reports/cleared_students.svg'),
                                     ('type', 'receiptstudents'),
                                     ('q', dict([('course', 'number__userstudent__course_id'),
                                                 ('year', 'number__userstudent__year__exact'),
                                                 ('college', 'liability__clearance__college_id')]))]))
            #if u1.privilege.can_list_cleared_signatory:
            #    options.append(dict([('privilege', 'cleared_signatory'),
            #                         ('name', 'List of Cleared Signatories'),
            #                         ('description', 'Generate a Copy Cleared Signatories'),
            #                         ('src', 'static/images/svg/reports/cleared_signatory.svg'),
            #                         ('type', 'receiptsignatory'),
            #                         ('q', dict([('position', 'number__usersignatory__position_id'),
            #                                     ('assign', 'number__usersignatory__assign__exact'),
            #                                     ('group', 'number__usersignatory__cdt1_id'),
            #                                     ('college', 'liability__clearance__college_id')]))
            #                         ]))
            if u1.privilege.can_list_uncleared_students:
                options.append(dict([('privilege', 'uncleared_students'),
                                     ('name', 'List of Uncleared Students'),
                                     ('description', 'Generate a copy Uncleared Students'),
                                     ('src', 'static/images/svg/reports/uncleared_students.svg'),
                                     ('type', 'receiptstudents'),
                                     ('q', dict([('course', 'number__userstudent__course_id'),
                                                 ('year', 'number__userstudent__year__exact'),
                                                 ('college', 'liability__clearance__college_id')]))
                                     ]))
            #if u1.privilege.can_list_uncleared_signatory:
            #    options.append(dict([('privilege', 'uncleared_signatory'),
            #                         ('name', 'List of  Signatories'),
            #                         ('description', 'Generate a Uncleared Signatories'),
            #                         ('src', 'static/images/svg/reports/uncleared_signatory.svg'),
            #                         ('type', 'receiptsignatory'),
            #                         ('q', dict([('position', 'number__usersignatory__position_id'),
            #                                     ('assign', 'number__usersignatory__assign__exact'),
            #                                     ('group', 'number__usersignatory__cdt1_id'),
            #                                     ('college', 'liability__clearance__college_id')]))
            #                         ]))
            if u1.privilege.can_list_cleared_students_by_college:
                options.append(dict([('privilege', 'students_by_college'),
                                     ('name', 'List of Students by College'),
                                     ('description', 'Generate a Copy Students List by College'),
                                     ('src', 'static/images/svg/reports/students_by_college.svg'),
                                     ('type', 'receiptstudents'),
                                     ('q', dict([('course', 'number__userstudent__course_id'),
                                                 ('year', 'number__userstudent__year__exact'),
                                                 ('college', 'liability__clearance__college_id')]))
                                     ]))
            #if u1.privilege.can_list_cleared_signatory_by_college:
            #    options.append(dict([('privilege', 'signatory_by_college'),
            #                         ('name', 'List of Signatories by College'),
            #                         ('description', 'Generate a Copy Signatories by College'),
            #                         ('src', 'static/images/svg/reports/signatory_by_college.svg'),
            #                         ('type', 'receiptsignatory'),
            #                         ('q', dict([('position', 'number__usersignatory__position_id'),
            #                                     ('assign', 'number__usersignatory__assign__exact'),
            #                                     ('group', 'number__usersignatory__cdt1_id'),
            #                                     ('college', 'liability__clearance__college_id')]))
            #                         ]))
            if u1.privilege.can_list_uncleared_students_by_college:
                options.append(dict([('privilege', 'students_by_college'),
                                     ('name', 'List of Students by College'),
                                     ('description', 'Generate a Copy Students by College'),
                                     ('src', 'static/images/svg/reports/students_by_college.svg'),
                                     ('type', 'receiptstudents'),
                                     ('q', dict([('course', 'number__userstudent__course_id'),
                                                 ('year', 'number__userstudent__year__exact'),
                                                 ('college', 'liability__clearance__college_id')]))
                                     ]))
            #if u1.privilege.can_list_uncleared_signatory_by_college:
            #    options.append(dict([('privilege', 'signatory_by_college'),
            #                         ('name', 'List of Signatories by College'),
            #                         ('description', 'Generate a Copy Signatories by College'),
            #                         ('src', 'static/images/svg/reports/signatory_by_college.svg'),
            #                         ('type', 'receiptsignatory'),
            #                         ('q', dict([('position', 'number__usersignatory__position_id'),
            #                                     ('assign', 'number__usersignatory__assign__exact'),
            #                                     ('group', 'number__usersignatory__cdt1_id'),
            #                                     ('college', 'liability__clearance__college_id')]))
            #                         ]))
            if u1.privilege.can_list_all_college:
                options.append(dict([('privilege', 'all_college'),
                                     ('name', 'List of all Students'),
                                     ('description', 'Generate a Copy All Students'),
                                     ('src', 'static/images/svg/reports/all_college.svg'),
                                     ('type', 'userstudent'),
                                     ('q', dict([('course', 'number__userstudent__course_id'),
                                                 ('year', 'number__userstudent__year__exact'),
                                                 ('college', 'liability__clearance__college_id')]))
                                     ]))
            return JsonResponse(data=dict([('privilege', options), ('id', u1.pk)]), status=200)
    else:
        return JsonResponse(status=203, data='No reports could be generated', safe=False)


def report_generator(request, pk, privilege, kwarg):
    if not request.user.is_authenticated:
        return JsonResponse(status=203, data='Unauthenticated', safe=False)
    user = request.user.cuser.type.type
    if user == 'signatory' or user == 'admin':
        sig = ''
        u = Report.objects.filter(position__usersignatory__number=request.user.username, pk=pk)
        if user == 'signatory':
            sig = UserSignatory.objects.get(number=request.user.username)
        if not u.exists():
            return JsonResponse(status=203, data='No reports could be generated', safe=False)
        pr = ['list_of_students',
              'list_of_signatory',
              'list_violation',
              'cleared_students',
              'cleared_signatory',
              'uncleared_students',
              'uncleared_signatory',
              'students_by_college',
              'signatory_by_college',
              'students_by_college',
              'signatory_by_college',
              'all_college']
        if pr.__contains__(str(privilege)):
            pdf = None
            fl = ''
            base_location = 'pwa/reports/templates'
            if 'list_of_students' == privilege:
                cre = ''
                yr = ''
                cl = ''
                t = ''
                lst = UserStudent.objects.all().order_by('number__last_name')
                if kwarg != '{}':
                    r = ''
                    try:
                        r = json.loads(kwarg)
                        e = dict(r)
                        lst = UserStudent.objects.filter(**e).order_by('number__last_name')
                    except:
                        return JsonResponse(status=203, data='No reports could be generated', safe=False)
                    if r.get('course_id'):
                        cre = Course.objects.get(pk=r.get('course_id')).course.upper()
                        t = ''.join([t, '-', cre])
                    if r.get('year__exact'):
                        yr = ordinal(r.get('year__exact'))
                        t = ''.join([t, '-', yr])
                    if r.get('course__college_id'):
                        cl = College.objects.get(pk=r.get('course__college_id')).college.upper()
                        t = ''.join([t, '-', cl])
                context = {
                    "title": "Student List",
                    "scope": cl or "College",
                    "student_list": lst,
                    "report_name": "Students List",
                    "type": "Reports for %s" % sig.cdt1.name,
                    "course": cre or "All",
                    "year": yr or "All",
                    "by": request.user.cuser.full_name(),
                    "date": timezone.localtime().now()
                }
                pdf = render_to_pdf('%s/student.list.html' % base_location, context)
                fl = 'List of %s Students - %s' % (t, timezone.localtime().now())
            elif 'list_of_signatory' == privilege:
                po = ''
                ai = ''
                cd = ''
                cl = ''
                t = ''
                lst = UserSignatory.objects.all().order_by('number__last_name')
                if kwarg != '{}':
                    r = ''
                    try:
                        r = json.loads(kwarg)
                        e = dict(r)
                        lst = UserSignatory.objects.filter(**e).order_by('number__last_name')
                    except:
                        return JsonResponse(status=203, data='No reports could be generated', safe=False)
                    if r.get('position_id'):
                        po = UserSignatory.objects.get(position_id=r.get('position_id')).position.position.upper()
                        t = ''.join([t, '-', po])
                    if r.get('assign__exact'):
                        ai = 'Assigned' if r.get('assign__exact') else 'Not Assigned'
                        t = ''.join([t, '-', ai])
                    if r.get('cdt1_id'):
                        cd = CDT1.objects.get(pk=r.get('cdt1_id')).name.upper()
                        t = ''.join([t, '-', cd])
                    if r.get('cdt1__college_id'):
                        cl = College.objects.get(pk=r.get('course__college_id')).college.upper()
                        t = ''.join([t, '-', cl])
                context = {
                    "title": "Signatory List",
                    "scope": cl or "All",
                    "signatory_list": lst,
                    "report_name": "Signatory List",
                    "position": po or "All",
                    "assign": ai or "All",
                    "group": cd or "All",
                    "type": "Reports for %s" % sig.cdt1.name,
                    "by": request.user.cuser.full_name(),
                    "date": timezone.localtime().now()
                }
                pdf = render_to_pdf('%s/signatory.list.html' % base_location, context)
                fl = 'List of %s Signatory - %s' % (t, timezone.localtime().now())
            elif 'list_violation' == privilege:
                cre = ''
                yr = ''
                cl = ''
                t = ''
                lst = Violation.objects.all().order_by('number__last_name')
                if kwarg != '{}':
                    r = ''
                    try:
                        r = json.loads(kwarg)
                        e = dict(r)
                        lst = Violation.objects.filter(**e).order_by('number__last_name')
                    except:
                        return JsonResponse(status=203, data='No reports could be generated', safe=False)
                    if r.get('number__userstudent__course_id'):
                        cre = Course.objects.get(pk=r.get('number__userstudent__course_id')).course.upper()
                        t = ''.join([t, '-', cre])
                    if r.get('number__userstudent__year__exact'):
                        yr = ordinal(r.get('number__userstudent__year__exact'))
                        t = ''.join([t, '-', yr])
                    if r.get('sys_id__clearance__college_id'):
                        cl = College.objects.get(pk=r.get('sys_id__clearance__college_id')).college.upper()
                        t = ''.join([t, '-', cl])
                context = {
                    "title": "Violation List",
                    "scope": cl or "All",
                    "violation_list": lst,
                    "report_name": "Violation List",
                    "type": "Reports for %s" % sig.cdt1.name,
                    "course": cre or "All",
                    "year": yr or "All",
                    "by": request.user.cuser.full_name(),
                    "date": timezone.localtime().now()
                }
                fl = 'List of %s Violation - %s' % (t, timezone.localtime().now())
                pdf = render_to_pdf('%s/violation.list.html' % base_location, context)
            elif 'cleared_students' == privilege:
                lst = Receipt.objects.filter(number__clearanceuserstorage__status=True).order_by('number__last_name').distinct('number')
                cre = ''
                yr = ''
                cl = ''
                t = ''
                if kwarg != '{}':
                    r = ''
                    try:
                        r = json.loads(kwarg)
                        e = dict(r)
                        lst = Receipt.objects.filter(**e).filter(number__clearanceuserstorage__status=True).order_by('number__last_name').distinct('number')
                    except:
                        return JsonResponse(status=203, data='No reports could be generated', safe=False)
                    if r.get('number__userstudent__course_id'):
                        cre = Course.objects.get(pk=r.get('course_id')).course.upper()
                        t = ''.join([t, '-', cre])
                    if r.get('number__userstudent__year__exact'):
                        yr = ordinal(r.get('number__userstudent__year__exact'))
                        t = ''.join([t, '-', yr])
                    if r.get('liability__clearance__college_id'):
                        cl = College.objects.get(pk=r.get('liability__clearance__college_id')).college.upper()
                        t = ''.join([t, '-', cl])
                context = {
                    "title": "Cleared Student List",
                    "scope": cl or "All",
                    "student_list": lst,
                    "report_name": "Cleared Student List",
                    "type": "Reports for %s" % sig.cdt1.name,
                    "course": cre or "All",
                    "year": yr or "All",
                    "by": request.user.cuser.full_name(),
                    "date": timezone.localtime().now()
                }
                fl = 'List of Cleared %s Students - %s' % (t, timezone.localtime().now())
                pdf = render_to_pdf('%s/receipt.students.list.html' % base_location, context)
            elif 'cleared_signatory' == privilege:
                po = ''
                ai = ''
                cd = ''
                cl = ''
                t = ''
                lst = Receipt.objects.filter(number__clearanceuserstorage__status=True).order_by('number__last_name')
                if kwarg != '{}':
                    r = ''
                    try:
                        r = json.loads(kwarg)
                        e = dict(r)
                        lst = Receipt.objects.filter(**e).filter(number__clearanceuserstorage__status=True).order_by('number__last_name')
                    except:
                        return JsonResponse(status=203, data='No reports could be generated', safe=False)
                    if r.get('position_id'):
                        po = UserSignatory.objects.get(position_id=r.get('position_id')).position.position.upper()
                        t = ''.join([t, '-', po])
                    if r.get('assign__exact'):
                        ai = 'Assigned' if r.get('assign__exact') else 'Not Assigned'
                        t = ''.join([t, '-', ai])
                    if r.get('cdt1_id'):
                        cd = CDT1.objects.get(pk=r.get('cdt1_id')).name.upper()
                        t = ''.join([t, '-', cd])
                    if r.get('cdt1__college_id'):
                        cl = College.objects.get(pk=r.get('course__college_id')).college.upper()
                        t = ''.join([t, '-', cl])
                context = {
                    "title": "Signatory Cleared List",
                    "scope":cl or "All",
                    "signatory_list": lst,
                    "report_name": "Signatory Cleared List",
                    "position": po or "All",
                    "assign": ai or "All",
                    "group": cd or "All",
                    "type": "Reports for %s" % sig.cdt1.name,
                    "by": request.user.cuser.full_name(),
                    "date": timezone.localtime().now()
                }
                fl = 'List of Cleared %s Signatories - %s' % (t, timezone.localtime().now())
                pdf = render_to_pdf('%s/receipt.signatory.list.html' % base_location, context)
            elif 'uncleared_students' == privilege:
                lst = Receipt.objects.filter(number__clearanceuserstorage__status=False).order_by('number__last_name').distinct('number')
                cre = ''
                yr = ''
                cl = ''
                t = ''
                if kwarg != '{}':
                    r = ''
                    try:
                        r = json.loads(kwarg)
                        e = dict(r)
                        lst = Receipt.objects.filter(**e).filter(number__clearanceuserstorage__status=False).order_by('number__last_name').distinct('number')
                    except:
                        return JsonResponse(status=203, data='No reports could be generated', safe=False)
                    if r.get('number__userstudent__course_id'):
                        cre = Course.objects.get(pk=r.get('number__userstudent__course_id')).course.upper()
                        t = ''.join([t, '-', cre])
                    if r.get('number__userstudent__year__exact'):
                        yr = ordinal(r.get('number__userstudent__year__exact'))
                        t = ''.join([t, '-', yr])
                    if r.get('liability__clearance__college_id'):
                        cl = College.objects.get(pk=r.get('liability__clearance__college_id')).college.upper()
                        t = ''.join([t, '-', cl])
                print(cre, yr, cl, r)
                context = {
                    "title": "Uncleared Students List",
                    "scope": cl or "All",
                    "student_list": lst,
                    "report_name": "Uncleared Students List",
                    "type": "Reports for %s" % sig.cdt1.name,
                    "course": cre or "All",
                    "year": yr or "All",
                    "by": request.user.cuser.full_name(),
                    "date": timezone.localtime().now()
                }
                fl = 'List of Uncleared %s Students - %s' % (t, timezone.localtime().now())
                pdf = render_to_pdf('%s/receipt.students.list.html' % base_location, context)
            elif 'uncleared_signatory' == privilege:
                po = ''
                ai = ''
                cd = ''
                cl = ''
                t = ''
                lst = Receipt.objects.filter(number__clearanceuserstorage__status=False).order_by('number__last_name')
                if kwarg != '{}':
                    r = ''
                    try:
                        r = json.loads(kwarg)
                        e = dict(r)
                        lst = Receipt.objects.filter(**e).filter(number__clearanceuserstorage__status=False).order_by('number__last_name')
                    except:
                        return JsonResponse(status=203, data='No reports could be generated', safe=False)
                    if r.get('position_id'):
                        po = UserSignatory.objects.get(position_id=r.get('position_id')).position.position.upper()
                        t = ''.join([t, '-', po])
                    if r.get('assign__exact'):
                        ai = 'Assigned' if r.get('assign__exact') else 'Not Assigned'
                        t = ''.join([t, '-', ai])
                    if r.get('cdt1_id'):
                        cd = CDT1.objects.get(pk=r.get('cdt1_id')).name.upper()
                        t = ''.join([t, '-', cd])
                    if r.get('cdt1__college_id'):
                        cl = College.objects.get(pk=r.get('course__college_id')).college.upper()
                        t = ''.join([t, '-', cl])
                context = {
                    "title": "Uncleared Signatory List",
                    "scope": cl or "All",
                    "signatory_list": lst,
                    "report_name": "Uncleared Signatory List",
                    "position": po or "All",
                    "assign": ai or "All",
                    "group": cd or "All",
                    "type": "Reports for %s" % sig.cdt1.name,
                    "by": request.user.cuser.full_name(),
                    "date": timezone.localtime().now()
                }
                fl = 'List of Uncleared %s Students - %s' % (t, timezone.localtime().now())
                pdf = render_to_pdf('%s/receipt.signatory.list.html' % base_location, context)
            elif 'students_by_college' == privilege:
                cre = ''
                yr = ''
                cl = ''
                t = ''
                lst = UserStudent.objects.all()
                if user == 'signatory':
                    lst = lst.filter(course__college_id=request.user.cuser.usersignatory.cdt1.college.college).order_by('number__last_name')
                if kwarg != '{}':
                    r = ''
                    try:
                        r = json.loads(kwarg)
                        e = dict(r)
                        lst = UserStudent.objects.filter(**e)
                        if user == 'signatory':
                            lst = lst.filter(course__college_id=request.user.cuser.usersignatory.cdt1.college.college).order_by('number__last_name')
                    except:
                        return JsonResponse(status=203, data='No reports could be generated', safe=False)
                    if r.get('number__userstudent__course_id'):
                        cre = Course.objects.get(pk=r.get('number__userstudent__course_id')).course.upper()
                        t = ''.join([t, '-', cre])
                    if r.get('number__userstudent__year__exact'):
                        yr = ordinal(r.get('number__userstudent__year__exact'))
                        t = ''.join([t, '-', yr])
                    if r.get('liability__clearance__college_id'):
                        cl = College.objects.get(pk=r.get('liability__clearance__college_id')).college.upper()
                        t = ''.join([t, '-', cl])
                context = {
                    "title": "Students College List",
                    "scope": cl or "College",
                    "student_list": lst,
                    "report_name": "Students College List",
                    "type": "Reports for %s" % sig.cdt1.name,
                    "course": cre or "All",
                    "year": yr or "All",
                    "by": request.user.cuser.full_name(),
                    "date": timezone.localtime().now()
                }
                fl = '%s College Students List - %s' % (t, timezone.localtime().now())
                pdf = render_to_pdf('%s/student.list.html' % base_location, context)
            elif 'signatory_by_college' == privilege:
                po = ''
                ai = ''
                cd = ''
                cl = ''
                t = ''
                lst = UserStudent.objects.all()
                if user == 'signatory':
                    lst = lst.filter(course__college_id=request.user.cuser.usersignatory.cdt1.college.college).order_by('number__last_name')
                if kwarg != '{}':
                    r = ''
                    try:
                        r = json.loads(kwarg)
                        e = dict(r)
                        lst = UserStudent.objects.filter(**e)
                        if user == 'signatory':
                            lst = lst.filter(course__college_id=request.user.cuser.usersignatory.cdt1.college.college).order_by('number__last_name')
                    except:
                        return JsonResponse(status=203, data='No reports could be generated', safe=False)
                    if r.get('position_id'):
                        po = UserSignatory.objects.get(position_id=r.get('position_id')).position.position.upper()
                        t = ''.join([t, '-', po])
                    if r.get('assign__exact'):
                        ai = 'Assigned' if r.get('assign__exact') else 'Not Assigned'
                        t = ''.join([t, '-', ai])
                    if r.get('cdt1_id'):
                        cd = CDT1.objects.get(pk=r.get('cdt1_id')).name.upper()
                        t = ''.join([t, '-', cd])
                    if r.get('cdt1__college_id'):
                        cl = College.objects.get(pk=r.get('course__college_id')).college.upper()
                        t = ''.join([t, '-', cl])
                context = {
                    "title": "College Signatory List",
                    "scope": "College",
                    "signatory_list": lst,
                    "report_name": "College Signatory List",
                    "position": po or "All",
                    "assign": ai or "All",
                    "group": cd or "All",
                    "type": "Reports for %s" % sig.cdt1.name,
                    "by": request.user.cuser.full_name(),
                    "date": timezone.localtime().now()
                }
                fl = '%s College Signatory List - %s' % (t, timezone.localtime().now())
                pdf = render_to_pdf('%s/student.list.html' % base_location, context)
            elif 'all_college' == privilege:
                cre = ''
                yr = ''
                cl = ''
                t = ''
                lst = UserStudent.objects.all().order_by('number__last_name')
                if kwarg != '{}':
                    r = ''
                    try:
                        r = json.loads(kwarg)
                        e = dict(r)
                        lst = UserStudent.objects.filter(**e).order_by('number__last_name')
                    except:
                        return JsonResponse(status=203, data='No reports could be generated', safe=False)
                    if r.get('number__userstudent__course_id'):
                        cre = Course.objects.get(pk=r.get('number__userstudent__course_id')).course.upper()
                        t = ''.join([t, '-', cre])
                    if r.get('number__userstudent__year__exact'):
                        yr = ordinal(r.get('number__userstudent__year__exact'))
                        t = ''.join([t, '-', yr])
                    if r.get('liability__clearance__college_id'):
                        cl = College.objects.get(pk=r.get('liability__clearance__college_id')).college.upper()
                        t = ''.join([t, '-', cl])
                context = {
                    "title": "All Students/Signatory List",
                    "scope": "All",
                    "all_list": lst,
                    "report_name": "All Students/Signatory List",
                    "type": "Full Report List for %s" % sig.cdt1.name,
                    "course": cre or "All",
                    "year": yr or "All",
                    "by": request.user.cuser.full_name(),
                    "date": timezone.localtime().now()
                }
                fl = 'List of %s All Students - %s' % (t, timezone.localtime().now())
                pdf = render_to_pdf('%s/all.list.html' % base_location, context)
            if pdf:
                response = HttpResponse(pdf, content_type='application/pdf')
                filename = "%s_Report.pdf" % (fl)
                content = "inline; filename='%s'" % filename
                download = request.GET.get("download")
                if download:
                    content = "attachment; filename='%s'" % filename
                response['Content-Disposition'] = content
                response['headers'] = {filename}
                return response
            return HttpResponse("Not found")


def report_clearance(request, uid, pk):
    if not request.user.is_authenticated:
        return JsonResponse(status=203, data='Unauthenticated', safe=False)
    if not UserStudent.objects.filter(number__number=uid).exists():
        return JsonResponse(status=203, data='Unauthenticated', safe=False)
    if not Receipt.objects.filter(number__number=uid, liability_id=pk).exists():
        return JsonResponse(status=203, data='Unauthenticated', safe=False)
    user = UserStudent.objects.get(number__number=uid)
    lst = Receipt.objects.filter(number__number=uid, liability_id=pk)
    u = request.user.cuser.type.type
    url = 'https://cleari.herokuapp.com/pub/verify/'
    key = signing.dumps({'uid': str(uid), 'pk': str(pk)})
    if u == 'signatory' or u == 'admin':
        context = {
            "title": "College Clearance of %s" % user.number.full_name(),
            "scope": "College",
            "clearance_list": lst,
            "report_name": "College Clearance of %s" % user.number.full_name(),
            "status":  lst.first().liability.clearance.clearanceuserstorage_set.first().status,
            "name": user.number.full_name(),
            "college": user.course.college.college.upper(),
            "url":"%s%s" % (url, key),
            "course": user.course.course.upper(),
            "year": ordinal(user.year),
            "by": request.user.cuser.full_name(),
            "date": timezone.localtime().now(),
            "college_desc": user.course.college.description.title(),
            "department_name": user.course.department.name.title(),
            "sy": "%s Semester - S.Y. %s-%s" % (ordinal(lst.first().liability.clearance.semester.semester),
                                                str(lst.first().liability.clearance.semester.school_year).split(':')[0],
                                                str(lst.first().liability.clearance.semester.school_year).split(':')[1])
        }
        pdf = render_to_pdf('pwa/reports/templates/clearance.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "%s_Report.pdf" % ('Test')
            content = "inline; filename='%s'" % filename
            download = request.GET.get("download")
            if download:
                content = "attachment; filename='%s'" % filename
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")
    else:
        return JsonResponse(status=203, data='Unauthenticated', safe=False)


def test(request, kwarg):
    lst = UserStudent.objects.all()
    if kwarg != '{}':
        parsed = kwarg
        r = json.loads(parsed)
        e = dict(r)
        lst = UserStudent.objects.filter(**e)
        print(lst)
    context = {
        "title": " Student List",
        "scope": "College",
        "student_list": lst,
        "report_name": "Students List",
        "type": "Department List",
        "course": "BSIT",
        "year": "All",
        "by": "Romart Mediante",
        "date": timezone.localtime().now()
    }
    #return render(request, 'pwa/reports/templates/student.list.html', context)
    pdf = render_to_pdf('pwa/reports/templates/student.list.html', context)
    if pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        filename = "%s_Report.pdf" % ('Test')
        content = "inline; filename='%s'" % filename
        download = request.GET.get("download")
        if download:
            content = "attachment; filename='%s'" % filename
        response['Content-Disposition'] = content
        return response
    return HttpResponse("Not found")

