from django.db import models

from apps.modules.cdt.models import CDT3


class ReportPrivilege(models.Model):
    name = models.CharField(max_length=130)
    can_list_of_students = models.BooleanField(default=False)
    can_list_of_signatory = models.BooleanField(default=False)
    can_list_violation = models.BooleanField(default=False)
    can_list_cleared_students = models.BooleanField(default=False)
    can_list_cleared_signatory = models.BooleanField(default=False)
    can_list_uncleared_students = models.BooleanField(default=False)
    can_list_uncleared_signatory = models.BooleanField(default=False)
    can_list_cleared_students_by_college = models.BooleanField(default=False)
    can_list_cleared_signatory_by_college = models.BooleanField(default=False)
    can_list_uncleared_students_by_college = models.BooleanField(default=False)
    can_list_uncleared_signatory_by_college = models.BooleanField(default=False)
    can_list_all_college = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Report(models.Model):
    position = models.ForeignKey(CDT3, on_delete=models.DO_NOTHING)
    privilege = models.ForeignKey(ReportPrivilege, on_delete=models.DO_NOTHING)

    def __str__(self):
        return str(''.join([str(self.position.position)," - ", str(self.privilege.name)]))
