from import_export import resources

from .models import DeletedUserBin


class DeleteResource(resources.ModelResource):
    class Meta:
        model = DeletedUserBin
