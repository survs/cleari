from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import DeletedUserBin


@admin.register(DeletedUserBin)
class DeletedUserBin(ImportExportModelAdmin):
    pass
