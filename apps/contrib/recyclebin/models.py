from django.db import models


class DeletedUserBin(models.Model):
    timestamp = models.DateTimeField()
    attachment = models.FileField(upload_to='deleted_bin/')

    def __str__(self):
        return str(self.attachment.name).partition('/')[2]
