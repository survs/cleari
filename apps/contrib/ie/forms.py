from django import forms

from apps.contrib.components import supported_types_choices


class DataExportForm(forms.Form):
    file_type = forms.ChoiceField(required=True, choices=tuple(supported_types_choices))


class DataImportForm(forms.Form):
    file = forms.FileField()
