from django.contrib.humanize.templatetags.humanize import ordinal
from django.utils import timezone

from apps.contrib.q.forms import SearchForm
from apps.modules.cdt.models import CDT1
from apps.modules.clearance.models import ClearanceDuration
from apps.modules.college.models import College
from apps.modules.course.models import Course
from apps.modules.receipt.models import Receipt
from apps.modules.semester.models import Semester
from apps.modules.users.models import CUser, UserStudent, UserSignatory
from apps.modules.violation.models import Violation


def sys_monitor(task: str, sem: str):
    """
    Description: Provide live update on clearance schedule and countdown
    :param task:
    Identify what task to get, Example clearance -> task='clearance'
    :param sem:
    Identify what semester to provide, Example sys_id -> sem=sys_id
    :returns:
        1: Return dictionary data for clearance due and duration
        2: Return dictionary data for school year and semester data
        3: Return 'Undefined' when task is not found
    """
    # Get active semester when sem is 'current' else get sem data
    try:
        s_semester = Semester.objects.get(active=True) if sem == 'current' else sem
        if sem != 'current':
            if not Semester.objects.filter(sys=sem).exists():
                return {'response': 'Semester does not exist'}
            s_semester = Semester.objects.get(sys=sem)
    except Semester.DoesNotExist:
        return {'response': 'No Active Semester yet'}
    if ClearanceDuration.objects.filter(sys=s_semester).exists():
        clr = ClearanceDuration.objects.get(sys=s_semester)
        # Compares task to be equal to 'clearance'
        if task == 'clearance':
            # Create a humanize date format for start clearance date
            c_start = timezone.localtime(clr.clearance_start)
            # Create a humanize date format for end clearance date
            c_end = timezone.localtime(clr.clearance_end)
            # Calculate the remaining in seconds before the due
            c_duration = timezone.localtime(clr.clearance_end) - timezone.localtime(timezone.now())
            # Parse duration into days
            days = c_duration.total_seconds() / 86400
            # Calculate difference of days into seconds
            left = 86400 * (days - int(days))
            # Calculate left into non-float hours
            hours = int(left / 3600)
            # Calculate left into float minutes
            minutes = 60 * ((left / 3600) - int(left / 3600))
            # Calculate left into seconds
            seconds = int(60 * (minutes - int(minutes)))
            # Calculate left into non float minutes
            minutes = int(minutes)
            # Return data results in a dictionary format
            return dict([('countdown', dict([('days', str(int(days))),
                                             ('hours', str(hours)),
                                             ('minutes', str(minutes)),
                                             ('seconds', str(seconds))])),
                         ('duration', ''.join([str(int(days)), ' days :',
                                               str(hours), ' hours :',
                                               str(minutes), ' minutes :',
                                               str(seconds), ' seconds remaining'])),
                         ('start', c_start),
                         ('current', timezone.localtime(timezone.now())),
                         ('end', c_end),
                         ('valid', days > 0 or hours > 0 or minutes > 0 or seconds > 0)])
        elif task == 'schoolsem':
            sys_current = str(s_semester.school_year).split(':')
            l = []
            for i in Semester.objects.all():
                sy = str(i.school_year).split(':')
                l.append(dict([('id', i.pk),
                               ('sys', i.sys),
                               ('name', ''.join([str(ordinal(i.semester)),
                                                 ' Semester, S.Y.',
                                                 sy[0], '-', sy[1],
                                                 ' | ', 'Current' if i.active else ''])),
                               ('active', i.active),
                               ('school_year', i.school_year),
                               ('semester', ordinal(i.semester))]))
            # Return data results in a dictionary format
            return dict([('semester', dict([('current',
                                             dict([('name',
                                                    ''.join(['S.Y.',
                                                             sys_current[0],
                                                             '-',
                                                             sys_current[1]])),
                                                   ('sem', str(ordinal(s_semester.semester)))])),
                                            ('list', l)]))])
        else:
            # Compares task to be equal to 'clearance'
            if task == 'clearance':
                return dict([('countdown', dict([('days', str(-0)),
                                                 ('hours', str(-0)),
                                                 ('minutes', str(-0)),
                                                 ('seconds', str(-0))])),
                             ('duration', ''.join([str(-0), ' days :',
                                                   str(-0), ' hours :',
                                                   str(-0), ' minutes :',
                                                   str(-0), ' seconds remaining'])),
                             ('start', -0-0),
                             ('current', timezone.localtime(timezone.now())),
                             ('end', -0),
                             ('valid', 'false')])
            elif task == 'schoolsem':
                sys_current = str(s_semester.school_year).split(':')
                l = []
                for i in Semester.objects.all():
                    sy = str(i.school_year).split(':')
                    l.append(dict([('id', i.pk),
                                   ('sys', i.sys),
                                   ('name', ''.join([str(ordinal(i.semester)),
                                                     ' Semester, S.Y.',
                                                     sy[0], '-', sy[1],
                                                     ' | ', 'Current' if i.active else ''])),
                                   ('active', i.active),
                                   ('school_year', i.school_year),
                                   ('semester', ordinal(i.semester))]))
                # Return data results in a dictionary format
                return dict([('semester', dict([('current',
                                                 dict([('name',
                                                        ''.join(['S.Y.',
                                                                 sys_current[0],
                                                                 '-',
                                                                 sys_current[1]])),
                                                       ('sem', str(ordinal(s_semester.semester)))])),
                                                ('list', l)]))])
    else:
        return {'response': 'Undefined Task'}


def load_user(user, sem):
    """
    Description: Provide User information in a dictionary format readily to render
    :param user:
    Contains User context data
    :param sem:
    Identify what semester to provide, Example sys_id -> sem=sys_id
    :return:
    """
    # Validates is user is not empty
    if user is not None:
        # Lower Case user type to prevent type inconsistencies
        u_type = str(user.type).lower()
        # Compare user type if it is a student
        if u_type == 'student':
            # Query for Student Data from Django Authentication Database
            u_data = CUser.objects.get(number=user.number)
            # Query for Student Data from Cleari Database
            u_student = UserStudent.objects.get(number_id=user.number)
            # Query for Student Course
            u_course = Course.objects.get(course=u_student.course)
            # Query for Student College
            u_college = College.objects.get(college=u_course.college)
            # Returns Current Active Semester for default and Returns Selected Semester for custom
            try:
                s_semester = Semester.objects.get(active=True) if sem == 'current' else sem
                if sem != 'current':
                    s_semester = Semester.objects.get(sys=sem)
            except Semester.DoesNotExist:
                return 'SE404'
            # Get active semester if non fallback to None type
            try:
                active = Semester.objects.get(active=True)
            except Semester.DoesNotExist:
                active = None
            # Call Search Form
            sf = SearchForm()
            # Integrate System Monitor on load_user dependency
            schoolsem = sys_monitor('schoolsem', s_semester)
            clearance = sys_monitor('clearance', s_semester)
            # Filters Student Liabilities related to current Semester and current Course
            liabilities = Receipt.objects.filter(receipt__icontains=u_data.number,
                                                 receipt__contains=u_course.course,
                                                 receipt__startswith=s_semester.sys)
            # Query for Students Violation
            u_violation = Violation.objects.filter(number_id=user.number, sys_id=s_semester)
            # Concatenates Student Name
            name = ''.join([u_data.first_name, ' ', u_data.middle_name, ' ', u_data.last_name]).title
            # Return data results in a dictionary format
            return dict([('user', dict([('name', name),
                                        ('id_number', u_data.number),
                                        ('address', u_data.address),
                                        ('dob', u_data.birth_date),
                                        ('course', ''.join([u_course.course, '-', u_course.description])),
                                        ('college', ''.join([u_college.college, '-', u_college.description])),
                                        ('type', str(u_data.type).lower()),
                                        ('has_liabilities', liabilities.exists()),
                                        ('liabilities', liabilities),
                                        ('has_violation', u_violation.exists()),
                                        ('violations', u_violation)])),
                         ('schoolsem', schoolsem),
                         ('data', active),
                         ('clearance', clearance),
                         ('search', sf)])
        elif u_type == 'signatory':
            # Query for Signatory Data from Django Authentication Database
            u_data = CUser.objects.get(number=user.number)
            # Query for Signatory Data from Cleari Database
            u_signatory = UserSignatory.objects.get(number_id=user.number)
            # Query for Signatory assigned College Model
            u_college = College.objects.get(pk=u_signatory.cdt1.college.pk)
            # Query for Office Model from Signatory Model ID
            cdt1 = CDT1.objects.get(pk=u_signatory.cdt1.pk)
            # Add Search Form to Signatory
            sf = SearchForm()
            # Filters Student with the same college as signatory office which has no liabilities
            stu1 = CUser.objects.filter(userstudent__course__college_id=u_college.pk).exclude(
                receipt__receipt__contains=cdt1.abbreviation
            )
            # Filters Student with the same college as signatory office which has liabilities
            # issued but not signed
            stu2 = CUser.objects.filter(userstudent__course__college_id=u_college.pk,
                                        receipt__receipt__contains=cdt1.abbreviation,
                                        receipt__is_requested=False,
                                        receipt__is_cleared=False)
            # Filters Student with the same college as signatory office which has liabilities
            # issued and requesting to be signed
            stu3 = CUser.objects.filter(userstudent__course__college_id=u_college.pk,
                                        receipt__receipt__contains=cdt1.abbreviation,
                                        receipt__is_requested=True,
                                        receipt__is_cleared=False)
            # Filters Student with the same college as signatory office which has liabilities
            # issued and signed
            stu4 = CUser.objects.filter(userstudent__course__college_id=u_college.pk,
                                        receipt__receipt__contains=cdt1.abbreviation,
                                        receipt__is_requested=True,
                                        receipt__is_cleared=True)
            # Return data results in a dictionary format
            a = []
            for i in stu1:
                a.append(dict([('pk', str(i.pk)),
                               ('id_number', str(i.number)),
                               ('full_name', i.full_name()),
                               ('department', i.userstudent.course.department.abbreviation),
                               ('dep_desc', i.userstudent.course.department.name),
                               ('course', i.userstudent.course.course),
                               ('year', i.userstudent.year),
                               ('college', i.userstudent.course.college.college)]))
            b = []
            for j in stu2:
                b.append(dict([('pk', str(j.pk)),
                               ('id_number', str(j.number)),
                               ('full_name', j.full_name()),
                               ('department', j.userstudent.course.department.abbreviation),
                               ('dep_desc', j.userstudent.course.department.name),
                               ('course', j.userstudent.course.course),
                               ('year', j.userstudent.year),
                               ('college', j.userstudent.course.college.college),
                               ('receipts', [entry for entry in j.receipt_set.values()])]))
            c = []
            for k in stu3:
                c.append(dict([('pk', str(k.pk)),
                               ('id_number', str(k.number)),
                               ('full_name', k.full_name()),
                               ('department', k.userstudent.course.department.abbreviation),
                               ('dep_desc', k.userstudent.course.department.name),
                               ('course', k.userstudent.course.course),
                               ('year', k.userstudent.year),
                               ('college', k.userstudent.course.college.college),
                               ('receipts', [entry for entry in k.receipt_set.values()])]))
            d = []
            for l in stu4:
                d.append(dict([('pk', str(l.pk)),
                               ('id_number', str(l.number)),
                               ('full_name', l.full_name()),
                               ('department', l.userstudent.course.department.abbreviation),
                               ('dep_desc', l.userstudent.course.department.name),
                               ('course', l.userstudent.course.course),
                               ('year', l.userstudent.year),
                               ('college', l.userstudent.course.college.college),
                               ('receipts', [entry for entry in l.receipt_set.values()])]))
            return dict([('user', dict([('name', user.full_name()),
                                        ('id_number', u_data.number),
                                        ('signatory_id', u_signatory.number.number),
                                        ('cdt', ''.join([cdt1.abbreviation, '-', cdt1.name])),
                                        ('position', ''.join([u_signatory.position.position])),
                                        ('college', ''.join([u_college.college, '-', u_college.description])),
                                        ('type', u_data.type.type.lower()),
                                        ('assigned', u_signatory.assign)])),
                         ('clients', dict([('fresh', a),
                                           ('issued', b),
                                           ('requesting', c),
                                           ('signed', d)]))])
        # Statement Block: Compares if user type is admin
        elif u_type == 'admin':
            return 'Admin'
        # Statement Block: Catch Unnecessary user type and returns Null
        else:
            return None
