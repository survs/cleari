from django import forms


class UserLogin(forms.Form):
    username = forms.CharField(max_length=150, required=True, help_text='Username')
    password = forms.CharField(widget=forms.PasswordInput, min_length=1, required=True, help_text='Password')
