import base64
import datetime
import hashlib
from io import BytesIO

from PIL import Image
from django.contrib.humanize.templatetags.humanize import naturaltime, naturalday
from django.core.signing import Signer
from django.shortcuts import HttpResponse
from django.utils import timezone
from django.utils.dateformat import format
from django.utils.dateparse import parse_datetime, parse_date

sign = Signer()

whitelist = set(
    'abcdefghijklmnopqrstuvwxy ABCDEFGHIJKLMNOPQRSTUVWXYZAaBbCcÇçDdEeFfGgĞğHhIıİiJjKkLlMmNnÑñOoÖöPpQqRrSsŞşTtUuÜüVvYyZz-')
whitelist_anc = set('ABCDEFGHIJKLMNOPQRSTUVWXYZ-1234567890')
alpha_caps = set('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
alpha_small = set('abcdefghijklmnopqrstuvwxy')
numeric = set('1234567890')
supported_types = ['xls', 'xlsx', 'csv', 'json', 'yaml']
supported_types_choices = [('xls', 'EXCEL'),
                           ('xlsx', 'EXCEL 2007'),
                           ('csv', 'CSV'),
                           ('json', 'JSON'),
                           ('yaml', 'YAML')]

file_types = [{'abv': '3dm', 'Name': 'Rhino 3D Model'},
              {'abv': '3ds', 'Name': 'Autodesk 3ds Max 3D modeling'},
              {'abv': '3g2', 'Name': '3rd Generation Partnership Project 3GPP2 Multimedia File'},
              {'abv': '3gp', 'Name': '3rd Generation Partnership Project 3GPP Multimedia File'},
              {'abv': '7z', 'Name': '7-Zip Compressed File'},
              {'abv': 'acc', 'Name': 'Graphics Accounts Data File'},
              {'abv': 'ai', 'Name': 'Adobe Illustrator File'},
              {'abv': 'aif', 'Name': 'Audio Interchange File'},
              {'abv': 'apk', 'Name': 'Android Package File'},
              {'abv': 'app', 'Name': 'Mac OS X Application bundle'},
              {'abv': 'asf', 'Name': 'Advanced Systems Format File'},
              {'abv': 'asp', 'Name': 'Active Server Page - may contain scripts written in VBScript or JavaScript.'},
              {'abv': 'aspx', 'Name': 'Active Server Page Extended File - may contain VBScript or C# code.'},
              {'abv': 'asx', 'Name': 'Microsoft ASF Redirector File'},
              {'abv': 'avi', 'Name': 'Audio Video Interleave File'},
              {'abv': 'bak', 'Name': 'Firefox Bookmarks Backup'},
              {'abv': 'bat', 'Name': 'DOS Batch File'},
              {'abv': 'bin', 'Name': 'CD/DVD image format'},
              {'abv': 'bmp', 'Name': 'Bitmap Image File'},
              {'abv': 'cab', 'Name': 'Windows Cabinet File'},
              {'abv': 'cad', 'Name': 'BobCAD-CAM File'},
              {'abv': 'cdr', 'Name': 'CorelDRAW Image File'},
              {'abv': 'certificate', 'Name': 'Security Certificate'},
              {'abv': 'cfg', 'Name': 'Configuration File'},
              {'abv': 'cfm', 'Name': 'ColdFusion Markup File'},
              {'abv': 'cgi', 'Name': 'Common Gateway Interface Script'},
              {'abv': 'class', 'Name': 'Java Class File Extension'},
              {'abv': 'com', 'Name': 'DOS Command File'},
              {'abv': 'cpl', 'Name': 'Windows Control Panel Item'},
              {'abv': 'cpp', 'Name': 'C++ Source Code File'},
              {'abv': 'crx', 'Name': 'Chrome Extension'},
              {'abv': 'csr', 'Name': 'Certificate Signing Request File'},
              {'abv': 'css', 'Name': 'Cascading Style Sheet'},
              {'abv': 'csv', 'Name': 'Comma Separated Values File'},
              {'abv': 'cue', 'Name': 'Cue Sheet File'},
              {'abv': 'cur', 'Name': 'Windows Cursor'},
              {'abv': 'dat', 'Name': 'Data File'},
              {'abv': 'db', 'Name': 'Mobile Device Database File'},
              {'abv': 'dbf', 'Name': 'Database File'},
              {'abv': 'dds', 'Name': 'DirectDraw Surface'},
              {'abv': 'dem', 'Name': 'Demo File'},
              {'abv': 'dll', 'Name': 'Dynamic Link Library'},
              {'abv': 'dmg', 'Name': 'Mac OS X Disk Image'},
              {'abv': 'dmp', 'Name': 'Windows Memory Dump'},
              {'abv': 'doc', 'Name': 'Microsoft Word Document'},
              {'abv': 'docx', 'Name': 'Microsoft Word Open XML Document'},
              {'abv': 'drv', 'Name': 'Device Driver'},
              {'abv': 'dtd', 'Name': 'Document Type Definition File'},
              {'abv': 'dwg', 'Name': 'AutoCAD Drawing Database File'},
              {'abv': 'dxf', 'Name': 'Drawing Exchange Format File'},
              {'abv': 'elf', 'Name': 'Nintendo Wii Game File'},
              {'abv': 'eps', 'Name': 'Encapsulated PostScript File'},
              {'abv': 'eps', 'Name': 'Encapsulated PostScript File'},
              {'abv': 'exe', 'Name': 'Windows Executable File'},
              {'abv': 'fla', 'Name': 'Adobe Animate Animation'},
              {'abv': 'flash', 'Name': 'Frictional Games Flashback File'},
              {'abv': 'flv', 'Name': 'Animate Video File'},
              {'abv': 'fnt', 'Name': 'Windows Font File'},
              {'abv': 'fon', 'Name': 'Generic Font File'},
              {'abv': 'gam', 'Name': 'Saved Game File'},
              {'abv': 'gbr', 'Name': 'Gerber File'},
              {'abv': 'ged', 'Name': 'GEDCOM Genealogy Data File'},
              {'abv': 'gif', 'Name': 'Graphical Interchange Format File'},
              {'abv': 'gpx', 'Name': 'GPS Exchange File'},
              {'abv': 'gz', 'Name': 'Gnu Zipped Archive'},
              {'abv': 'gzip', 'Name': 'Gnu Zipped File'},
              {'abv': 'hqz', 'Name': 'Internet Explorer Macintosh Edition File'},
              {'abv': 'html', 'Name': 'Hypertext Markup Language File'},
              {'abv': 'ibooks', 'Name': 'Multi-Touch iBook'},
              {'abv': 'icns', 'Name': 'Mac OS X Icon Resource File'},
              {'abv': 'ico', 'Name': 'Icon File'},
              {'abv': 'ics', 'Name': 'Calendar File'},
              {'abv': 'iff-file', 'Name': 'Interchange File Format'},
              {'abv': 'indd', 'Name': 'Adobe InDesign Document'},
              {'abv': 'iso', 'Name': 'Disc Image File'},
              {'abv': 'iso', 'Name': 'Arbortext IsoDraw Document'},
              {'abv': 'jar', 'Name': 'Java Archive File'},
              {'abv': 'jpg', 'Name': 'Joint Photographic Experts Group Image'},
              {'abv': 'js', 'Name': 'Javascript'},
              {'abv': 'jsp', 'Name': 'Java Server Page'},
              {'abv': 'key', 'Name': 'Software License Key File/Security Key'},
              {'abv': 'kml', 'Name': 'Keyhole Markup Language File'},
              {'abv': 'kmz', 'Name': 'Google Earth Placemark File'},
              {'abv': 'lnk', 'Name': 'Windows File Shortcut'},
              {'abv': 'log', 'Name': 'Log File'},
              {'abv': 'lua', 'Name': 'Lua Source File'},
              {'abv': 'm3u', 'Name': 'Media Playlist File'},
              {'abv': 'm4a', 'Name': 'MPEG-4 Audio File'},
              {'abv': 'm4v', 'Name': 'iTunes Video File'},
              {'abv': 'mach', 'Name': 'MACH Input Files'},
              {'abv': 'max', 'Name': '3ds Max Scene File'},
              {'abv': 'mdb', 'Name': 'Microsoft Access Database'},
              {'abv': 'mdf', 'Name': 'Media Disc Image File'},
              {'abv': 'mid', 'Name': 'MIDI File'},
              {'abv': 'mim', 'Name': 'Multi-Purpose Internet Mail Message File'},
              {'abv': 'mov', 'Name': 'Apple QuickTime Movie'},
              {'abv': 'mp3', 'Name': 'MP3 Audio File'},
              {'abv': 'mp4', 'Name': 'MPEG-4 Video File'},
              {'abv': 'mpa', 'Name': 'MPEG-2 Audio File'},
              {'abv': 'mpg', 'Name': 'MPEG Video File'},
              {'abv': 'msg', 'Name': 'Outlook Mail Message'},
              {'abv': 'msi', 'Name': 'Windows Installer Package'},
              {'abv': 'nes', 'Name': 'Nintendo (NES) ROM File'},
              {'abv': 'obj', 'Name': 'Win32 object file.'},
              {'abv': 'odb', 'Name': 'OpenDocument Database'},
              {'abv': 'odc', 'Name': 'Office Data Connection File'},
              {'abv': 'odf', 'Name': 'OpenDocument Formula'},
              {'abv': 'odg', 'Name': 'OpenDocument Graphic File'},
              {'abv': 'odi', 'Name': 'OpenDocument Image'},
              {'abv': 'odp', 'Name': 'OpenDocument Presentation'},
              {'abv': 'ods', 'Name': 'OpenDocument Spreadsheet'},
              {'abv': 'odt', 'Name': 'OpenDocument Text Document'},
              {'abv': 'odx', 'Name': 'BizTalk Server Orchestration File'},
              {'abv': 'ogg', 'Name': 'Ogg Vorbis Audio File'},
              {'abv': 'otf', 'Name': 'OpenType Font'},
              {'abv': 'pages', 'Name': 'Pages Document'},
              {'abv': 'pct', 'Name': 'Picture File'},
              {'abv': 'pdb', 'Name': 'Program Database'},
              {'abv': 'pdf', 'Name': 'Portable Document Format File'},
              {'abv': 'php', 'Name': 'Hypertext Preprocessor Source Code File'},
              {'abv': 'pif', 'Name': 'Program Information File'},
              {'abv': 'pkg', 'Name': 'Symbian Package File'},
              {'abv': 'pl', 'Name': 'Perl Script'},
              {'abv': 'png', 'Name': 'Portable Network Graphic'},
              {'abv': 'pps', 'Name': 'PowerPoint Slide Show'},
              {'abv': 'ppt', 'Name': 'PowerPoint Presentation'},
              {'abv': 'pptx', 'Name': 'PowerPoint Open XML Presentation'},
              {'abv': 'ps', 'Name': 'PostScript File'},
              {'abv': 'psd', 'Name': 'Adobe Photoshop Document'},
              {'abv': 'pub', 'Name': 'Publisher Document/Public Key File'},
              {'abv': 'py', 'Name': 'Python Script'},
              {'abv': 'ra', 'Name': 'Real Audio File'},
              {'abv': 'rar', 'Name': 'WinRAR Compressed Archive'},
              {'abv': 'raw', 'Name': 'Raw Image Data File'},
              {'abv': 'rm', 'Name': 'RealMedia File'},
              {'abv': 'rom', 'Name': 'N64 Game ROM File'},
              {'abv': 'rpm', 'Name': 'Red Hat Package Manager File'},
              {'abv': 'rss', 'Name': 'Rich Site Summary'},
              {'abv': 'rtf', 'Name': 'Rich Text Format File'},
              {'abv': 'sav', 'Name': 'Saved Game'},
              {'abv': 'sdf', 'Name': 'Standard Data File'},
              {'abv': 'sitx', 'Name': 'StuffIt X Archive'},
              {'abv': 'sql', 'Name': 'Structured Query Language Data File'},
              {'abv': 'sql', 'Name': 'Structured Query Language Data File'},
              {'abv': 'srt', 'Name': 'SubRip Subtitle File'},
              {'abv': 'svg', 'Name': 'Scalable Vector Graphics File'},
              {'abv': 'swf', 'Name': 'Shockwave Flash Movie'},
              {'abv': 'sys', 'Name': 'Windows System File'},
              {'abv': 'tar', 'Name': 'Consolidated Unix File Archive'},
              {'abv': 'tex', 'Name': 'LaTeX Source Document'},
              {'abv': 'tga', 'Name': 'Targa Graphic'},
              {'abv': 'thm', 'Name': 'Thumbnail Image File'},
              {'abv': 'tiff', 'Name': 'Tagged Image File Format'},
              {'abv': 'tmp', 'Name': 'Temporary File'},
              {'abv': 'torrent', 'Name': 'BitTorrent File'},
              {'abv': 'ttf', 'Name': 'TrueType Font'},
              {'abv': 'txt', 'Name': 'Plain Text File'},
              {'abv': 'uue', 'Name': 'Uuencoded File'},
              {'abv': 'vb', 'Name': 'Visual Basic Project Item File'},
              {'abv': 'vcd', 'Name': 'Virtual CD'},
              {'abv': 'vcf', 'Name': 'vCard File'},
              {'abv': 'vob', 'Name': 'DVD Video Object File'},
              {'abv': 'wav', 'Name': 'WAVE Audio File'},
              {'abv': 'wma', 'Name': 'Windows Media Audio File'},
              {'abv': 'wmv', 'Name': 'Windows Media Video File'},
              {'abv': 'wpd', 'Name': 'WordPerfect Document'},
              {'abv': 'wps', 'Name': 'Microsoft Works Word Processor Document'},
              {'abv': 'wsf', 'Name': 'Windows Script File'},
              {'abv': 'xhtml', 'Name': 'Extensible Hypertext Markup Language File'},
              {'abv': 'xlr', 'Name': 'Works Spreadsheet'},
              {'abv': 'xls', 'Name': 'Excel Spreadsheet'},
              {'abv': 'xlsx', 'Name': 'Microsoft Excel Open XML Spreadsheet'},
              {'abv': 'xml', 'Name': 'Extensible Markup Language File'},
              {'abv': 'yuv', 'Name': 'YUV Encoded Image File'},
              {'abv': 'zip', 'Name': 'Zip file.'}]


user_types = [('all', 'All'),
              ('student', 'Student'),
              ('signatory', 'Signatory'),
              ('administrator', 'Administrator'),
              ('django', 'Django Users')]


def get_hash(file):
    md5 = hashlib.md5()

    for c in file.chunks():
        md5.update(c)

    return md5.hexdigest()


def ftype(abv):
    for i in file_types:
        if str(abv).lower() == i.get('abv'):
            return i.get('Name')


def export(resource, model_name, ext_name, file_type):
    res = resource
    data_set = res.export()
    response = HttpResponse(f_type(data_set, file_type), content_type=f_mime(file_type))
    response['Content-Disposition'] = ''.join(
        ['attachment; filename="', str(model_name), '- [', str(ext_name).title(), ']', '.', str(file_type), '"'])
    return response


def filter_export(resource, queryset, model_name, ext_name, file_type):
    data_set = resource.export(queryset)
    response = HttpResponse(f_type(data_set, file_type), content_type=f_mime(file_type))
    response['Content-Disposition'] = ''.join(
        ['attachment; filename="', str(model_name), ' - [', str(ext_name).title(), ']', '.', str(file_type), '"'])
    return response


def f_type(data, typ):
    if typ == 'xls':
        return data.xls
    elif typ == 'xlsx':
        return data.xlsx
    elif typ == 'csv':
        return data.csv
    elif typ == 'json':
        return data.json
    elif typ == 'html':
        return data.html
    elif typ == 'yaml':
        return data.yaml
    else:
        return data


def f_mime(typ):
    if typ == 'xls':
        return 'application/vnd.ms-excel'
    elif typ == 'xlsx':
        return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    elif typ == 'csv':
        return 'text/csv'
    elif typ == 'json':
        return 'application/json'
    elif typ == 'html':
        return 'text/html'
    elif typ == 'yaml':
        return 'application/yaml'
    else:
        return None


def get_system_time():
    return timezone.now()


def sign_text(data):
    return str(sign.signature(data))


def get_url(request: str, urls_list: []):
    """
    This function will provide a string sequence next URL path with
        request path
    """
    # If the list is empty return the request
    if not urls_list:
        return request
    # Structure next URL path
    new_url = urls_list.pop() + '?next=' + '&'.join(urls_list) + '&request=' + request
    # return Structured next URL
    return new_url


def ph_resize(path, w, h):
    buffer = BytesIO()
    try:
        image = Image.open(path)
    except:
        image = Image.open('static/images/default.png')
    png_info = image.info
    image = image.resize((w, h), Image.ANTIALIAS)
    image.mode = "RGB"
    image.save(buffer, format="PNG", **png_info)
    result = buffer.getvalue()
    return ''.join(["data:image/png;base64,", str(base64.b64encode(result)).replace('b\'', '').replace('\'', '')])


def img_to_b64(path):
    import base64
    with open(path, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    return encoded_string


def validate_receipt(r):
    from apps.modules.receipt.models import Receipt
    return Receipt.objects.verify(receipt=str(r))


def compare(value1, value2):
    return True if str(value1) == str(value2) else False


def to_str(value):
    return str(value)


def to_date(value):
    t = parse_datetime(str(value))
    return str(t.date())


def to_human_date(value):
    result = parse_datetime(str(value)) - datetime.datetime.astimezone(datetime.datetime.now())
    if result.days < -1:
        return ''.join([str(naturalday(parse_date(format(parse_datetime(str(value)), 'Y-m-d')))).title(),
                        ' at ', format(parse_datetime(str(value)), 'g:i A')])
    else:
        return naturaltime(parse_datetime(str(value)))


def bytes_to_human(byts):
    kb = byts / 1024
    if kb > 1024:
        mb = kb / 1024
        if mb > 1024:
            gb = mb / 1024
            if gb > 1024:
                tb = gb / 1024
                if tb > 1024:
                    return ''.join([str(), ' TB'])
                else:
                    return ''.join([str(round(gb, 1)), ' TB'])
            else:
                return ''.join([str(round(gb, 1)), ' GB'])
        else:
            return ''.join([str(round(mb, 1)), ' MB'])
    else:
        return ''.join([str(round(kb, 1)), ' KB'])


def get_profile(user, chat_id):
    from apps.modules.message.models import Message
    from apps.modules.users.models import CUser
    pk = str(Message.objects.get_recipient(user, chat_id))
    try:
        user = CUser.objects.get(number=pk)
        return user.profile
    except CUser.DoesNotExist:
        return ''


def get_name(value, typ):
    from apps.modules.users.models import CUser, User as DUser
    try:
        user = CUser.objects.get(number=value)
    except CUser.DoesNotExist:
        user = DUser.objects.get(username=value)
    if typ == 'name':
        return str(user.first_name).title()
    elif typ == 'full':
        try:
            surname = ''.join([user.middle_name, ' ']) if user.middle_name else ''
        except AttributeError:
            surname = ''
        return ''.join([user.first_name, ' ', surname, user.last_name]).title()


def get_recipient(user, chat_id):
    from apps.modules.message.models import Message
    return get_name(str(Message.objects.get_recipient(user, chat_id)), 'full')


def get_file_name(location):
    d = str(location).split('/')
    return d.pop()


def full_name(user_id):
    from apps.modules.users.models import CUser
    return CUser.objects.get(number=user_id).full_name()
