import django_eventstream
from channels.auth import AuthMiddlewareStack
from channels.http import AsgiHandler
from channels.routing import URLRouter
from django.conf.urls import url

from apps.modules.message import routing as msg_route

urlpatterns = [
    url(r'^events/', AuthMiddlewareStack(
        URLRouter(django_eventstream.routing.urlpatterns)
    )),
    url(r'^message/sse', AuthMiddlewareStack(
        URLRouter(msg_route.urlpatterns)
    )),
    url(r'', AsgiHandler),
]
