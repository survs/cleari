import json

from django.contrib.auth.models import User as DUser
from django.db import IntegrityError
from django.forms.models import model_to_dict
from django.http import JsonResponse, StreamingHttpResponse
from django.middleware.csrf import get_token
from django.shortcuts import render

from apps.modules.cdt.forms import CDT1Form, CDT2Form, CDT3Form
from apps.modules.cdt.models import CDT1
from apps.modules.clearance.forms import ClearanceDurationForm
from apps.modules.clearance.models import Clearance, ClearanceDependency
from apps.modules.college.forms import CollegeForm
from apps.modules.course.forms import CourseForm
from apps.modules.course.models import Course
from apps.modules.receipt.models import Receipt
from apps.modules.semester.forms import SemesterForm
from apps.modules.users.forms import UserEditForm, UserPhotoForm
from apps.modules.users.models import CUser
from apps.modules.violation.models import Violation
from .async_data_provider import cdt1, cdt2, cdt3, clearance, clearance_duration, college, course, semester, receipt, \
    student, signatory, user, user_types, violation, signatory_fresh, signatory_issued, signatory_requesting, \
    signatory_signed


def module_data_provider(request, module, rpp, page, ob, search):
    if not request.user.is_authenticated:
        return JsonResponse(status=204, data='Unauthenticated', safe=False)
    u = request.user.username
    # Get user type form request data
    du = DUser.objects.get(username=u)
    # Verify if the user is a staff or a System admin
    if du.is_staff or du.is_superuser:
        if module == 'cdt1':
            return JsonResponse(safe=False, data=cdt1(rpp, page, ob, search))
        elif module == 'cdt2':
            return JsonResponse(safe=False, data=cdt2(rpp, page, ob, search))
        elif module == 'cdt3':
            return JsonResponse(safe=False, data=cdt3(rpp, page, ob, search))
        elif module == 'clearance':
            return JsonResponse(safe=False, data=clearance(rpp, page, ob, search))
        elif module == 'clearance_duration':
            return JsonResponse(safe=False, data=clearance_duration(rpp, page, ob, search))
        elif module == 'college':
            return JsonResponse(safe=False, data=college(rpp, page, ob, search))
        elif module == 'course':
            return JsonResponse(safe=False, data=course(rpp, page, ob, search))
        elif module == 'receipt':
            return JsonResponse(safe=False, data=receipt(rpp, page, ob, search))
        elif module == 'student':
            return JsonResponse(safe=False, data=student(rpp, page, ob, search))
        elif module == 'signatory':
            return JsonResponse(safe=False, data=signatory(rpp, page, ob, search))
        elif module == 'user':
            return JsonResponse(safe=False, data=user(rpp, page, ob, search))
        elif module == 'semester':
            return JsonResponse(safe=False, data=semester(rpp, page, ob, search))
        elif module == 'user_type':
            return JsonResponse(safe=False, data=user_types(rpp, page, ob, search))
        elif module == 'violation':
            return JsonResponse(safe=False, data=violation(rpp, page, ob, search))
        elif module == 'table':
            return render(request, 'pwa/components/table.html')
        else:
            return JsonResponse(data='Module requested not Valid', safe=False)
    elif du.cuser.type.type == 'signatory':
        if module == 'fresh':
            return JsonResponse(safe=False, data=signatory_fresh(rpp, page, ob, search, du.username))
        elif module == 'issued':
            return JsonResponse(safe=False, data=signatory_issued(rpp, page, ob, search, du.username))
        elif module == 'request':
            return JsonResponse(safe=False, data=signatory_requesting(rpp, page, ob, search, du.username))
        elif module == 'signed':
            return JsonResponse(safe=False, data=signatory_signed(rpp, page, ob, search, du.username))
    else:
        return du.cuser.type.type


def data_filter(request, module, q):
    if module == 'course':
        c = Course.objects.filter(college__college=q)
        a = []
        for i in c:
            a.append(dict([('id', i.pk),
                           ('course', i.course),
                           ('description', i.description),
                           ('department_id', i.department_id),
                           ('department', i.department.abbreviation),
                           ('department_name', i.department.name),
                           ('college_id', i.college_id),
                           ('college', i.college.college),
                           ('college_desc', i.college.description)]))
        context = dict([('course', a)])
        return StreamingHttpResponse(streaming_content=json.dumps(context))


def issue_clearance(request):
    csrf = get_token(request)
    if not request.user.is_authenticated:
        return JsonResponse(status=204, data='Unauthenticated', safe=False)
    if not CUser.objects.filter(number=request.user.username, type_id=1).exists():
        return JsonResponse(status=204, data='Unauthorized', safe=False)
    if request.method == 'POST':
        cl = request.POST['course_list']
        cr = Clearance.objects.get(pk=request.POST['clearance_id'])
        return StreamingHttpResponse(streaming_content=ic_process(cl, cr))
    else:
        return JsonResponse(status=203, data={'csrf': csrf}, safe=False)


def ic_process(cl, cr):
    count = 0
    for i in str(cl).split(','):
        c = CUser.objects.filter(userstudent__course_id=i).all()
        count = count + c.count()
    yield '{"con": ' + str(count) + '}'
    total = count
    for i in str(cl).split(','):
        c = CUser.objects.filter(userstudent__course_id=i).all()
        for j in c:
            try:
                for l in cr.clearancedependency_set.all():
                    # Concatenates receipt ID into system recognisable format
                    receipt_id = ''.join(
                        [str(cr.semester.sys), '-', str(cr.pk), '-', (j.userstudent.course.course), '-',
                         str(l.signatory.position).replace(' ', ''), '-',
                         j.number, '-',
                         str(l.signatory.cdt1.abbreviation)])
                    # Verify if the receipt is not already exist
                    if not Receipt.objects.filter(receipt=receipt_id).exists():
                        r = Receipt(receipt=str(receipt_id).upper(),
                                    number_id=CUser.objects.get(number=j.number),
                                    signatory_id=l.signatory.pk,
                                    is_requested=False,
                                    liability_id=cr.pk)
                        # Save data to database(Postgres)
                        r.save()
            except IntegrityError as e:
                yield '{"error":' + str(j.number) + ', "details":' + str(e) + '}'
            count = count - 1
            process = 100 - round(count / total * 100)
            yield '{"process":' + str(process) + '}'


def signatories_data_provider(request, college):
    csrf = get_token(request)
    if not request.user.is_authenticated:
        return JsonResponse(status=204, data='Unauthenticated', safe=False)
    if not CUser.objects.filter(number=request.user.username, type_id=1).exists():
        return JsonResponse(status=204, data='Unauthorized', safe=False)
    if request.method == 'GET':
        if college == 'csrf':
            return JsonResponse(status=203, data={'csrf': csrf}, safe=False)
        else:
            d = CDT1.objects.filter(college__college=college)
            m = []
            for i in d:
                n = []
                for j in i.usersignatory_set.all():
                    n.append([('id', j.pk),
                              ('number', j.number.number),
                              ('full_name', j.number.full_name()),
                              ('position', j.position.position),
                              ('assign', j.assign)])
                m.append(
                    dict([('id', i.pk), ('abbreviation', i.abbreviation), ('name', i.name), ('type', i.type.type),
                          ('signatories', n)]))
            return JsonResponse(data=m, safe=False, status=200)
    else:
        d = Clearance.objects.create(name=request.POST['name'],
                                     semester_id=request.POST['semester_id'],
                                     college_id=request.POST['college_id'])

        signatory_list = request.POST['signatory_list']
        liability_list = request.POST['liability_list']
        for i in str(signatory_list).split(','):
            ClearanceDependency.objects.create(clearance_id=d.pk,
                                               signatory_id=i,
                                               as_name='')
        return JsonResponse(data='Clearance Successfully Created', status=200, safe=False)


def module_view_provider(request, module):
    if not request.user.is_authenticated:
        return JsonResponse(status=204, data='Unauthenticated', safe=False)
    csrf = get_token(request)
    u = request.user.username
    # Get user type form request data
    du = DUser.objects.get(username=u)
    # Verify if the user is a staff or a System admin
    if du.is_staff or du.is_superuser:
        if module == 'cdt1':
            cdt1 = CDT1Form()
            return render(request, 'pwa/form_provider/form.html', {'form': cdt1, 'referer': module, 'csrf': csrf})
        elif module == 'cdt2':
            cdt2 = CDT2Form()
            return render(request, 'pwa/form_provider/form.html', {'form': cdt2, 'referer': module, 'csrf': csrf})
        elif module == 'cdt3':
            cdt3 = CDT3Form()
            return render(request, 'pwa/form_provider/form.html', {'form': cdt3, 'referer': module, 'csrf': csrf})
        elif module == 'cd':
            cd = ClearanceDurationForm()
            return render(request, 'pwa/form_provider/form.html', {'form': cd, 'referer': module, 'csrf': csrf})
        elif module == 'c':
            return render(request, 'pwa/editor/clearance.html', {'csrf': csrf})
        elif module == 'c_issue':
            return render(request, 'pwa/components/issue.html', {'csrf': csrf})
        elif module == 'college':
            c = CollegeForm()
            return render(request, 'pwa/form_provider/form.html', {'form': c, 'referer': module, 'csrf': csrf})
        elif module == 'course':
            c = CourseForm()
            return render(request, 'pwa/form_provider/form.html', {'form': c, 'referer': module, 'csrf': csrf})
        elif module == 'semester':
            s = SemesterForm()
            return render(request, 'pwa/form_provider/form.html', {'form': s, 'referer': module, 'csrf': csrf})
        elif module == 'violation':
            v = Violation.objects.all()
            return render(request, 'pwa/form_provider/form.html', {'form': v, 'referer': module, 'csrf': csrf})
        else:
            return JsonResponse(data='Module requested not Valid', safe=False)


def user_asset_provider(request, asset):
    global data
    csrf = get_token(request)
    if not request.user.is_authenticated:
        return JsonResponse(status=204, data='Unauthenticated', safe=False)
    if not DUser.objects.filter(username=request.user.username).exists():
        data = model_to_dict(DUser.objects.get(username=request.user.username))
    if CUser.objects.filter(number=request.user.username).exists():
        data = model_to_dict(CUser.objects.get(number=request.user.username))
        if asset == 'myprofile_view':
            if request.method == 'GET':
                e = UserEditForm(initial=data)
                return render(request, 'pwa/form_provider/form.html', {'form': e, 'referer': asset, 'csrf': csrf})
            else:
                c = CUser.objects.edit(request.user.username,
                                       request.POST['first_name'],
                                       request.POST['middle_name'],
                                       request.POST['last_name'],
                                       request.POST['birth_date'],
                                       request.POST['address'])
                if c == 'Saved':
                    return JsonResponse(status=200, data='Changes Saved', safe=False)
                else:
                    return JsonResponse(status=203, data=c, safe=False)
        elif asset == 'myprofile_data':
            e = CUser.objects.get(number=request.user.username)
            context = dict([('number', e.number),
                            ('first_name', e.first_name),
                            ('middle_name', e.middle_name),
                            ('last_name', e.last_name),
                            ('address', e.address),
                            ('birth_date', e.birth_date)])
            return JsonResponse(status=200, data=context, safe=False)
        elif asset == 'myprofile_photo':
            e = CUser.objects.get(number=request.user.username)
            form = UserPhotoForm(request.POST, request.FILES, instance=e)
            if form.is_valid():
                form.save()
                return JsonResponse(status=200, data='Changed', safe=False)
            else:
                return JsonResponse(status=203, data=str(form.errors), safe=False)
        elif asset == 'myprofile_photo_view':
            e = UserPhotoForm()
            return render(request, 'pwa/form_provider/file.html', {'form': e, 'referer': asset, 'csrf': csrf,
                                                                   'pure_file': True,
                                                                   'field_name': 'profile'})
        elif asset == 'csrf':
            return JsonResponse(status=203, data={'csrf': csrf}, safe=False)
        else:
            return JsonResponse(status=203, data='Request not Valid', safe=False)

