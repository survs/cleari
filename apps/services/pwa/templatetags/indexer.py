from django import template

register = template.Library()
from apps.modules.receipt.models import Receipt
import datetime
from django.contrib.auth.models import User as DUser
from django.contrib.humanize.templatetags.humanize import naturaltime, naturalday
from django.utils.dateformat import format
from django.utils.dateparse import parse_datetime, parse_date

from apps.modules.users.models import CUser
from apps.modules.message.models import Message

@register.filter
def split_dex(value, num):
    v = str(value).split('-')
    return v[num]


@register.filter
def split_start(value, num):
    value = str(value)
    return value[:num]


@register.filter
def split_end(value, num):
    value = str(value)
    return value[num:]


@register.filter
def compare(value1, value2):
    return True if str(value1) == str(value2) else False


@register.filter
def to_str(value):
    return str(value)


@register.filter
def bytes_to_human(byts):
    kb = byts / 1024
    if kb > 1024:
        mb = kb / 1024
        if mb > 1024:
            gb = mb / 1024
            if gb > 1024:
                tb = gb / 1024
                if tb > 1024:
                    return ''.join([str(round(gb, 3)), ' TB'])
                else:
                    return ''.join([str(round(gb, 1)), ' TB'])
            else:
                return ''.join([str(round(gb, 1)), ' GB'])
        else:
            return ''.join([str(round(mb, 1)), ' MB'])
    else:
        return ''.join([str(round(kb, 1)), ' KB'])


@register.filter
def get_file_name(location):
    d = str(location).split('/')
    return d.pop()


@register.filter
def angular_tags(form):
    s = str(form).replace('<select', '<md-select').replace('</select', '</md-select').replace('<option value="',
                                                                                              '<md-option ng-value="').replace(
        '</option', '</md-option')
    return s


@register.filter
def dpcl(form):
    s = str(form).replace(
        '<input type="text" name="clearance_start" ng-model="form.clearance_start" style="display:none;" required id="id_clearance_start" />',
        '<md-datepicker name="clearance_start" ng-model="form.clearance_start"  md-placeholder="Enter start date" md-open-on-focus></md-datepicker>')
    s = str(s).replace(
        '<input type="text" name="clearance_end" ng-model="form.clearance_end" style="display:none;" required id="id_clearance_end" />',
        '<md-datepicker name="clearance_end" ng-model="form.clearance_end" md-placeholder="Enter due date" md-open-on-focus></md-datepicker>')
    return s


@register.filter
def validate_receipt(r):
    return Receipt.objects.verify(receipt=str(r))


@register.filter
def compare(value1, value2):
    return True if str(value1) == str(value2) else False


@register.filter
def to_str(value):
    return str(value)


@register.filter
def to_date(value):
    result = parse_datetime(str(value)) - datetime.datetime.astimezone(datetime.datetime.now())
    if result.days < -1:
        return ''.join([str(naturalday(parse_date(format(parse_datetime(str(value)), 'Y-m-d')))).title(),
                        ' at ', format(parse_datetime(str(value)), 'g:i A')])
    else:
        return naturaltime(parse_datetime(str(value)))


@register.filter
def bytes_to_human(byts):
    kb = byts / 1024
    if kb > 1024:
        mb = kb / 1024
        if mb > 1024:
            gb = mb / 1024
            if gb > 1024:
                tb = gb / 1024
                if tb > 1024:
                    return ''.join([str(round(gb, 3)), ' TB'])
                else:
                    return ''.join([str(round(gb, 1)), ' TB'])
            else:
                return ''.join([str(round(gb, 1)), ' GB'])
        else:
            return ''.join([str(round(mb, 1)), ' MB'])
    else:
        return ''.join([str(round(kb, 1)), ' KB'])


@register.filter
def get_name(value, typ):
    try:
        user = CUser.objects.get(number=value)
    except CUser.DoesNotExist:
        user = DUser.objects.get(username=value)
    if typ == 'name':
        return str(user.first_name).title()
    elif typ == 'full':
        try:
            surname = ''.join([user.middle_name, ' ']) if user.middle_name else ''
        except AttributeError:
            surname = ''
        return ''.join([user.first_name, ' ', surname, user.last_name]).title()


@register.filter
def get_recipient(user, chat_id):
    return get_name(str(Message.objects.get_recipient(user, chat_id)), 'full')


@register.filter
def get_file_name(location):
    d = str(location).split('/')
    return d.pop()
