from django.conf.urls import include, url
from django.urls import path
from django.views.decorators.http import require_POST

from apps.modules.reports.views import report
from . import views, sse
from .module_view_provider import module_data_provider as mdp, module_view_provider as mvp, user_asset_provider as uap, \
    signatories_data_provider as sdp, data_filter as df, issue_clearance as ic

app_name = 'pwa'

urlpatterns = [
    # Progressive View URLs
    path('welcome', views.welcome, name='welcome'),
    path('home', views.home, name='home'),
    path('home/<str:query>', views.home_ext_provider, name='home_ext_provider'),
    path('profile', views.profile, name='profile'),
    path('profile/provider', views.profile_data_provider, name='profile_provider'),
    path('semester/provider', views.semester_provider, name='semester_provider'),
    path('admin/cpanel/provider', views.admin_cpanel_provider, name='admin_cpanel_provider'),
    path('home/manage/view', views.manage_view_provider, name="manage_view_provider"),
    path('home/manage/<str:card>', views.manage_card_provider, name="manage_card_provider"),
    path('login/<str:typ>', views.login_page, name='login'),
    path('login_submit', require_POST(views.login_submit), name='login'),
    path('verify_user', views.is_login, name='verify_user'),
    path('logout', views.logout_page, name='logout'),
    path('about', views.home, name='about'),
    path('reports', report, name='report'),
    path('timebound/<str:sys>', views.time_bond_provider, name='time_bound'),
    path('liabilities/<str:sem>/s/<str:uid>', views.user_liabilities_api, name="user_liabilities_api"),
    path('liabilities/<str:sem>', views.user_liabilities, name="user_liabilities"),
    path('user_liabilities/<str:sem>', views.signatory_liabilities, name="signatory_liabilities"),
    path('receipt/<str:receipt>', views.user_receipt, name="user_receipt"),
    path('receipt/r/<str:receipt>', views.request_approval, name="request_approval"),
    path('receipt/c/<str:receipt>', views.cancel_request, name="cancel_request"),
    path('receipt/s/<str:receipt>', views.signatory_sign, name="signatory_sign"),
    path('cdt/', include('apps.modules.cdt.pwa_urls', namespace='cdt_module')),
    path('clearance/', include('apps.modules.clearance.pwa_urls', namespace='clearance_module')),
    path('college/', include('apps.modules.college.pwa_urls', namespace='college_module')),
    path('course/', include('apps.modules.course.pwa_urls', namespace='course_module')),
    path('message/', include('apps.modules.message.pwa_urls', namespace='message')),
    path('receipt/', include('apps.modules.receipt.pwa_urls', namespace='receipt_module')),
    path('semester/', include('apps.modules.semester.pwa_urls', namespace='semester_module')),
    path('user/', include('apps.modules.users.pwa_urls', namespace='users_module')),
    path('reports/', include('apps.modules.reports.urls', namespace='reports_module')),
    # Configuration of account
    path('configure/email', views.set_email, name='set_email'),
    path('configure/password', views.change_password, name='set_password'),
    # Module Data Provider
    path('module/provider/<str:module>/<str:rpp>/<str:page>/<str:ob>/<str:search>', mdp, name='module_data_provider'),
    path('module/provider/view/<str:module>', mvp, name='module_view_provider'),
    path('module/clearance/<str:college>', sdp, name='signatories_data_provider'),
    path('module/filter/<str:module>/<str:q>', df, name="data_filter"),
    # User Asset Provider
    path('user/provider/<str:asset>', uap, name="user_asset_provider"),
    # Clearance Issuance
    path('issue/clearance', ic, name='issue_clearance'),
    # Server Sents Implementation
    url(r'^server_time', sse.home),
    # Developer Sandbox
    url(r'^sandbox', views.sandbox, name='sandbox')
]