from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User as DUser
from django.contrib.humanize.templatetags.humanize import ordinal
from django.http import JsonResponse
from django.middleware.csrf import get_token
from django.shortcuts import render, HttpResponse
from django.utils import timezone

from apps.contrib.activity import load_user, sys_monitor
from apps.contrib.auth.forms import UserLogin
from apps.contrib.components import ph_resize
from apps.modules.course.models import Course
from apps.modules.receipt.models import Receipt
from apps.modules.semester.models import Semester
from apps.modules.users.models import CUser, UserStudent, UserSignatory
from .forms import check_password


def index(request):
    """
    Description: Default page of the system
    :param request:
    Contains HTTP META request as Anonymous or Authenticated User
    :return:
        1: Render Cleari Homepage
    """
    context = {
        'data': {
            'vapid_pub_key': settings.WEBPUSH_SETTINGS.get('VAPID_PUBLIC_KEY')
        },
        'app': {
            'startup': {
                'title': 'Loading Cleari',
                'icon': ''.join([settings.STATIC_URL, 'images/icons/apple-152.png'])
            }
        }
    }
    # Render index template into HTML
    return render(request, 'pwa/index.html', context)


def welcome(request):
    return render(request, 'pwa/landing.html')


def is_login(request):
    if request.user.is_authenticated:
        return JsonResponse(status=202, data='Authenticated', safe=False)
    else:
        return JsonResponse(status=204, data='Unauthenticated', safe=False)


def login_page(request, typ):
    csrf = get_token(request)
    if typ == 'csrf':
        return JsonResponse(status=203, data={'csrf': csrf}, safe=False)
    elif typ == 'page':
        return render(request, 'pwa/auth/login.html', {'csrf': csrf})


def login_submit(request):
    # Call User Login form to validate POST data
    ulf = UserLogin(request.POST)
    if ulf.is_valid():
        # Once data is fields are valid it proceeds to django authentication function
        u = authenticate(request, username=ulf.cleaned_data['username'],
                         password=ulf.cleaned_data['password'])
        if not DUser.objects.filter(username=ulf.cleaned_data['username']).exists():
            return JsonResponse(status=203, data={'message': 'User does not Exist', 'code': 'username'}, safe=False)
        x = DUser.objects.get(username=ulf.cleaned_data['username'])
        if not x.check_password(ulf.cleaned_data['password']):
            return JsonResponse(status=203, data={'message': 'Password didn\'t match', 'code': 'password'}, safe=False)
        # Verify authentication data is not NUll
        print(u, ulf.cleaned_data['username'], ulf.cleaned_data['password'])
        if u is not None:
            # Try to login User supplied data
            try:
                du = DUser.objects.get(username=ulf.cleaned_data['username'])
                login(request, u)
                # if du.is_staff or du.is_superuser:
                #    return JsonResponse(status=206, data='Admin User', safe=False)
                return JsonResponse(status=200, data='Success Login', safe=False)
            except:
                return JsonResponse(status=204, data={'message': 'Something went wrong', 'code': 'server'}, safe=False)
        else:
            # Redirect user to login with error message
            return JsonResponse(status=203, data={'message': 'User not found', 'code': 'form'}, safe=False)
    else:
        # Redirect user to login with error message regarding on form validation
        return JsonResponse(status=203, data={'message': str(ulf.errors), 'code': 'form'}, safe=False)


def logout_page(request):
    """
    Description: Allows users to logout from the system
    :param request:
    Contains HTTP META request with user session information
    :return:
        1: Redirect to Login Page
    """
    # Divert Anonymous Users to login page
    if not request.user.is_authenticated:
        return HttpResponse(status=203)
    # Logout the user from the system
    logout(request)
    # Throw Message that the user successfully logout
    return HttpResponse(status=202)


def home(request):
    """
    Description: Default homepage of Cleari Users after logging in
    :param request:
    Contains HTTP META request with authenticated user session
    :returns:
        1: Redirect to Cleari Admin if it's a System Admin
        2: Render Homepage for users
    """
    if not request.user.is_authenticated:
        return JsonResponse(status=204, data='Unauthenticated', safe=False)
    u = request.user.username
    # Get user type form request data
    du = DUser.objects.get(username=u)
    # Verify if the user is a staff or a System admin
    if du.is_staff or du.is_superuser:
        # Redirect to system admin page
        return render(request, 'pwa/templates/admin.html', status=200)
    if not CUser.objects.filter(number=u).exists():
        return JsonResponse(status=307, data='ncu', safe=False)
    typ = CUser.objects.get(number=u).type
    if str(typ).lower() == 'student':
        return render(request, 'pwa/templates/student.html', status=200)
    elif str(typ).lower() == 'signatory':
        return render(request, 'pwa/templates/signatory.html', status=200)
    else:
        return render(request, 'pwa/templates/home.html', status=200)


def sandbox(request):
    return render(request, 'pwa/sandbox.html', status=200)


def admin_cpanel_provider(request):
    if not request.user.is_authenticated:
        return JsonResponse(status=204, data='Unauthenticated', safe=False)
    u = request.user.username
    # Get user type form request data
    du = DUser.objects.get(username=u)
    # Verify if the user is a staff or a System admin
    if du.is_staff or du.is_superuser:
        context = [
            {'name': 'Users',
             'description': 'Manage Admin, Signatory, Student Users',
             'src': 'static/images/svg/use.svg',
             'href': [
                 {'href': '#/home/manage/users', 'name': 'Manage'}
             ]},
            {'name': 'Clearance',
             'description': 'Add, Edit Clearance Duration, Assign Liability to signatories',
             'src': 'static/images/svg/cle.svg',
             'href': [
                 {'href': '#/home/manage/clearance', 'name': 'Manage'}
             ]},
            {'name': 'College',
             'description': 'Manage College Information',
             'src': 'static/images/svg/col.svg',
             'href': [
                 {'href': '#/home/manage/college', 'name': 'Manage'}
             ]},
            {'name': 'Course',
             'description': 'Manage Course Information',
             'src': 'static/images/svg/cou.svg',
             'href': [
                 {'href': '#/home/manage/course', 'name': 'Manage'}
             ]},
            {'name': 'Groups',
             'description': 'Manage Clubs, Organization, Departments and Society',
             'src': 'static/images/svg/gro.svg',
             'href': [
                 {'href': '#/home/manage/cdt', 'name': 'Manage'}
             ]},
            {'name': 'Semester',
             'description': 'Add, Activate, Deactivate, Semesters',
             'src': 'static/images/svg/sem.svg',
             'href': [
                 {'href': '#/home/manage/semester', 'name': 'Manage'}
             ]},
            {'name': 'Receipts',
             'description': 'Manage Student Receipts Issued',
             'src': 'static/images/svg/rec.svg',
             'href': [
                 {'href': '#/home/manage/receipts', 'name': 'Manage'}
             ]},
            {'name': 'Violation',
             'description': 'Check Users had violated',
             'src': 'static/images/svg/vio.svg',
             'href': [
                 {'href': '#/home/manage/violation', 'name': 'Manage'}
             ]}
        ]
        return JsonResponse(data=context, status=200, safe=False)
    else:
        return JsonResponse(status=204, data='Unauthenticated', safe=False)


def manage_card_provider(request, card):
    if not request.user.is_authenticated:
        return JsonResponse(status=204, data='Unauthenticated', safe=False)
    u = request.user.username
    # Get user type form request data
    du = DUser.objects.get(username=u)
    # Verify if the user is a staff or a System admin
    if du.is_staff or du.is_superuser and card:
        if card == 'cdt':
            return render(request, 'pwa/manage/cdt.html', status=200)
        elif card == 'clearance':
            return render(request, 'pwa/manage/clearance.html', status=200)
        elif card == 'college':
            return render(request, 'pwa/manage/college.html', status=200)
        elif card == 'course':
            return render(request, 'pwa/manage/course.html', status=200)
        elif card == 'receipts':
            return render(request, 'pwa/manage/receipt.html', status=200)
        elif card == 'semester':
            return render(request, 'pwa/manage/semester.html', status=200)
        elif card == 'users':
            return render(request, 'pwa/manage/users.html', status=200)
        elif card == 'student':
            return render(request, 'pwa/manage/student.html', status=200)
        elif card == 'violation':
            return render(request, 'pwa/manage/violation.html', status=200)
    else:
        return JsonResponse(status=204, data='Unauthorized', safe=False)


def manage_view_provider(request):
    if not request.user.is_authenticated:
        return JsonResponse(status=204, data='Unauthenticated', safe=False)
    u = request.user.username
    # Get user type form request data
    du = DUser.objects.get(username=u)
    # Verify if the user is a staff or a System admin
    if du.is_staff or du.is_superuser:
        return render(request, 'pwa/manage/manage.html', status=200)
    else:
        return JsonResponse(status=204, data='Unauthorized', safe=False)


def signatory_liabilities(request, sem):
    if not request.user.is_authenticated and UserSignatory.objects.filter(number=request.user.username).exists():
        return JsonResponse(status=403, data='Not allowed to perform')
    return JsonResponse(data=load_user(CUser.objects.get(number=request.user.username), sem), safe=False, status=200)


def user_liabilities_api(request, sem, uid):
    if not request.user.is_authenticated:
        return JsonResponse(status=403, data='Unauthorized')
    if not CUser.objects.filter(number=request.user.username, type_id=3).exists():
        return JsonResponse(status=403, data='Unauthorized', safe=False)
    try:
        u_data = CUser.objects.get(number=uid)
    except CUser.DoesNotExist:
        return HttpResponse(status=417)
    # Query for Student Data from Cleari Database
    try:
        u_student = UserStudent.objects.get(number=uid)
    except CUser.DoesNotExist:
        return HttpResponse(status=417)
    except:
        return JsonResponse(dict([('error', '1'), ('details', 'User does not have Student Information')]), safe=False,
                            status=203)
    # Query for Student Course
    u_course = Course.objects.get(course=u_student.course)
    s_semester = Semester.objects.get(active=True) if sem == 'current' else sem
    try:
        if sem != 'current':
            s_semester = Semester.objects.get(sys=sem)
    except Semester.DoesNotExist:
        return HttpResponse(status=417)
    liabilities = Receipt.objects.filter(receipt__icontains=u_data.number,
                                         receipt__contains=u_course.course,
                                         receipt__startswith=s_semester.sys).order_by('signatory__cdt1__abbreviation')
    valid = sys_monitor('clearance', s_semester.sys).get('valid')
    data = []
    for i in liabilities:
        if valid:
            if not i.is_requested:
                rt = '0'  # Request
            elif not i.received_by and i.is_requested:
                rt = '1'  # Cancel
            else:
                rt = '2'  # Requested
        else:
            if Receipt.objects.verify(i.receipt):
                rt = '2'  # Requested
            else:
                rt = '3'  # Restricted
        if i.received_by and i.is_requested:
            ic = 'call_received'
            dc = 'Request Received'
        elif not i.received_by and i.is_requested:
            ic = 'call_sent'
            dc = 'Request Sent'
        else:
            ic = 'false'
            dc = ''
        dialog_info = dict([('user_id', str(i.signatory.number)),
                            ('profile', ph_resize(i.signatory.number.profile, 150, 150)),
                            ('name', str(i.signatory.number.full_name())),
                            ('position', str(i.signatory.position)),
                            ('office', str(i.signatory.cdt1.name))])
        data.append(dict([
            ('receipt', str(i.receipt)),
            ('liability', str(i.signatory.position)),
            ('status', dict([('icon', 'check_circle' if i.is_cleared else 'cancel'),
                             ('description', 'Signed' if i.is_cleared else 'Not Signed')])),
            ('valid', dict([('icon', 'fingerprint' if Receipt.objects.verify(i.receipt) else 'report'),
                            ('description',
                             'Signature Verified' if Receipt.objects.verify(i.receipt) else 'Signature Invalid')])),
            ('office', str(i.signatory.cdt1.abbreviation)),
            ('request_status', dict([('icon', ic),
                                     ('description', dc)])),
            ('request_status_code', str(rt)),
            ('dialog_info', dialog_info)
        ]))
    return JsonResponse(data, safe=False, status=200)


def user_liabilities(request, sem):
    if not request.user.is_authenticated:
        return JsonResponse(status=403, data='Not allowed to perform')
    try:
        u_data = CUser.objects.get(number=request.user.username)
    except CUser.DoesNotExist:
        return HttpResponse(status=417)
    # Query for Student Data from Cleari Database
    try:
        u_student = UserStudent.objects.get(number=request.user.username)
    except CUser.DoesNotExist:
        return HttpResponse(status=417)
    except:
        return JsonResponse(dict([('error', '1'), ('details', 'User does not have Student Information')]), safe=False,
                            status=203)
    # Query for Student Course
    u_course = Course.objects.get(course=u_student.course)
    s_semester = Semester.objects.get(active=True) if sem == 'current' else sem
    try:
        if sem != 'current':
            s_semester = Semester.objects.get(sys=sem)
    except Semester.DoesNotExist:
        return HttpResponse(status=417)
    liabilities = Receipt.objects.filter(receipt__icontains=u_data.number,
                                         receipt__contains=u_course.course,
                                         receipt__startswith=s_semester.sys).order_by('signatory__cdt1__abbreviation')
    valid = sys_monitor('clearance', s_semester.sys).get('valid')
    data = []
    for i in liabilities:
        if valid:
            if not i.is_requested:
                rt = '0'  # Request
            elif not i.received_by and i.is_requested:
                rt = '1'  # Cancel
            else:
                rt = '2'  # Requested
        else:
            if Receipt.objects.verify(i.receipt):
                rt = '2'  # Requested
            else:
                rt = '3'  # Restricted
        if i.received_by and i.is_requested:
            ic = 'call_received'
            dc = 'Request Received'
        elif not i.received_by and i.is_requested:
            ic = 'call_sent'
            dc = 'Request Sent'
        else:
            ic = 'false'
            dc = ''
        dialog_info = dict([('user_id', str(i.signatory.number)),
                            ('profile', ph_resize(i.signatory.number.profile, 150, 150)),
                            ('name', str(i.signatory.number.full_name())),
                            ('position', str(i.signatory.position)),
                            ('office', str(i.signatory.cdt1.name))])
        data.append(dict([
            ('receipt', str(i.receipt)),
            ('full_name', i.number.full_name()),
            ('receipts', [entry for entry in i.number.receipt_set.values()]),
            ('liability', str(i.signatory.position)),
            ('status', dict([('icon', 'check_circle' if i.is_cleared else 'cancel'),
                             ('description', 'Signed' if i.is_cleared else 'Not Signed')])),
            ('valid', dict([('icon', 'fingerprint' if Receipt.objects.verify(i.receipt) else 'report'),
                            ('description',
                             'Signature Verified' if Receipt.objects.verify(i.receipt) else 'Signature Invalid')])),
            ('office', str(i.signatory.cdt1.abbreviation)),
            ('request_status', dict([('icon', ic),
                                     ('description', dc)])),
            ('request_status_code', str(rt)),
            ('dialog_info', dialog_info)
        ]))
    return JsonResponse(data, safe=False, status=200)


def user_receipt(request, receipt):
    if not request.user.is_authenticated:
        return JsonResponse(status=403, data='Not allowed to perform')
    r = Receipt.objects.filter(receipt=receipt)
    if r.exists():
        d = r.first()
        context = {
            'receipt': str(d.receipt),
            'number': str(d.number),
            'name': str(d.number.full_name()),
            'remarks': str(d.remarks),
            'is_cleared': str(d.is_cleared),
            'is_requested': str(d.is_requested),
            'requested_date': str(d.requested_date),
            'received_by': str(d.received_by),
            'received_date': str(d.received_date),
            'position': str(d.liability.signatory),
            'liability_id': str(d.liability),
            'liability_description': str(d.signatory.cdt1.abbreviation),
            'valid': str(Receipt.objects.verify(str(d.receipt))),
            'signature': str(d.signature)
        }
        return JsonResponse(context, status=200, safe=False)
    else:
        return HttpResponse(status=404)


def profile_data_provider(request):
    """
    Description: Default homepage of Cleari Users after logging in
    :param request:
    Contains HTTP META request with authenticated user session
    :returns:
        1: Redirect to Cleari Admin if it's a System Admin
        2: Render Homepage for users
    """
    if not request.user.is_authenticated:
        return JsonResponse(status=204, data='Unauthenticated', safe=False)
    # Get user type form request data
    if not CUser.objects.filter(number=request.user.username).exists():
        return JsonResponse(status=203, data='ncu', safe=False)
    user = CUser.objects.get(number=request.user.username)
    t = str(user.type).lower()
    photo = ph_resize(user.profile, 150, 150)
    if t == 'student':
        try:
            ext = dict([('course', dict([('abbreviation', user.userstudent.course.course),
                                         ('description', user.userstudent.course.description),
                                         ('department',
                                          dict([('abbreviation', user.userstudent.course.department.abbreviation),
                                                ('name', user.userstudent.course.department.name)]))])),
                        ('year', user.userstudent.year),
                        ('college', dict([('name', user.userstudent.course.college.college),
                                          ('description', user.userstudent.course.college.description)]))])
        except:
            ext = dict([('error', '1'), ('details', 'User does not have Student Information')])
    elif t == 'signatory':
        try:
            ext = dict([('cdt', dict([('name', user.usersignatory.cdt1.name),
                                      ('type', str(user.usersignatory.cdt1.type).title()),
                                      ('college', dict([('name', user.usersignatory.cdt1.college.college),
                                                        ('description', user.usersignatory.cdt1.college.description)])),
                                      ('abbreviation', user.usersignatory.cdt1.abbreviation)])),
                        ('assign', user.usersignatory.assign),
                        ('position', str(user.usersignatory.position).title())])
        except:
            ext = dict([('error', '1'), ('details', 'User does not have Signatory Information')])
    elif t == 'admin':
        ext = dict([('admin', 'Admin')])
    else:
        ext = 'None'
    # Render Homepage
    context = dict([('user', dict([('photo', str(photo).replace("b'", 'data:image/png;base64,').replace("='", "=")),
                                   ('number', user.number),
                                   ('full_name', user.full_name()),
                                   ('address', user.address),
                                   ('birth_date', user.birth_date),
                                   ('type', str(user.type).lower())])),
                    ('extra', ext)])
    return JsonResponse(data=context, status=200, safe=False)


def semester_provider(request):
    data = sys_monitor('schoolsem', 'current')
    return JsonResponse(data, status=200)


def profile(request):
    if not request.user.is_authenticated:
        return JsonResponse(status=204, data='Unauthenticated', safe=False)
    # Get user type form request data
    if not CUser.objects.filter(number=request.user.username).exists():
        return JsonResponse(status=307, data='ncu', safe=False)
    user = CUser.objects.get(number=request.user.username)
    t = str(user.type).lower()
    if t == 'student':
        return render(request, 'pwa/profile/student.html')
    elif t == 'signatory':
        return render(request, 'pwa/profile/signatory.html')
    elif t == 'admin':
        return render(request, 'pwa/profile/admin.html')


def home_ext_provider(request, query):
    if not request.user.is_authenticated:
        return JsonResponse(status=204, data='Unauthenticated', safe=False)
    try:
        user = CUser.objects.get(number=request.user.username)
    except CUser.DoesNotExist:
        return HttpResponse(status=417)
    typ_student = str(user.type).lower() == 'student'
    typ_admin = str(user.type).lower() == 'admin'
    typ_signatory = str(user.type).lower() == 'signatory'
    if query == 'STUDENT_menu' and typ_student:
        menu = [
            {'icon': 'home', 'name': 'Home', 'href': '#/home'},
            {'icon': 'account_circle', 'name': 'My Profile', 'href': '#/profile'},
            {'icon': 'inbox', 'name': 'Inbox', 'href': '#/inbox'},
            {'icon': 'close', 'name': 'Logout', 'href': '#/logout'},
            {'icon': 'developer_mode', 'name': 'Dev Sandbox', 'href': '#/sandbox'},
        ]
        return JsonResponse(menu, status=200, safe=False)
    elif query == 'STUDENT_options' and typ_student:
        options = [
            {'icon': 'settings', 'name': 'Settings', 'href': '#/settings'},
            {'icon': 'info', 'name': 'About', 'href': '#/about'},
        ]
        return JsonResponse(options, status=200, safe=False)
    elif query == 'SIGNATORY_menu' and typ_signatory:
        menu = [
            {'icon': 'home', 'name': 'Home', 'href': '#/home'},
            {'icon': 'account_circle', 'name': 'My Profile', 'href': '#/profile'},
            {'icon': 'insert_chart', 'name': 'Reports', 'href': '#/reports'},
            {'icon': 'inbox', 'name': 'Inbox', 'href': '#/inbox'},
            {'icon': 'close', 'name': 'Logout', 'href': '#/logout'},
        ]
        return JsonResponse(menu, status=200, safe=False)
    elif query == 'SIGNATORY_options' and typ_signatory:
        options = [
            {'icon': 'settings', 'name': 'Settings', 'href': '#/settings'},
            {'icon': 'info', 'name': 'About', 'href': '#/about'},
        ]
        return JsonResponse(options, status=200, safe=False)
    elif query == 'ADMIN_menu' and typ_admin:
        menu = [
            {'icon': 'home', 'name': 'Home', 'href': '#/home'},
            {'icon': 'account_circle', 'name': 'My Profile', 'href': '#/profile'},
            {'icon': 'build', 'name': 'Backend Administration', 'href': '/admin'},
            {'icon': 'inbox', 'name': 'Inbox', 'href': '#/inbox'},
            {'icon': 'close', 'name': 'Logout', 'href': '#/logout'},
            {'icon': 'developer_mode', 'name': 'Dev Sandbox', 'href': '#/sandbox'},
        ]
        return JsonResponse(menu, status=200, safe=False)
    elif query == 'ADMIN_options' and typ_admin:
        options = [
            {'icon': 'settings', 'name': 'Settings', 'href': '#/settings'},
            {'icon': 'info', 'name': 'About', 'href': '#/about'},
        ]
        return JsonResponse(options, status=200, safe=False)
    elif query == 'user_info':
        user = {
            'name': user.full_name(),
            'username': str(user.number),
            'type': str(user.type)
        }
        return JsonResponse(user, status=200, safe=False)
    else:
        return HttpResponse(status=417)


def time_bond_provider(request, sys):
    data = sys_monitor('clearance', sys)
    return JsonResponse(data, status=200)


def sw(request):
    return render(request, 'pwa/sw.html', content_type="application/x-javascript")


def select_semester(request, sy, sem):
    usr = CUser.objects.get(number=request.user.username)
    try:
        smr = Semester.objects.filter(school_year=sy, semester=sem, sys=''.join([sem, sy]))
        if smr.exists():
            data = Semester.objects.get(sys=''.join([sem, sy]))
            return render(request, 'core/home.html', load_user(usr, sem=data))
        else:
            messages.add_message(request, messages.ERROR, 'School Year & Semester Not found')
            return render(request, 'core/home.html', load_user(usr, sem='current'))
    except:
        messages.add_message(request, messages.ERROR, 'School Year & Semester Not found')
        return render(request, 'core/home.html', load_user(usr, sem='current'))


def export_form(request):
    return render(request, 'pwa/form_provider/form.html')


def inbox_provider(request):
    return HttpResponse(status=200, content='Message API not yet docked');


def request_approval(request, receipt):
    """
    Description: Allows student to request clearance to be signed
    :param request:
    Contains HTTP META request with signatory information
    :param liability_id:
    Liability ID in String form: Ex. liability_id=SEMESTER_ID-LIABILITY_ID-COURSE_ID
    :param office:
    Office ID in String form Ex. office=OFFICE_ID
    :return:
        1: Successfully sent signing request
        2: Unsuccessful in sending request
    """
    # Verify if receipt exists on database
    if Receipt.objects.filter(receipt=receipt).exists():
        # Prepare to write information on is_requested to TRUE
        r = Receipt.objects.get(receipt=receipt)
        r.is_requested = True
        r.requested_date = timezone.localtime()
        # Save Information into the database
        r.save()
        # Show Success message and then redirect to homepage
        return JsonResponse(status=200, data='Request Successfully Sent, Stay tuned for Respected Signatory Response',
                            safe=False)
    else:
        # Show error(s) message and the redirect to homepage
        return JsonResponse(status=203, data='Receipt Not Found, Please contact System Administrator', safe=False)


def signatory_sign(request, receipt):
    if not request.user.is_authenticated:
        return JsonResponse(status=204, data='Unauthenticated', safe=False)
    if not CUser.objects.filter(number=request.user.username, type_id=3).exists():
        return JsonResponse(status=204, data='Unauthorized', safe=False)
    if Receipt.objects.filter(receipt=receipt).exists() and request.method == 'POST':
        remarks = request.POST['remarks']
        r = Receipt.objects.sign(request.user.username, receipt, remarks)
        return JsonResponse(data=r, status=200, safe=False)
    else:
        return JsonResponse(data="Sign Request Failed", status=203, safe=False)


def cancel_request(request, receipt):
    """
    Description: Allows student to request clearance to be signed
    :param request:
    Contains HTTP META request with signatory information
    :param liability_id:
    Liability ID in String form: Ex. liability_id=SEMESTER_ID-LIABILITY_ID-COURSE_ID
    :param office:
    Office ID in String form Ex. office=OFFICE_ID
    :return:
        1: Successfully sent signing request
        2: Unsuccessful in sending request
    """
    # Verify if receipt exists on database
    if Receipt.objects.filter(receipt=receipt).exists():
        # Prepare to write information on is_requested to TRUE
        r = Receipt.objects.get(receipt=receipt)
        r.is_requested = False
        r.requested_date = None
        # Save Information into the database
        r.save()
        # Show Success message and then redirect to homepage
        return JsonResponse(status=200, data='Request Successfully Canceled',
                            safe=False)
    else:
        # Show error(s) message and the redirect to homepage
        return JsonResponse(status=200, data='Request Unsuccessfully Canceled',
                            safe=False)


def pub_verify(request, code):
    from django.core import signing
    try:
        load = signing.loads(code)
        uid = load.get('uid')
        pk = load.get('pk')
        lst = Receipt.objects.filter(number__number=uid, liability_id=pk)
        user = UserStudent.objects.get(number__number=uid)
        context = {
            "title": "College Clearance of %s" % user.number.full_name(),
            "scope": "College",
            "clearance_list": lst,
            "report_name": "College Clearance of %s" % user.number.full_name(),
            "status": lst.first().liability.clearance.clearanceuserstorage_set.first().status,
            "name": user.number.full_name(),
            "college": user.course.college.college.upper(),
            "course": user.course.course.upper(),
            "year": ordinal(user.year),
            "url":'unset',
            "by": request.user.cuser.full_name(),
            "date": timezone.localtime().now(),
            "college_desc": user.course.college.description.title(),
            "department_name": user.course.department.name.title(),
            "sy": "%s Semester - S.Y. %s-%s" % (ordinal(lst.first().liability.clearance.semester.semester),
                                                str(lst.first().liability.clearance.semester.school_year).split(':')[0],
                                                str(lst.first().liability.clearance.semester.school_year).split(':')[1])
        }
        return render(request, 'pwa/reports/clearance_web.html', context)
    except:
        return HttpResponse('Bad Public Code')


def set_email(request):
    if not request.user.is_authenticated:
        return JsonResponse(status=204, data='Unauthenticated', safe=False)
    # Get user type form request data
    if not CUser.objects.filter(number=request.user.username).exists():
        return JsonResponse(status=307, data='Unauthorized', safe=False)
    if request.method == 'POST':
       try:
            r = request.POST
            u = DUser.objects.get(username=request.user.username)
            u.email = r['email']
            u.save()
            return JsonResponse(data='Email Successfully Set', safe=False)
       except:
            return JsonResponse(data='Something went wrong', safe=False)
    else:
        uf = DUser.objects.filter(username=request.user.username).exists()
        u = DUser.objects.get(username=request.user.username)
        if uf:
            return JsonResponse(data={'email': str(u.email)}, safe=False)
        else:
            return JsonResponse(data='Invalid Request', safe=False)


def change_password(request):
    if not request.user.is_authenticated:
        return JsonResponse(status=204, data='Unauthenticated', safe=False)
    # Get user type form request data
    if not CUser.objects.filter(number=request.user.username).exists():
        return JsonResponse(status=307, data='Unauthorized', safe=False)
    if request.method == 'POST':
        try:
            r = request.POST
            if check_password(r['old'], request.user.password):
                u = DUser.objects.get(username=request.user.username)
                u.set_password(r['new'])
                u.save()
                return JsonResponse(data='Password Changed', safe=False)
            else:
                return JsonResponse(data='Wrong old password', safe=False)
        except:
            return JsonResponse(data='Something went wrong', status=203, safe=False)
    else:
        return JsonResponse(data='Invalid request', status=203, safe=False)