import json

from django.contrib.humanize.templatetags.humanize import ordinal
from django.contrib.postgres.search import SearchVector
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import FieldDoesNotExist
from django.db.models import QuerySet

from apps.contrib.components import ph_resize
from apps.modules.cdt.models import CDT1, CDT2, CDT3
from apps.modules.clearance.models import Clearance, ClearanceDuration, ClearanceDependency
from apps.modules.college.models import College
from apps.modules.course.models import Course
from apps.modules.receipt.models import Receipt
from apps.modules.semester.models import Semester
from apps.modules.users.models import UserStudent, UserSignatory, CUser, CUserType
from apps.modules.violation.models import Violation


# This is not the best solution for searching and filtering!
# Very expensive memory allocation
# Next Sprint would be the next improvement


def cdt1(rpp, page, ob, search):
    cdt1_data = CDT1.objects.all()
    try:
        if ob:
            CDT1._meta.get_field(str(ob).replace('-', '').split('__')[0])
            cdt1_data = cdt1_data.order_by(ob)
        else:
            cdt1_data = cdt1_data.order_by()
    except FieldDoesNotExist:
        cdt1_data = cdt1_data.order_by()
    if search == 'unset':
        return cdt1_manifest(cdt1_data, rpp, page, cdt1_data.count())
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            ac = []
            r = []
            for i in cdt1_data:
                ac.append(dict([('module', 'cdt1'),
                                ('id', i.pk),
                                ('abbreviation', i.abbreviation),
                                ('college', i.college.college),
                                ('name', i.name),
                                ('type', i.type.type)]))
            if prop == 'generic':
                cdt1_data = cdt1_data.annotate(search=SearchVector('abbreviation') +
                                               SearchVector('college__college') +
                                               SearchVector('name') +
                                               SearchVector('type_type')).filter(search=val)
            elif prop == 'abbreviation':
                cdt1_data = cdt1_data.annotate(search=SearchVector('abbreviation')).filter(search=val)
            elif prop == 'college':
                cdt1_data = cdt1_data.annotate(search=SearchVector('college__college')).filter(search=val)
            elif prop == 'name':
                cdt1_data = cdt1_data.annotate(search=SearchVector('name')).filter(search=val)
            elif prop == 'type':
                cdt1_data = cdt1_data.annotate(search=SearchVector('type_type')).filter(search=val)
            else:
                return 'Search is not Valid'
            return cdt1_manifest(cdt1_data, rpp, page, cdt1_data.__len__())
        except FileNotFoundError:
            return 'Search is not Valid'


def cdt1_manifest(r, rpp, page, count):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    t = []
    ac = []
    for i in c:
        ac.append(dict([('module', 'cdt1'),
                        ('id', i.pk),
                        ('abbreviation', i.abbreviation),
                        ('college', i.college.college),
                        ('name', i.name),
                        ('type', i.type.type)]))
    manifest = dict([('rows', ac),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest


def cdt2(rpp, page, ob, search):
    cdt2_data = CDT2.objects.all()
    try:
        if ob:
            CDT2._meta.get_field(str(ob).replace('-', '').split('__')[0])
            cdt2_data = cdt2_data.order_by(ob)
        else:
            cdt2_data = cdt2_data.order_by()
    except FieldDoesNotExist:
        cdt2_data = cdt2_data.order_by()
    if search == 'unset':
        return cdt2_manifest(cdt2_data, rpp, page, cdt2_data.count())
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            if prop == 'generic':
                cdt2_data = cdt2_data.annotate(search=SearchVector('type')).filter(search=val)
            elif prop == 'type':
                cdt2_data = cdt2_data.annotate(search=SearchVector('type')).filter(search=val)
            else:
                return 'Search is not Valid'
            return cdt2_manifest(cdt2_data, rpp, page, cdt2_data.__len__())
        except:
            return 'Search is not Valid'


def cdt2_manifest(r, rpp, page, count):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    t = []
    ac = []
    for j in c:
        ac.append(dict([('module', 'cdt2'),
                        ('id', j.pk),
                        ('type', j.type)]))
    manifest = dict([('rows', ac),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest


def cdt3(rpp, page, ob, search):
    cdt3_data = CDT3.objects.all()
    try:
        if ob:
            CDT3._meta.get_field(str(ob).replace('-', '').split('__')[0])
            cdt3_data = cdt3_data.order_by(ob)
        else:
            cdt3_data = cdt3_data.order_by()
    except FieldDoesNotExist:
        cdt3_data = cdt3_data.order_by()
    if search == 'unset':
        return cdt3_manifest(cdt3_data, rpp, page, cdt3_data.count())
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            if prop == 'generic':
                cdt3_data = cdt3_data.annotate(search=SearchVector('position')).filter(search=val)
            elif prop == 'position':
                cdt3_data = cdt3_data.annotate(search=SearchVector('position')).filter(search=val)
            else:
                return 'Search is not Valid'
            return cdt3_manifest(cdt3_data, rpp, page, cdt3_data.__len__())
        except:
            return 'Search is not Valid'


def cdt3_manifest(r, rpp, page, count):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    ac = []
    for k in c:
        ac.append(dict([('module', 'cdt3'),
                        ('id', k.pk),
                        ('position', k.position)]))
    manifest = dict([('rows', ac),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest


def clearance(rpp, page, ob, search):
    c_data = Clearance.objects.all()
    try:
        if ob:
            Clearance._meta.get_field(str(ob).replace('-', '').split('__')[0])
            c_data = c_data.order_by(ob)
        else:
            c_data = c_data.order_by()
    except FieldDoesNotExist:
        c_data = c_data.order_by()
    if search == 'unset':
        return clearance_manifest(c_data, rpp, page, c_data.count())
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            if prop == 'generic':
                c_data = c_data.annotate(search=SearchVector('name') +
                                         SearchVector('semester') +
                                         SearchVector('college_college')).filter(search=val)
            elif prop == 'active':
                c_data = c_data.annotate(search=SearchVector('active')).filter(search=val)
            elif prop == 'name':
                c_data = c_data.annotate(search=SearchVector('name')).filter(search=val)
            elif prop == 'semester':
                c_data = c_data.annotate(search=SearchVector('semester')).filter(search=val)
            elif prop == 'college':
                c_data = c_data.annotate(search=SearchVector('college_college')).filter(search=val)
            else:
                return 'Search is not Valid'
            return clearance_manifest(c_data, rpp, page, c_data.__len__())
        except:
            return 'Search is not Valid'


def clearance_manifest(r, rpp, page, count):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    ac = []
    for j in c:
        m = ClearanceDependency.objects.filter(clearance_id=j.pk)
        p = []
        for l in m:
            p.append(dict([('signatory', l.signatory.number.full_name()),
                           ('assign', 'Assigned' if l.signatory.assign else 'Not Assigned'),
                           ('position', l.signatory.position.position)]))
        ac.append(dict([('id', j.pk),
                        ('active', 'Active' if j.active else 'Inactive'),
                        ('name', j.name),
                        ('semester', str(j.semester)),
                        ('college', j.college.college),
                        ('signatories', p)]))
    manifest = dict([('rows', ac),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest


def clearance_duration(rpp, page, ob, search):
    c_data = ClearanceDuration.objects.all()
    try:
        if ob:
            ClearanceDuration._meta.get_field(str(ob).replace('-', '').split('__')[0])
            c_data = c_data.order_by(ob)
        else:
            c_data = c_data.order_by()
    except FieldDoesNotExist:
        c_data = c_data.order_by()
    if search == 'unset':
        return clearance_duration_manifest(c_data, rpp, page, c_data.count())
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            r = []
            ac = []
            for i in c_data:
                d = str(i.sys.school_year).split(':')
                ac.append(dict([('id', i.pk),
                                ('name', ''.join([' Semester, S.Y.', d[0], '-', d[1]])),
                                ('active', 'Active' if i.sys.active else 'Inactive'),
                                ('sy', i.sys.school_year),
                                ('sem', str(ordinal(i.sys.semester))),
                                ('start', str(i.clearance_start)),
                                ('end', str(i.clearance_end))]))
            if prop == 'generic':
                for j in ac:
                    if str(j.get('name')).find(val) >= 0 or str(j.get('active')).find(val) >= 0 or \
                            str(j.get('sy')).find(val) >= 0 or str(j.get('sem')).find(val) >= 0 or \
                            str(j.get('start')).find(val) >= 0 or str(j.get('end')).find(val) >= 0:
                        r.append(j)
            elif prop == 'name':
                for j in ac:
                    if str(j.get('name')).find(val) >= 0:
                        r.append(j)
            elif prop == 'active':
                for j in ac:
                    if str(j.get('active')).find(val) >= 0:
                        r.append(j)
            elif prop == 'sy':
                for j in ac:
                    if str(j.get('sy')).find(val) >= 0:
                        r.append(j)
            elif prop == 'sem':
                for j in ac:
                    if str(j.get('sem')).find(val) >= 0:
                        r.append(j)
            elif prop == 'start':
                for j in ac:
                    if str(j.get('start')).find(val) >= 0:
                        r.append(j)
            elif prop == 'end':
                for j in ac:
                    if str(j.get('end')).find(val) >= 0:
                        r.append(j)
            else:
                return 'Search is not Valid'
            return clearance_manifest(r, rpp, page, r.__len__())
        except:
            return 'Search is not Valid'


def clearance_duration_manifest(r, rpp, page, count):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    t = []
    if type(r) == QuerySet:
        ac = []
        for i in c:
            d = str(i.sys.school_year).split(':')
            ac.append(dict([('id', i.pk),
                            ('name', ''.join([' Semester, S.Y.', d[0], '-', d[1]])),
                            ('active', 'Active' if i.sys.active else 'Inactive'),
                            ('sy', i.sys.school_year),
                            ('sem', str(ordinal(i.sys.semester))),
                            ('start', str(i.clearance_start)),
                            ('end', str(i.clearance_end))]))
        t = ac
    elif type(r) == list:
        ac = []
        for i in c:
            ac.append(dict([('id', i.get('id')),
                            ('name', i.get('school_year')),
                            ('active', i.get('active')),
                            ('sy', i.get('school_year')),
                            ('sem', i.get('semester')),
                            ('start', i.get('clearance_start')),
                            ('end', i.get('clearance_end'))]))
        t = ac
    manifest = dict([('rows', t),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest


def college(rpp, page, ob, search):
    c_data = College.objects.all()
    try:
        if ob:
            College._meta.get_field(str(ob).replace('-', '').split('__')[0])
            c_data = c_data.order_by(ob)
        else:
            c_data = c_data.order_by()
    except FieldDoesNotExist:
        c_data = c_data.order_by()
    if search == 'unset':
        return college_manifest(c_data, rpp, page, c_data.count())
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            if prop == 'generic':
                c_data = c_data.annotate(search=SearchVector('college') +
                                                SearchVector('description')).filter(search=val)
            elif prop == 'college':
                c_data = c_data.annotate(search=SearchVector('college')).filter(search=val)
            elif prop == 'description':
                c_data = c_data.annotate(search=SearchVector('description')).filter(search=val)
            else:
                return 'Search is not Valid'
            return college_manifest(c_data, rpp, page, c_data.__len__())
        except:
            return 'Search is not Valid'


def college_manifest(r, rpp, page, count):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    ac = []
    for i in c:
        ac.append(dict([('id', i.pk),
                        ('college', i.college),
                        ('description', i.description)]))
    manifest = dict([('rows', ac),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest


def course(rpp, page, ob, search):
    c_data = Course.objects.all()
    try:
        if ob:
            Course._meta.get_field(str(ob).replace('-', '').split('__')[0])
            c_data = c_data.order_by(ob)
        else:
            c_data = c_data.order_by()
    except FieldDoesNotExist:
        c_data = c_data.order_by()
    if search == 'unset':
        return course_manifest(c_data, rpp, page, c_data.count())
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            if prop == 'generic':
                c_data = c_data.annotate(search=SearchVector('course') +
                                                SearchVector('description') +
                                                SearchVector('department_abbreviation') +
                                                SearchVector('department_name') +
                                                SearchVector('college_college') +
                                                SearchVector('college_description')).filter(search=val)
            elif prop == 'course':
                c_data = c_data.annotate(search=SearchVector('course')).filter(search=val)
            elif prop == 'description':
                c_data = c_data.annotate(search=SearchVector('description')).filter(search=val)
            elif prop == 'department':
                c_data = c_data.annotate(search=SearchVector('department_abbreviation') +
                                         SearchVector('department_name')).filter(search=val)
            elif prop == 'college':
                c_data = c_data.annotate(search=SearchVector('college_college') +
                                         SearchVector('college_description')).filter(search=val)
            else:
                return 'Search is not Valid'
            return course_manifest(c_data, rpp, page, c_data.__len__())
        except:
            return 'Search is not Valid'


def course_manifest(r, rpp, page, count):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    ac = []
    for i in c:
        ac.append(dict([('id', i.pk),
                        ('course', i.course),
                        ('description', i.description),
                        ('department_id', i.department_id),
                        ('department', i.department.abbreviation),
                        ('department_name', i.department.name),
                        ('college_id', i.college_id),
                        ('college', i.college.college),
                        ('college_desc', i.college.description)]))
    manifest = dict([('rows', ac),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest


def receipt(rpp, page, ob, search):
    receipt_data = Receipt.objects.all()
    try:
        if ob:
            Receipt._meta.get_field(str(ob).replace('-', '').split('__')[0])
            receipt_data = receipt_data.order_by(ob)
        else:
            receipt_data = receipt_data.order_by()
    except FieldDoesNotExist:
        receipt_data = receipt_data.order_by()
    if search == 'unset':
        return receipt_manifest(receipt_data, rpp, page, receipt_data.count())
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            ac = []
            r = []
            for i in receipt_data:
                ac.append(dict([('id', i.pk),
                                ('receipt', i.receipt),
                                ('receipts', [entry for entry in i.number.receipt_set.values()]),
                                ('full_name', i.number.full_name()),
                                ('number', i.number_id),
                                ('remarks', i.remarks),
                                ('cleared', i.is_cleared),
                                ('requested', i.is_requested),
                                ('request_date', i.requested_date),
                                ('receive_by', i.received_by),
                                ('received_date', i.received_date),
                                ('liability', i.liability.clearance.name),
                                ('signature', i.signature),
                                ('verified', Receipt.objects.verify(i.receipt))]))
            if prop == 'generic':
                for j in ac:
                    if str(j.get('receipt')).find(val) >= 0 or str(j.get('number')).find(val) >= 0 or \
                            str(j.get('remarks')).find(val) >= 0 or str(j.get('cleared')).find(val) >= 0 or \
                            str(j.get('requested')).find(val) >= 0 or str(j.get('request_date')).find(val) >= 0 or \
                            str(j.get('receive_by')).find(val) >= 0 or str(j.get('receive_date')).find(val) >= 0 or \
                            str(j.get('liability')).find(val) >= 0 or str(j.get('signature')).find(val) >= 0 or \
                            str(j.get('verified')).find(val) >= 0:
                        r.append(j)
            elif prop == 'receipt':
                for j in ac:
                    if str(j.get('receipt')).find(val) >= 0:
                        r.append(j)
            elif prop == 'number':
                for j in ac:
                    if str(j.get('number')).find(val) >= 0:
                        r.append(j)
            elif prop == 'remarks':
                for j in ac:
                    if str(j.get('remarks')).find(val) >= 0:
                        r.append(j)
            elif prop == 'cleared':
                for j in ac:
                    if str(j.get('cleared')).find(val) >= 0:
                        r.append(j)
            elif prop == 'requested':
                for j in ac:
                    if str(j.get('requested')).find(val) >= 0:
                        r.append(j)
            elif prop == 'request_date':
                for j in ac:
                    if str(j.get('request_date')).find(val) >= 0:
                        r.append(j)
            elif prop == 'receive_by':
                for j in ac:
                    if str(j.get('receive_by')).find(val) >= 0:
                        r.append(j)
            elif prop == 'receive_date':
                for j in ac:
                    if str(j.get('receive_date')).find(val) >= 0:
                        r.append(j)
            elif prop == 'liability':
                for j in ac:
                    if str(j.get('liability')).find(val) >= 0:
                        r.append(j)
            elif prop == 'signature':
                for j in ac:
                    if str(j.get('signature')).find(val) >= 0:
                        r.append(j)
            elif prop == 'verified':
                for j in ac:
                    if str(j.get('verified')).find(val) >= 0:
                        r.append(j)
            else:
                return 'Search is not Valid'
            print(r.__len__())
            return receipt_manifest(r, rpp, page, r.__len__())
        except:
            return 'Search is not Valid'


def receipt_manifest(r, rpp, page, count):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    t = []
    if type(r) == QuerySet:
        ar = []
        for i in c:
            ar.append(dict([('id', i.pk),
                            ('receipt', i.receipt),
                            ('receipts', [entry for entry in i.number.receipt_set.values()]),
                            ('full_name', i.number.full_name()),
                            ('number', i.number_id),
                            ('remarks', i.remarks),
                            ('cleared', i.is_cleared),
                            ('requested', i.is_requested),
                            ('request_date', i.requested_date),
                            ('receive_by', i.received_by),
                            ('received_date', i.received_date),
                            ('liability', i.liability.clearance.name),
                            ('signature', i.signature),
                            ('verified', Receipt.objects.verify(i.receipt))]))
        t = ar
    elif type(r) == list:
        ar = []
        for i in c:
            ar.append(dict([('id', i.get('id')),
                            ('receipt', i.get('receipt')),
                            ('full_name', i.get('full_name')),
                            ('receipts', i.get('receipts')),
                            ('number', i.get('number_id')),
                            ('remarks', i.get('remarks')),
                            ('cleared', i.get('is_cleared')),
                            ('requested', i.get('is_requested')),
                            ('request_date', i.get('requested_date')),
                            ('receive_by', i.get('received_by')),
                            ('received_date', i.get('received_date')),
                            ('liability', i.get('liability')),
                            ('signature', i.get('signature')),
                            ('verified', i.get('receipt'))]))
        t = ar
    manifest = dict([('rows', t),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest


def semester(rpp, page, ob, search):
    c_data = Semester.objects.all()
    try:
        if ob:
            Semester._meta.get_field(str(ob).replace('-', '').split('__')[0])
            c_data = c_data.order_by(ob)
        else:
            c_data = c_data.order_by()
    except FieldDoesNotExist:
        c_data = c_data.order_by()
    if search == 'unset':
        return semester_manifest(c_data, rpp, page, c_data.count())
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            ac = []
            r = []
            for i in c_data:
                d = str(i.school_year).split(':')
                ac.append(dict([('id', i.pk),
                                ('sys', i.sys),
                                ('active', 'Active' if i.active else 'Inactive'),
                                ('school_year', ''.join([d[0], '-', d[1]])),
                                ('semester', ordinal(i.semester))]))
            if prop == 'generic':
                for j in ac:
                    if str(j.get('sys')).find(val) >= 0 or str(j.get('active')).find(val) >= 0 or \
                            str(j.get('school_year')).find(val) >= 0 or str(j.get('semester')).find(val) >= 0:
                        r.append(j)
            elif prop == 'sys':
                for j in ac:
                    if str(j.get('sys')).find(val) >= 0:
                        r.append(j)
            elif prop == 'active':
                for j in ac:
                    if str(j.get('active')).find(val) >= 0:
                        r.append(j)
            elif prop == 'school_year':
                for j in ac:
                    if str(j.get('school_year')).find(val) >= 0:
                        r.append(j)
            elif prop == 'semester':
                for j in ac:
                    if str(j.get('semester')).find(val) >= 0:
                        r.append(j)
            else:
                return 'Search is not Valid'
            return semester_manifest(r, rpp, page, r.__len__())
        except:
            return 'Search is not Valid'


def semester_manifest(r, rpp, page, count):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    t = []
    if type(r) == QuerySet:
        ac = []
        for i in c:
            d = str(i.school_year).split(':')
            ac.append(dict([('id', i.pk),
                            ('sys', i.sys),
                            ('active', 'Active' if i.active else 'Inactive'),
                            ('school_year', ''.join([d[0], '-', d[1]])),
                            ('semester', ordinal(i.semester))]))
        t = ac
    elif type(r) == list:
        ac = []
        for i in c:
            ac.append(dict([('id', i.get('id')),
                            ('sys', i.get('sys')),
                            ('active', i.get('active')),
                            ('school_year', i.get('school_year')),
                            ('semester', i.get('semester'))]))
        t = ac
    manifest = dict([('rows', t),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest


def student(rpp, page, ob, search):
    c_data = UserStudent.objects.all().order_by('pk')
    try:
        if ob:
            UserStudent._meta.get_field(str(ob).replace('-', '').split('__')[0])
            c_data = c_data.order_by(ob)
        else:
            c_data = c_data.order_by()
    except FieldDoesNotExist:
        c_data = c_data.order_by()
    if search == 'unset':
        return student_manifest(c_data, rpp, page, c_data.count())
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            if prop == 'generic':
                c_data = c_data.annotate(search=SearchVector('number_id') +
                                                SearchVector('number__first_name') +
                                                SearchVector('number__middle_name') +
                                                SearchVector('number__last_name') +
                                                SearchVector('year') +
                                                SearchVector('course__course') +
                                                SearchVector('course__description') +
                                                SearchVector('course__department__abbreviation') +
                                                SearchVector('course__department__name') +
                                                SearchVector('course__college__college') +
                                                SearchVector('course__college__description')).filter(search=val)
            elif prop == 'number':
                c_data = c_data.annotate(search=SearchVector('number_id')).filter(search=val)
            elif prop == 'full_name':
                c_data = c_data.annotate(search=SearchVector('number__first_name') +
                                                SearchVector('number__middle_name') +
                                                SearchVector('number__last_name')).filter(search=val)
            elif prop == 'year':
                c_data = c_data.annotate(search=SearchVector('year')).filter(search=val)
            elif prop == 'course':
                c_data = c_data.annotate(search=SearchVector('course__course') +
                                                SearchVector('course__description')).filter(search=val)
            elif prop == 'department':
                c_data = c_data.annotate(search=SearchVector('course__department__abbreviation') +
                                                SearchVector('course__department__name')).filter(search=val)
            elif prop == 'college':
                c_data = c_data.annotate(search=SearchVector('course__college__college') +
                                         SearchVector('course__college__description')).filter(search=val)
            else:
                return 'Search is not Valid'
            return student_manifest(c_data, rpp, page, c_data.__len__())
        except:
            return 'Search is not Valid'


def student_manifest(r, rpp, page, count):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    ac = []
    for j in c:
        ac.append(dict([('id', j.pk),
                        ('number', j.number_id),
                        ('full_name', j.number.full_name()),
                        ('year', j.year),
                        ('course', j.course.course),
                        ('course_id', j.course_id),
                        ('course_desc', j.course.description),
                        ('department', j.course.department.abbreviation),
                        ('department_id', j.course.department_id),
                        ('department_name', j.course.department.name),
                        ('college', j.course.college.college),
                        ('college_id', j.course.college_id),
                        ('college_desc', j.course.college.description),
                        ]))
    manifest = dict([('rows', ac),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest


def signatory(rpp, page, ob, search):
    c_data = UserSignatory.objects.all()
    try:
        if ob:
            UserSignatory._meta.get_field(str(ob).replace('-', '').split('__')[0])
            c_data = c_data.order_by(ob)
        else:
            c_data = c_data.order_by()
    except FieldDoesNotExist:
        c_data = c_data.order_by()
    if search == 'unset':
        return signatory_manifest(c_data, rpp, page, c_data.count())
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            if prop == 'generic':
                c_data = c_data.annotate(search=SearchVector('number_id') +
                                                SearchVector('number__first_name') +
                                                SearchVector('number__middle_name') +
                                                SearchVector('number__last_name') +
                                                SearchVector('position__position') +
                                                SearchVector('cdt1__abbreviation') +
                                                SearchVector('cdt1__college__college') +
                                                SearchVector('cdt1__college__description') +
                                                SearchVector('cdt1__name') +
                                                SearchVector('cdt1__type__type')).filter(search=val)
            elif prop == 'number':
                c_data = c_data.annotate(search=SearchVector('number_id')).filter(search=val)
            elif prop == 'full_name':
                c_data = c_data.annotate(search=SearchVector('number__first_name') +
                                                SearchVector('number__middle_name') +
                                                SearchVector('number__last_name')).filter(search=val)
            elif prop == 'assign':
                c_data = c_data.annotate(search=SearchVector('assign')).filter(search=True if val == 'Assign' else False)
            elif prop == 'position':
                c_data = c_data.annotate(search=SearchVector('position__position')).filter(search=val)
            elif prop == 'type':
                c_data = c_data.annotate(search=SearchVector('cdt1__type__type')).filter(search=val)
            elif prop == 'college':
                c_data = c_data.annotate(search=SearchVector('cdt1__college__college') +
                                                SearchVector('cdt1__college__description')).filter(search=val)
            else:
                return 'Search is not Valid'
            return signatory_manifest(c_data, rpp, page, c_data.__len__())
        except:
            return 'Search is not Valid'


def signatory_manifest(r, rpp, page, count):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    ac = []
    for k in c:
        ac.append(dict([('id', k.pk),
                        ('number', k.number_id),
                        ('full_name', k.number.full_name()),
                        ('assign', k.assign),
                        ('position', k.position.position),
                        ('position_id', k.position_id),
                        ('abbreviation', k.cdt1.abbreviation),
                        ('college', k.cdt1.college.college),
                        ('college_desc', k.cdt1.college.description),
                        ('name', k.cdt1.name),
                        ('type', k.cdt1.type.type),
                        ('cdt1_id', k.cdt1_id)]))
    manifest = dict([('rows', ac),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest


def user(rpp, page, ob, search):
    c_data = CUser.objects.all().order_by('pk')
    try:
        if ob:
            CUser._meta.get_field(str(ob).replace('-', '').split('__')[0])
            c_data = c_data.order_by(ob)
        else:
            c_data = c_data.order_by()
    except FieldDoesNotExist:
        c_data = c_data.order_by()
    if search == 'unset':
        return user_manifest(c_data, rpp, page, c_data.count())
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            c_data = c_data.annotate(search=SearchVector('number') +
                                            SearchVector('first_name') +
                                            SearchVector('middle_name') +
                                            SearchVector('last_name') +
                                            SearchVector('address') +
                                            SearchVector('type__type')).filter(search=val)
            if prop == 'generic':
                c_data = c_data.annotate(search=SearchVector('number') +
                                                SearchVector('first_name') +
                                                SearchVector('middle_name') +
                                                SearchVector('last_name') +
                                                SearchVector('address') +
                                                SearchVector('type__type')).filter(search=val)
            elif prop == 'number':
                c_data = c_data.annotate(search=SearchVector('number')).filter(search=val)
            elif prop == 'full_name':
                c_data = c_data.annotate(search=SearchVector('first_name') +
                                                SearchVector('middle_name') +
                                                SearchVector('last_name')).filter(search=val)
            elif prop == 'dob':
                c_data = c_data.annotate(search=SearchVector('birth_date')).filter(search=val)
            elif prop == 'address':
                c_data = c_data.annotate(search=SearchVector('address')).filter(search=val)
            elif prop == 'type':
                c_data = c_data.annotate(search=SearchVector('type__type')).filter(search=val)
            else:
                return 'Search is not Valid'
            return user_manifest(c_data, rpp, page, c_data.__len__())
        except:
            return 'Search is not Valid'


def user_manifest(r, rpp, page, count):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    ac = []
    for i in c:
        ac.append(dict([('id', i.pk),
                        ('number', i.number),
                        ('full_name', i.full_name()),
                        ('dob', str(i.birth_date)),
                        ('address', i.address),
                        ('type', i.type.type)]))
    t = ac
    manifest = dict([('rows', ac),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest


def user_types(rpp, page, ob, search):
    c_data = CUserType.objects.all().order_by('pk')
    try:
        if ob:
            CUserType._meta.get_field(str(ob).replace('-', '').split('__')[0])
            c_data = c_data.order_by(ob)
        else:
            c_data = c_data.order_by()
    except FieldDoesNotExist:
        c_data = c_data.order_by()
    if search == 'unset':
        return user_types_manifest(c_data, rpp, page, c_data.count())
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            ac = []
            r = []
            for i in c_data:
                ac.append(dict([('id', i.pk),
                                ('type', i.type)]))
            if prop == 'generic':
                for j in ac:
                    if str(j.get('type')).find(val) >= 0:
                        r.append(j)
            elif prop == 'type':
                for j in ac:
                    if str(j.get('type')).find(val) >= 0:
                        r.append(j)
            else:
                return 'Search is not Valid'
            return user_types_manifest(r, rpp, page, r.__len__())
        except:
            return 'Search is not Valid'


def user_types_manifest(r, rpp, page, count):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    t = []
    if type(r) == QuerySet:
        ac = []
        for l in c:
            ac.append(dict([('id', l.pk),
                            ('type', l.type)]))
        t = ac
    elif type(r) == list:
        ac = []
        for l in c:
            ac.append(dict([('id', l.get('id')),
                            ('type', l.get('type'))]))
        t = ac
    manifest = dict([('rows', t),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest


def violation(rpp, page, ob, search):
    c_data = Violation.objects.all()
    try:
        if ob:
            Violation._meta.get_field(str(ob).replace('-', '').split('__')[0])
            c_data = c_data.order_by(ob)
        else:
            c_data = c_data.order_by()
    except FieldDoesNotExist:
        c_data = c_data.order_by()
    if search == 'unset':
        return violation_manifest(c_data, rpp, page, c_data.count())
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            ac = []
            r = []
            for i in c_data:
                ac.append(dict([('id', i.pk),
                                ('violation', i.violation),
                                ('number', i.number_id),
                                ('sys', i.sys_id_id),
                                ('dv', i.date_violated),
                                ('resolve', i.resolve),
                                ('reason', i.reason)]))
            if prop == 'generic':
                for j in ac:
                    if str(j.get('violation')).find(val) >= 0 or str(j.get('number')).find(val) >= 0 or \
                            str(j.get('sys')).find(val) >= 0 or str(j.get('dv')).find(val) >= 0 or \
                            str(j.get('resolve')).find(val) >= 0 or str(j.get('reason')).find(val) >= 0:
                        r.append(j)
            elif prop == 'violation':
                for j in ac:
                    if str(j.get('violation')).find(val) >= 0:
                        r.append(j)
            elif prop == 'number':
                for j in ac:
                    if str(j.get('number')).find(val) >= 0:
                        r.append(j)
            elif prop == 'sys':
                for j in ac:
                    if str(j.get('sys')).find(val) >= 0:
                        r.append(j)
            elif prop == 'dv':
                for j in ac:
                    if str(j.get('dv')).find(val) >= 0:
                        r.append(j)
            elif prop == 'resolve':
                for j in ac:
                    if str(j.get('resolve')).find(val) >= 0:
                        r.append(j)
            elif prop == 'reason':
                for j in ac:
                    if str(j.get('reason')).find(val) >= 0:
                        r.append(j)
            else:
                return 'Search is not Valid'
            return violation_manifest(r, rpp, page, r.__len__())
        except:
            return 'Search is not Valid'


def violation_manifest(r, rpp, page, count):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    if type(r) == QuerySet:
        ac = []
        for i in c:
            ac.append(dict([('id', i.pk),
                            ('violation', i.violation),
                            ('number', i.number_id),
                            ('sys', i.sys_id_id),
                            ('dv', i.date_violated),
                            ('resolve', i.resolve),
                            ('reason', i.reason)]))
        t = ac
    elif type(r) == list:
        ac = []
        for i in c:
            ac.append(dict([('id', i.get('id')),
                            ('violation', i.get('violation')),
                            ('number', i.get('number_id')),
                            ('sys', i.get('sys_id_id')),
                            ('dv', i.get('date_violated')),
                            ('resolve', i.get('resolve')),
                            ('reason', i.get('reason'))]))
        t = ac
    manifest = dict([('rows', t),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest


def signatory_fresh(rpp, page, ob, search, number):
    # Query for Signatory Data from Cleari Database
    u_signatory = UserSignatory.objects.get(number_id=number)
    # Query for Signatory assigned College Model
    u_college = College.objects.get(pk=u_signatory.cdt1.college.pk)
    # Query for Office Model from Signatory Model ID
    cdt1 = CDT1.objects.get(pk=u_signatory.cdt1.pk)
    c_data = UserStudent.objects.filter(course__college_id=u_college.pk) \
        .exclude(number__receipt__isnull=False).order_by('pk')
    try:
        if ob:
            UserStudent._meta.get_field(str(ob).replace('-', '').split('__')[0])
            c_data = c_data.order_by(ob)
        else:
            c_data = c_data.order_by()
    except FieldDoesNotExist:
        c_data = c_data.order_by()
    if search == 'unset':
        return signatory_fresh_manifest(c_data, rpp, page, c_data.count(), u_signatory)
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            if prop == 'generic':
                c_data = c_data.annotate(search=SearchVector('number__number') +
                                                SearchVector('number__first_name') +
                                                SearchVector('number__middle_name') +
                                                SearchVector('number__last_name') +
                                                SearchVector('course__course') +
                                                SearchVector('course__description') +
                                                SearchVector('course__department__abbreviation') +
                                                SearchVector('course__department__name') +
                                                SearchVector('course__college__college') +
                                                SearchVector('course__college__description')).filter(search=val)
            elif prop == 'number':
                c_data = c_data.annotate(search=SearchVector('number__number').filter(search=val))
            elif prop == 'full_name':
                c_data = c_data.annotate(SearchVector('number__first_name') +
                                         SearchVector('number__middle_name') +
                                         SearchVector('number__last_name')).filter(search=val)
            elif prop == 'year':
                c_data = c_data.annotate(SearchVector('course__userstudent__year')) \
                    .filter(search=val if str(val).isdecimal() else 0)
            elif prop == 'course':
                c_data = c_data.anotate(SearchVector('course__course') +
                                        SearchVector('course__description')).filter(search=val)
            elif prop == 'department':
                c_data = c_data.anotate(SearchVector('course__department__abbreviation') +
                                        SearchVector('course__department__name')).filter(search=val)
            elif prop == 'college':
                c_data = c_data.anotate(SearchVector('course__college__college') +
                                        SearchVector('course__college__description')).filter(search=val)
            else:
                return 'Search is not Valid'
            return signatory_fresh_manifest(c_data, rpp, page, c_data.__len__(), u_signatory)
        except:
            return 'Search is not Valid'


def signatory_fresh_manifest(r, rpp, page, count, s):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    ac = []
    for j in c:
        r = ''
        for i in j.number.receipt_set.values():
            if str(i.get('signatory_id')) == str(s.number.id.pk):
                r = i.get('receipt')
        ac.append(dict([('id', j.pk),
                        ('number', j.number_id),
                        ('profile', ph_resize(j.number.profile, 150, 150)),
                        ('full_name', j.number.full_name()),
                        ('year', j.year),
                        ('year_ordinal', ordinal(j.year)),
                        ('course', j.course.course),
                        ('course_id', j.course_id),
                        ('course_desc', j.course.description),
                        ('department', j.course.department.abbreviation),
                        ('department_id', j.course.department_id),
                        ('department_name', j.course.department.name),
                        ('college', j.course.college.college),
                        ('college_id', j.course.college_id),
                        ('receipt', r),
                        ('college_desc', j.course.college.description),
                        ]))
    manifest = dict([('rows', ac),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest


def signatory_issued(rpp, page, ob, search, number):
    # Query for Signatory Data from Cleari Database
    u_signatory = UserSignatory.objects.get(number_id=number)
    # Query for Signatory assigned College Model
    u_college = College.objects.get(pk=u_signatory.cdt1.college.pk)
    # Query for Office Model from Signatory Model ID
    cdt1 = CDT1.objects.get(pk=u_signatory.cdt1.pk)
    c_data = UserStudent.objects.filter(course__college_id=u_college.pk,
                                        number__receipt__receipt__iendswith=cdt1.abbreviation,
                                        number__receipt__is_requested=False,
                                        number__receipt__is_cleared=False).order_by('pk')
    try:
        if ob:
            UserStudent._meta.get_field(str(ob).replace('-', '').split('__')[0])
            c_data = c_data.order_by(ob)
        else:
            c_data = c_data.order_by()
    except FieldDoesNotExist:
        c_data = c_data.order_by()
    if search == 'unset':
        return signatory_issued_manifest(c_data, rpp, page, c_data.count(), u_signatory)
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            if prop == 'generic':
                c_data = c_data.annotate(search=SearchVector('number__number') +
                                                SearchVector('number__first_name') +
                                                SearchVector('number__middle_name') +
                                                SearchVector('number__last_name') +
                                                SearchVector('course__course') +
                                                SearchVector('course__description') +
                                                SearchVector('course__department__abbreviation') +
                                                SearchVector('course__department__name') +
                                                SearchVector('course__college__college') +
                                                SearchVector('course__college__description')).filter(search=val)
            elif prop == 'number':
                c_data = c_data.annotate(search=SearchVector('number__number').filter(search=val))
            elif prop == 'full_name':
                c_data = c_data.annotate(SearchVector('number__first_name') +
                                         SearchVector('number__middle_name') +
                                         SearchVector('number__last_name')).filter(search=val)
            elif prop == 'year':
                c_data = c_data.annotate(SearchVector('course__userstudent__year')) \
                    .filter(search=val if str(val).isdecimal() else 0)
            elif prop == 'course':
                c_data = c_data.anotate(SearchVector('course__course') +
                                        SearchVector('course__description')).filter(search=val)
            elif prop == 'department':
                c_data = c_data.anotate(SearchVector('course__department__abbreviation') +
                                        SearchVector('course__department__name')).filter(search=val)
            elif prop == 'college':
                c_data = c_data.anotate(SearchVector('course__college__college') +
                                        SearchVector('course__college__description')).filter(search=val)
            else:
                return 'Search is not Valid'
            return signatory_fresh_manifest(c_data, rpp, page, c_data.__len__(), u_signatory )
        except:
            return 'Search is not Valid'


def signatory_issued_manifest(r, rpp, page, count, s):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    ac = []
    for j in c:
        r = ''
        for i in j.number.receipt_set.values():
            if str(i.get('signatory_id')) == str(s.number.id.pk):
                r = i.get('receipt')
        ac.append(dict([('id', j.pk),
                        ('number', j.number_id),
                        ('profile', ph_resize(j.number.profile, 150, 150)),
                        ('full_name', j.number.full_name()),
                        ('year', j.year),
                        ('year_ordinal', ordinal(j.year)),
                        ('course', j.course.course),
                        ('course_id', j.course_id),
                        ('course_desc', j.course.description),
                        ('department', j.course.department.abbreviation),
                        ('department_id', j.course.department_id),
                        ('department_name', j.course.department.name),
                        ('college', j.course.college.college),
                        ('college_id', j.course.college_id),
                        ('college_desc', j.course.college.description),
                        ('receipt', r),
                        ('receipts', [entry for entry in j.number.receipt_set.values()])
                        ]))
    manifest = dict([('rows', ac),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest


def signatory_requesting(rpp, page, ob, search, number):
    # Query for Signatory Data from Cleari Database
    u_signatory = UserSignatory.objects.get(number_id=number)
    # Query for Signatory assigned College Model
    u_college = College.objects.get(pk=u_signatory.cdt1.college.pk)
    # Query for Office Model from Signatory Model ID
    cdt1 = CDT1.objects.get(pk=u_signatory.cdt1.pk)
    c_data = UserStudent.objects.filter(course__college_id=u_college.pk,
                                        number__receipt__receipt__iendswith=cdt1.abbreviation,
                                        number__receipt__is_requested=True,
                                        number__receipt__is_cleared=False).order_by('pk')
    try:
        if ob:
            UserStudent._meta.get_field(str(ob).replace('-', '').split('__')[0])
            c_data = c_data.order_by(ob)
        else:
            c_data = c_data.order_by()
    except FieldDoesNotExist:
        c_data = c_data.order_by()
    if search == 'unset':
        print(c_data)
        return signatory_requesting_manifest(c_data, rpp, page, c_data.count(), u_signatory )
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            if prop == 'generic':
                c_data = c_data.annotate(search=SearchVector('number__number') +
                                                SearchVector('number__first_name') +
                                                SearchVector('number__middle_name') +
                                                SearchVector('number__last_name') +
                                                SearchVector('course__course') +
                                                SearchVector('course__description') +
                                                SearchVector('course__department__abbreviation') +
                                                SearchVector('course__department__name') +
                                                SearchVector('course__college__college') +
                                                SearchVector('course__college__description')).filter(search=val)
            elif prop == 'number':
                c_data = c_data.annotate(search=SearchVector('number__number').filter(search=val))
            elif prop == 'full_name':
                c_data = c_data.annotate(SearchVector('number__first_name') +
                                         SearchVector('number__middle_name') +
                                         SearchVector('number__last_name')).filter(search=val)
            elif prop == 'year':
                c_data = c_data.annotate(SearchVector('course__userstudent__year')) \
                    .filter(search=val if str(val).isdecimal() else 0)
            elif prop == 'course':
                c_data = c_data.anotate(SearchVector('course__course') +
                                        SearchVector('course__description')).filter(search=val)
            elif prop == 'department':
                c_data = c_data.anotate(SearchVector('course__department__abbreviation') +
                                        SearchVector('course__department__name')).filter(search=val)
            elif prop == 'college':
                c_data = c_data.anotate(SearchVector('course__college__college') +
                                        SearchVector('course__college__description')).filter(search=val)
            else:
                return 'Search is not Valid'
            return signatory_requesting_manifest(c_data, rpp, page, c_data.__len__(), u_signatory )
        except:
            return 'Search is not Valid'


def signatory_requesting_manifest(r, rpp, page, count, s):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    ac = []
    for j in c:
        r = ''
        for i in j.number.receipt_set.values():
            if str(i.get('signatory_id')) == str(s.number.id.pk):
                r = i.get('receipt')
        ac.append(dict([('id', j.pk),
                        ('number', j.number_id),
                        ('profile', ph_resize(j.number.profile, 150, 150)),
                        ('full_name', j.number.full_name()),
                        ('year', j.year),
                        ('year_ordinal', ordinal(j.year)),
                        ('course', j.course.course),
                        ('course_id', j.course_id),
                        ('course_desc', j.course.description),
                        ('department', j.course.department.abbreviation),
                        ('department_id', j.course.department_id),
                        ('department_name', j.course.department.name),
                        ('college', j.course.college.college),
                        ('college_id', j.course.college_id),
                        ('college_desc', j.course.college.description),
                        ('receipt', r),
                        ('receipts', [entry for entry in j.number.receipt_set.values()])
                        ]))
    manifest = dict([('rows', ac),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest


def signatory_signed(rpp, page, ob, search, number):
    # Query for Signatory Data from Cleari Database
    u_signatory = UserSignatory.objects.get(number_id=number)
    # Query for Signatory assigned College Model
    u_college = College.objects.get(pk=u_signatory.cdt1.college.pk)
    # Query for Office Model from Signatory Model ID
    cdt1 = CDT1.objects.get(pk=u_signatory.cdt1.pk)
    c_data = UserStudent.objects.filter(course__college_id=u_college.pk,
                                        number__receipt__receipt__iendswith=cdt1.abbreviation,
                                        number__receipt__is_requested=True,
                                        number__receipt__is_cleared=True).order_by('pk')
    try:
        if ob:
            UserStudent._meta.get_field(str(ob).replace('-', '').split('__')[0])
            c_data = c_data.order_by(ob)
        else:
            c_data = c_data.order_by()
    except FieldDoesNotExist:
        c_data = c_data.order_by()
    if search == 'unset':
        return signatory_signed_manifest(c_data, rpp, page, c_data.count(), u_signatory)
    else:
        try:
            s = json.loads(search)
            prop = [entry for entry in s.keys()][0]
            val = [entry for entry in s.values()][0]
            if prop == 'generic':
                c_data = c_data.annotate(search=SearchVector('number__number') +
                                                SearchVector('number__first_name') +
                                                SearchVector('number__middle_name') +
                                                SearchVector('number__last_name') +
                                                SearchVector('course__course') +
                                                SearchVector('course__description') +
                                                SearchVector('course__department__abbreviation') +
                                                SearchVector('course__department__name') +
                                                SearchVector('course__college__college') +
                                                SearchVector('course__college__description')).filter(search=val)
            elif prop == 'number':
                c_data = c_data.annotate(search=SearchVector('number__number').filter(search=val))
            elif prop == 'full_name':
                c_data = c_data.annotate(SearchVector('number__first_name') +
                                         SearchVector('number__middle_name') +
                                         SearchVector('number__last_name')).filter(search=val)
            elif prop == 'year':
                c_data = c_data.annotate(SearchVector('course__userstudent__year')) \
                    .filter(search=val if str(val).isdecimal() else 0)
            elif prop == 'course':
                c_data = c_data.anotate(SearchVector('course__course') +
                                        SearchVector('course__description')).filter(search=val)
            elif prop == 'department':
                c_data = c_data.anotate(SearchVector('course__department__abbreviation') +
                                        SearchVector('course__department__name')).filter(search=val)
            elif prop == 'college':
                c_data = c_data.anotate(SearchVector('course__college__college') +
                                        SearchVector('course__college__description')).filter(search=val)
            else:
                return 'Search is not Valid'
            return signatory_fresh_manifest(c_data, rpp, page, c_data.__len__(), u_signatory)
        except:
            return 'Search is not Valid'


def signatory_signed_manifest(r, rpp, page, count, s):
    paginator = Paginator(r, rpp)
    try:
        c = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        c = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 7777), deliver last page of results.
        c = paginator.page(paginator.num_pages)
    ac = []
    for j in c:
        r = ''
        for i in j.number.receipt_set.values():
            if str(i.get('signatory_id')) == str(s.number.id.pk):
                r = i.get('receipt')
        ac.append(dict([('id', j.pk),
                        ('number', j.number_id),
                        ('profile', ph_resize(j.number.profile, 150, 150)),
                        ('full_name', j.number.full_name()),
                        ('year', j.year),
                        ('year_ordinal', ordinal(j.year)),
                        ('course', j.course.course),
                        ('course_id', j.course_id),
                        ('course_desc', j.course.description),
                        ('department', j.course.department.abbreviation),
                        ('department_id', j.course.department_id),
                        ('department_name', j.course.department.name),
                        ('college', j.course.college.college),
                        ('college_id', j.course.college_id),
                        ('college_desc', j.course.college.description),
                        ('receipt', r),
                        ('receipts', [entry for entry in j.number.receipt_set.values()])
                        ]))
    manifest = dict([('rows', ac),
                     ('total_rows', str(count)),
                     ('total_page', paginator.num_pages),
                     ('rpp', rpp),
                     ('page', page)])
    return manifest
