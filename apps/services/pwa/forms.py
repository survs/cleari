from django import forms
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from django.utils import timezone


class EmailForm(forms.Form):
    email = forms.EmailField(required=False)


class NewPasswordForm(forms.ModelForm):
    confirm_password = forms.CharField(widget=forms.PasswordInput)
    password_new1 = forms.CharField(widget=forms.PasswordInput)
    password_new2 = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('password_new1','password_new2','confirm_password',)

    def clean_password_new1(self):
        if self.data['password_new1'] != self.data['password_new2']:
            raise forms.ValidationError('Passwords are not the same')
        else:
            return self.data['password_new1']

    def clean_password_new2(self):
        if self.data['password_new2'] != self.data['password_new2']:
            raise forms.ValidationError('Passwords are not the same')
        else:
            return self.data['password_new2']

    def clean_password_old(self):
        password = self.data['password_old']
        if not check_password(password, self.instance.password):
            raise forms.ValidationError('Passwords does not match')
        else:
            return password


class ConfirmPasswordForm(forms.ModelForm):
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('confirm_password',)

    def clean(self):
        cleaned_data = super(ConfirmPasswordForm, self).clean()
        confirm_password = cleaned_data.get('confirm_password')
        if not check_password(confirm_password, self.instance.password):
            self.add_error('confirm_password', 'Password does not match.')

    def save(self, commit=True):
        user = super(ConfirmPasswordForm, self).save(commit)
        user.last_login = timezone.now()
        if commit:
            user.save()
        return user
