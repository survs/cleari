from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User as DUser
from django.shortcuts import render, redirect

from apps.contrib.auth.forms import UserLogin


def login_page(request):
    """
    Description: Authentication page or login page which users log into.
    :param request:
    Contains HTTP META request as Anonymous User
    :returns:
        1: Redirect to Cleari Administration if it is a System admin
        2: Redirect to Homepage if authentication is successful
        3-5: Redirect to Login due to authentication problems
        6: Render Login Form into HTML Format
    """
    # Filter HTTP Request for POST method
    if request.method == 'POST':
        # Call User Login form to validate POST data
        ulf = UserLogin(request.POST)
        if ulf.is_valid():
            # Once data is fields are valid it proceeds to django authentication function
            u = authenticate(request, username=ulf.cleaned_data['username'],
                             password=ulf.cleaned_data['password'])
            # Verify authentication data is not NUll
            if u is not None:
                # Try to login User supplied data
                try:
                    du = DUser.objects.get(username=ulf.cleaned_data['username'])
                    login(request, u)
                    if du.is_staff or du.is_superuser:
                        return redirect('/admin')
                    return redirect('core:home')
                except:
                    # Redirect user to login with error message
                    messages.add_message(request, messages.ERROR, 'Something went wrong!')
                    return redirect('core:login')
            else:
                # Redirect user to login with error message
                messages.add_message(request, messages.ERROR, 'User not Found')
                return redirect('core:login')
        else:
            # Redirect user to login with error message regarding on form validation
            messages.add_message(request, messages.ERROR, ulf.errors)
            return redirect('core:login')
    else:
        # Call User Login Form to load login fields for GET request and render to HTML
        ulf = UserLogin()
        return render(request, 'registration/login.html', {'form': ulf})


def logout_page(request):
    """
    Description: Allows users to logout from the system
    :param request:
    Contains HTTP META request with user session information
    :return:
        1: Redirect to Login Page
    """
    # Divert Anonymous Users to login page
    if not request.user.is_authenticated:
        messages.add_message(request, messages.INFO, 'Login First')
        return redirect('core:login')
    # Logout the user from the system
    logout(request)
    # Throw Message that the user successfully logout
    messages.add_message(request, messages.INFO, 'Successfully Logout')
    # Redirect to Login Page
    return redirect('core:login')
