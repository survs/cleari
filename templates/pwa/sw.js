(function () {
        'use strict';

        let cacheNameStatic = 'cleari_pwa';
//update nasad
        self.addEventListener('install', function (event) {
            let urlToCache = [
                {% block urls %}{%endblock %}
        ]
            event.waitUntil(caches.open(cacheNameStatic).then(function (cache) {
                return cache.addAll(urlToCache)
            }));
        });
        self.addEventListener('fetch', function (event) {
            event.respondWith(caches.match(event.request).then(function (response) {
                if (response)
                    return response;
                return fetch(event.request);
            }))
        });
        self.addEventListener('activate', function (event) {
            event.waitUntil(caches.keys().then(function (cacheNames) {
                return Promise.all(cacheNames.filter(function (cacheName) {
                    return cacheName.startsWith('cleari_pwa-') && cacheName !== cacheNameStatic;
                }).map(function (cacheName) {
                    return cache.delete(cacheName);
                }));
            }))
        });
        self.addEventListener('message', function (event) {
            if (event.data.action === 'skipWaiting') {
                self.skipWaiting();
                console.log("Executed");
            }
        });
        self.addEventListener('push', function (event) {
            console.log('[Service Worker] Push Received.');
            console.log(`[Service Worker] Push had this data: "${event.data.text()}"`);

            const title = 'Push Codelab';
            const options = {
                body: 'Yay it works.',
                icon: 'images/icon.png',
                badge: 'images/badge.png'
            };

            const notificationPromise = self.registration.showNotification(title, options);
            event.waitUntil(notificationPromise);
        });
    }
)();
