from channels.routing import ProtocolTypeRouter, URLRouter

from apps.services.pwa import routing as pwa_r

application = ProtocolTypeRouter({
    'http': URLRouter(pwa_r.urlpatterns),
})
