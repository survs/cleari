define(["main"], function (main) {

    main.controller("ProfileCtrl", function ($scope, $rootScope, $http, $mdDialog, $mdToast) {
        $rootScope.page_type = 'Profile';

        function init() {
            $http({
                method  : 'GET',
            url: 'pwa/profile/provider'
            }).then(function(response) {
            switch (response.status) {
                case 200:
                    let user = response.data.user;
                    let extra = response.data.extra;
                    let dob, addr;
                    if (user.address == null) {
                        addr = 'Not Set'
                    } else {
                        addr = user.address
                    }
                    if (user.birth_date == null) {
                        dob = 'Not Set'
                    } else {
                        dob = user.birth_date
                    }
                    $scope.profile = {
                        'photo': user.photo,
                        'number': user.number,
                        'full_name': user.full_name,
                        'address': addr,
                        'birthday': dob,
                        'type': user.type
                    };
                    $rootScope.photo = user.photo;
                    if (user.type === 'student') {
                        $scope.extra = {
                            'c_abbreviation': extra.course.abbreviation,
                            'c_des': extra.course.description,
                            'c_year': extra.year,
                            'dep_abbreviation': extra.course.department.abbreviation,
                            'dept_name': extra.course.department.name
                        }
                    } else if (user.type === 'signatory') {
                        $scope.extra = {
                            'assign': extra.assign,
                            'position': extra.position,
                            'type': extra.cdt.type,
                            'name': extra.cdt.name,
                            'abbreviation': extra.cdt.abbreviation,
                            'c_abbreviation': extra.cdt.college.name,
                            'c_des': extra.cdt.college.description,
                        }
                    }
                    break;
                case 203:
                    break;
                case 204:
                    break;
                default:
                    console.log("Something went wrong")
            }
            });
        }

        $scope.changePhoto = function (ev) {
            $mdDialog.show({
                locals: {title: 'Change Profile Photo', submit_name: 'Save Changes', photo: $scope.profile.photo},
                controller: ChangePhotoCtrl,
                templateUrl: '/pwa/user/provider/myprofile_photo_view',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: true
            }).then(function (answer) {
                toast($mdToast, answer);
                init()
            }, function () {
                toast($mdToast, 'Canceled')
            });
        };
        $scope.edit = function (ev) {
            $mdDialog.show({
                locals: {title: 'Edit my Profile', submit_name: 'Save Changes'},
                controller: EditProfileCtrl,
                templateUrl: '/pwa/user/provider/myprofile_view',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: true
            }).then(function (answer) {
                toast($mdToast, answer);
                init()
            }, function () {
                toast($mdToast, 'Canceled')
            });
        };
        init()
    });

    function ChangePhotoCtrl($scope, $mdDialog, $http, $mdToast, title, submit_name, photo) {
        $scope.profile = photo;
        console.log($scope.form);
        $scope.title = title;
        $scope.submit_name = submit_name;
        $scope.submit = function () {
            let fd = new FormData();
            fd.append('profile', $scope.form.profile[0]);
            fd.append('csrfmiddlewaretoken', $scope.form.csrfmiddlewaretoken);
            $http.post('/pwa/user/provider/myprofile_photo', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function (r) {
                switch (r.status) {
                    case 200:
                        photo =
                            $mdDialog.hide('Profile Photo changes saved');
                        break;
                    case 203:
                        csrf_refresh($scope, $http);
                        $mdDialog.hide('Changes not Saved');
                        break;
                    default:
                        csrf_refresh($scope, $http);
                        $mdDialog.hide('Something went wrong')
                }
            })
        };
        $scope.cancel = function () {
            $mdDialog.cancel()
        };
    }

    function EditProfileCtrl($scope, $mdDialog, $http, $mdToast, title, submit_name,) {
        $scope.title = title;
        $scope.submit_name = submit_name;
        $http({
            method: 'GET',
            url: '/pwa/user/provider/myprofile_data'
        }).then(function (r) {
            switch (r.status) {
                case 200:
                    $scope.form.first_name = r.data.first_name;
                    $scope.form.middle_name = r.data.middle_name;
                    $scope.form.last_name = r.data.last_name;
                    $scope.form.address = r.data.address;
                    $scope.form.birth_date = r.data.birth_date;
                    break;
                case 203:
                    toast($mdToast, r.data);
                    break;
                default:
                    toast($mdToast, r.data);
            }
        });
        $scope.submit = function () {
            require(['param'], function (param) {
                $http({
                    method: 'POST',
                    data: param($scope.form),
                    url: '/pwa/user/provider/myprofile_view',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).then(function (r) {
                    switch (r.status) {
                        case 200:
                            $mdDialog.hide('Profile changes saved');
                            break;
                        case 203:
                            csrf_refresh($scope, $http);
                            $mdDialog.hide('Changes not Saved');
                            break;
                        default:
                            csrf_refresh($scope, $http);
                            $mdDialog.hide('Something went wrong')
                    }
                })
            })
        };
        $scope.cancel = function () {
            $mdDialog.cancel()
        };
    }

    function csrf_refresh($scope, $http) {
        $http({
            method: 'GET',
            url: '/pwa/user/provider/csrf',
        }).then(function (response) {
            if (response.status === 203) {
                $scope.form.csrfmiddlewaretoken = response.data.csrf
            }
        });
    }

    function toast($mdToast, content) {
        $mdToast.show(
            $mdToast.simple()
                .textContent(content)
                .position('bottom right')
                .hideDelay(3000)
        );
    }
});
