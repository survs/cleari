define(["main"], function (main) {
    main.controller("ManageCtrl", ["$scope", "$routeParams", "$rootScope", "$http", "$timeout", "$mdDialog", "$mdToast", function ($scope, $routeParams, $rootScope, $http, $timeout, $mdDialog, $mdToast) {
        $rootScope.page_type = 'Manage';
        $scope.title = $routeParams.card;
        $scope.add = function (ev, module, title) {
            $mdDialog.show({
                locals: {
                    title: title,
                    module: module,
                    route_id: $routeParams.card,
                    $scopeIn: $scope,
                    typer: 'add',
                    instance: ''
                },
                controller: DialogCtrl,
                templateUrl: '/pwa/module/provider/view/' + module,
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: true
            }).then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
        };
        $scope.edit = function (ev, module, title, pk) {
            $mdDialog.show({
                locals: {
                    title: title,
                    module: module,
                    route_id: $routeParams.card,
                    $scopeIn: $scope,
                    typer: 'edit',
                    instance: pk
                },
                controller: DialogCtrl,
                templateUrl: '/pwa/module/provider/view/' + module,
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: true
            }).then(function (answer) {
                toast($mdToast, answer)
            }, function () {
                toast($mdToast, 'Canceled')
            });
        };
        $scope.delete = function (ev, module, pk) {
            let confirm = $mdDialog.confirm()
                .title('Would you like to delete ' + pk.n)
                .textContent('This data will be removed from the system')
                .ariaLabel('Asking')
                .targetEvent(ev)
                .ok('Yes')
                .cancel('No');

            $mdDialog.show(confirm).then(function () {
                $http({
                    method: 'GET',
                    url: '/pwa/' + $routeParams.card + '/delete/' + module + '/' + pk.id,
                }).then(function (r) {
                    toast($mdToast, r.data);
                    refreshData($scope, $routeParams, NgTableParams);
                });
            }, function () {
                toast($mdToast, 'Deletion Canceled')
            });
        };
        $scope.issue = function (ev, module, title, pk) {
            $mdDialog.show({
                locals: {
                    title: title,
                    module: module,
                    route_id: $routeParams.card,
                    $scopeIn: $scope,
                    typer: 'issue',
                    instance: pk
                },
                controller: DialogCtrl,
                templateUrl: 'pwa/module/provider/view/' + module,
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: true
            }).then(function (answer) {
                toast($mdToast, answer)
            }, function () {
                toast($mdToast, 'Canceled')
            });
        };
        $scope.info = function (ev, module, title, pk) {
            if (module[0] === 'c') {
                $mdDialog.show({
                    locals: {instance: pk},
                    controller: InfoCtrl,
                    templateUrl: '/pwa/module/provider/table',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                }).then(function (answer) {
                    toast($mdToast, answer)
                }, function () {
                    toast($mdToast, 'Canceled')
                });
            }
        };
    }]);
    main.controller('CDT1Ctrl', ['Resource', '$rootScope', '$scope', function (service, scope) {
        let ctrl = this;
        this.displayed = [];
        this.options = [5, 20, 30, 50, 100];
        this.itemsByPage = this.options[2];
        this.openMenu = function ($mdMenu, ev) {
            $mdMenu.open(ev);
        };
        this.callServer = function callServer(tableState) {
            ctrl.isLoading = true;
            let pagination = tableState.pagination;
            let start = pagination.start || 0;
            let number = pagination.number || 10;
            let module = 'cdt1';
            service.getPage(start, number, tableState, module).then(function (result) {
                ctrl.displayed = result.data;
                tableState.pagination.numberOfPages = result.numberOfPages;
                tableState.pagination.totalItemCount = result.totalItemCount;
                ctrl.isLoading = false;
            });
        };
    }]);
    main.controller('CDT2Ctrl', ['Resource', '$rootScope', function (service) {
        let ctrl = this;
        this.displayed = [];
        this.options = [5, 20, 30, 50, 100];
        this.itemsByPage = this.options[2];
        this.openMenu = function ($mdMenu, ev) {
            $mdMenu.open(ev);
        };
        this.callServer = function callServer(tableState) {
            ctrl.isLoading = true;
            let pagination = tableState.pagination;
            let start = pagination.start || 0;
            let number = pagination.number || 10;
            let module = 'cdt2';
            service.getPage(start, number, tableState, module).then(function (result) {
                ctrl.displayed = result.data;
                tableState.pagination.numberOfPages = result.numberOfPages;
                tableState.pagination.totalItemCount = result.totalItemCount;
                ctrl.isLoading = false;
            });
        };
    }]);
    main.controller('CDT3Ctrl', ['Resource', '$rootScope', function (service) {
        let ctrl = this;
        this.displayed = [];
        this.options = [5, 20, 30, 50, 100];
        this.itemsByPage = this.options[2];
        this.openMenu = function ($mdMenu, ev) {
            $mdMenu.open(ev);
        };
        this.callServer = function callServer(tableState) {
            ctrl.isLoading = true;
            let pagination = tableState.pagination;
            let start = pagination.start || 0;
            let number = pagination.number || 10;
            let module = 'cdt3';
            service.getPage(start, number, tableState, module).then(function (result) {
                ctrl.displayed = result.data;
                tableState.pagination.numberOfPages = result.numberOfPages;
                tableState.pagination.totalItemCount = result.totalItemCount;
                ctrl.isLoading = false;
            });
        };
    }]);
    main.controller('ClearanceCtrl', ['Resource', '$rootScope', function (service) {
        let ctrl = this;
        this.displayed = [];
        this.options = [5, 20, 30, 50, 100];
        this.itemsByPage = this.options[2];
        this.openMenu = function ($mdMenu, ev) {
            $mdMenu.open(ev);
        };
        this.callServer = function callServer(tableState) {
            ctrl.isLoading = true;
            let pagination = tableState.pagination;
            let start = pagination.start || 0;
            let number = pagination.number || 10;
            let module = 'clearance';
            service.getPage(start, number, tableState, module).then(function (result) {
                ctrl.displayed = result.data;
                tableState.pagination.numberOfPages = result.numberOfPages;
                tableState.pagination.totalItemCount = result.totalItemCount;
                ctrl.isLoading = false;
            });
        };
    }]);
    main.controller('ClearanceDurationCtrl', ['Resource', '$rootScope', '$mdDialog', function (service, rt, dialog) {
        let ctrl = this;
        this.displayed = [];
        this.options = [5, 20, 30, 50, 100];
        this.itemsByPage = this.options[2];
        this.openMenu = function ($mdMenu, ev) {
            $mdMenu.open(ev);
        };
        this.add = function () {
            dialog.show({
                        locals: {title:'Add Clearance Duration', instance:undefined},
                        controller: "CDCRUDCtrl",
                        templateUrl: '/static/templates/message.dialog.tmpl.html',
                        parent: angular.element(document.body),
                        clickOutsideToClose: false,
                        fullscreen: true
                    }).then(function (answer) {
                        toast(toaster, answer)
                    }, function () {
                        toast(toaster, 'Canceled')
                    });
        };
        this.callServer = function callServer(tableState) {
            ctrl.isLoading = true;
            let pagination = tableState.pagination;
            let start = pagination.start || 0;
            let number = pagination.number || 10;
            let module = 'clearance_duration';
            service.getPage(start, number, tableState, module).then(function (result) {
                ctrl.displayed = result.data;
                tableState.pagination.numberOfPages = result.numberOfPages;
                tableState.pagination.totalItemCount = result.totalItemCount;
                ctrl.isLoading = false;
            });
        };
    }]);
    main.controller('CollegeCtrl', ['Resource', '$rootScope', function (service) {
        let ctrl = this;
        this.displayed = [];
        this.options = [5, 20, 30, 50, 100];
        this.itemsByPage = this.options[2];
        this.openMenu = function ($mdMenu, ev) {
            $mdMenu.open(ev);
        };
        this.callServer = function callServer(tableState) {
            ctrl.isLoading = true;
            let pagination = tableState.pagination;
            let start = pagination.start || 0;
            let number = pagination.number || 10;
            let module = 'college';
            service.getPage(start, number, tableState, module).then(function (result) {
                ctrl.displayed = result.data;
                tableState.pagination.numberOfPages = result.numberOfPages;
                tableState.pagination.totalItemCount = result.totalItemCount;
                ctrl.isLoading = false;
            });
        };
    }]);
    main.controller('CourseCtrl', ['Resource', '$rootScope', function (service) {
        let ctrl = this;
        this.displayed = [];
        this.options = [5, 20, 30, 50, 100];
        this.itemsByPage = this.options[2];
        this.openMenu = function ($mdMenu, ev) {
            $mdMenu.open(ev);
        };
        this.callServer = function callServer(tableState) {
            ctrl.isLoading = true;
            let pagination = tableState.pagination;
            let start = pagination.start || 0;
            let number = pagination.number || 10;
            let module = 'course';
            service.getPage(start, number, tableState, module).then(function (result) {
                ctrl.displayed = result.data;
                tableState.pagination.numberOfPages = result.numberOfPages;
                tableState.pagination.totalItemCount = result.totalItemCount;
                ctrl.isLoading = false;
            });
        };
    }]);
    main.controller('SemesterCtrl', ['Resource', '$rootScope', function (service) {
        let ctrl = this;
        this.displayed = [];
        this.options = [5, 20, 30, 50, 100];
        this.itemsByPage = this.options[2];
        this.openMenu = function ($mdMenu, ev) {
            $mdMenu.open(ev);
        };
        this.callServer = function callServer(tableState) {
            ctrl.isLoading = true;
            let pagination = tableState.pagination;
            let start = pagination.start || 0;
            let number = pagination.number || 10;
            let module = 'semester';
            service.getPage(start, number, tableState, module).then(function (result) {
                ctrl.displayed = result.data;
                tableState.pagination.numberOfPages = result.numberOfPages;
                tableState.pagination.totalItemCount = result.totalItemCount;
                ctrl.isLoading = false;
            });
        };
    }]);
    main.controller('ReceiptCtrl', ['Resource', '$rootScope', '$http', function (service, rt, $http) {
        let ctrl = this;
        this.displayed = [];
        this.options = [5, 20, 30, 50, 100];
        this.itemsByPage = this.options[2];
        this.openMenu = function ($mdMenu, ev) {
            $mdMenu.open(ev);
        };
        this.export = function (instance) {
            let base_url = 'pwa/reports/gen/inv/'+instance.number+'/';
                let cl = [];
                let i;
                let r = instance.receipts;
                console.log(instance)
                for(i in r){
                    cl.push(r[i].liability_id)
                }
                let unique = Array.from(new Set(cl));
                $http({
                    method: 'GET',
                    url: base_url+ unique.toString(),
                    responseType: 'blob'
                }).then(function (data) {
                    console.log(data);
                    let linkElement = document.createElement('a');
                    try {
                        console.log(data)
                        let url = window.URL.createObjectURL(data.data);
                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download",
                            'Clearance of '+String(instance.full_name).replace('.', '').replace('.', '').replace('.', ''));

                        let clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    } catch (ex) {
                        console.log(ex);
                    }
                });
             }
        this.callServer = function callServer(tableState) {
            ctrl.isLoading = true;
            let pagination = tableState.pagination;
            let start = pagination.start || 0;
            let number = pagination.number || 10;
            let module = 'receipt';
            service.getPage(start, number, tableState, module).then(function (result) {
                ctrl.displayed = result.data;
                tableState.pagination.numberOfPages = result.numberOfPages;
                tableState.pagination.totalItemCount = result.totalItemCount;
                ctrl.isLoading = false;
            });
        };
    }]);
    main.controller('StudentCtrl', ['Resource', '$rootScope', function (service) {
        let ctrl = this;
        this.displayed = [];
        this.options = [5, 20, 30, 50, 100];
        this.itemsByPage = this.options[2];
        this.openMenu = function ($mdMenu, ev) {
            $mdMenu.open(ev);
        };
        this.callServer = function callServer(tableState) {
            ctrl.isLoading = true;
            let pagination = tableState.pagination;
            let start = pagination.start || 0;
            let number = pagination.number || 10;
            let module = 'student';
            service.getPage(start, number, tableState, module).then(function (result) {
                ctrl.displayed = result.data;
                tableState.pagination.numberOfPages = result.numberOfPages;
                tableState.pagination.totalItemCount = result.totalItemCount;
                ctrl.isLoading = false;
            });
        };
    }]);
    main.controller('SignatoryCtrl', ['Resource', '$rootScope', function (service) {
        let ctrl = this;
        this.displayed = [];
        this.options = [5, 20, 30, 50, 100];
        this.itemsByPage = this.options[2];
        this.openMenu = function ($mdMenu, ev) {
            $mdMenu.open(ev);
        };
        this.callServer = function callServer(tableState) {
            ctrl.isLoading = true;
            let pagination = tableState.pagination;
            let start = pagination.start || 0;
            let number = pagination.number || 10;
            let module = 'signatory';
            service.getPage(start, number, tableState, module).then(function (result) {
                ctrl.displayed = result.data;
                tableState.pagination.numberOfPages = result.numberOfPages;
                tableState.pagination.totalItemCount = result.totalItemCount;
                ctrl.isLoading = false;
            });
        };
    }]);
    main.controller('UserCtrl', ['Resource', '$rootScope', function (service) {
        let ctrl = this;
        this.displayed = [];
        this.options = [5, 20, 30, 50, 100];
        this.itemsByPage = this.options[2];
        this.openMenu = function ($mdMenu, ev) {
            $mdMenu.open(ev);
        };
        this.callServer = function callServer(tableState) {
            ctrl.isLoading = true;
            let pagination = tableState.pagination;
            let start = pagination.start || 0;
            let number = pagination.number || 10;
            let module = 'user';
            service.getPage(start, number, tableState, module).then(function (result) {
                ctrl.displayed = result.data;
                tableState.pagination.numberOfPages = result.numberOfPages;
                tableState.pagination.totalItemCount = result.totalItemCount;
                ctrl.isLoading = false;
            });
        };
    }]);
    main.controller('UserTypeCtrl', ['Resource', '$rootScope', function (service) {
        let ctrl = this;
        this.displayed = [];
        this.options = [5, 20, 30, 50, 100];
        this.itemsByPage = this.options[2];
        this.openMenu = function ($mdMenu, ev) {
            $mdMenu.open(ev);
        };
        this.callServer = function callServer(tableState) {
            ctrl.isLoading = true;
            let pagination = tableState.pagination;
            let start = pagination.start || 0;
            let number = pagination.number || 10;
            let module = 'user_type';
            service.getPage(start, number, tableState, module).then(function (result) {
                ctrl.displayed = result.data;
                tableState.pagination.numberOfPages = result.numberOfPages;
                tableState.pagination.totalItemCount = result.totalItemCount;
                ctrl.isLoading = false;
            });
        };
    }]);
    main.controller('ViolationCtrl', ['Resource', '$rootScope', function (service) {
        let ctrl = this;
        this.displayed = [];
        this.options = [5, 20, 30, 50, 100];
        this.itemsByPage = this.options[2];
        this.openMenu = function ($mdMenu, ev) {
            $mdMenu.open(ev);
        };
        this.callServer = function callServer(tableState) {
            ctrl.isLoading = true;
            let pagination = tableState.pagination;
            let start = pagination.start || 0;
            let number = pagination.number || 10;
            let module = 'violation';
            service.getPage(start, number, tableState, module).then(function (result) {
                ctrl.displayed = result.data;
                tableState.pagination.numberOfPages = result.numberOfPages;
                tableState.pagination.totalItemCount = result.totalItemCount;
                ctrl.isLoading = false;
            });
        };
    }]);
    main.service('Resource', ['$q', '$filter', '$timeout', function ($q, $filter, $timeout) {
        this.getPage = function (start, number, params, module) {
            let search = 'unset';
            let current_page = Math.floor(params.pagination.start / params.pagination.number) + 1;
            let dataset = [];
            let status = false;
            let deferred = $q.defer();
            let page_count = 0;
            let order_by = '';
            let row_total = 0;
            let ppn = 30;
            if (params.search.predicateObject) {
                let d = params.search.predicateObject;
                if (d.$ !== undefined) {
                    search = "{\"generic\":\"" + d.$ + "\"}";
                } else {
                    let property = Object.keys(d)["0"];
                    let value = Object.values(d)["0"];
                    if (property !== undefined && value !== undefined) {
                        search = "{\"" + property + "\":\"" + value + "\"}";
                    } else {
                        search = 'unset'
                    }
                }
            }
            if (params.sort.predicate) {
                if (params.sort.reverse) {
                    order_by = '-' + params.sort.predicate
                } else {
                    order_by = params.sort.predicate
                }
            } else {
                order_by = 'unset'
            }
            if (params.pagination.number !== undefined) {
                ppn = params.pagination.number
            }
            require(['oboe'], function (oboe) {
                oboe('/pwa/module/provider/' + module + '/' + ppn + '/' + current_page + '/' + order_by + '/' + search)
                    .node('!', function (chunks) {
                        dataset = chunks.rows;
                        page_count = chunks.total_page;
                        row_total = chunks.total_rows;
                        return oboe.drop;
                    })
                    .done(function () {
                        status = true;
                    })
            });
            function m() {
                $timeout(function () {
                    if (status) {
                deferred.resolve({
                    data: dataset,
                    totalItemCount: row_total,
                    numberOfPages: page_count,
                    len: dataset.length
                });
                    } else {
                        m()
                    }
                }, 1000)
            }

            m();
            return deferred.promise;
        };

        return {
            getPage: this.getPage
        };

    }]);
    function csrf_refresh($scope, $http, route_id, type) {
        let i = '';
        if (type === 'edit') {
            i = '/0'
        }
        $http({
            method: 'GET',
            url: '/pwa/' + route_id + '/' + type + '/csrf' + i,
        }).then(function (response) {
            if (response.status === 203) {
                $scope.form.csrfmiddlewaretoken = response.data.csrf
            }
        });
    }
    function jsUcfirst(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    function InfoCtrl($scope, instance) {
        $scope.info_data = instance.signatories;
    }
    function DialogCtrl($scope, $scopeIn, $mdDialog, title, $http, module, $routeParams, typer, instance, $timeout, $element) {
        if (module[0] === 'c') {
            $scope.p_daemon = true;
            $scope.p_done = true;
            $scope.selector = false;
            $http({
                method: 'GET',
                url: 'pwa/module/filter/course/' + instance.college
            }).then(function (r) {
                if (r.status === 200) {
                    $scope.courses = r.data.course
                }
            });
            $scope.courseSelected = [];
            $scope.toggleSwitch = function () {
                if ($scope.courseSelected.length === $scope.courses.length) {
                    $scope.courseSelected = [];
                    $scope.select_all = false;
                    $scope.neutral_selection = false
                } else {
                    $scope.courseSelected = $scope.courses;
                    $scope.select_all = true;
                    $scope.neutral_selection = false
                }
                v()
            };
            $scope.updateSwitch = function () {
                if ($scope.courseSelected.length === $scope.courses.length) {
                    $scope.select_all = true;
                    $scope.neutral_selection = false
                } else if ($scope.courseSelected.length <= $scope.courses.length) {
                    $scope.select_all = false;
                    $scope.neutral_selection = true
                } else {
                    $scope.courseSelected = $scope.courses;
                    $scope.select_all = true;
                    $scope.neutral_selection = false
                }
                v()
            };
            $scope.confirm = function () {
                $scope.p_daemon = false;
                let t = [];
                for (let i = 0; i < $scope.courseSelected.length; i++) {
                    t.push($scope.courseSelected[i].id)
                }
                $scope.form.course_list = String(t);
                $scope.form.clearance_id = instance.id;
                require(['oboe', 'param'], function (oboe, param) {
                    oboe({
                        method: 'POST',
                        url: '/pwa/issue/clearance',
                        body: param($scope.form),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .node('!', function (chunks) {
                            $scope.creation_process = chunks.process;
                            $scope.percentage = chunks.process;
                            return oboe.drop;
                        })
                        .done(function (chunks) {
                            if (chunks.process === 100) {
                                $scope.p_daemon = true;
                                $scope.p_done = false;
                            }
                            $scope.selector = true;
                            $scope.confirm_toggler = true;
                            console.log(chunks)
                        })
                        .fail(function (e) {
                            console.log('Opps', e)
                        });
                });
            };

            function v() {
                if ($scope.courseSelected !== undefined && $scope.courses !== undefined) {
                    if ($scope.courseSelected.length < 1) {
                        $scope.confirm_toggler = true
                    } else $scope.confirm_toggler = $scope.courseSelected.length > $scope.courses.length;

                } else {
                    $scope.confirm_toggler = true
                }

            }

            function csrf() {
                $http({
                    method: 'GET',
                    url: '/pwa/issue/clearance',
                }).then(function (response) {
                    if (response.status === 203) {
                        $scope.form.csrfmiddlewaretoken = response.data.csrf
                    }
                });
            }

            v()
        }

        $scope.title = jsUcfirst(typer) + ' ' + title;
        $scope.submit_name = 'Save';
        $scope.t = typer;
        $scope.toggler = true;
        $scope.loadCollege = function () {
            // Use timeout to simulate a 650ms request.
            return $timeout(function () {
                $http({
                    method: 'GET',
                    url: 'pwa/module/provider/college/60/1/unset/unset'
                }).then(function (r) {
                    if (r.status === 200) {
                        $scope.colleges = r.data.rows
                    }
                })
            }, 650);
        };
        // The md-select directive eats keydown events for some quick select
        // logic. Since we have a search input here, we don't need that logic.
        $element.find('input').on('keydown', function (ev) {
            ev.stopPropagation();
        });

        $scope.onChange = function () {
            if (module === 'c') {
                $http.get("pwa/semester/provider")
                    .then(function (response) {
                        console.log(response.data);
                        if (response.status === 200) {
                            $scope.sy = response.data.semester.current.name;
                            $scope.sy_sem = response.data.semester.current.sem;
                            $scope.sem = response.data.semester.list
                        }
                    });
                $http({
                    method: 'GET',
                    url: '/pwa/module/clearance/' + $scope.college.college
                }).then(function (r) {
                    $scope.lists = [
                        {
                            label: "Liabilities",
                            type: 'non',
                            allowedTypes: ['liability'],
                            liabilities: r.data
                        },
                        {
                            label: "Chosen Liabilities",
                            type: 'chosen',
                            allowedTypes: ['liability'],
                            liabilities: []
                        }
                    ];
                    $scope.save = function () {
                        let signatory_list = [];
                        let liability_list = [];
                        let l = $scope.lists[1].liabilities;
                        for (let i = 0; i < l.length; i++) {
                            liability_list.push(l[i].id);
                            for (let j = 0; j < l[i].signatories.length; j++) {
                                signatory_list.push(l[i].signatories[j]["0"][1])
                            }
                        }
                        // Set Data
                        $scope.form.semester_id = $scope.sems;
                        $scope.form.college_id = $scope.college.id;
                        $scope.form.signatory_list = String(signatory_list);
                        $scope.form.liability_list = String(liability_list);
                        csrf();
                        require(['param'], function (param) {
                            $http({
                                method: 'POST',
                                url: '/pwa/module/clearance/college',
                                data: param($scope.form),
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            }).then(function (r) {
                                if (r.status === 200) {
                                    $mdDialog.hide(r.data);
                                    refreshData($scope, $routeParams, NgTableParams);
                                } else {
                                    toast($mdToast, r.data)
                                }
                            })
                        });
                    };
                    $scope.assign = function (instance) {
                        console.log(instance)
                    };
                    $scope.$watch('lists', function (lists) {
                        if (lists[1].liabilities.length > 0) {
                            $scope.toggler = false;
                            csrf()
                        } else {
                            $scope.toggler = true;
                        }
                    }, true);

                    function csrf() {
                        $http({
                            method: 'GET',
                            url: '/pwa/module/clearance/college',
                        }).then(function (response) {
                            if (response.status === 203) {
                                $scope.form.csrfmiddlewaretoken = response.data.csrf
                            }
                        });
                    }
                });
            }
        };
        if (typer === 'edit') {
            $http({
                method: 'GET',
                url: '/pwa/' + $routeParams.card + '/edit/' + module + '/' + instance.id
            }).then(function (r) {
                console.log(r, module);
                if (r.status === 200) {
                    switch (module) {
                        case 'cdt1':
                            $scope.form.abbreviation = r.data.initial.abbreviation;
                            $scope.form.college = r.data.initial.college;
                            $scope.form.name = r.data.initial.name;
                            $scope.form.type = r.data.initial.type;
                            break;
                        case 'cdt2':
                            console.log(r.data.initial.type);
                            $scope.form.type = r.data.initial.type;
                            break;
                        case 'cdt3':
                            $scope.form.position = r.data.initial.position;
                            break;
                        case 'cd':
                            $scope.form.sys = r.data.initial.sys;
                            $scope.form.clearance_start = r.data.initial.clearance_start;
                            $scope.form.clearance_end = r.data.initial.clearance_start;
                            break;
                        case 'cl':
                            $scope.form.liability = r.data.initial.liability;
                            $scope.form.description = r.data.initial.description;
                            $scope.form.signatory = r.data.initial.signatory;
                            $scope.form.course = r.data.initial.course;
                            $scope.form.sem = r.data.initial.sem;
                            $scope.form.sys = r.data.initial.sys;
                            break;
                        case 'college':
                            $scope.form.college = r.data.initial.college;
                            $scope.form.description = r.data.initial.description;
                            break;
                        case 'course':
                            $scope.form.course = r.data.initial.course;
                            $scope.form.description = r.data.initial.description;
                            $scope.form.department = r.data.initial.department;
                            $scope.form.college = r.data.initial.college;
                            break;
                        case 'semester':
                            $scope.form.sem = r.data.initial.sem;
                            $scope.form.yr1 = r.data.initial.yr1;
                            $scope.form.yr2 = r.data.initial.yr2;
                            $scope.form.clearance_start = r.data.initial.clearance_start;
                            $scope.form.clearance_end = r.data.initial.clearance_end;
                            break;
                        case 'users':
                            $scope.form.number = r.data.initial.number;
                            $scope.form.first_name = r.data.initial.first_name;
                            $scope.form.last_name = r.data.initial.last_name;
                            $scope.form.middle_name = r.data.initial.middle_name;
                            $scope.form.birth_date = r.data.initial.birth_date;
                            $scope.form.address = r.data.initial.address;
                            $scope.form.type = r.data.initial.type;
                            break;
                        case 'student':
                            $scope.form.number = r.data.initial.number;
                            $scope.form.year = r.data.initial.year;
                            $scope.form.course = r.data.initial.course;
                            break;
                        case 'signatory':
                            $scope.form.number = r.data.initial.number;
                            $scope.form.assign = r.data.initial.assign;
                            $scope.form.position = r.data.initial.position;
                            $scope.form.signature = r.data.initial.signature;
                            break;
                        case 'types':
                            $scope.form.type = r.data.initial.type;
                            break;
                        default:
                            console.log(module)
                    }
                }
            });
        }
        $scope.submit = function (type) {
            require(['param'], function (param) {
                let i = '';
                if (instance.id) {
                    i = '/' + instance.id;
                }
                $http({
                    method: 'POST',
                    url: '/pwa/' + $routeParams.card + '/' + typer + '/' + module + i,
                    data: param($scope.form),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).then(function (r) {
                    switch (r.status) {
                        case 200:
                            refreshData($scopeIn, $routeParams, NgTableParams);
                            $mdDialog.hide('Success');
                            break;
                        case 203:
                            $scope.server_errors = r.data;
                            csrf_refresh($scope, $http, $routeParams.card, type);
                            break;
                    }
                })
            });
        };
        $scope.hide = function () {
            $mdDialog.hide('Closed');
        };
        $scope.cancel = function () {
            $mdDialog.cancel();
        };

    }
    function toast($mdToast, content) {
        $mdToast.show(
            $mdToast.simple()
                .textContent(content)
                .position('bottom right')
                .hideDelay(3000)
        );
    }
});
