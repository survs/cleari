define(["main"], function (main) {
    main.controller("ReportsCtrl", function ($scope, $rootScope, $http, $mdDialog, $mdToast) {
        $rootScope.page_type = 'Reports';
        let id = '';
        $http.get("pwa/reports/option/provider/")
            .then(function (response) {
                console.log(response.data);
                if (response.status === 200) {
                    $scope.privilege_list = response.data.privilege;
                    id = response.data.id;}
            });
        $scope.open = function (instance, ev) {
            $mdDialog.show({
                locals: {
                    id: id,
                    $scopeIn: $scope,
                    instance: instance
                },
                controller: "ReportDialogCtrl",
                templateUrl: 'static/templates/dynamic.report.dialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: true
            }).then(function (answer) {
                toast($mdToast, answer)
            }, function () {
                toast($mdToast, 'Canceled')
            });
        };
    });

    main.controller("ReportDialogCtrl", function ($scope, $scopeIn, $mdDialog, $http, instance, id, $window, $timeout) {
        $scope.title = instance.name;
        let base_url = 'pwa/reports/gen/'+id+'/';
        let kwargs = {};
        $scope.course_list = '';
        $scope.college_list = '';
        $scope.year_list = '';
        $http.get("pwa/reports/source/provider/")
            .then(function (response) {
                if (response.status === 200) {
                    let r = response.data;
                    $scope.course_list = r.course;
                    $scope.college_list = r.college;
                    $scope.year_list = r.year;
                    id = r.id;}
            });
        switch (instance.type){
            case 'userstudent':
                $scope.tog = true;
                $scope.course_switch = function(e){
                    if (e !== 'unset'){
                        kwargs[instance.q.course] = e
                    }else{
                        delete kwargs[instance.q.course];
                    }
                };
                $scope.college_switch = function(e){
                    if (e !== 'unset'){
                        kwargs[instance.q.college] = e
                    }else{
                        delete kwargs[instance.q.college];
                    }
                };
                $scope.year_switch = function(e){
                    if (e !== 'unset'){
                        kwargs[instance.q.year] = e
                    }else{
                        delete kwargs[instance.q.year];
                    }
                };
                break;
            case 'usersignatory':
                $scope.tog = false;
                $scope.position_switch = function (e) {
                    if (e !== 'unset'){
                        kwargs[instance.q.position] = e
                    }else{
                        delete kwargs[instance.q.position];
                    }
                };
                $scope.assign_switch = function (e) {
                    if (e !== 'unset'){
                        kwargs[instance.q.assign] = e
                    }else{
                        delete kwargs[instance.q.assign];
                    }
                };
                $scope.group_switch = function (e) {
                    if (e !== 'unset'){
                        kwargs[instance.q.group] = e
                    }else{
                        delete kwargs[instance.q.group];
                    }
                };
                $scope.college_switch = function (e) {
                    if (e !== 'unset'){
                        kwargs[instance.q.college] = e
                    }else{
                        delete kwargs[instance.q.college];
                    }
                };
                break;
            case 'violation':
                $scope.tog = true;
                $scope.course_switch = function(e){
                    if (e !== 'unset'){
                        kwargs[instance.q.course] = e
                    }else{
                        delete kwargs[instance.q.course];
                    }
                };
                $scope.college_switch = function(e){
                    if (e !== 'unset'){
                        kwargs[instance.q.college] = e
                    }else{
                        delete kwargs[instance.q.college];
                    }
                };
                $scope.year_switch = function(e){
                    if ($scope.e !== 'unset'){
                        kwargs[instance.q.year] = e
                    }else{
                        delete kwargs[instance.q.year];
                    }
                };
                break;
            case 'receiptstudents':
                $scope.tog = true;
                $scope.course_switch = function(e){
                    if (e !== 'unset'){
                        kwargs[instance.q.course] = e
                    }else{
                        delete kwargs[instance.q.course];
                    }
                };
                $scope.college_switch = function(e){
                    if (e !== 'unset'){
                        kwargs[instance.q.college] = e
                    }else{
                        delete kwargs[instance.q.college];
                    }
                };
                $scope.year_switch = function(e){
                    if (e !== 'unset'){
                        kwargs[instance.q.year] = e
                    }else{
                        delete kwargs[instance.q.year];
                    }
                };
                break;
            case 'receiptsignatory':
                $scope.tog = false;
                $scope.position_switch = function (e) {
                    if (e !== 'unset'){
                        kwargs[instance.q.position] = e
                    }else{
                        delete kwargs[instance.q.position];
                    }
                };
                $scope.assign_switch = function (e) {
                    if (e !== 'unset'){
                        kwargs[instance.q.assign] = e
                    }else{
                        delete kwargs[instance.q.assign];
                    }
                };
                $scope.group_switch = function (e) {
                    if (e !== 'unset'){
                        kwargs[instance.q.group] = e
                    }else{
                        delete kwargs[instance.q.group];
                    }
                };
                $scope.college_switch = function (e) {
                    if (e !== 'unset'){
                        kwargs[instance.q.college] = e
                    }else{
                        delete kwargs[instance.q.college];
                    }
                };
                break;
            default:
                console.log('Ghost...')
        }
        $scope.default = function () {
            $http({
                method: 'GET',
                url: base_url+ instance.privilege +'/'+ JSON.stringify(kwargs) || 'unset',
                responseType: 'blob'
            }).then(function (data) {
                console.log(data);
                let linkElement = document.createElement('a');
                try {
                    console.log(data)
                    let url = window.URL.createObjectURL(data.data);
                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute("download",
                        'Report-'+instance.privilege);

                    let clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });
                    linkElement.dispatchEvent(clickEvent);
                } catch (ex) {
                    console.log(ex);
                }
            });
        };
        $scope.preview = function(){
            $window.open(base_url + instance.privilege +'/'+JSON.stringify(kwargs) || 'unset');
        };
        $scope.hide = function () {
            $mdDialog.hide('Closed');
        };
        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    });

    function toast($mdToast, content) {
        $mdToast.show(
            $mdToast.simple()
                .textContent(content)
                .position('bottom right')
                .hideDelay(3000)
        );
    }
});
