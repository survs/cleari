define(["main"], function (main) {
    main.controller("LogoutCtrl", function ($scope, $rootScope, $http, $location, $window) {
        $http.get("pwa/logout").then(function (response) {
            if (response.status === 202) {
                $location.path('/login');
                $window.location.reload();
            } else if (response.status === 203) {
                $location.path('/login');
                $window.location.reload();
            }
        });
    })
});