define(["main"], function (main) {

    main.controller('ChatCtrl', ['$scope', '$rootScope', '$routeParams', '$http', '$mdToast', '$mdDialog', '$timeout', '$window', '$mdMenu', 'Messenger', 'systemFactory', ChatCtrl]);
    main.controller('ActionCtrl', ['$scope', '$http', '$mdToast', '$mdDialog', '$copyToClipboard', '$filter', 'instance', 'init', 'Messenger', ActionCtrl]);
    main.controller('ActionFileCtrl', ['$scope', '$http', '$mdToast', '$mdDialog', '$copyToClipboard', '$filter', 'instance', 'init', 'Messenger', ActionFileCtrl]);
    main.controller('WatchCtrl', ['instance', 'init', '$sce', WatchCtrl]);

    function ChatCtrl($scope, $rootScope, $routeParams, $http, $mdToast, $mdDialog, $timeout, $window, $mdMenu, Messenger, systemFactory) {
        $scope.files_base_url = '/pwa/message/d/files/';
        $scope.chat_list = [];
        $scope.recipient_name = '';
        $scope.go_back = function() {
            $window.history.back();
        };
        let me = '';
        let them = '';
        let me_100 = '';
        let them_100 = '';
        $scope.chat_box = false;
        let chat = this;
        systemFactory.set_main_scope(undefined);
        systemFactory.set_inbox_scope(undefined);
        systemFactory.set_chat_scope($scope);
        Messenger.chatProfile($routeParams.chat_id).then(function (r) {
            me = r.me_40;
            them = r.them_40;
            me_100 = r.me_100;
            them_100 = r.them_100;
            $scope.recipient_name = r.name;
            Messenger.pushMessage($scope, $routeParams.chat_id).then(function () {
                $scope.chat_progress = false;
                $scope.chat_box = false;
                if ($scope.chat_list.length < 9) {
                    Messenger.loadMessage($scope, $routeParams.chat_id).then(function () {
                        $timeout(function () {
                            n["0"].childNodes["0"].scrollTop = n["0"].childNodes["0"].scrollHeight;
                            $scope.chat_box = true;
                        }, 1000)
                    })
                } else {
                    $timeout(function () {
                        n["0"].childNodes["0"].scrollTop = n["0"].childNodes["0"].scrollHeight;
                        $scope.chat_box = true;
                    }, 1000)
                }
            }, function () {
                toast($mdToast, 'Failed to fetch messages')
            });
        });
        $scope.convo_info = function(){
            $mdDialog.show(
                $mdDialog.alert()
                    .title('Info')
                    .htmlContent('<div class="unite_center"><div class="flex unite_wrapper">' +
                        '       <div class="ch1"><img src="'+me_100+'"' +
                                 '></div><div class="ch2">' +
                                 '<img src="'+them_100+'"></div></div></div> ')
                    .ariaLabel('Message Details')
                    .ok('Got it')
            )

        };
        $scope.convo_delete = function(){

        };
        $scope.action = function (ev, instance) {
            $mdDialog.show({
                locals: {instance: instance, init: $scope},
                controller: "ActionCtrl as ic",
                templateUrl: '/static/templates/action.dialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: false
            }).then(function (value) {
                toast($mdToast, value)
            }, function () {

            });
        };
        $scope.action_file = function (ev, instance) {
            $mdDialog.show({
                locals: {instance: instance, init: $scope},
                controller: "ActionFileCtrl as ic",
                templateUrl: '/static/templates/action_file.dialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: false
            }).then(function (value) {
                toast($mdToast, value)
            }, function () {

            });
        };
        $scope.reply = function () {
            Messenger.set_chat_id($routeParams.chat_id);
            Messenger.set_body($scope.body);
            Messenger.set_attachment($scope.attachment);
            Messenger.reply().then(function (response) {
                $scope.chat_list.push(response);
                $scope.body = '';
                $scope.attachment = undefined;
                n["0"].childNodes["0"].scrollTop = n["0"].childNodes["0"].scrollHeight;
            })
        };
        $scope.add_attachment = function() {
            $timeout(function() {
                angular.element(document.querySelector('#fileSelector')).triggerHandler('click');
            });
        };
        $scope.load_old = function () {
            $scope.chat_progress = true;
            Messenger.loadMessage($scope, $routeParams.chat_id).then(function () {
                $scope.chat_progress = false;
            });
        };
        $scope.bth = function (byts) {
            if (byts!==undefined){
                let kb = byts / 1024;
            if (kb > 1024){
                let mb = kb / 1024;
                if (mb > 1024) {
                    let gb = mb / 1024;
                    if (gb > 1024) {
                        let tb = gb / 1024;
                        if (tb > 1024) {
                            return Math.round(gb) + ' TB'
                        } else {
                            return Math.round(gb) + ' TB'
                        }
                    }else{
                        return Math.round(gb) + ' GB'
                    }
                }else {
                    return Math.round(mb) + ' MB'
                }
            }else {
                return Math.round(kb) + ' KB'
            }
            }
        };
        let n = angular.element(document.querySelector('#vertical-container'));
        $scope.chat_head = function (t) {
            if (t === 'me') {
                return me
            } else {
                return them
            }
        };
    }


    function WatchCtrl(instance, init, $sce) {
        this.instance = instance;
        this.theme = {url: "s/css/videogular.css"};
        this.sources = [{
            src: $sce.trustAsResourceUrl(init.files_base_url + instance.attachment.blob_url),
            type: instance.attachment.content_type
        }]
    }

    function ActionCtrl(scope, http, toaster, dialog, ctc, filter, instance, init, Messenger) {
            let ic = this;
            ic.intance = instance;
            ic.detail = function () {
                dialog.hide();
                dialog.show(
                    dialog.alert()
                        .title('Message Details')
                        .htmlContent('<span>Type: '+instance.type+'</span><br/>\n' +
                                     '<span>From: '+instance.placeholder.user+'</span><br/>'+
                                     '<span>Sent: '+filter('date')(instance.created, 'MMMM d, y h:mm a')+'</span>')
                        .ariaLabel('Message Details')
                        .ok('Got it')
                )
            };
            ic.copy = function () {
                ctc.copy(instance.body).then(function () {
                    dialog.hide('Message Copied');
                });
            };
            ic.delete = function () {
                Messenger.del(instance.msg_id, instance.id).then(function () {
                    let i = init.chat_list.findIndex(value => value.id === instance.id);
                    init.chat_list.splice(i, 1);
                    dialog.hide('Deleted');
                }, function (r2) {
                    dialog.hide('Failed ' + r2.status)
                })
            };
        }

    function ActionFileCtrl(scope, http, toaster, dialog, ctc, filter, instance, init, Messenger) {
            let ic = this;
        ic.instance = instance;
        ic.watch = function () {
            dialog.hide();
            dialog.show({
                locals: {instance: instance, init: init},
                controller: "WatchCtrl as ic",
                templateUrl: '/static/templates/watch.dialog.tmpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: false
            }).then(function (value) {
                toast($mdToast, value)
            });
        };
            ic.detail = function () {
                dialog.hide();
                dialog.show(
                    dialog.alert()
                        .title('Attachment Details')
                        .htmlContent('<span>Type: '+instance.type+'</span><br/>\n' +
                            '<span>File Type: ' + '(' + String(instance.attachment.ext).toUpperCase() + ') - ' + instance.attachment.ext_desc + '</span><br/>\n' +
                                     '<span>From: '+instance.placeholder.user+'</span><br/>'+
                                     '<span>Sent: '+filter('date')(instance.created, 'MMMM d, y h:mm a')+'</span>')
                        .ariaLabel('Message Details')
                        .ok('Got it')
                )
            };
            ic.copy = function () {
                ctc.copy(instance.body).then(function () {
                    dialog.hide('Message Copied');
                });
            };
        ic.download = function () {
            http({
                method: 'GET',
                url: init.files_base_url + instance.attachment.blob_url,
                params: {name: name},
                responseType: 'arraybuffer'
            }).then(function (data) {
                let filename = instance.attachment.name;
                let contentType = instance.attachment.content_type;

                let linkElement = document.createElement('a');
                try {
                    let blob = new Blob([data.data], {type: contentType});
                    let url = window.URL.createObjectURL(blob);

                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute("download", filename);

                    let clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });
                    linkElement.dispatchEvent(clickEvent);
                } catch (ex) {
                    console.log(ex);
                }
            });
        };
            ic.delete = function () {
                Messenger.del(instance.msg_id, instance.id).then(function () {
                    let i = init.chat_list.findIndex(value => value.id === instance.id);
                    init.chat_list.splice(i, 1);
                    dialog.hide('Deleted');
                }, function (r2) {
                    dialog.hide('Failed ' + r2.status)
                })
            };
        }
    function toast($mdToast, content) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(content)
                    .position('bottom right')
                    .hideDelay(3000)
            );
    }
});

