define(["main"], function (main) {

    main.controller("SettingsCtrl", function ($scope, $rootScope, $http, $location, $mdDialog, $mdToast, $timeout) {
        $rootScope.page_type = 'Settings';
        let n = this;
        this.setPassword = function () {
            $mdDialog.show({
                    controller: "PasswordCtrl",
                    templateUrl: '/static/templates/password.dialog.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    fullscreen: false
                })
                    .then(function (answer) {
                        //Somethind todo when user answer the button
                    }, function () {
                        toast($mdToast, "Dialog closed")
                    });
            };
        this.setEmail = function () {
            $mdDialog.show({
                    controller: "EmailCtrl",
                    templateUrl: '/static/templates/email.dialog.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    fullscreen: false
                })
                    .then(function (answer) {
                        //Somethind todo when user answer the button
                    }, function () {
                        toast($mdToast, "Dialog closed")
                    });
            };
        this.togglePushWeb = function () {
            $scope.pm = false;
            $timeout(function() {
                document.querySelector('.js-push-btn').click();
                let d = document.querySelector('meta[name="push_status"]');
                switch (d.content){
                    case 0:
                        $scope.enabled = false;
                        $scope.pm1 = false;
                        $scope.pm = false;
                        break;
                    case 1:
                        $scope.enabled = true;
                        $scope.pm1 = false;
                        $scope.pm = true;
                        $scope.pmm = 'Turn off Push Notification';
                        break;
                    case 2:
                        $scope.pm = true;
                        $scope.pmm = 'Push Push Notification Block by Browser';
                        $scope.pm1 = true;
                        alert('Push Push Notification Block by Browser');
                        break;

                }
            });
        }
    });

    main.controller("PasswordCtrl", function ($scope, $http, $q,  $mdDialog, $mdToast, $window) {
        function csrf() {
                let deferred = $q.defer();
                $http({
                    method: 'GET',
                    url: '/pwa/user/provider/csrf',
                }).then(function (response) {
                    if (response.status === 203) {
                        deferred.resolve({csrf:response.data.csrf});
                    }else {
                        deferred.reject('There was an error');
                    }
                });
                return deferred.promise;
            }
        $scope.submit = function(){
            csrf().then(function (key) {
                       let fd = new FormData();
                       fd.append('old', $scope.oldPass);
                       fd.append('new', $scope.newPass);
                       fd.append('csrfmiddlewaretoken', key.csrf);
                       $http.post("pwa/configure/password", fd, {
                           transformRequest: angular.identity,
                           headers: {'Content-Type': undefined}
                       }).then(function (r) {
                           if (r.status===200){
                               toast($mdToast,  r.data);
                               $window.location.reload();
                           }else{
                               toast($mdToast,  r.data)
                           }
                       });
                   });
        };
        $scope.cancel = function () {
                $mdDialog.cancel();
            };
    });
    main.controller("EmailCtrl", function ($scope, $http, $q, $mdDialog, $mdToast) {
        function csrf() {
                let deferred = $q.defer();
                $http({
                    method: 'GET',
                    url: '/pwa/user/provider/csrf',
                }).then(function (response) {
                    if (response.status === 203) {
                        deferred.resolve({csrf:response.data.csrf});
                    }else {
                        deferred.reject('There was an error');
                    }
                });
                return deferred.promise;
            }
        $http({
            method: 'GET',
            url: 'pwa/configure/email',
        }).then(function (response) {
            if (response.status === 200) {
                $scope.email_set = response.data.email
            }
        });
        $scope.cancel = function () {
                $mdDialog.cancel();
            };
        $scope.submit = function(){
            csrf().then(function (key) {
                       let fd = new FormData();
                       fd.append('email', $scope.email_set);
                       fd.append('csrfmiddlewaretoken', key.csrf);
                       $http.post("pwa/configure/email", fd, {
                           transformRequest: angular.identity,
                           headers: {'Content-Type': undefined}
                       }).then(function (r) {
                           if (r.status===200){
                               toast($mdToast,  r.data)
                           }else{
                               toast($mdToast,  r.data)
                           }
                       });
                   });
        };
    });
    function toast($mdToast, content) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(content)
                    .position('bottom right')
                    .hideDelay(3000)
            );
        }
});