define(["angularAMD", "angular-route", "angular", "angular-animate", "angular-aria", "angular-messages", "angular-material", "angular-sanitize", "loadCSS", "ng-file-upload", "angular-drag-and-drop-lists", "eventsource.min", "reconnecting-eventsource", "smart-table.min", "angular-moment", "videogular", "vg-controls", "vg-overlay-play", "vg-buffering"], function (angularAMD) {
    let main = angular.module("webapp", ["ngRoute", 'ngMaterial', 'ngSanitize', 'ngMessages', 'ngFileUpload', 'dndLists', 'smart-table', 'angularMoment', 'com.2fdevs.videogular', 'com.2fdevs.videogular.plugins.controls',
        'com.2fdevs.videogular.plugins.overlayplay', 'com.2fdevs.videogular.plugins.buffering']);
    main.directive('head', ['$rootScope', '$compile',
        function ($rootScope, $compile) {
            return {
                restrict: 'E',
                link: function (scope, elem) {
                    var html = '<link rel="stylesheet" ng-repeat="(routeCtrl, cssUrl) in routeStyles" ng-href="{{cssUrl}}" />';
                    elem.append($compile(html)(scope));
                    scope.routeStyles = {};
                    $rootScope.$on('$routeChangeStart', function (e, next, current) {
                        if (current && current.$$route && current.$$route.css) {
                            if (!angular.isArray(current.$$route.css)) {
                                current.$$route.css = [current.$$route.css];
                            }
                            angular.forEach(current.$$route.css, function (sheet) {
                                delete scope.routeStyles[sheet];
                            });
                        }
                        if (next && next.$$route && next.$$route.css) {
                            if (!angular.isArray(next.$$route.css)) {
                                next.$$route.css = [next.$$route.css];
                            }
                            angular.forEach(next.$$route.css, function (sheet) {
                                scope.routeStyles[sheet] = sheet;
                            });
                        }
                    });
                }
            };
        }
    ]);
    main.directive('ngScrollBottom', ['$timeout', function ($timeout) {
  return {
    scope: {
      ngScrollBottom: "="
    },
    link: function ($scope, $element) {
      $scope.$watchCollection('ngScrollBottom', function (newValue) {
          if (newValue) {
          $timeout(function(){
              if (!!$element[0].childNodes[0].scrollTop) {
                  $element[0].childNodes[0].scrollTop = $element[0].childNodes[0].scrollHeight
              } else {
                  $element[0].childNodes[0].scrollTop = 0
              }
          }, 0);
        }
      });
    }
  }
}]);
    main.directive('execOnScrollToTop', function () {

        return {

            restrict: 'A',
            link: function ($scope, $element, $attrs) {
                let fn = $scope.$eval($attrs.execOnScrollToTop);
                $element["0"].childNodes["0"].onscroll = function (e) {
                    if (!e.target.scrollTop) {
                        $scope.$apply(fn);
                    }

                };
            }

        };

    });
    main.directive('onLongPress', function($timeout) {
    	return {
    		restrict: 'A',
    		link: function($scope, $elm, $attrs) {
    			$elm.bind('touchstart', function(evt) {
    				// Locally scoped variable that will keep track of the long press
    				$scope.longPress = true;

    				// We'll set a timeout for 600 ms for a long press
    				$timeout(function() {
    					if ($scope.longPress) {
    						// If the touchend event hasn't fired,
    						// apply the function given in on the element's on-long-press attribute
    						$scope.$apply(function() {
    							$scope.$eval($attrs.onLongPress)
    						});
    					}
    				}, 600);
    			});

    			$elm.bind('touchend', function(evt) {
    				// Prevent the onLongPress event from firing
    				$scope.longPress = false;
    				// If there is an on-touch-end function attached to this element, apply it
    				if ($attrs.onTouchEnd) {
    					$scope.$apply(function() {
    						$scope.$eval($attrs.onTouchEnd)
    					});
    				}
    			});
    		}
    	};
    })
    main.provider('$copyToClipboard', [function () {
            this.$get = ['$q', '$window', function ($q, $window) {
                let body = angular.element($window.document.body);
                let textarea = angular.element('<textarea/>');
                textarea.css({
                    position: 'fixed',
                    opacity: '0'
                });
                return {
                    copy: function (stringToCopy) {
                        var deferred = $q.defer();
                        deferred.notify("copying the text to clipboard");
                        textarea.val(stringToCopy);
                        body.append(textarea);
                        textarea[0].select();

                        try {
                            var successful = $window.document.execCommand('copy');
                            if (!successful) throw successful;
                            deferred.resolve(successful);
                        } catch (err) {
                            deferred.reject(err);
                            //window.prompt("Copy to clipboard: Ctrl+C, Enter", toCopy);
                        } finally {
                            textarea.remove();
                        }
                        return deferred.promise;
                    }
                };
            }];
        }]);
    main.config(function ($routeProvider, $locationProvider) {
        $routeProvider
            .when("/", angularAMD.route({
                 templateUrl: "/pwa/home",
                 controller: "HomeCtrl",
                 controllerUrl: "static/js/home.js",
                 css: ['static/css/home.css', 'static/css/md-data-table.res.css', 'static/css/student.css'],
            }))
            .when("/home", angularAMD.route({
                templateUrl: "/pwa/home",
                controller: "HomeCtrl",
                controllerUrl: "static/js/home.js",
                css: ['static/css/home.css', 'static/css/md-data-table.res.css', 'static/css/student.css'],
            }))
            .when("/home/manage/:card", angularAMD.route({
                controller: "ManageCtrl",
                controllerUrl: 'static/js/manage.js',
                css: ['static/css/manage.css', 'static/css/md-data-table.res.css'],
                templateUrl:
                    function (stateParams) {
                        return 'pwa/home/manage/' + stateParams.card;
                    }
            }))
            .when("/profile", angularAMD.route({
                templateUrl: "/pwa/profile",
                controller: "ProfileCtrl",
                controllerUrl: "static/js/profile.js",
                css: ['static/css/profile.css'],
            }))
            .when("/inbox", angularAMD.route({
                templateUrl: "pwa/message/view/provider/inbox",
                controller: "InboxCtrl",
                controllerUrl: "static/js/inbox.js",
                css: ['static/css/inbox.css', 'static/css/animate.min.css'],
            }))
            .when("/inbox/chat/:chat_id", angularAMD.route({
                controller: "ChatCtrl as chat",
                controllerUrl: "static/js/chat.js",
                templateUrl: "pwa/message/view/provider/chat",
                css: ['static/css/chat.css', 'static/css/animate.min.css'],
            }))
            .when("/settings", angularAMD.route({
                templateUrl: "/static/templates/settings.tmpl.html",
                controller: "SettingsCtrl",
                controllerUrl: "static/js/settings.js",
                css: ['static/css/settings.css'],
            }))
            .when("/about", angularAMD.route({
                templateUrl: "/pwa/about",
                controller: "AboutCtrl",
                controllerUrl: "static/js/about.js",
                css: ['static/css/about.css'],
            }))
            .when("/reports", angularAMD.route({
                templateUrl: "/pwa/reports",
                controller: "ReportsCtrl",
                controllerUrl: "static/js/report.js",
                css: ['static/css/report.css', 'static/css/animate.min.css'],
            }))
            .when("/login", angularAMD.route({
                templateUrl: "/pwa/login/page",
                controller: "LoginCtrl",
                controllerUrl: "static/js/login.js",
                css: ['static/css/login.css'],
            }))
            .when("/logout", angularAMD.route({
                templateUrl: "/pwa/logout",
                controller: "LogoutCtrl",
                controllerUrl: 'static/js/logout.js',
            }))
            .when("/sandbox", angularAMD.route({
                templateUrl: "/pwa/sandbox",
                controller: "SandboxCtrl",
                controllerUrl: 'static/js/sandbox.js',
                css: ['static/css/sandbox.css'],
            }))
            .otherwise({
                redirectTo: "/"
            });
        $locationProvider.hashPrefix('');
    });
    main.controller('mainController', function ($scope, $rootScope, $routeParams, $timeout, $mdSidenav, $location, $mdDialog, $http, $interval, $window, systemFactory) {
        loadCSS("static/css/angular-material.min.css");
        loadCSS("static/css/docs.css");
        loadCSS("static/iconfont/material-icons.css");
        loadCSS("static/iconfont/flaticon.css");
        loadCSS("static/css/nav.css");
        loadCSS("static/css/app.css");
        $rootScope.$on('$routeChangeStart', function () {
            $scope.main_view = true;
            $scope.route_loader = false;
            $rootScope.page_type = '';
        });
        $rootScope.$on('$viewContentLoaded', function () {
            timebound();
            $timeout(function () {
                $scope.route_loader = true;
                $scope.progressDisabler = true;
            }, 2000);
            $http({
                method: 'GET',
                url: 'pwa/verify_user'
            }).then(function (response) {
                if (response.status === 204) {
                    $rootScope.route_loader = false;
                    $location.path('/login');
                } else {
                    if ($location.path() === '/login') {
                        $location.path('/home')
                    } else {
                        $scope.route_loader = true;
                    }
                }
            });
            $scope.go_back = function() {
                $window.history.back();
            };
            $scope.main_view = false;
            if (($rootScope.page_type === 'Home') ||
                ($rootScope.page_type === 'Profile') ||
                ($rootScope.page_type === 'Settings') ||
                ($rootScope.page_type === 'About') ||
                ($rootScope.page_type === 'Manage') ||
                ($rootScope.page_type === 'Reports') ||
                ($rootScope.page_type === 'Sandbox')){
                systemFactory.set_chat_scope(undefined);
                systemFactory.set_inbox_scope(undefined);
                systemFactory.set_main_scope($scope);
            }
            if (($rootScope.page_type === 'Home') ||
                ($rootScope.page_type === 'My Messages') ||
                ($rootScope.page_type === 'Profile') ||
                ($rootScope.page_type === 'Settings') ||
                ($rootScope.page_type === 'About') ||
                ($rootScope.page_type === 'Manage') ||
                ($rootScope.page_type === 'Chat') ||
                ($rootScope.page_type === 'Reports') ||
                ($rootScope.page_type === 'Sandbox')) {
                $scope.menu_button = false;
                $http.get("/pwa/home/user_info")
                    .then(function (response) {
                        if (response.status === 200) {
                            $scope.username = response.data.name;
                            $rootScope.user_type = response.data.type;
                            $http.get("/pwa/home/" + $rootScope.user_type + "_menu")
                                .then(function (response) {
                                    if (response.status === 200) {
                                        $scope.navigation_menus = response.data
                                    }
                                });
                            $http.get("/pwa/home/" + $rootScope.user_type + "_options")
                                .then(function (response) {
                                    if (response.status === 200) {
                                        $scope.navigation_options = response.data
                                    }
                                });
                        }
                    });
            }
            if ($rootScope.page_type === 'Manage') {
                $scope.subheader_loader = true;
                $scope.ht_html =
                    "<span>Home Dashboard</span>" +
                    "<span class=\"hide-sm hide-xs\"><i class=\"material-icons\">navigate_next</i></span>" +
                    $rootScope.page_type +
                    "<i class=\"material-icons\">navigate_next</i>" +
                    jsUcfirst($routeParams.card);
                $rootScope.clearance_subheader = true;
            } else {
                $scope.subheader_loader = true;
                $scope.ht_html = $rootScope.page_type;
                $rootScope.clearance_subheader = true;
            }
        });
        timer();
        let tn = document.getElementsByClassName('togglernav');
        $scope.onSwipeLeft = function() {
            $mdSidenav('right').toggle()
        };
        $scope.onSwipeRight = function() {
            $mdSidenav('left').toggle()
         };
        systemFactory.set_Sidenav($mdSidenav);
        $scope.tc = 'keyboard_arrow_left';
        $scope.nav_collapse_text = 'Collapse Navigation';
        $scope.toggleLeft = systemFactory.buildToggler('left');
        $scope.toggleCollapse = function () {
            $scope.nav_title_collapse = !$scope.nav_title_collapse;
            if ($scope.nav_collapse === true) {
                $scope.nav_collapse_text = 'Collapse Navigation';
                $scope.tc = 'keyboard_arrow_left';
                $scope.nav_collapse = false;
                tn[0].style.width = '';
                //tn[1].style.width = '';
            } else {
                $scope.nav_collapse_text = 'Expand Navigation';
                $scope.tc = 'keyboard_arrow_right';
                $scope.nav_collapse = true;
                tn[0].style.width = '57px';
            }
        };

        function timebound() {
            $http.get("/pwa/timebound/current")
                .then(function (response) {
                    if (response.status === 200) {
                        $timeout(function () {
                            $rootScope.subheader_loader = true;
                        }, 1000);
                        timeboundStorage(response.data.start, response.data.end, response.data.current, 'set');
                        $timeout(
                            function () {
                                timer();
                            }, 500
                        )
                    }
                });
        }

        function timer() {
            let r = timeboundStorage(0, 0, 0, 'get');
            let countDownDate = new Date(r.due).getTime();
            let x = $interval(function () {
                let n = new Date().getTime();
                let distance = countDownDate - n;
                let days = Math.floor(distance / (1000 * 60 * 60 * 24));
                let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                let seconds = Math.floor((distance % (1000 * 60)) / 1000);
                $rootScope.timebound_counter = days + 'd ' + hours + 'h ' + minutes + 'm ' + seconds + 's';
                $rootScope.days = days;
                $rootScope.hours = hours;
                $rootScope.minutes = minutes;
                $rootScope.seconds = seconds;
                if (days === 0) {
                    $rootScope.dz = true
                } else {
                    $rootScope.dz = false
                }
                if (hours === 0) {
                    $rootScope.hz = true
                } else {
                    $rootScope.hz = false
                }
                if (minutes === 0) {
                    $rootScope.mz = true
                } else {
                    $rootScope.mz = false
                }
                if (seconds === 0) {
                    $rootScope.sz = true
                } else {
                    $rootScope.sz = false
                }
                if (distance < 0) {
                    $interval.cancel(x);
                    $rootScope.countdown_expired = true
                }
            }, 1000);
        }

        function timeboundStorage(start, due, current, typ) {
            if (typeof(Storage) !== "undefined") {
                if (typ === 'set') {
                    if (start && due) {
                        localStorage.setItem("current", current);
                        localStorage.setItem("start", start);
                        localStorage.setItem("due", due);
                    } else {
                        return console.log('start and due are required')
                    }
                } else if (typ === 'get') {
                    d = {
                        'start': localStorage.getItem("start"),
                        'due': localStorage.getItem("due")
                    };
                    return d
                }
            } else {
                console.log("Sorry, your browser does not support Web Storage...")
            }
        }

        function jsUcfirst(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
        systemFactory.set_instance($scope);
        systemFactory.daemon();
    });
    main.factory('systemFactory', ['$mdToast', '$http',systemFactory]);
    function systemFactory(mdToast, http) {
        let sys = {};
        let mdSidenav;
        let main;
        let _chat_scope;
        let _inbox_scope;
        let _main_scope;
        sys.buildToggler = function(componentId) {
            return function () {
                mdSidenav(componentId).toggle();
            };
        };
        sys.set_Sidenav = function(mds){
            mdSidenav = mds
        };
        sys.set_instance = function (instance) {
            main = instance
        };
        sys.set_page_type = function (type) {
            main.page_type = type
        };
        sys.get_page_type = function () {
            return main.page_type;
        };
        sys.set_page_title = function (title) {
            main.page_title = title
        };
        sys.get_page_title = function () {
            return main.page_title;
        };
        sys.set_chat_scope = function(scope){
            _chat_scope = scope;
        };

        sys.set_inbox_scope = function (scope) {
            _inbox_scope = scope
        };

        sys.set_main_scope = function (scope) {
            _main_scope = scope
        };
        sys.daemon = function () {
            let es = new ReconnectingEventSource('/message/sse');
            let tune = new Audio('static/sound/popcorn.ogg');
            es.onopen = function () {
                console.log('connected');
            };

            es.onerror = function (e) {
                console.log('connection error', e);
            };

            es.addEventListener('stream-reset', function (e) {
                e = JSON.parse(e.data);
                console.log('stream reset: ' + JSON.stringify(e.channels));
            }, false);

            es.addEventListener('stream-error', function (e) {
                // hard stop
                es.close();
                e = JSON.parse(e.data);
                console.log('stream error: ' + e.condition + ': ' + e.text);
            }, false);
            es.addEventListener('message', function (e) {
                console.log('message: ' + e.data);
                let p = JSON.parse(e.data);
                console.log('Inbox:',_inbox_scope,'Main:', _main_scope,'Chat:', _chat_scope);
                if (_inbox_scope!==undefined){
                    http.get("pwa/message/d/inbox").then(function (r) {
                        if (r.status === 200){
                            _inbox_scope.messages = r.data;
                        }else {
                            toast($mdToast, 'Something went wrong fetching new messages')
                        }
                    });
                }
                if (_main_scope!==undefined){
                    console.log(_main_scope)
                    Messagetoast(mdToast, p);
                }
                if (_chat_scope!==undefined){
                    _chat_scope.chat_list.push(p);
                    _chat_scope.$apply();
                }
                tune.play();
            }, false);
            return es
        };
        return sys
    }
    main.service('Messenger', ['$http', '$timeout', '$q', '$mdToast', MessengerService]);
    function MessengerService(http, timeout, q, mdToast) {
        let _attachment;
        let _body = '';
        let _recipient = '';
        let _chat_id = '';
        let x = this;
        function csrf() {
            let deferred = q.defer();
            http({
                method: 'GET',
                url: '/pwa/user/provider/csrf',
            }).then(function (response) {
                if (response.status === 203) {
                    deferred.resolve({csrf:response.data.csrf});
                }else {
                    deferred.reject('There was an error');
                }
            });
            return deferred.promise;
        }

        this.getInbox = function () {
            let deferred = q.defer();
            http.get("pwa/message/d/inbox").then(function (r) {
                if (r.status === 200){
                    deferred.resolve({
                            data:r.data
                        });
                }else {
                    deferred.reject({
                            data:'Can\'t fetch messages'
                        });
                }
            });
            return deferred.promise;
        };

        this.getMaxPage = function (chat_id) {
            let deferred = q.defer();
            http({
                method: 'GET',
                url: 'pwa/message/d/chat/chunks/l/0/' + chat_id + '/1',
            }).then(function (response) {
                if (response.status === 200) {
                    deferred.resolve({
                        num: response.data.num_pages,
                        count: response.data.count
                    });
                } else {
                    deferred.reject('There was an error');
                }
            });
            return deferred.promise;
        };
        this.set_attachment = function (attachment) {
                _attachment = attachment;
            };
        this.set_body = function (body) {
                _body = body
            };
        this.set_chat_id = function (chat_id) {
                _chat_id = chat_id
            };
        this.set_recipient = function (recipient) {
            _recipient = recipient
        };
        this.reply = function () {
                let deferred = q.defer();
                csrf().then(function (key) {
                    let bool = 0;
                    let file;
                    if (_attachment !== undefined){
                        file = _attachment[0];
                        bool = 1
                    }else{
                        bool = 0
                    }
                    let fd = new FormData();
                    fd.append('attachment', file);
                    fd.append('has_attachment', bool);
                    fd.append('body', _body);
                    fd.append('csrfmiddlewaretoken', key.csrf);
                    http.post('/pwa/message/r/reply/' + _chat_id, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    }).then(function (r) {
                        if (r.status===200){
                            deferred.resolve(r.data);
                        }else{
                            deferred.reject('There was an error');
                        }
                    });
                });
            return deferred.promise;
        };
        this.send = function () {
            let deferred = q.defer();
            csrf().then(function (key) {
                let bool = 0;
                let file;
                if (_attachment !== undefined) {
                    file = _attachment[0];
                    bool = 1
                } else {
                    bool = 0
                }
                let fd = new FormData();
                fd.append('attachment', file);
                fd.append('has_attachment', bool);
                fd.append('body', _body);
                fd.append('recipients', _recipient);
                fd.append('csrfmiddlewaretoken', key.csrf);
                http.post('/pwa/message/r/new', fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }).then(function (r) {
                    if (r.status === 200) {
                        deferred.resolve(r.data);
                    } else {
                        deferred.reject('There was an error');
                    }
                });
            });
            return deferred.promise;
        };
        this.del = function (chat_id, bubble_id) {
            let deferred = q.defer();
            http({
                method: 'GET',
                url: '/pwa/message/r/delete/' + chat_id + '/' + bubble_id,
            }).then(function (response) {
                if (response.status === 200) {
                    switch (JSON.parse(response.data).code) {
                        case '200':
                            deferred.resolve({status: 200});
                            break;
                        case '404':
                            deferred.reject({status: 'Not Found'});
                            break;
                        case '500':
                            deferred.reject({status: 'Bad Deletion'});
                            break;
                        default:
                            deferred.reject({status: 'Network Error'});
                    }
                } else {
                    deferred.reject({status: 'Network Error'});
                }
            });
            return deferred.promise;
        };
        this.pushMessage = function (scope, chat_id) {
            let deferred = q.defer();
            this.getMaxPage(chat_id).then(function (r) {
                require(['oboe'], function (oboe) {
                    oboe('pwa/message/d/chat/chunks/c/0/' + chat_id + '/' + r.num)
                        .node('!', function (chunks) {
                            if (chunks.r === undefined) {
                                scope.chat_list.push(chunks);
                            } else if (chunks.r === "eof") {
                                deferred.resolve();
                            } else {
                                deferred.reject();
                            }
                            return oboe.drop;
                        })
                        .done(function () {
                            scope.$apply();
                        });
                });
            });
            return deferred.promise;
        };
        this.loadMessage = function (scope, chat_id) {
            let deferred = q.defer();
            this.getMaxPage(chat_id).then(function (r) {
                let cl_count = scope.chat_list.length;
                let l = Math.round((r.count - cl_count) / 10);
                let m = Math.round(r.count / 10);
                if (l > 0) {
                    require(['oboe'], function (oboe) {
                        oboe('pwa/message/d/chat/chunks/c/1/' + chat_id + '/' + l)
                            .node('!', function (chunks) {
                                if (chunks.r === undefined) {
                                    scope.chat_list.unshift(chunks);
                                } else if (chunks.r === "eof") {
                                    deferred.resolve();
                                } else {
                                    deferred.reject();
                                }
                                return oboe.drop;
                            })
                            .done(function () {
                                scope.$apply();
                            });
                    });
                } else {
                    deferred.resolve();
                }
            });
            return deferred.promise;
        };
        this.chatProfile = function (chat_id) {
            let deferred = q.defer();
            http({
                method: 'GET',
                url: 'pwa/message/d/chat/head/' + chat_id,
            }).then(function (response) {
                if (response.status === 200) {
                    let r = JSON.parse(response.data)
                    deferred.resolve({
                        me_40: r.me_40,
                        them_40: r.them_40,
                        me_100: r.me_100,
                        them_100: r.them_100,
                        name:r.r_name
                    });
                } else {
                    deferred.reject('There was an error');
                }
            });
            return deferred.promise;
        }
    }
    main.controller('NewMessageCtrl', ['$mdConstant', '$mdDialog', '$timeout', '$location', 'Messenger', 'uid', 'name', NewMessageCtrl]);
    function NewMessageCtrl(mdConstant, mdDialog, timeout, location, Messenger, uid, name) {
        let nmc = this;
        nmc.recipient = [];
        nmc.pre_recipient = '';
        nmc.pre_configured = false;
        if (uid){
            nmc.pre_recipient = uid;
            nmc.pre_configured = true;
            nmc.recipient_name = name;
            nmc.message = 'Please be kind to your respective recipients. Your conversation are recorded.'
        }
        nmc.keys = [mdConstant.KEY_CODE.ENTER, mdConstant.KEY_CODE.COMMA, 186];
        nmc.hide = function () {
            mdDialog.hide('Closed');
        };
        nmc.cancel = function () {
            mdDialog.cancel();
        };
        nmc.add_attachment = function() {
            timeout(function() {
                angular.element(document.querySelector('#fileSelector')).triggerHandler('click');
            });
            console.log(nmc.attachment);
        };
        nmc.send = function () {
            let temp;
            if (uid){
                temp = nmc.pre_recipient;
            }else{
                temp = this.recipient.toString()
            }
            Messenger.set_recipient(temp);
            Messenger.set_body(this.body);
            Messenger.set_attachment(this.attachment);
            Messenger.send().then(function (response) {
                if (response.error.length===0 && response.success.length===1){
                    mdDialog.hide('Message sent');
                    location.path('inbox/chat/'+response.success[0].split(':')[1])
                }else if(response.error.length===0 && response.success.length>0){
                    mdDialog.hide('Messages successfully disseminated');
                }else if (response.success.length===0 && response.error.length>0){
                    mdDialog.hide('Messages not disseminated');
                }else{
                    mdDialog.hide(response.success.length+' out of '+
                        Number(response.success.length+response.error.length)+ ' successfully disseminated');
                }
                nmc.body = '';
                nmc.attachment = undefined;
            });
        };
    }
    main.controller('NotificationToastCtrl', function($scope, $mdToast, $location, instance) {
      $scope.message = instance.body;
      $scope.from = instance.placeholder.user;
      $scope.closeToast = function() {
        $mdToast
          .hide()
      };
      $scope.openMoreInfo = function() {
        location.path('inbox/chat/'+ instance.chat_id + instance.id)
      };
    });
    function Messagetoast($mdToast, instance) {
        $mdToast.show({
          locals: {instance:instance},
          hideDelay   : 5000,
          position    : 'bottom right',
          controller  : 'NotificationToastCtrl',
          templateUrl : 'static/templates/toast.message.tmpl.html'
        });
    }
    function toast($mdToast, content) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(content)
                    .position('bottom right')
                    .hideDelay(3000)
            );
    }
    return angularAMD.bootstrap(main);
});