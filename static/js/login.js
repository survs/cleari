define(["main"], function (main) {
    main.controller("LoginCtrl", function ($scope, $rootScope, $http, $location, $timeout, $window) {
        require(['param'], function (param) {
            csrf_refresh($scope, $http);
            $rootScope.page_type = 'Login';
            $rootScope.submit_text = 'Login';
            $rootScope.imagePath = '../static/images/asset/buksu-gate.jpg';
            $scope.submit = function () {
                $http({
                    method: 'POST',
                    url: 'pwa/login_submit',
                    data: param($scope.form),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).then(function (response) {
                    csrf_refresh($scope, $http);
                    switch (response.status) {
                        case 200:
                            $rootScope.sidenav_collapse = false;
                            $rootScope.toolbar_collapse = false;
                            $location.path('/home');
                            $window.location.reload();
                            break;
                        case 203:
                            switch (response.data.code) {
                                case 'username':
                                    alert('User not Found');
                                    break;
                                case 'password':
                                    alert('User password mismatched');
                                    break;
                                case 'form':
                                    alert(response.data.message);
                                    break;
                                case 'server':
                                    alert(response.data.message);
                                    break;
                            }
                            break;
                        case 206:
                            console.log(response.data);
                            break;
                        default:
                            console.log('Unidentified error')
                    }
                });
            }
        });

        function csrf_refresh($scope, $http) {
            $http({
                method: 'GET',
                url: 'pwa/login/csrf'
            }).then(function (response) {
                if (response.status === 203) {
                    $scope.form.csrfmiddlewaretoken = response.data.csrf
                }
            });
        }
    })
});