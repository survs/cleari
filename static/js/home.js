receipt = undefined;
(function () {
    'use strict';
    define(["main"], function (main) {
        main.controller("HomeCtrl", function ($scope, $rootScope, $http, $interval, $timeout, $mdDialog, $mdToast, $location, $window) {
            $rootScope.page_type = 'Home';
            $rootScope.header_text = "Home Dashboard";
            $http({
                method: 'GET',
                url: 'pwa/profile/provider'
            }).then(function (r) {
                switch (r) {
                    case 200:
                        $scope.photo = r.photo;
                        break;
                }
            });
            angular.element(document.getElementsByClassName("home-scroll")[0]).bind("scroll", function () {
                let element = angular.element(document.getElementsByClassName("tt_c"))[0];
                if (this.scrollTop >= 115) {
                    element.classList.remove("toggle_toolbar");
                } else {
                    element.classList.add("toggle_toolbar");
                }
            });
        });
        main.controller('StudentCtrl', ['$scope', '$http', '$mdToast', '$mdDialog', '$timeout', '$rootScope','StudentResource', StudentCtrl]);
        main.controller('SignatoryCtrl', ['$scope', '$http', '$mdToast', '$mdDialog', '$timeout', '$rootScope', 'StudentResource', SignatoryCtrl]);
        main.controller('AdminCtrl', ['$scope', '$http', '$mdToast', '$mdDialog', '$timeout', '$rootScope', AdminCtrl]);
        main.controller('FreshCtrl', ['$mdToast', '$mdDialog', '$scope', 'SignatoryResource', FreshCtrl]);
        main.controller('IssuedCtrl', ['$mdToast', '$mdDialog', '$scope', 'SignatoryResource', IssuedCtrl]);
        main.controller('RequestCtrl', ['$mdToast', '$mdDialog', '$http', '$q', '$scope', 'SignatoryResource', RequestCtrl]);
        main.controller('SignedCtrl', ['$mdToast', '$mdDialog', '$scope', 'SignatoryResource', SignedCtrl]);
        main.controller('InfoCtrl', ['$scope', '$http', '$mdToast', '$mdDialog', 'instance', InfoCtrl]);
        main.controller('viewProfileCtrl', ['$scope', '$http', '$mdToast', '$mdDialog', 'instance', viewProfileCtrl]);
        main.controller('viewClearanceCtrl', ['$scope', '$http', '$mdToast', '$mdDialog', 'StudentResource', 'instance', viewClearanceCtrl]);
        main.service('StudentResource', ['$q', '$filter', '$timeout', '$http', StudentService]);
        main.service('SignatoryResource', ['$q', '$filter', '$timeout', SignatoryService]);
        main.directive("refreshTable", function () {
            return {
                require: 'stTable',
                restrict: "A",
                link: function (scope, elem, attr, table) {
                    scope.$on("refreshClearance", function () {
                        table.pipe(table.tableState());
                    });
                }
            }
        });
        function AdminCtrl($scope, $http, $mdToast, $mdDialog, $timeout, $rootScope) {
            let ac = this;
            $http.get("pwa/semester/provider")
                .then(function (response) {
                    console.log(response.data);
                    if (response.status === 200) {
                        $rootScope.sy = response.data.semester.current.name;
                        $rootScope.sy_sem = response.data.semester.current.sem;
                        $rootScope.sem = response.data.semester.list;
                    }
                });
            $http({
                method: 'GET',
                url: 'pwa/admin/cpanel/provider'
            }).then(function (response) {
                if (response.status === 200) {
                    ac.cp_cards = response.data
                }
            });
        }
        function InfoCtrl(scope, http, toaster, dialog, instance) {
            let ic = this;
            ic.title = 'Signatory Information';
            ic.profile = instance.dialog_info.profile;
            ic.name = instance.dialog_info.name;
            ic.position = instance.dialog_info.position;
            ic.office = instance.dialog_info.office;
            ic.cancel = function () {
                dialog.cancel();
            };
            ic.answer = function (answer) {
                dialog.hide(answer);
            };
            ic.message = function () {
                dialog.hide();
                dialog.show({
                        locals: {uid:instance.dialog_info.user_id, name:instance.dialog_info.name},
                        controller: "NewMessageCtrl as nmc",
                        templateUrl: '/static/templates/message.dialog.tmpl.html',
                        parent: angular.element(document.body),
                        clickOutsideToClose: false,
                        fullscreen: true
                    }).then(function (answer) {
                        toast(toaster, answer)
                    }, function () {
                        toast(toaster, 'Canceled')
                    });
            }
        }
        function StudentService($q, $filter, $timeout, http) {
            let m = {};
            let sem = undefined;
            m.sem_set = function (set) {
                sem = set
            };
            this.getLiability = function (receipt) {
                let deferred = $q.defer();
                http.get("pwa/receipt/" + receipt)
                    .then(function (response) {
                            if (response.status === 200) {
                               deferred.resolve({data:response.data})
                            } else if (response.status === 417) {
                               deferred.reject({error:'Authentication Error'})
                            }else{
                               deferred.reject({error:'Network Error'})
                            }
                        }
                    );
                return deferred.promise;
            };
            this.getClearance = function (start, number, params) {
                let dataset = [];
                let status = false;
                let deferred = $q.defer();
                require(['oboe'], function (oboe) {
                    oboe('pwa/liabilities/'+ sem || 'current')
                        .node('!', function (chunks) {
                            dataset = chunks;
                            return oboe.drop;
                        }).done(function () {
                        status = true;
                    })
                });

                function m() {
                    $timeout(function () {
                        if (status) {
                            let filtered = params.search.predicateObject ? $filter('filter')(dataset, params.search.predicateObject) : dataset;
                            if (params.sort.predicate) {
                                console.log(params.sort.predicate);
                                filtered = $filter('orderBy')(filtered, params.sort.predicate, params.sort.reverse);
                            }
                            let result = filtered.slice(start, start + number);
                            deferred.resolve({
                                data: result,
                                numberOfPages: Math.ceil(filtered.length / number),
                            });
                        } else {
                            m()
                        }
                    }, 1000)
                }
                m();
                return deferred.promise;
            };
            this.getClearanceAPI = function (start, number, params, uid) {
                let dataset = [];
                let status = false;
                let deferred = $q.defer();
                require(['oboe'], function (oboe) {
                    oboe('pwa/liabilities/current/s/'+uid)
                        .node('!', function (chunks) {
                            dataset = chunks;
                            return oboe.drop;
                        }).done(function () {
                        status = true;
                    })
                });

                function m() {
                    $timeout(function () {
                        if (status) {
                            let filtered = params.search.predicateObject ? $filter('filter')(dataset, params.search.predicateObject) : dataset;
                            if (params.sort.predicate) {
                                console.log(params.sort.predicate);
                                filtered = $filter('orderBy')(filtered, params.sort.predicate, params.sort.reverse);
                            }
                            let result = filtered.slice(start, start + number);
                            deferred.resolve({
                                data: result,
                                numberOfPages: Math.ceil(filtered.length / number),
                            });
                        } else {
                            m()
                        }
                    }, 1000)
                }
                m();
                return deferred.promise;
            };
            return {
                getClearance: this.getClearance,
                getClearanceAPI: this.getClearanceAPI,
                getLiability: this.getLiability,
                setSem:m
            };
        }
        function SignatoryService($q, $filter, $timeout) {
            let prepare_request = function (start, number, params, type) {
                let search = 'unset';
                let current_page = Math.floor(params.pagination.start / params.pagination.number) + 1;
                let dataset = [];
                let status = false;
                let deferred = $q.defer();
                let page_count = 0;
                let order_by = '';
                let row_total = 0;
                let ppn = 30;
                if (params.search.predicateObject) {
                    let d = params.search.predicateObject;
                    if (d.$ !== undefined) {
                        search = "{\"generic\":\"" + d.$ + "\"}";
                    } else {
                        let property = Object.keys(d)["0"];
                        let value = Object.values(d)["0"];
                        if (property !== undefined && value !== undefined) {
                            search = "{\"" + property + "\":\"" + value + "\"}";
                        } else {
                            search = 'unset'
                        }
                    }
                }
                if (params.sort.predicate) {
                    if (params.sort.reverse) {
                        order_by = '-' + params.sort.predicate
                    } else {
                        order_by = params.sort.predicate
                    }
                } else {
                    order_by = 'unset'
                }
                if (params.pagination.number !== undefined) {
                    ppn = params.pagination.number
                }
                require(['oboe'], function (oboe) {
                    oboe('/pwa/module/provider/' + type + '/' + ppn + '/' + current_page + '/' + order_by + '/' + search)
                        .node('!', function (chunks) {
                            dataset = chunks.rows;
                            page_count = chunks.total_page;
                            row_total = chunks.total_rows;
                            return oboe.drop;
                        })
                        .done(function () {
                            status = true;
                        })
                });

                function m() {
                    $timeout(function () {
                        if (status) {
                            deferred.resolve({
                                data: dataset,
                                totalItemCount: row_total,
                                numberOfPages: page_count,
                                len: dataset.length
                            });
                        } else {
                            m()
                        }
                    }, 1000)
                }

                m();
                return deferred.promise;
            };
            this.fresh = function (start, number, params) {
                return prepare_request(start, number, params, 'fresh')
            };
            this.issued = function (start, number, params) {
                return prepare_request(start, number, params, 'issued')
            };
            this.request = function (start, number, params) {
                return prepare_request(start, number, params, 'request')
            };
            this.signed = function (start, number, params) {
                return prepare_request(start, number, params, 'signed')
            };
            return {
                get_fresh: this.fresh,
                get_issued: this.issued,
                get_requesting: this.request,
                get_signed: this.signed
            }
        }
        function StudentCtrl(scope, $http, $mdToast, $mdDialog, timeout, $rootScope, service) {
            let ctrl = this;
            $http.get("pwa/semester/provider")
                .then(function (response) {
                    console.log(response.data);
                    if (response.status === 200) {
                        $rootScope.sy = response.data.semester.current.name;
                        $rootScope.sy_sem = response.data.semester.current.sem;
                        $rootScope.sem = response.data.semester.list
                        ctrl.sy = response.data.semester.current.name;
                        ctrl.sy_sem = response.data.semester.current.sem;
                        ctrl.sem = response.data.semester.list,
                        service.setSem.sem_set('current')
                    }
                });
            ctrl.updateSemester = function () {
                console.log(ctrl.sems)
                $http.get("pwa/liabilities/"+ctrl.sems.sys)
                .then(function (response) {
                    console.log(response.data);
                    if (response.status === 200) {
                        $rootScope.sy = response.data.semester.current.name;
                        $rootScope.sy_sem = response.data.semester.current.sem;
                        $rootScope.sem = response.data.semester.list;
                        ctrl.sy = response.data.semester.current.name;
                        ctrl.sy_sem = response.data.semester.current.sem;
                        ctrl.sem = response.data.semester.list
                        service.setSem.sem_set(ctrl.sems.sys)
                        toast($mdToast, "Applying");
                        scope.$broadcast('refreshClearance');
                    }
                });
            };
            ctrl.m_refresh = function () {
                toast($mdToast, "Refreshing");
                scope.$broadcast('refreshClearance');
            };
            ctrl.request = function (instance) {
                $http.get("pwa/receipt/r/" + instance.receipt).then(function (r) {
                    if (r.status === 200) {
                        scope.$broadcast('refreshClearance');
                        timeout(function () {
                            toast($mdToast, "Request sent")
                        }, 1000)
                    } else {
                        toast($mdToast, "Failed to request")
                    }
                })
            };
            ctrl.cancel = function (instance) {
                scope.$broadcast('refreshClearance');
                $http.get("pwa/receipt/c/" + instance.receipt).then(function (r) {
                    if (r.status === 200) {
                        scope.$broadcast('refreshClearance');
                        timeout(function () {
                            toast($mdToast, "Success cancellation of request")
                        }, 1000)
                    } else {
                        toast($mdToast, "Failed to cancel request")
                    }
                })
            };
            ctrl.info = function (ev, instance) {
                console.log(instance);
                $mdDialog.show({
                    locals: {instance: instance},
                    controller: "InfoCtrl as ic",
                    templateUrl: '/static/templates/info.dialog.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: false
                })
                    .then(function (answer) {
                        //Somethind todo when user answer the button
                    }, function () {
                        toast($mdToast, "Dialog closed")
                    });
            };
            ctrl.downloadClearance = function (instance) {
                console.log(instance)
                let base_url = 'pwa/reports/gen/inv/'+instance.number+'/';
                let cl = [];
                let i;
                let r = instance.receipts;
                for(i in r){
                    cl.push(r[i].liability_id)
                }
                let unique = Array.from(new Set(cl));
                $http({
                    method: 'GET',
                    url: base_url+ unique.toString(),
                    responseType: 'blob'
                }).then(function (data) {
                    let linkElement = document.createElement('a');
                    try {
                        console.log(data)
                        let url = window.URL.createObjectURL(data.data);
                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download",
                            'Clearance of '+String(instance.full_name).replace('.', '').replace('.', '').replace('.', ''));

                        let clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    } catch (ex) {
                        console.log(ex);
                    }
                });
             };
            ctrl.displayed = [];
            ctrl.options = [5, 20, 30, 50, 100];
            ctrl.itemsByPage = ctrl.options[2];
            ctrl.openMenu = function ($mdMenu, ev) {
                $mdMenu.open(ev);
            };
            ctrl.callServer = function callServer(tableState) {
                ctrl.isLoading = true;
                let pagination = tableState.pagination;
                let start = pagination.start || 0;
                let number = pagination.number || 10;
                service.getClearance(start, number, tableState).then(function (result) {
                    ctrl.displayed = result.data;
                    tableState.pagination.numberOfPages = result.numberOfPages;
                    ctrl.isLoading = false;
                });
            };

        }
        function SignatoryCtrl(scope, $http, $mdToast, $mdDialog, timeout, $rootScope, service) {
            $http.get("pwa/semester/provider")
                .then(function (response) {
                    console.log(response.data);
                    if (response.status === 200) {
                        $rootScope.sy = response.data.semester.current.name;
                        $rootScope.sy_sem = response.data.semester.current.sem;
                        $rootScope.sem = response.data.semester.list;
                    }
                });
            scope.showClearance = function(ev, instance) {
                $mdDialog.show({
                        locals: {instance: instance},
                        multiple: true,
                        controller: "viewClearanceCtrl as vcc",
                        templateUrl: '/static/templates/clearance.dialog.tmpl.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        fullscreen: true
                    })
                        .then(function (answer) {
                            //Somethind todo when user answer the button
                        }, function () {
                            toast($mdToast, "Dialog closed")
                        });
             };

             scope.showProfile = function(ev, instance) {
                 $mdDialog.show({
                         locals: {instance: instance},
                         controller: "viewProfileCtrl as ic",
                         templateUrl: '/static/templates/student.info.dialog.tmpl.html',
                         parent: angular.element(document.body),
                         targetEvent: ev,
                         clickOutsideToClose: true,
                         fullscreen: false
                     })
                         .then(function (answer) {
                             //Somethind todo when user answer the button
                         }, function () {
                             toast($mdToast, "Dialog closed")
                         });
             };
             scope.downloadClearance = function (instance) {
                let base_url = 'pwa/reports/gen/inv/'+instance.number+'/';
                let cl = [];
                let i;
                let r = instance.receipts;
                for(i in r){
                    cl.push(r[i].liability_id)
                }
                let unique = Array.from(new Set(cl));
                $http({
                    method: 'GET',
                    url: base_url+ unique.toString(),
                    responseType: 'blob'
                }).then(function (data) {
                    let linkElement = document.createElement('a');
                    try {
                        console.log(data)
                        let url = window.URL.createObjectURL(data.data);
                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download",
                            'Clearance of '+String(instance.full_name).replace('.', '').replace('.', '').replace('.', ''));

                        let clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    } catch (ex) {
                        console.log(ex);
                    }
                });
             }

        }
        function FreshCtrl($mdToast, $mdDialog, scope, service) {
            let ctrl = this;
            ctrl.displayed = [];
            ctrl.options = [5, 20, 30, 50, 100];
            ctrl.itemsByPage = ctrl.options[2];
            ctrl.openMenu = function ($mdMenu, ev) {
                $mdMenu.open(ev);
            };
            ctrl.m_refresh = function () {
                toast($mdToast, "Refreshing");
                scope.$broadcast('refreshClearance');
            };
            this.callServer = function callServer(tableState) {
                ctrl.isLoading = true;
                let pagination = tableState.pagination;
                let start = pagination.start || 0;
                let number = pagination.number || 10;
                service.get_fresh(start, number, tableState).then(function (result) {
                    ctrl.displayed = result.data;
                    tableState.pagination.numberOfPages = result.numberOfPages;
                    tableState.pagination.totalItemCount = result.totalItemCount;
                    ctrl.isLoading = false;
                });
            };
        }
        function IssuedCtrl($mdToast, $mdDialog, scope, service) {
            let ctrl = this;
            ctrl.displayed = [];
            ctrl.options = [5, 20, 30, 50, 100];
            ctrl.itemsByPage = ctrl.options[2];
            ctrl.openMenu = function ($mdMenu, ev) {
                $mdMenu.open(ev);
            };
            ctrl.m_refresh = function () {
                toast($mdToast, "Refreshing");
                scope.$broadcast('refreshClearance');
            };
            this.callServer = function callServer(tableState) {
                ctrl.isLoading = true;
                let pagination = tableState.pagination;
                let start = pagination.start || 0;
                let number = pagination.number || 10;
                service.get_issued(start, number, tableState).then(function (result) {
                    ctrl.displayed = result.data;
                    tableState.pagination.numberOfPages = result.numberOfPages;
                    tableState.pagination.totalItemCount = result.totalItemCount;
                    ctrl.isLoading = false;
                });
            };
        }
        function RequestCtrl($mdToast, $mdDialog, $http, q, scope, service) {
            let ctrl = this;
            ctrl.displayed = [];
            ctrl.options = [5, 20, 30, 50, 100];
            ctrl.itemsByPage = ctrl.options[2];
            function csrf() {
                let deferred = q.defer();
                $http({
                    method: 'GET',
                    url: '/pwa/user/provider/csrf',
                }).then(function (response) {
                    if (response.status === 203) {
                        deferred.resolve({csrf:response.data.csrf});
                    }else {
                        deferred.reject('There was an error');
                    }
                });
                return deferred.promise;
            }
            ctrl.openMenu = function ($mdMenu, ev) {
                $mdMenu.open(ev);
            };
            ctrl.m_refresh = function () {
                toast($mdToast, "Refreshing");
                scope.$broadcast('refreshClearance');
            };
            ctrl.sign = function (ev, c) {
                let receipt = c.receipt;
                let confirm = $mdDialog.prompt()
                 .title('Remarks')
                 .textContent('Put remarks for '+c.full_name)
                 .placeholder('Fully paid membership fee')
                 .ariaLabel('Remarks')
                 .targetEvent(ev)
                 .required(true)
                 .ok('Approve Request')
                 .cancel('Cancel');

                $mdDialog.show(confirm).then(function(result) {
                   csrf().then(function (key) {
                       let fd = new FormData();
                       fd.append('remarks', result);
                       fd.append('csrfmiddlewaretoken', key.csrf);
                       $http.post("pwa/receipt/s/" + receipt, fd, {
                           transformRequest: angular.identity,
                           headers: {'Content-Type': undefined}
                       }).then(function (r) {
                           if (r.status===200){
                               scope.$broadcast('refreshClearance');
                               toast($mdToast,  r.data)
                           }else{
                               scope.$broadcast('refreshClearance');
                               toast($mdToast,  r.data)
                           }
                       });
                   });
                }, function() {
                  toast($mdToast,  'Confirmation Declined')
                });

                };
            this.callServer = function callServer(tableState) {
                ctrl.isLoading = true;
                let pagination = tableState.pagination;
                let start = pagination.start || 0;
                let number = pagination.number || 10;
                service.get_requesting(start, number, tableState).then(function (result) {
                    ctrl.displayed = result.data;
                    tableState.pagination.numberOfPages = result.numberOfPages;
                    tableState.pagination.totalItemCount = result.totalItemCount;
                    ctrl.isLoading = false;
                });
            };
        }
        function SignedCtrl($mdToast, $mdDialog, scope, service) {
            let ctrl = this;
            ctrl.displayed = [];
            ctrl.options = [5, 20, 30, 50, 100];
            ctrl.itemsByPage = ctrl.options[2];
            ctrl.openMenu = function ($mdMenu, ev) {
                $mdMenu.open(ev);
            };
            ctrl.m_refresh = function () {
                toast($mdToast, "Refreshing");
                scope.$broadcast('refreshClearance');
            };
            this.callServer = function callServer(tableState) {
                ctrl.isLoading = true;
                let pagination = tableState.pagination;
                let start = pagination.start || 0;
                let number = pagination.number || 10;
                service.get_signed(start, number, tableState).then(function (result) {
                    ctrl.displayed = result.data;
                    tableState.pagination.numberOfPages = result.numberOfPages;
                    tableState.pagination.totalItemCount = result.totalItemCount;
                    ctrl.isLoading = false;
                });
            };
        }
        function viewProfileCtrl(scope, http, toaster, dialog, instance) {
            let ic = this;
            ic.title = 'Student Profile';
            ic.profile = instance.profile;
            ic.name = instance.full_name;
            ic.course = instance.course;
            ic.course_desc = instance.course_desc;
            ic.college = instance.college;
            ic.college_desc = instance.college_desc;
            ic.department = instance.department;
            ic.department_name = instance.department_name;
            ic.year = instance.year_ordinal;
            ic.cancel = function () {
                dialog.cancel();
            };
            ic.answer = function (answer) {
                dialog.hide(answer);
            };
            ic.message = function () {
                dialog.hide();
                dialog.show({
                        locals: {uid:instance.number, name:instance.full_name},
                        controller: "NewMessageCtrl as nmc",
                        templateUrl: '/static/templates/message.dialog.tmpl.html',
                        parent: angular.element(document.body),
                        clickOutsideToClose: false,
                        fullscreen: true
                    }).then(function (answer) {
                        toast(toaster, answer)
                    }, function () {
                        toast(toaster, 'Canceled')
                    });
            }
        }
        function viewClearanceCtrl(scope, http, toaster, dialog, service, instance) {
            let ic = this;
            ic.displayed = [];
            ic.options = [5, 20, 30, 50, 100];
            ic.itemsByPage = ic.options[2];
            ic.name = instance.full_name;
            ic.callServer = function callServer(tableState) {
                ic.isLoading = true;
                let pagination = tableState.pagination;
                let start = pagination.start || 0;
                let number = pagination.number || 10;
                service.getClearanceAPI(start, number, tableState, instance.number).then(function (result) {
                    ic.displayed = result.data;
                    tableState.pagination.numberOfPages = result.numberOfPages;
                    ic.isLoading = false;
                });
            };
            ic.info = function (ev, instance) {
                dialog.show({
                    multiple:true,
                    locals: {instance: instance},
                    controller: "InfoCtrl as ic",
                    templateUrl: '/static/templates/info.dialog.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: false
                })
                    .then(function (answer) {
                        //Somethind todo when user answer the button
                    }, function () {
                        toast(toaster, "Dialog closed")
                    });
            };
            ic.m_refresh = function () {
                toast(toaster, "Refreshing");
                scope.$broadcast('refreshClearance');
            };
            ic.openMenu = function ($mdMenu, ev) {
                $mdMenu.open(ev);
            };
            ic.info_liability = function(){
                console.log(instance)
                service.getLiability(instance.receipt).then(function (r) {
                    console.log(r)
                })
            };
            ic.hide = function () {
                dialog.hide();
            };
            ic.cancel = function () {
                dialog.cancel();
            };
            ic.answer = function (answer) {
                dialog.hide(answer);
            };
        }
        function toast($mdToast, content) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(content)
                    .position('bottom right')
                    .hideDelay(3000)
            );
        }
    });
})();
