define(["main"], function (main) {

    main.controller("InboxCtrl", function ($scope, $rootScope, $http, $mdDialog, $mdToast, systemFactory, Messenger) {
        $rootScope.page_type = 'My Messages';
        Messenger.getInbox().then(function (d) {
            $scope.messages = d.data
        });
        systemFactory.set_main_scope(undefined);
        systemFactory.set_chat_scope(undefined);
        systemFactory.set_inbox_scope($scope);
        $scope.new_message = function(ev){
            $mdDialog.show({
                locals: {uid:undefined, name:undefined},
                controller: "NewMessageCtrl as nmc",
                templateUrl: '/static/templates/message.dialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: true
            }).then(function (answer) {
                toast($mdToast, answer)
                Messenger.getInbox().then(function (d) {
                    $scope.messages = d.data
                });
            }, function () {
                //toast($mdToast, 'Canceled')
            });
        };
        $scope.itemOnLongPress = function(instance) {
	        console.log(instance);
        };

        $scope.itemOnTouchEnd = function(instance) {
	        console.log(instance);
        };
        $scope.toggleLeft = systemFactory.buildToggler('left');
    });

    function toast($mdToast, content) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(content)
                    .position('bottom right')
                    .hideDelay(3000)
            );
    }
});