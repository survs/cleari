(function () {
    'use strict';

    let applicationServerPublicKey = document.querySelector('meta[name="web-push"]').content;
    let push_status = document.querySelector('meta[name="push_status"]');

    const pushButton = document.querySelector('.js-push-btn');

    let isSubscribed = false;
    let swRegistration = null;

    function registerServiceWorker() {
        if (!navigator.serviceWorker && !window.PushManager) return;
        navigator.serviceWorker.register('./sw.js').then(function (reg) {
            console.log('Service Worker and Push is supported');
            swRegistration = reg;
            initializeUI();
            if (!navigator.serviceWorker.controller) {
                return;
            }
            if (reg.waiting) {
                console.log("updates are ready");
                _trackWaiting(reg.waiting);
                return;
            }
            if (reg.installing) {
                _trackInstalling(reg.installing);
                return;
            }
            reg.addEventListener('updatefound', function () {
                console.log("Updates Found!");
                _trackInstalling(reg.installing);
            });
        }).catch(function () {
            console.log('Service Worker and Push are not supported');
        });
    }


    function _trackInstalling(worker) {
        worker.addEventListener('statechange', function () {
            console.log('TI-State:', worker.state);
            if (worker.state == 'installed') {
                console.log("Updates Installed");
            }
        })
    }

    function _trackWaiting(worker) {
        worker.postMessage({action: 'skipWaiting'});
        console.log('State Waiting Executed');
    }

    registerServiceWorker();


    function urlB64ToUint8Array(base64String) {
        const padding = '='.repeat((4 - base64String.length % 4) % 4);
        const base64 = (base64String + padding)
            .replace(/\-/g, '+')
            .replace(/_/g, '/');

        const rawData = window.atob(base64);
        const outputArray = new Uint8Array(rawData.length);

        for (let i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i);
        }
        return outputArray;
    }


    function unsubscribeUser() {
        registration.pushManager.getSubscription()
            .then(
                function (subscription) {
                    if (!subscription) {
                        console.log('Not yet subscribe');
                        return;
                    }
                    updateSubscriptionOnServer(subscription, 'unsubscribe');
                    isSubscribed = true;

                    updateBtn();
                }
            )
    }

    function subscribeUser() {
        console.log('subscribe clicked');
        const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
        swRegistration.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: applicationServerKey
        })
            .then(function (subscription) {
                console.log('User is subscribed.');

                updateSubscriptionOnServer(subscription, 'subscribe');

                isSubscribed = true;

                updateBtn();
            })
            .catch(function (err) {
                console.log('Failed to subscribe the user: ', err);
                updateBtn();
            });
    }

    function initializeUI() {
        pushButton.addEventListener('click', function () {
            pushButton.disabled = true;
            if (isSubscribed) {
                unsubscribeUser()
            } else {
                subscribeUser();
            }
        });
        // Set the initial subscription value
        swRegistration.pushManager.getSubscription()
            .then(function (subscription) {
                isSubscribed = !(subscription === null);

                if (isSubscribed) {
                    console.log('User IS subscribed.');
                } else {
                    console.log('User is NOT subscribed.');
                }

                updateBtn();
            });
    }

    function updateBtn() {
        if (Notification.permission === 'denied') {
            push_status.content = 2;
            pushButton.disabled = true;
            updateSubscriptionOnServer(null);
            return;
        }

        if (isSubscribed) {
            push_status.content = 1;
        } else {
            push_status.content = 0;
        }

        pushButton.disabled = false;
    }

    function updateSubscriptionOnServer(subscription, statusType) {
        let browser = navigator.userAgent.match(/(firefox|msie|chrome|safari|trident)/ig)[0].toLowerCase(),
            data = {
                status_type: statusType,
                subscription: subscription.toJSON(),
                browser: browser,
                group: pushButton.dataset.group
            };

        fetch(pushButton.dataset.url, {
            method: 'post',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(data),
            credentials: 'include'
        }).then(
            function (response) {
                // Check the information is saved successfully into server
                if ((response.status === 201) && (statusType === 'subscribe')) {
                    // Show unsubscribe button instead
                    pushButton.textContent = 'Unsubscribe to Push Messaging';
                    pushButton.disabled = false;
                }

                // Check if the information is deleted from server
                if ((response.status === 202) && (statusType === 'unsubscribe')) {
                    // Get the Subscription
                    getSubscription(registration)
                        .then(
                            function (subscription) {
                                // Remove the subscription
                                subscription.unsubscribe()
                                    .then(
                                        function (successful) {
                                            pushButton.textContent = 'Subscribe to Push Messaging';
                                            pushButton.disabled = false;
                                        }
                                    )
                            }
                        )
                        .catch(
                            function (error) {
                                pushButton.textContent = 'Unsubscribe to Push Messaging';
                                pushButton.disabled = false;
                            }
                        );
                }
            }
        )
    }



})();